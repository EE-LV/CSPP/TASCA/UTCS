﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">DSCGetVarList,PSP;WebpubLaunchBrowser,None;DSCSecurity,False;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project "UTCS.lvproj" is used to develop the Unified TASCA Control System Project and is based on LVOOP, NI Actor Framework and CS++.

Related documents and information
=================================
- README.txt
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/TASCA/UTCS
- Documentation:
  - Refer to Documantation Folder
  - Project-Wikis: https://wiki.gsi.de/foswiki/bin/view/Tasca/UTCSP, https://github.com/HB-GSI/CSPP/wiki
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview

Author: H.Brand@gsi.de

Copyright 2017  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{0012D517-49CA-42F3-990D-E5E856243C40}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingInterval</Property>
	<Property Name="varPersistentID:{004ACC9C-E195-449C-BD83-8A44B8572B54}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-Rate</Property>
	<Property Name="varPersistentID:{008D165A-E535-4F2B-95E1-2523271063BE}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_DMA-IQ-TO</Property>
	<Property Name="varPersistentID:{0097F763-5D96-4346-9F37-230D1309F2C9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXIDC6_range</Property>
	<Property Name="varPersistentID:{00CA9C6C-35E7-4CF0-9A78-2590A584E55B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{010A006A-8210-4BF4-9772-0646FCFCE7B2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP3</Property>
	<Property Name="varPersistentID:{013476CE-FA67-4A70-AA09-B1135272F8E7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Reset</Property>
	<Property Name="varPersistentID:{02311B37-F10F-4E8D-B8C3-6DEF986B3FD3}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS Error Source</Property>
	<Property Name="varPersistentID:{02DFB1C4-D65F-4BEB-983B-8309C3530731}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IL</Property>
	<Property Name="varPersistentID:{0306AF84-1B67-40E2-A12E-BE27D3FD2ABA}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/SV-Lib Path</Property>
	<Property Name="varPersistentID:{035529A3-86B0-49B9-819C-C0CDC61922FE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FirmwareRevision</Property>
	<Property Name="varPersistentID:{0378EC7D-0ADB-4A17-AB2A-3FCF7C142093}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{03C96142-8960-4380-95EC-A27EB60BEAE7}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_FirmwareRevision</Property>
	<Property Name="varPersistentID:{03EB1326-61A7-4C37-807D-4F5EB73DF1B8}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period</Property>
	<Property Name="varPersistentID:{04329132-2F98-4212-A9C9-6C6FDD4484CD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Pyrometer-IL-Enabled</Property>
	<Property Name="varPersistentID:{059E7741-4E6E-4B04-9F0C-B7158261B2AB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_0</Property>
	<Property Name="varPersistentID:{05FF032A-8C05-494B-B5D2-2307443FA796}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{0667D014-4750-4713-A932-5143DE59711E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_position</Property>
	<Property Name="varPersistentID:{06A35DA8-E153-4B8C-9697-AF3F99169046}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SetID</Property>
	<Property Name="varPersistentID:{071F5D99-0226-413C-AC9D-F573F3BA85A3}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{0767E0E6-2A50-4930-B75C-84330CC78C14}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ZeroAdjustPressure</Property>
	<Property Name="varPersistentID:{076FED91-2F3B-48E6-BF90-4BE49B3F8FBF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{078CD25A-DBCB-4601-8A34-8740FE842A75}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ErrorMessage</Property>
	<Property Name="varPersistentID:{084BFA67-D603-48E7-8BBE-272C2E6762FA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF</Property>
	<Property Name="varPersistentID:{0870570A-ABD9-4F9C-8DAE-CE518AADF28E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-I</Property>
	<Property Name="varPersistentID:{08719588-C80C-4686-9A6A-A1679716FC97}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_2</Property>
	<Property Name="varPersistentID:{087F2599-3002-4A36-B6D6-0FC7F2EA2C8B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingIterations</Property>
	<Property Name="varPersistentID:{08BC0EEC-0B71-4AFA-8281-74A79EA189EF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ControllerMode</Property>
	<Property Name="varPersistentID:{08BE0EDC-418A-402E-B52B-F0A23266D503}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/RatTrap</Property>
	<Property Name="varPersistentID:{08CDA135-B1A9-45B0-BB7A-779E96AB616A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP6</Property>
	<Property Name="varPersistentID:{08D7FB95-2937-4A44-8633-48ECA95D16F5}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{095154CE-709B-4414-834E-6792F51C3310}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SPS_0</Property>
	<Property Name="varPersistentID:{096AF365-2DAD-4744-809A-E2F24974CCC2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FirmwareRevision</Property>
	<Property Name="varPersistentID:{09A00C5F-97E0-4513-B8C5-988A0073AC21}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DT3.currinfo_0</Property>
	<Property Name="varPersistentID:{09D0C4CC-1D8B-438F-ACA8-113C78B6F4EF}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Active</Property>
	<Property Name="varPersistentID:{09F28787-FFE1-492E-8FA8-A25FC02800B2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD12_current_setpoint</Property>
	<Property Name="varPersistentID:{09FFB832-B5E5-41E7-B294-83C100847D1F}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Reset</Property>
	<Property Name="varPersistentID:{0A0287E1-A308-4FA5-9575-72368EE9E8DE}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Counting</Property>
	<Property Name="varPersistentID:{0A3837A9-0006-4A17-8F64-63B96CFFE052}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_3</Property>
	<Property Name="varPersistentID:{0A3A8BB1-DFEE-4856-83D6-9CA6F86CAA2F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_0</Property>
	<Property Name="varPersistentID:{0A6DFE1A-848E-4679-8F8B-2745B02317B9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SelfTest</Property>
	<Property Name="varPersistentID:{0A6E473C-1EA5-4422-89D1-036E16155FE6}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIVV1S.positi</Property>
	<Property Name="varPersistentID:{0B6B66C1-9F22-4B74-933D-193ACAD42B97}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Pressure_1</Property>
	<Property Name="varPersistentID:{0BBE5CC4-BF8A-403A-B3B7-04BBB91E534D}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Virtual Accelerator</Property>
	<Property Name="varPersistentID:{0BD5DD24-1B73-4682-9F08-4BB309DFB064}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_2</Property>
	<Property Name="varPersistentID:{0CF685D7-7D79-4F4F-9CF3-2C7323B08C5A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF</Property>
	<Property Name="varPersistentID:{0D5479A6-5A77-4487-A8BF-F7BC2A6385AB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{0DC439E6-F253-436D-8A36-E6B361288306}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_2</Property>
	<Property Name="varPersistentID:{0EAD75AC-909D-493D-A0F2-3B7226A88617}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Start-1</Property>
	<Property Name="varPersistentID:{0EC9505B-3D4D-4807-889D-DF269236F8B8}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length</Property>
	<Property Name="varPersistentID:{0F997A6B-EFA4-4C60-A422-EBC28AB99CB7}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{0FAE9865-FA08-4F34-A394-89E2F01E19AE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_0</Property>
	<Property Name="varPersistentID:{0FC3CA60-D9DA-4E88-A354-18F4F92628E0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_2</Property>
	<Property Name="varPersistentID:{10205DD1-E71B-4EC3-AC2C-C6A8EB55CFFE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingCounter</Property>
	<Property Name="varPersistentID:{104B9FDD-80F6-4FAC-A9DC-CC3CCCA7FB93}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6.currinfo</Property>
	<Property Name="varPersistentID:{105B3A26-2207-4302-83C5-0BB76A31FD16}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_0</Property>
	<Property Name="varPersistentID:{11427A73-E406-4104-B697-60BB679AAAA5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_2</Property>
	<Property Name="varPersistentID:{11605D03-785C-4275-97D9-33194B583D01}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_2</Property>
	<Property Name="varPersistentID:{118A95DF-8C2F-470F-A910-36F048E4CDA7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_2</Property>
	<Property Name="varPersistentID:{11E5D07D-EECC-4AFE-9DC8-801B98CAF75F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_3</Property>
	<Property Name="varPersistentID:{12093BEB-E487-4653-9CA2-F62983254C22}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SPS_2</Property>
	<Property Name="varPersistentID:{123C9594-2478-41A9-8307-11A1B543C688}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_3</Property>
	<Property Name="varPersistentID:{1248E7A8-747E-459C-90C4-49AC27BD9162}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5-Proxy_Activate</Property>
	<Property Name="varPersistentID:{129262F5-D01C-4820-9243-3DBC4C65A59B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Reset-Interlock</Property>
	<Property Name="varPersistentID:{132E3D99-4E12-423E-98AA-C3E418D7ACFB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-MP-Max-Limit</Property>
	<Property Name="varPersistentID:{134AF681-0C11-4B64-A4DF-CD99438CDF61}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Chopper</Property>
	<Property Name="varPersistentID:{13F06CED-5E24-4C7C-8A3F-25E8DF21A989}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{145EF10F-F682-4874-972F-752FC54823B5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_1</Property>
	<Property Name="varPersistentID:{146EC5DA-B3A4-417A-A4B6-611E8F83A0E3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_1</Property>
	<Property Name="varPersistentID:{150EDDDA-7571-4FB5-A54D-C1690D3BF770}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_CircuitMode_1</Property>
	<Property Name="varPersistentID:{1529D110-7E04-4E77-9767-40F3D76DC1E1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{152E9588-40F0-4349-81F0-8988AEE8FE67}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_1</Property>
	<Property Name="varPersistentID:{15A1A602-A057-4424-92D9-62309D5125AD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SetID</Property>
	<Property Name="varPersistentID:{15CB5078-1C86-45D5-A2F2-A950AED40063}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_1</Property>
	<Property Name="varPersistentID:{16B39520-AA3C-42DC-9577-2300D742FB4F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_3</Property>
	<Property Name="varPersistentID:{16C63A5F-14A6-4F5F-9D5B-45E7EA6EC604}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ErrorMessage</Property>
	<Property Name="varPersistentID:{16F0FD54-2A44-4816-8062-F31284E8FE9A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_3</Property>
	<Property Name="varPersistentID:{171F77B4-508A-48C6-8D15-F73A86847238}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Magnet-IL</Property>
	<Property Name="varPersistentID:{17471761-DCF3-4B88-8496-2F44E77D6153}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IL-Enabled</Property>
	<Property Name="varPersistentID:{174E50D8-7F4F-4171-93A2-496E6797844D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_1</Property>
	<Property Name="varPersistentID:{176717B1-2BE7-4DD0-B56A-AAC86FD59D5F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL-Main</Property>
	<Property Name="varPersistentID:{17FEB562-23AA-4922-8942-DFB1CEDB4841}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_RatTrap-IL-Enabled</Property>
	<Property Name="varPersistentID:{18082FF5-EAF5-49A3-BB24-99AD7FA3B8F3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_1</Property>
	<Property Name="varPersistentID:{188EA689-275E-4A36-A2AB-5F143C64D216}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingIterations</Property>
	<Property Name="varPersistentID:{18A4BCD5-7038-4013-A5DE-521EF533C899}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_DriverRevision</Property>
	<Property Name="varPersistentID:{18EF9C0A-02FF-443C-9084-B9C83709A1B6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SelfTest</Property>
	<Property Name="varPersistentID:{1902F500-99F2-4DB6-B976-70CF0D3B21BD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{195183CF-3909-4AF5-B9BF-8B9ED7B6E620}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4.currinfo</Property>
	<Property Name="varPersistentID:{1994E6C2-E32B-4AD7-A068-B702D9FF456D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_1</Property>
	<Property Name="varPersistentID:{19B60B66-FE53-47F5-919D-803A28B14449}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_0</Property>
	<Property Name="varPersistentID:{1A2D5677-1957-49FB-A155-CB56B3B0953D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_dtCounter</Property>
	<Property Name="varPersistentID:{1A889EC0-546B-4E76-993E-0C2DF64AB255}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_2</Property>
	<Property Name="varPersistentID:{1B03B074-2C59-4B1D-A302-A73C1E3FC500}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_0</Property>
	<Property Name="varPersistentID:{1B13F317-6EF8-40FA-99C8-367B2E7186C9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_2</Property>
	<Property Name="varPersistentID:{1B1BB550-9E29-4637-98B5-CD6CAE11068F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_3</Property>
	<Property Name="varPersistentID:{1C2DA609-1230-4A34-9E33-4DD58A4334C0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Counting-Enabled</Property>
	<Property Name="varPersistentID:{1C2E06FE-4A43-48B6-8D4F-BE6DEF631127}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC-Proxy_Activate</Property>
	<Property Name="varPersistentID:{1C350BF3-DB70-4C1F-9BA7-707C10399EEE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Dipole-IL</Property>
	<Property Name="varPersistentID:{1C41DF31-B9DC-4E8C-9FFF-7C1FC2F4D353}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_DriverRevision</Property>
	<Property Name="varPersistentID:{1C5FB013-C247-40B2-AB69-069291A742D8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PIDIntergral</Property>
	<Property Name="varPersistentID:{1CD96A74-EDEE-4A7B-9782-6A9FE1DDF3E4}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{1D0A1841-F46C-497B-A08B-A95C008348C3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PressureSP</Property>
	<Property Name="varPersistentID:{1E625852-B980-41A5-9D8A-9C9ED2B889E9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Max-DetEvents-Limit</Property>
	<Property Name="varPersistentID:{1E6ED094-A94C-4FA5-8F4C-D54B8DDFE964}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SetID</Property>
	<Property Name="varPersistentID:{1E823E64-B064-4161-B380-E5CAD7999346}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{1E880D46-9FCB-4FE9-8F07-3C1FBCE2E3EE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{1E90A27C-4C6D-44C3-A152-2CAED3FEC64F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{1EBA8E26-5B3E-4DCF-83B5-C01F2D77053D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{1F2042A6-0016-45A6-8874-EECE18AC2E77}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Magnet-IL</Property>
	<Property Name="varPersistentID:{1F59C3AF-32F2-438B-B931-92940F241D4F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_2</Property>
	<Property Name="varPersistentID:{206B4647-45C8-49A1-82F1-10E4B79FAD77}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period-Max</Property>
	<Property Name="varPersistentID:{207D15FF-DA5A-403F-A93C-C8D0B9DCAF61}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PIDIntegral</Property>
	<Property Name="varPersistentID:{2082399D-B01A-446F-A200-36B4E9A6480A}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2_P.posits</Property>
	<Property Name="varPersistentID:{20CABAE7-6B1A-4021-BA94-B9C39910693B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_1</Property>
	<Property Name="varPersistentID:{20DE2CE8-9EAD-4147-9A9E-4D333E2227B3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_2</Property>
	<Property Name="varPersistentID:{21200E29-55D9-43BC-B1D6-B29C0DEB931A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3_range</Property>
	<Property Name="varPersistentID:{215AB5DF-6F50-483E-B0B7-0BA1C9808F15}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_3</Property>
	<Property Name="varPersistentID:{21D15EC0-AE09-44B8-9C31-882E92AFDE02}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_range</Property>
	<Property Name="varPersistentID:{228D083B-3582-45DB-9FB0-80287FED2392}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{22A309A2-A927-4CCD-B776-4506B37CCE16}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2-Proxy_Activate</Property>
	<Property Name="varPersistentID:{22AE3BCC-B587-4F61-B3B4-56A9942D4576}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Charge-State</Property>
	<Property Name="varPersistentID:{22D4668B-B09B-42F6-81FD-14FE71596B81}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-MP-Length-IL</Property>
	<Property Name="varPersistentID:{22FAE343-0273-4022-8896-A52A45093A8B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP3_Status</Property>
	<Property Name="varPersistentID:{232E6C3D-2216-4F49-ACA6-1972367F7659}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UXADT2-I</Property>
	<Property Name="varPersistentID:{234A7027-E30D-4C1A-981C-5591B5D9D198}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{234ED5B5-A7EA-4B75-9A07-DEA37062FE2E}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{23C69D28-52DE-4632-B01C-369E7E861586}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Logging</Property>
	<Property Name="varPersistentID:{23FAC9FB-11CC-4E33-AB3C-6178C0B8F5BF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_3</Property>
	<Property Name="varPersistentID:{24144DFC-EACA-4FD3-A1D0-1C7C16E3E538}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_1</Property>
	<Property Name="varPersistentID:{241816A0-0DAE-4876-BB86-EA2D705939DC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingMode</Property>
	<Property Name="varPersistentID:{242F93C2-A0E0-420F-AE1D-7449E0D3E5E6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingIterations</Property>
	<Property Name="varPersistentID:{24378AE8-F120-4467-B59E-B1BDAD58C64E}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_ErrorCode</Property>
	<Property Name="varPersistentID:{246C6FE1-004F-4EA8-8E34-C5A370B54E54}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_2</Property>
	<Property Name="varPersistentID:{24A08D6D-8ABB-49AD-A2BD-17AA2829318C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_2</Property>
	<Property Name="varPersistentID:{24B77937-E313-4242-8AE6-EA41470D83BE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_A</Property>
	<Property Name="varPersistentID:{24CBB8D4-6B64-44CF-B70C-2A3532F9E921}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{25E4C37F-681F-4B68-8354-7FEE1A0B96CB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{26360796-DF8B-471B-A348-9D2D22E5FBD4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingDeltaT</Property>
	<Property Name="varPersistentID:{266D5487-44E9-4BD8-A9AE-B81ECC07FE3A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{26CEDBE9-5840-47E3-9E2C-55C8E2FFD650}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_VacuumBurst</Property>
	<Property Name="varPersistentID:{2751F0F9-AEF2-4F4A-B7D9-D56C1B2793A2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{275E51EB-0B32-4AB4-9933-7FAF2B7FDE21}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{28112410-C07A-4959-8B84-BE55BEFAF3BE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_3</Property>
	<Property Name="varPersistentID:{28142FC2-D4C3-4AEF-ABF9-8C4BC2E3E154}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{28475BFE-0A32-4A65-A5BD-B54C52A125AF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow2_TASCAGC</Property>
	<Property Name="varPersistentID:{286B0C8B-CE00-4AF5-9B87-B04DD995E956}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_3</Property>
	<Property Name="varPersistentID:{2897909C-090D-4E85-92D8-0DFBD50BA227}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF</Property>
	<Property Name="varPersistentID:{28BA3D07-D084-4E5D-8EAE-1297E314809B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP6_Status</Property>
	<Property Name="varPersistentID:{28E1A294-4F07-41D9-BC59-25F8C239456D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{2990FC13-2B13-4497-80D0-F9BA2D315B59}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PressureSP</Property>
	<Property Name="varPersistentID:{2A6599BC-B367-4930-821D-81927A38401C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_2</Property>
	<Property Name="varPersistentID:{2AEB73AE-4C43-40B6-B04D-43E8BC410A22}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300-Proxy_Activate</Property>
	<Property Name="varPersistentID:{2B157E60-B0F4-4B6C-9341-07150D3E511F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SetID</Property>
	<Property Name="varPersistentID:{2C2F2B13-76BC-42B5-9379-235BD50A5946}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Error Message</Property>
	<Property Name="varPersistentID:{2C6C4F81-24DF-4A97-992D-7A2997C95408}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_set-position</Property>
	<Property Name="varPersistentID:{2C72BD87-27DC-4AAD-A3AB-ECA6C1E4E70D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_0</Property>
	<Property Name="varPersistentID:{2CEB095D-3225-47A4-BEBD-71BB8C0C566A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_2</Property>
	<Property Name="varPersistentID:{2D745095-07C7-4F10-861F-8BB9AC732C67}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_0</Property>
	<Property Name="varPersistentID:{2D8EA861-1DE0-4C62-A497-A2A829A52609}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{2DA47572-9BCD-4A9D-9429-F468A686B846}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{2DC723E5-3B91-4D7B-9625-CB780B6B0FA9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{2DFB75BA-1BDC-4419-A4EF-6468D7E20DBB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{2E3A9426-887E-43C3-8B78-8EA1E4DD9E8B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{2EB1FFFB-82F7-4DE3-88FC-9A0CB6E6972D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ErrorMessage</Property>
	<Property Name="varPersistentID:{2EE66EC1-FA55-452E-8903-B32E51A7B20E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_DriverRevision</Property>
	<Property Name="varPersistentID:{2F40B4D0-5C35-424B-B202-C095564555EF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_1</Property>
	<Property Name="varPersistentID:{2F757444-EA2D-4FF3-9C6A-A2801B1494B6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-RatTrap-IL</Property>
	<Property Name="varPersistentID:{2F75CBF8-26A3-45DD-9473-E15E7D0D5BD3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_UnderrangeControl</Property>
	<Property Name="varPersistentID:{2F898D07-BD3A-4D01-8FAB-1C6FEB39F4D6}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{2FEB9049-9DFC-498C-87F6-A1C9BB55362E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Interlock-Reason</Property>
	<Property Name="varPersistentID:{2FFC9E8E-A577-4D89-B18D-5AB086758A9F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_SelfTest</Property>
	<Property Name="varPersistentID:{304CBEA0-67FC-44DB-9476-4269A18FBF56}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_0</Property>
	<Property Name="varPersistentID:{308AAF9B-2605-4AD7-8EF0-32DE3D1A8B60}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_1</Property>
	<Property Name="varPersistentID:{30953174-BAF7-4685-9FC6-77A185736CE4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingIterations</Property>
	<Property Name="varPersistentID:{30BBD2BF-20DD-408D-B8E0-8ADFE566DF76}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{30D07277-7BB3-437C-861E-AC3DCA00EAEF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_2</Property>
	<Property Name="varPersistentID:{3103B388-E312-4128-B2A3-BDDA1382834B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_1</Property>
	<Property Name="varPersistentID:{311D53A1-0710-43D4-81C1-5C623AC3915B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SetID</Property>
	<Property Name="varPersistentID:{319F6B75-BB3A-44DD-A08F-E023E9234553}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Max-QMP-Limit</Property>
	<Property Name="varPersistentID:{31A4C7DC-9ADF-4EE8-BFF8-5A0A5CF0CE11}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{323D30CB-47EC-4771-84D9-A84A8B312DF8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_1</Property>
	<Property Name="varPersistentID:{326BE2E9-81B5-4D02-843F-6E439D86840D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Max-QMP-Limit</Property>
	<Property Name="varPersistentID:{32D81367-98C1-45EA-9B47-9F5E38A503B8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_3</Property>
	<Property Name="varPersistentID:{32F6B36F-98D1-4994-A0B6-6E09C03E3F27}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_1</Property>
	<Property Name="varPersistentID:{33FBAAC2-38D1-45FC-9D77-744FD9B55C57}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ControllerMode</Property>
	<Property Name="varPersistentID:{341A7A4D-66B9-4688-9999-C7F538CC32E9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_B</Property>
	<Property Name="varPersistentID:{3421895C-9623-451D-819A-6FA4E2EA0DB6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_0</Property>
	<Property Name="varPersistentID:{3480BCF0-8D08-4411-BAB6-81ED21E2FF04}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_3</Property>
	<Property Name="varPersistentID:{34B039BE-9CA6-447C-B880-559D3DC484CF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{34C05351-E776-4288-90CC-59914AB2B626}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{357DD1D5-0E47-4163-9091-5032F8AD6046}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/MP-Length-IL-Enabled</Property>
	<Property Name="varPersistentID:{3581E2BF-1564-47AC-AD96-B81F1D35412D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_3</Property>
	<Property Name="varPersistentID:{35820349-0758-482C-BE38-AEE2286045A9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingMode</Property>
	<Property Name="varPersistentID:{35AD3259-FC38-43CD-BBCF-E8D27F77E2FC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SetID</Property>
	<Property Name="varPersistentID:{361E08AA-D071-4877-9021-D226B173E4D9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{3672E402-F9D8-44FF-AB88-E4D39CE41FDC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingCounter</Property>
	<Property Name="varPersistentID:{367C3310-3BCD-4B03-AC83-EE1D2B63D29A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP4_Status</Property>
	<Property Name="varPersistentID:{369DC77B-FB7E-4E5E-83E2-C3BAF1034166}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Reset</Property>
	<Property Name="varPersistentID:{36A8556C-D2CC-41C5-8852-29EDA8DCDA62}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_RTCGC</Property>
	<Property Name="varPersistentID:{36CF1159-3B2E-4D91-B254-ACF016C49271}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{37048A9A-B533-4BAB-8A3D-1D4B82A731C2}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/String Size</Property>
	<Property Name="varPersistentID:{37456E81-647D-441C-B399-0FDEDD6E3218}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{3782F03B-DFCA-4FBD-94B7-F72A71EC0E87}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_1</Property>
	<Property Name="varPersistentID:{378E0B3A-B45D-49A9-8173-C4973DB25767}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_3</Property>
	<Property Name="varPersistentID:{379E0D80-FFA6-4C98-9726-C1287C456841}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_1</Property>
	<Property Name="varPersistentID:{37C242CF-967A-4C2A-951C-E322ED5E2937}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_2</Property>
	<Property Name="varPersistentID:{37EB4ED6-7C33-4322-B280-2DC2FF2F677A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Max-QMP-Limit</Property>
	<Property Name="varPersistentID:{387628E9-F4B5-4FD8-9430-E21050574390}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingTime</Property>
	<Property Name="varPersistentID:{387F1C23-E8DB-490B-9F5B-C0A9641642E2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FirmwareRevision</Property>
	<Property Name="varPersistentID:{3906D797-06F4-4D12-B268-B98B39DE2F4F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_0</Property>
	<Property Name="varPersistentID:{3912AB18-E6D3-45F5-94A3-6A88538247C8}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Dipole-IL</Property>
	<Property Name="varPersistentID:{394F30C6-7D8E-4619-A7F9-20F40DD1E833}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV5</Property>
	<Property Name="varPersistentID:{39C792E7-A3C8-4FE0-B9CB-52011FDDAF58}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{39E4D37B-DE32-4EFF-B279-794768EFD50D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS</Property>
	<Property Name="varPersistentID:{3A0DCE92-23F8-4465-A3A0-7FDA62759A0B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ErrorMessage</Property>
	<Property Name="varPersistentID:{3AB8BE18-D503-44CF-8CD4-2229E9BC9F1B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{3B03C756-3070-4C1A-A233-BC48B82C337F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{3B0FBC57-A950-4A2A-B37B-6F05601677CD}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV3_Status</Property>
	<Property Name="varPersistentID:{3B46818A-4565-486C-A54C-4408252EFF0E}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Status_0</Property>
	<Property Name="varPersistentID:{3B5B31B7-B339-42E3-85C2-D1D4A92CFCAB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingInterval</Property>
	<Property Name="varPersistentID:{3BC1EA78-719C-43EE-96D4-6BA5C491B10A}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_PollingInterval</Property>
	<Property Name="varPersistentID:{3BFF606C-AA81-402A-85CC-0C6926E0C174}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD11.currenti</Property>
	<Property Name="varPersistentID:{3C3103CE-C568-4B57-A80F-78780B219BA4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS</Property>
	<Property Name="varPersistentID:{3C3D3F0B-2A00-45C8-9517-EE69D268D15B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffsets</Property>
	<Property Name="varPersistentID:{3C666255-0699-4BE3-A20D-75138577F9F3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ErrorCode</Property>
	<Property Name="varPersistentID:{3CD320A7-56A4-4CA1-8A8C-67B6449EF59D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{3CFBA496-59AD-4298-9A2C-F9DC9F1ACF54}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingIterations</Property>
	<Property Name="varPersistentID:{3CFF4F65-012D-4C7E-A902-05D2B483E539}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_3</Property>
	<Property Name="varPersistentID:{3D408742-12EC-489E-B0B8-F7182F122F82}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Reset</Property>
	<Property Name="varPersistentID:{3D88C118-F4F3-4961-A093-9AF3D17CD987}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-SetID</Property>
	<Property Name="varPersistentID:{3DAC2C1A-BD4C-481B-97EA-CF4A746BAC11}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXADT2.currinfo_0</Property>
	<Property Name="varPersistentID:{3E1EC226-34D6-4A7E-9F5E-A7AB7EC08B4C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingDeltaT</Property>
	<Property Name="varPersistentID:{3E3CC3A6-9DB7-4E65-927B-21DE47CCAB60}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SelfTest</Property>
	<Property Name="varPersistentID:{3E45C639-33CD-4AD3-964F-8B8951A12D2A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PIDLead</Property>
	<Property Name="varPersistentID:{3E9BC354-FA0A-41EE-82FA-3706BC0D3D98}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_0</Property>
	<Property Name="varPersistentID:{3EF4C42B-9AB8-4871-A5F4-35420AC524D8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{3F2FEDEC-536A-4AC9-8127-A9807F894C8C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_3</Property>
	<Property Name="varPersistentID:{3F558A8F-37A1-47A1-B084-04D8081FC04D}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4.currinfo_0</Property>
	<Property Name="varPersistentID:{3F625540-8FAC-4F9B-BCC1-BE9E5F0C0CE3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP4_Status</Property>
	<Property Name="varPersistentID:{40065960-1EBB-45B8-AEA9-B3692E02DC1E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{402D8296-DCCF-41BB-92BA-EE3EA22D4B26}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP4</Property>
	<Property Name="varPersistentID:{4054E165-4E85-4132-A6E3-84F8C6AC2590}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{40870411-4FC1-46A8-A934-A4DB5AF978A0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4-Proxy_Activate</Property>
	<Property Name="varPersistentID:{40911ADA-4F6A-43A6-94ED-E730D4DF9C01}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_0</Property>
	<Property Name="varPersistentID:{40CBEFCC-12C9-4FBE-97AB-9426D1F31D85}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow3_TASCAGC</Property>
	<Property Name="varPersistentID:{40F21143-1131-40B0-A938-234C611B6253}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{4127A9C9-4EF4-4D8D-88F4-130AFEA2CD62}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_PollingMode</Property>
	<Property Name="varPersistentID:{41283491-8DF1-40F2-85AB-60258833F3BF}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Pyrometer-IL</Property>
	<Property Name="varPersistentID:{41AF0030-2EE5-4296-B6E2-17B24C8288A5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SetID</Property>
	<Property Name="varPersistentID:{41BB30D2-C377-42EC-98E7-5F34118E14D3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{41BC9B01-19C6-476B-9F32-F4090A4990DF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV2_Status</Property>
	<Property Name="varPersistentID:{41FD1439-C5AD-49E0-A33E-CFE1AFBFF898}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_3</Property>
	<Property Name="varPersistentID:{429E4FE6-0F60-4D7D-850E-FDBED834F55B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2_range</Property>
	<Property Name="varPersistentID:{43776265-AF33-411F-94FC-8CA285CF4E6A}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2.currinfo_0</Property>
	<Property Name="varPersistentID:{43DCF2F1-4583-4275-A95C-416165AD718C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV4_Status</Property>
	<Property Name="varPersistentID:{44146C74-4F4A-44E3-B192-10FAC1A6289A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingDeltaT</Property>
	<Property Name="varPersistentID:{445E578B-0FD9-4625-A46D-2C5EF40403F6}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Status_3</Property>
	<Property Name="varPersistentID:{456CF0AE-D002-40F3-A60F-A68DB19964FE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffsets</Property>
	<Property Name="varPersistentID:{46125F6C-052E-4FDB-8B00-5AC2596C47FE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingDeltaT</Property>
	<Property Name="varPersistentID:{46597644-0DCC-4119-A84C-F55E1A31EB02}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3-I</Property>
	<Property Name="varPersistentID:{46C301EA-BB57-4AAC-92C6-0A650AF4D125}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_1</Property>
	<Property Name="varPersistentID:{46EA811B-204F-4CE4-8F74-03433614FD37}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_2</Property>
	<Property Name="varPersistentID:{4710DE2E-9945-4D8B-85F4-CD86BF719A9A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_0</Property>
	<Property Name="varPersistentID:{478C76EE-786E-4305-ABA5-CCE1EAC4B9DB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{4805BD1E-B7FC-4A65-AFF7-83D283B08EBC}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UT2DCX_current</Property>
	<Property Name="varPersistentID:{481E28BA-4368-4256-AE8E-5F7182843F85}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{48407556-E5D8-47AF-BA6C-AF8DD3D344E9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL-Main</Property>
	<Property Name="varPersistentID:{485375B2-010C-4333-B8AD-68F44E99E8CB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureMode</Property>
	<Property Name="varPersistentID:{48AAD71A-48CF-4620-B8FD-2E484BEB4CF1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_0</Property>
	<Property Name="varPersistentID:{4931413D-86C2-4D7E-BDFE-3B28EFC2835B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{49499955-200A-42BB-9ACB-001D5DA3C3EB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_1</Property>
	<Property Name="varPersistentID:{49A49DF3-F321-4F8D-A268-AD13EA1803E1}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6.currinfo_1</Property>
	<Property Name="varPersistentID:{49B2B518-741E-4E98-BB20-4F8FC5F7FA35}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_2</Property>
	<Property Name="varPersistentID:{49B6BD33-FCD4-402D-8889-1E4DB8809FDC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SelfTest</Property>
	<Property Name="varPersistentID:{49DAB477-D800-42FD-A487-2EAA99B10D71}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UXADT2-Q</Property>
	<Property Name="varPersistentID:{4A33483B-EFA1-49E7-BEA7-88EBE9A6FE1F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_position</Property>
	<Property Name="varPersistentID:{4AB1BDC7-FABA-4E0C-B30A-B0D117FE9307}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DetectorRate</Property>
	<Property Name="varPersistentID:{4B6B712C-E42B-4925-B914-A9AB8CD44EEB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IMax</Property>
	<Property Name="varPersistentID:{4B9B3C07-766C-49BD-95CE-5F558822BEA2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SelftestResultCode</Property>
	<Property Name="varPersistentID:{4BAC0BCE-166A-457D-B371-BBD6DE845959}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Range</Property>
	<Property Name="varPersistentID:{4BF4DC47-A9EC-4384-B6EB-944A4A1721F7}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Det-Rate-Limit-Enabled</Property>
	<Property Name="varPersistentID:{4C89BCA2-6031-4E7C-AFB4-15960DC3ED22}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SelftestResultCode</Property>
	<Property Name="varPersistentID:{4C8E3974-A7FF-4597-ADF6-985F5B794650}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-TO</Property>
	<Property Name="varPersistentID:{4CC54FEE-323D-42B9-9BBA-4555876B6676}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SelftestResultCode</Property>
	<Property Name="varPersistentID:{4CDC14E9-4738-457E-BEB2-FD242250AF3A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_3</Property>
	<Property Name="varPersistentID:{4D1CEB26-B7B8-46AC-B3BA-41A188ACC526}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{4D30B408-EE97-4834-87C1-DA5B55232C73}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{4D3EA93C-F421-441D-836C-CA0296799E0D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SetID</Property>
	<Property Name="varPersistentID:{4E36C434-FE5A-49B6-9E64-ADF20B581393}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-IL-Min</Property>
	<Property Name="varPersistentID:{4E38032C-C2CA-454F-B268-DE583E77E140}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_SelftestResultCode</Property>
	<Property Name="varPersistentID:{4E4E6196-3B08-45F4-892C-41199E452A3D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_DriverRevision</Property>
	<Property Name="varPersistentID:{4E5F0136-3C8C-4610-A4E1-499C1822DF71}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4_P.positi</Property>
	<Property Name="varPersistentID:{4EE084D2-45BF-45E4-8C42-72E33A23F466}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ChargeState</Property>
	<Property Name="varPersistentID:{4EE365B4-1293-4A54-A7DC-F7C675A82836}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3_current</Property>
	<Property Name="varPersistentID:{4F120434-F83C-4E20-AEAC-8C241650AB05}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{4F37E166-C1AE-4BAB-9E34-F14B0204F7A0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_3</Property>
	<Property Name="varPersistentID:{4F654605-F084-462D-BC41-07D9101ED891}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_2</Property>
	<Property Name="varPersistentID:{4F7E326F-50F7-40CB-A12A-EE854069E1BB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Chopper-In</Property>
	<Property Name="varPersistentID:{4F972C8D-2363-4575-895B-E1A60F62721F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Counting_Enabled</Property>
	<Property Name="varPersistentID:{4FF083D5-0FDF-481C-90D2-F1177F3CF2E1}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Status_2</Property>
	<Property Name="varPersistentID:{502877A5-4DD6-40B7-B148-125E4EB8FDF9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{50669BEC-36E1-4CC6-9787-FE73E4C00EC9}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS Error Message</Property>
	<Property Name="varPersistentID:{50720479-7E0B-4B51-BF60-85CA20B3E109}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pyrometer-IL</Property>
	<Property Name="varPersistentID:{50B5B530-39A0-4795-96B6-267823E2A633}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Dipole-IMin</Property>
	<Property Name="varPersistentID:{50D42B9E-A1AE-4880-B5CF-AF6938D48193}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{50D51377-78FD-4F3C-930F-87079156654F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{5101E6A2-1FB4-4440-ACDD-08B96FC89869}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_2</Property>
	<Property Name="varPersistentID:{511AD8E3-04B8-4051-B41D-7208C0269C71}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Logging_Enabled</Property>
	<Property Name="varPersistentID:{517E2AF4-5FD9-49D5-85CD-AC1EACAAFE0F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_3</Property>
	<Property Name="varPersistentID:{518E48AC-5CAB-48E3-9910-D11C09134FD6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_3</Property>
	<Property Name="varPersistentID:{5198AA4F-FFC9-4A64-BB99-D03134D9FAE5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_0</Property>
	<Property Name="varPersistentID:{51A0CFBC-AD65-400A-B405-CB5F7B596A76}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS</Property>
	<Property Name="varPersistentID:{51D74D53-C81D-4889-A42C-6FAC6179EA67}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingTime</Property>
	<Property Name="varPersistentID:{51D74F69-92DC-42FD-8951-F24DF7421C09}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ControllerMode</Property>
	<Property Name="varPersistentID:{526B7B41-818E-4CED-9965-1E01D663FA5E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Decay-Delay-2</Property>
	<Property Name="varPersistentID:{528FE41F-059E-41A9-BEDE-FB3BE018B65C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_2</Property>
	<Property Name="varPersistentID:{52EDD6A6-2CD9-403B-A882-7116D3043382}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2_P.positi</Property>
	<Property Name="varPersistentID:{53E4964B-085E-4573-9AC6-2CF56E717BCA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_1</Property>
	<Property Name="varPersistentID:{54902E02-1552-47D2-9E21-2B9477789785}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{549EE34D-FA28-4B55-8E25-7A61EB91DA76}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{559E7914-0434-4935-9793-F1AD97A0A09C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_0</Property>
	<Property Name="varPersistentID:{55BE76B8-2C0C-4280-A140-5EFBD86723CE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_1</Property>
	<Property Name="varPersistentID:{55F6A93A-E59E-4915-AC95-5A655E90DAFB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_1</Property>
	<Property Name="varPersistentID:{561C90B7-D16D-4DBD-83FB-BD519A2CD16A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_1</Property>
	<Property Name="varPersistentID:{563CA5B7-4CC9-484C-BE62-049AA2E607E8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PIDGain</Property>
	<Property Name="varPersistentID:{56BACBC2-F77D-48B9-AF7B-85CD1DD6D2BB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_1</Property>
	<Property Name="varPersistentID:{56E6E2EE-43DD-4017-A84B-A8E037B895C8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FirmwareRevision</Property>
	<Property Name="varPersistentID:{56F05A2E-5E81-4337-A3D7-EFE2E7D416CF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{56F60A63-7137-4C61-9AF8-067E2E9BD513}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{57211D3D-D262-472E-BDE9-11B303D3721D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{57287249-8812-4C68-947C-B79EB2DB64FE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_2</Property>
	<Property Name="varPersistentID:{576D1012-311D-437D-BAB5-87C053736940}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_0</Property>
	<Property Name="varPersistentID:{576D2AB0-D175-414F-B297-4BC13A2500FF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_2</Property>
	<Property Name="varPersistentID:{57A5EF59-DFD7-4438-8097-E02D4F0873F2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SetID</Property>
	<Property Name="varPersistentID:{58388058-0598-496D-87DA-A67EBC0B6BA7}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Dipole-IL</Property>
	<Property Name="varPersistentID:{58C495FF-F130-4F4A-883A-ED4366F74F74}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_1</Property>
	<Property Name="varPersistentID:{58D3EE8E-A818-4ABA-AB14-E59EE1C35AB0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Magnet-IL</Property>
	<Property Name="varPersistentID:{59DAB054-D57D-4528-AF4D-252ABB1C93FF}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD11.currents</Property>
	<Property Name="varPersistentID:{59F967B6-8681-41ED-A168-E620922E57F0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_2</Property>
	<Property Name="varPersistentID:{5A5A56A1-08E9-438C-B70C-1881530F2B1E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_RatTrap</Property>
	<Property Name="varPersistentID:{5A6E8271-B909-4843-8DA2-047CDE6B4BAF}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Period</Property>
	<Property Name="varPersistentID:{5A9081F5-E5AD-484A-95ED-A8FEFF001540}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS Error Code</Property>
	<Property Name="varPersistentID:{5A9247C8-39EC-419C-8196-FBF2773F4A17}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{5AC6142F-9366-4E71-B5B4-B3C277A5C42A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Magnet-IL-Enabled</Property>
	<Property Name="varPersistentID:{5AFD83D8-C574-4FFF-9C35-9851ACF87E70}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripModes</Property>
	<Property Name="varPersistentID:{5B64365D-E4EC-4D04-BCC3-B1E87B2681EB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_1</Property>
	<Property Name="varPersistentID:{5B7C999B-AA94-4DE7-9303-EF96AAF6F0DB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{5BE00C5D-AF19-4074-83F0-4C73D271546E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_-1</Property>
	<Property Name="varPersistentID:{5C11AB4B-B215-4C9D-8D94-78A320C7C207}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{5CFCC4AA-3FD5-4FA7-B296-70708E5DDCA7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ErrorCode</Property>
	<Property Name="varPersistentID:{5D881C82-2FFB-433F-8B25-EB7B1F762B35}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureMode</Property>
	<Property Name="varPersistentID:{5E784432-60B2-4723-B232-B32033A1274C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingIterations</Property>
	<Property Name="varPersistentID:{5E81D1D7-4117-4A4D-B049-F0244D17C80D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingMode</Property>
	<Property Name="varPersistentID:{5ECFC258-6DD5-4C09-BD69-5247C83F0B41}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMILProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{5F6B3350-9A73-4C8B-9A14-F4BA80D794BB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Delay-1</Property>
	<Property Name="varPersistentID:{5FA2CE5D-9C56-4126-AF88-A7DFAC8B4052}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_B</Property>
	<Property Name="varPersistentID:{5FBD4981-EBB8-432C-8515-1FD012B45043}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_0</Property>
	<Property Name="varPersistentID:{5FD2132E-C89A-48DA-9FA3-25FA31437A26}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Reset</Property>
	<Property Name="varPersistentID:{60354BBF-33EB-4D85-A851-9535F239C352}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_0</Property>
	<Property Name="varPersistentID:{614738F4-9B96-487C-9A47-F774DA2E7390}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-RatTrap-IL</Property>
	<Property Name="varPersistentID:{6148712E-E668-4E4F-8EB2-ECA1296F3A3B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{6159BC7B-089F-40F1-927C-50F95144F2C8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_UnderrangeControl</Property>
	<Property Name="varPersistentID:{615C5597-77C1-471C-8462-3353DF9A1435}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow0_TASCAGC</Property>
	<Property Name="varPersistentID:{6196F80F-BFFD-422B-9206-99E069D2F0CD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{62746352-99CB-4CA5-8324-1F1475655927}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Trafo-Frequency</Property>
	<Property Name="varPersistentID:{633EB90A-473C-4627-AC4B-BEB6ABAC0A9B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{635D571C-63EB-4521-8FF6-A02DFBEBCF88}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_1</Property>
	<Property Name="varPersistentID:{638CE681-AC2B-4F73-A538-E45081EF924E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_1</Property>
	<Property Name="varPersistentID:{639890B3-9266-4A53-9A73-85B09063C405}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingTime</Property>
	<Property Name="varPersistentID:{63FCDAED-1D9D-4A9B-A7B2-820174F79330}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Det-Rate-Limit-IL</Property>
	<Property Name="varPersistentID:{6402DCB3-CB06-40F1-9C10-4E09477BAC39}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Dipole-IMax</Property>
	<Property Name="varPersistentID:{64181170-8239-4DD8-9256-ED6806DFCED6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{643C7D7D-4453-44BA-9887-4D7CC22B8A36}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_DMA-IQ-Enabled</Property>
	<Property Name="varPersistentID:{64676704-78E7-4C77-AE03-C98D96300D6D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_2</Property>
	<Property Name="varPersistentID:{64691758-DD14-4420-A3BD-817B6DD1D6C5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{646CD287-727E-4DDA-BD4C-F7B0BF25EBC7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_2</Property>
	<Property Name="varPersistentID:{649624F8-04D0-4652-ADC7-62D0853055E7}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_DriverRevision</Property>
	<Property Name="varPersistentID:{64DEF7B5-749A-43E4-BC3A-D26DE3D93F56}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowStatus</Property>
	<Property Name="varPersistentID:{6596B42E-101F-4774-886B-B701D8177128}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{667623F1-504F-402D-9D51-15ED97421A44}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_FilterTime_1</Property>
	<Property Name="varPersistentID:{66AA452C-6F92-458D-BD4E-1EC2692C74BA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{66D39B39-E06E-414F-B843-CC7EFAC205A9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Decay-Delay-1</Property>
	<Property Name="varPersistentID:{67029A21-B0C4-4EF8-8781-5AF33C12E00A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingCounter</Property>
	<Property Name="varPersistentID:{673A7215-6CC1-4AA3-BD6E-C8F65C187D01}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_B</Property>
	<Property Name="varPersistentID:{67CBAF71-7018-4745-A5AC-F18802CC50D1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_2</Property>
	<Property Name="varPersistentID:{682457B6-8E9A-48D2-B003-C686EF597AD9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{68502AF9-5B47-49B6-A9A2-4B4BD8CA97DD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1-Proxy_Activate</Property>
	<Property Name="varPersistentID:{68572599-47CF-4922-8465-F935DECB56C0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{68D3EE44-539A-4572-BCC0-E5D28082DEE1}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_HW-Reset</Property>
	<Property Name="varPersistentID:{692AC76E-DF8B-4FBB-ADC1-B94DF82CE95D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ZeroAdjustMFC</Property>
	<Property Name="varPersistentID:{693BD4A8-3E8C-46AF-9CDF-043CBFE183EB}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2.currinfo</Property>
	<Property Name="varPersistentID:{6A5F1C48-9688-4398-92BD-420F1138F0D8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingCounter</Property>
	<Property Name="varPersistentID:{6A91482E-C641-4E53-B65B-00001EB0A489}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SecValvesByPass</Property>
	<Property Name="varPersistentID:{6ACDF85B-4845-4226-AF12-60C577C21E20}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV3</Property>
	<Property Name="varPersistentID:{6AE0B099-8475-42F3-AB71-BE877FAA3FE5}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Trafo-Generate</Property>
	<Property Name="varPersistentID:{6AEB37E6-32A4-449B-A49B-03734FAD97DE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChModes</Property>
	<Property Name="varPersistentID:{6B51F9C7-0596-4904-9F12-84147B12E714}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{6BA2FC94-747C-4C77-B868-C8ABC252E29F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SelfTest</Property>
	<Property Name="varPersistentID:{6BE878C3-515B-4E6D-9F3B-1194A264C841}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{6BEA0F5B-4746-46C6-9959-24446FCD7F1E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV2</Property>
	<Property Name="varPersistentID:{6BED0FD5-1A83-41CF-A719-28DFD1B8B762}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{6CEE10F6-B40F-4B10-B8D6-0410A4E9C0CE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{6CF30C4C-5F09-468A-9807-B4E3F1862AB2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_DriverRevision</Property>
	<Property Name="varPersistentID:{6CFE26CB-AEB0-4D92-8567-5A15D67073E6}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DT3.currinfo_1</Property>
	<Property Name="varPersistentID:{6D0057A3-BD1A-4EB9-88EC-42F3AD9C2037}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP5</Property>
	<Property Name="varPersistentID:{6D032DEB-2096-422E-8276-4C704B829922}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_TASCAGC</Property>
	<Property Name="varPersistentID:{6D08A20A-3793-48B9-BCB2-EE29E0291DD9}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXADT2.currinfo</Property>
	<Property Name="varPersistentID:{6D824B44-CAA7-45AE-891E-7D10AD79CD29}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP4</Property>
	<Property Name="varPersistentID:{6E22D094-7E9D-426A-B0F6-093F835147DA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Reset</Property>
	<Property Name="varPersistentID:{6E284848-3FA3-4B9A-95B2-500A9398E2C9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{6E646F58-38AD-41C4-ADC9-10575026E1DA}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Pressure_0</Property>
	<Property Name="varPersistentID:{6E7AB485-6A23-4670-B6AA-1D00ADD7A7F5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_B</Property>
	<Property Name="varPersistentID:{6E873269-7FC1-4351-9E2C-4C95AE736804}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VacuumBurst</Property>
	<Property Name="varPersistentID:{6F047CAC-FD95-45E8-90AA-0E1DD0001047}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Interval</Property>
	<Property Name="varPersistentID:{6F52466A-236D-413D-95D2-3C271E559C03}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_0</Property>
	<Property Name="varPersistentID:{6FBFC067-5428-4B14-A3B3-CC4D44CF6E05}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingMode</Property>
	<Property Name="varPersistentID:{6FC207F1-B98C-44F2-B50D-6676C73D8062}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ResourceName</Property>
	<Property Name="varPersistentID:{6FD49255-6AE1-4352-B53A-A7DA271B2CDA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{6FFEFEC2-E063-447A-AC91-98279A4F849D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_0</Property>
	<Property Name="varPersistentID:{70166AAF-B1B0-43AC-AC2A-D0FC1454E116}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{70290F1A-5743-4402-B251-D75C23B67BD6}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{713C6D96-CC77-4CE7-9BA1-17955A2C745E}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{71963AB4-1B53-4991-9E30-722F4792C60E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{71A78678-240E-40EF-BED2-FCE1F901EE76}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ErrorCode</Property>
	<Property Name="varPersistentID:{71CDC1F8-ED59-4CCE-8DB0-FF05115CC6D3}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-Interlock</Property>
	<Property Name="varPersistentID:{71D08680-BC7F-4D11-AC1D-CDBAEF4828A7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ControllerMode</Property>
	<Property Name="varPersistentID:{71E2485F-6C14-4EC3-8F5A-0BE5E87CA7C0}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SetID</Property>
	<Property Name="varPersistentID:{7223FAA3-B40A-4585-92A1-AB5F8459BA87}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_1</Property>
	<Property Name="varPersistentID:{727BD3F6-0D34-4340-96AA-B06A63C9256A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{73D0326E-15C8-459E-9E92-B95DFA938857}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_3</Property>
	<Property Name="varPersistentID:{745E6C30-8B28-423B-AF22-DB5EDBC90D4F}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-I</Property>
	<Property Name="varPersistentID:{748831B3-F174-4E26-BDB5-562BFC5EC0A3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{74BA7A97-DBFD-463B-87E5-E1A7AE936E83}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_ErrorMessage</Property>
	<Property Name="varPersistentID:{74D877A7-57CB-46F1-B50C-421EEE7E7B44}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_0</Property>
	<Property Name="varPersistentID:{74E98A75-80FF-4866-A2ED-CC08D626550C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_0</Property>
	<Property Name="varPersistentID:{754C0185-D127-4E1A-80B6-9F1205B1BCDC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_1</Property>
	<Property Name="varPersistentID:{7568030F-E241-4D94-8774-4CB902C69A8E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PIDIntegral</Property>
	<Property Name="varPersistentID:{75ED0805-2246-425B-BC1E-60B3FD37B418}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_DriverRevision</Property>
	<Property Name="varPersistentID:{7602BD80-0697-40AC-B869-377101339BDB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChModes</Property>
	<Property Name="varPersistentID:{76F021A1-FA2E-4D35-B0C4-FB4068ACF1DE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowStatus</Property>
	<Property Name="varPersistentID:{771D3114-DD5C-4E89-8C79-E05A05519802}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_FilterTime_3</Property>
	<Property Name="varPersistentID:{773BD42D-7226-4BE9-898E-2EB9DF5D8700}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Dipole-IMin</Property>
	<Property Name="varPersistentID:{7769962A-BA2F-4B9B-AA87-8814C96EFEFB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Charge-IL</Property>
	<Property Name="varPersistentID:{77D45549-891B-4236-954B-0C56CA2F17D9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_0</Property>
	<Property Name="varPersistentID:{77EB1F09-DC46-4FAD-9D41-7CA6635A06B3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_2</Property>
	<Property Name="varPersistentID:{7855EB6D-99C4-4313-90BB-B9311A1A5FB7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{78764EC1-80F2-49AB-B1B1-73A779A60DE4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ErrorMessage</Property>
	<Property Name="varPersistentID:{78822173-7EDE-48B9-8A7B-6BA7FBE69C45}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ErrorCode</Property>
	<Property Name="varPersistentID:{78EFD606-E753-485D-B4A3-C8F1B41FC7FF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_2</Property>
	<Property Name="varPersistentID:{79423A91-B433-48BC-B22A-2488C11F3B4B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingMode</Property>
	<Property Name="varPersistentID:{79434964-C11D-4785-831D-89B2E86A5D31}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_2</Property>
	<Property Name="varPersistentID:{795E10E8-0F5A-4254-9942-BFF886E223E5}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-MP-Length-IL</Property>
	<Property Name="varPersistentID:{79B406A9-BCCB-4BFF-8C80-15BE7FDD3552}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingCounter</Property>
	<Property Name="varPersistentID:{7A3BADE4-9B52-47EE-AED2-9117281AF73F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{7A5EE1C5-125E-40C1-BCAB-C0050F92B497}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_3</Property>
	<Property Name="varPersistentID:{7A7FA4E5-93FE-4E1C-BAAF-F40FD887E929}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/InterlockReason</Property>
	<Property Name="varPersistentID:{7A95946A-C6D6-4975-B3D7-26638EF33C78}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SPS_3</Property>
	<Property Name="varPersistentID:{7AF86E91-9D10-4314-AC30-128E66642CA3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureSP</Property>
	<Property Name="varPersistentID:{7B37BBCB-9D0B-4706-A36F-EF0709667E36}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_3</Property>
	<Property Name="varPersistentID:{7B556986-0BF4-4E53-A6D6-419C6C14F626}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{7BD5CC24-7E7B-43DD-83B6-B5DF4894DFD0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Length</Property>
	<Property Name="varPersistentID:{7C2756C0-6A22-4DB5-9F52-F50B267ACB0E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_3</Property>
	<Property Name="varPersistentID:{7CCAC268-B2CD-4CCC-91F3-CF4409DF20D2}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Chopper-Out</Property>
	<Property Name="varPersistentID:{7CCF3321-F64C-4E5A-8969-C81E4FF2085F}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS State</Property>
	<Property Name="varPersistentID:{7CF8B021-30F0-43C0-888F-EB60C6559E55}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{7D262BCC-059C-46FC-919C-C838973F05A6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Max-DetEvents-Limit</Property>
	<Property Name="varPersistentID:{7D97FDC9-86FE-40D9-B2AE-FB8D990413CD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_2</Property>
	<Property Name="varPersistentID:{7D9D0BD6-3045-484A-927E-A3E473AC504D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{7DF23428-CE28-492A-8203-A0BC4614FBC9}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4.currinfo_1</Property>
	<Property Name="varPersistentID:{7E4CA64E-2FBF-4986-A8B3-6DB7C3CC57BC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_3</Property>
	<Property Name="varPersistentID:{7EF4F2F7-5E5C-43EB-B812-E1E3132E62A8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_current</Property>
	<Property Name="varPersistentID:{7F28ED00-BE56-41C3-A606-844F758B7394}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_A</Property>
	<Property Name="varPersistentID:{7FEB02B5-8FE8-4E7B-A60D-51B89FF75351}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_3</Property>
	<Property Name="varPersistentID:{807867D3-5728-4B83-BC24-114984BF25F0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_2</Property>
	<Property Name="varPersistentID:{80973CF1-8234-4F2C-A889-D98EA7468259}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_0</Property>
	<Property Name="varPersistentID:{80B14C96-C39A-429B-9CA1-8DAAF8C298D9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{80B405E3-4210-4066-BB44-69084C943361}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{80BF1064-7E2F-48DC-81AC-1433D30947DC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{80C6DD74-2727-422D-AA58-249A215451FB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingCounter</Property>
	<Property Name="varPersistentID:{80E36194-3836-44F3-8A9B-6738E3F05FC0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_0</Property>
	<Property Name="varPersistentID:{81519105-06D5-49D3-9324-EC703C724F9E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_3</Property>
	<Property Name="varPersistentID:{8152A337-92D1-495D-955F-CDE699440A1B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_3</Property>
	<Property Name="varPersistentID:{816738D1-A76A-4D5A-A3D7-E370824EDB50}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_0</Property>
	<Property Name="varPersistentID:{818E4A67-781D-447F-8921-508BDCB84B2C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV4</Property>
	<Property Name="varPersistentID:{819B8173-41BB-4FDA-BCEF-0F1A76DB1C66}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_0</Property>
	<Property Name="varPersistentID:{81AA3D49-E81B-48F5-8E13-A076135ADCB0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_3</Property>
	<Property Name="varPersistentID:{81BB1512-FD6F-4696-8B8B-A991D9954856}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{8242CBA6-3412-4CA5-AC44-D8C7C81B3B6E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMILProxy_Activate</Property>
	<Property Name="varPersistentID:{82648CF5-809D-4246-A5C0-43112D296360}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Inhibit</Property>
	<Property Name="varPersistentID:{8278F8B1-B727-403D-814A-9C599F5D33DB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_2</Property>
	<Property Name="varPersistentID:{833DA055-971D-461F-B71A-124B5CBE6182}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_0</Property>
	<Property Name="varPersistentID:{835C953F-120B-4C95-83EA-A403B12B1E7A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP3</Property>
	<Property Name="varPersistentID:{836ACC5F-C03D-4482-8A51-CE4B3B99415D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Length-IL-Max</Property>
	<Property Name="varPersistentID:{8373A76D-16C7-4EA3-A15B-A28EA7D42283}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{83DD22B4-44AC-4F92-851F-2021B4CDB5F4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_UnderrangeControl</Property>
	<Property Name="varPersistentID:{8476D1D0-330A-4B6F-A08C-856A2B31FEB0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{849DB8C3-A530-4944-B892-1BA793C1A73D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PIDGain</Property>
	<Property Name="varPersistentID:{84AD42BC-005F-4117-8ADC-5B95F553DB45}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2-Q</Property>
	<Property Name="varPersistentID:{84B5159E-5F48-4B91-8225-69D905384004}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_B</Property>
	<Property Name="varPersistentID:{850D648E-039A-4274-98AD-626FC4D4B169}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{854B9C20-5F43-47A3-95DF-7B607363F04A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ErrorCode</Property>
	<Property Name="varPersistentID:{862F14AD-58BD-419F-82B9-155F4484079D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_3</Property>
	<Property Name="varPersistentID:{86C60C62-97F6-4557-96A8-7E8B06282F60}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{8778AFAC-C945-460E-914B-7CF49EB635E8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP5_Status</Property>
	<Property Name="varPersistentID:{878AE749-694A-4238-8046-A579047E1073}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-DMA-IQ-Timeout</Property>
	<Property Name="varPersistentID:{8811A3EC-D2F3-45FA-82CE-EFEDF4E6686C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Delay-2</Property>
	<Property Name="varPersistentID:{8814C6E2-AE2E-42FB-BFFD-84161C4C7817}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Log All Device Errors</Property>
	<Property Name="varPersistentID:{883827D5-A923-4C9F-BC6C-7EE3AA5D46AA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_3</Property>
	<Property Name="varPersistentID:{88738C1F-136D-429F-AB26-787DAF2978DE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FirmwareRevision</Property>
	<Property Name="varPersistentID:{88750632-6672-4DE1-9438-90F0ED7BA880}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX.currinfo_0</Property>
	<Property Name="varPersistentID:{88AD486A-6CF0-4333-ACA8-F95D09FC366B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Counting-Reset</Property>
	<Property Name="varPersistentID:{88F72D8E-6DAD-4BE0-9C9C-7BB43A057C31}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SetID</Property>
	<Property Name="varPersistentID:{89CFD731-6BF7-43F9-987E-ECD8451746BD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-MP-Extrema</Property>
	<Property Name="varPersistentID:{8A6E1605-946F-492F-B3A7-5F20546E5B84}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{8A7B9697-63BB-4C72-96D9-3DF6CE076A72}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Trafo-Frequency</Property>
	<Property Name="varPersistentID:{8AF0EB90-296A-4B5A-850A-41CD8365253D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_3</Property>
	<Property Name="varPersistentID:{8B072DF6-B063-4258-B733-1F1404CD2ED4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_1</Property>
	<Property Name="varPersistentID:{8B1FB5CB-FBB4-4DFD-B8AC-C7F19CFD33AD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_1</Property>
	<Property Name="varPersistentID:{8B8A8279-08E2-458A-A9D1-9ADE237A47AB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PIDGain</Property>
	<Property Name="varPersistentID:{8BA50C11-D45D-4259-A9E9-FCB275B5AFB5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_0</Property>
	<Property Name="varPersistentID:{8BB7E34C-CBD3-4F82-B29F-005E61918547}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{8C1E18FB-7A02-4A79-B044-33362118B245}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SPS_1</Property>
	<Property Name="varPersistentID:{8C60725E-B524-45ED-B2C5-52BBADF178B2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_2</Property>
	<Property Name="varPersistentID:{8CB0409A-2AA6-4502-8AF4-3747A79A058A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_0</Property>
	<Property Name="varPersistentID:{8CB6A298-027D-4EC6-B801-4930C17FECEC}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-Min</Property>
	<Property Name="varPersistentID:{8D5E926E-15B2-45DB-A8D6-B7A56BE66CB6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Reset</Property>
	<Property Name="varPersistentID:{8DA9385C-11B1-436D-A6FC-A6363AF67E9F}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Pressure_2</Property>
	<Property Name="varPersistentID:{8E083EC8-DB81-4001-B258-D07B23B0F8C9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_1</Property>
	<Property Name="varPersistentID:{8EBBA5D8-D11B-4A94-B592-86042B93223E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{8EC92F26-EA1C-476A-9506-603959AF672A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_1</Property>
	<Property Name="varPersistentID:{8EE2B146-C22F-4ECC-A4FB-2BCDB7986FAE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV5_Status</Property>
	<Property Name="varPersistentID:{8F2198A7-A0F9-48A6-AABB-1EFE1B25FEC4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_1</Property>
	<Property Name="varPersistentID:{8F565BEA-AC96-4DE3-93EE-97872676CAEA}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2-I</Property>
	<Property Name="varPersistentID:{8F974F1F-ACC1-4068-A1B1-2FEC15041D5A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_0</Property>
	<Property Name="varPersistentID:{8FF26A81-62CF-4990-BCD5-F52E13899CC5}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-Counts</Property>
	<Property Name="varPersistentID:{9006A80B-5C00-48FD-9869-A1693875C9EE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DMA_Enabled</Property>
	<Property Name="varPersistentID:{900B40B3-5CC5-4DB4-A6B5-C50A801CC3CF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_0</Property>
	<Property Name="varPersistentID:{90AD1FE7-671D-47F5-A3A3-B4FD0974837F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Reset</Property>
	<Property Name="varPersistentID:{910C9D71-540B-45A3-B9AA-468C978BC159}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_0</Property>
	<Property Name="varPersistentID:{91230CB5-5102-46F9-B36B-3D2C2EECB8E8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_0</Property>
	<Property Name="varPersistentID:{9182028F-CD4D-4765-B3F6-7DAAFD7D3B2E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{91ECC29D-0B1B-4811-88D3-CAF3136AF6D1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_A</Property>
	<Property Name="varPersistentID:{92875E6C-F626-4269-A88B-7CA3E33F94BE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureSP</Property>
	<Property Name="varPersistentID:{92F391E3-8BA7-403F-947E-E2F34168E2F7}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Chopper_output</Property>
	<Property Name="varPersistentID:{9302755B-9573-4D42-98A5-767671F13B0D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{936D84F3-3516-4C44-BF5A-2482BFFD6D9E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/MP-Length</Property>
	<Property Name="varPersistentID:{937F5D10-651A-4DAF-ADD4-BB5EC8C5282C}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{940A1B50-1514-4A72-A445-D747332BD273}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_2</Property>
	<Property Name="varPersistentID:{940C307B-37EA-4560-B753-B49E51E6AF44}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{9450955A-A1C0-452E-8BE5-FC83D0269FD5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MainValve</Property>
	<Property Name="varPersistentID:{94635914-C5C5-44CA-9BDF-E117C0C2FE1A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{94692E95-3256-4B64-A105-4273C381C4C4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{9483DBDE-C176-4896-B8D2-6751DC546EB2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Magnet-IL-Enabled</Property>
	<Property Name="varPersistentID:{95006DDC-824D-4707-B3AC-D58E3BB60B62}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ErrorCode</Property>
	<Property Name="varPersistentID:{956F72D5-177E-40BB-B4BF-AEF10A2133CE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_3</Property>
	<Property Name="varPersistentID:{963540D1-67E9-406D-99EA-5566F53C3677}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PressureMode</Property>
	<Property Name="varPersistentID:{96379395-D462-4351-8BC8-39AE9385B22D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_3</Property>
	<Property Name="varPersistentID:{967F3386-AE3E-4382-9CCC-B91BEB3B3A03}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{971BBDDE-5A44-441B-8307-85E1A6A8EF9C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{976960CF-1113-43AC-BEC6-41679488456A}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_CircuitMode_2</Property>
	<Property Name="varPersistentID:{990E2B7C-3AAB-4E40-99C9-B86A4E7CA10A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2_current</Property>
	<Property Name="varPersistentID:{9911CC56-DC04-412B-B1B5-6CC22A45DF53}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_ResourceName</Property>
	<Property Name="varPersistentID:{997A1D0D-BA74-4977-BAC7-A7F661D8143B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{9A17E67E-023E-4C73-BCF2-7882D05F0EA1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_0</Property>
	<Property Name="varPersistentID:{9A82E008-AFE6-4EF5-957C-9A64DDDAD00D}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_PollingCounter</Property>
	<Property Name="varPersistentID:{9A888689-151A-4BE8-903F-A4E144E4379D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP3_Status</Property>
	<Property Name="varPersistentID:{9BB721F6-AC45-4DA5-BF5C-494678339480}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SelfTest</Property>
	<Property Name="varPersistentID:{9CA82DF2-C669-4FF5-868A-136910E2FCF4}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_FilterTime_2</Property>
	<Property Name="varPersistentID:{9D1815C4-2877-44D7-BAC8-3E70C25B3DD3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_2</Property>
	<Property Name="varPersistentID:{9D58E2EB-40D1-4937-8BEE-B4D15666E9F5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PIDGain</Property>
	<Property Name="varPersistentID:{9DAA60DA-FD28-4112-922A-F951D63BC2D9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Charge-IL-Enabled</Property>
	<Property Name="varPersistentID:{9DAEA1EB-1A22-4988-9ABB-D69FCE6164DF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_3</Property>
	<Property Name="varPersistentID:{9E068B7B-FF28-4004-901D-59C41BCB1F98}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Pressure_3</Property>
	<Property Name="varPersistentID:{9E117CE7-BC69-402D-B0DD-E247D2E50A5F}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Dipole-IMax</Property>
	<Property Name="varPersistentID:{9E219713-6E3E-4883-8370-6113284AD510}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Dipole-IL-Enabled</Property>
	<Property Name="varPersistentID:{9E3938D6-F67C-4FE5-8298-6F6F42E828A1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_3</Property>
	<Property Name="varPersistentID:{9E3CC6D5-7487-49BD-8670-AC75B1A156E1}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SPS_B</Property>
	<Property Name="varPersistentID:{9E9FA864-7DA9-429D-96A8-44686332C3DD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_2</Property>
	<Property Name="varPersistentID:{9EA7275F-645E-4C8C-826F-0E06EDAA4712}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_2</Property>
	<Property Name="varPersistentID:{9EE208E4-5861-461A-85D3-D933866F4A7D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{9F023D5D-FC84-46CE-A68A-B70B006189D6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_3</Property>
	<Property Name="varPersistentID:{9F18C906-5A56-485A-9755-C68248683D21}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{9F43DD71-66AD-4F1A-84D6-DBB830084BE7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ResourceName</Property>
	<Property Name="varPersistentID:{9F8F91A0-55E5-4A49-A4F2-9DE8053A5F86}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{9FBE4B14-077E-4E99-92E0-3B1C20428894}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{9FEED76A-2AC5-4606-8F63-C0155E80EFCB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_0</Property>
	<Property Name="varPersistentID:{A13740C9-726F-4820-83BE-5BAB6B755B64}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX_P.positi</Property>
	<Property Name="varPersistentID:{A13D5AA8-5073-4BBD-8432-DE9265A0038F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_3</Property>
	<Property Name="varPersistentID:{A14E2FC7-5475-4D5F-89D3-791146F27387}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXFVV1S.positi</Property>
	<Property Name="varPersistentID:{A1646924-56E6-4E09-BA48-0997CA278181}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingInterval</Property>
	<Property Name="varPersistentID:{A1911881-70C0-4961-BE06-D485F939BE66}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{A22C4C1D-65AA-40D6-A7B1-AFB30AE5367E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{A2437880-9A54-4E97-9D53-492714C153FA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingDeltaT</Property>
	<Property Name="varPersistentID:{A34C61C2-70B4-4421-8D39-AFB4C9AC42E3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PIDLead</Property>
	<Property Name="varPersistentID:{A363F9D7-37A9-49E8-9329-3EA03FACD947}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_1</Property>
	<Property Name="varPersistentID:{A38EB8CD-5C47-430B-A690-1843C63EE7C9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingTime</Property>
	<Property Name="varPersistentID:{A39B5282-E01E-4FC1-B561-092549336074}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS</Property>
	<Property Name="varPersistentID:{A3C428F9-EC7A-4832-8C41-9983563FD3D0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_0</Property>
	<Property Name="varPersistentID:{A40EB589-622B-47DE-8771-431B1013B646}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Length-IL-Min</Property>
	<Property Name="varPersistentID:{A55AD4E6-3A84-45AF-AF81-351BBD9A437C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_0</Property>
	<Property Name="varPersistentID:{A570B54B-B520-4356-93E2-2E22B813BA91}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SelftestResultCode</Property>
	<Property Name="varPersistentID:{A5B923CA-2994-40F8-B24B-372D3FD5252B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF</Property>
	<Property Name="varPersistentID:{A6121137-4CD2-4001-A1DB-71F1A7AF966B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{A6270DD9-E7EA-4901-8431-611E7E0AEC26}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_3</Property>
	<Property Name="varPersistentID:{A674BFCC-A31A-4CCD-B5A3-7A559CD76913}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingMode</Property>
	<Property Name="varPersistentID:{A6B6892F-A37B-4032-90A2-18A4EFF01B75}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-Reset</Property>
	<Property Name="varPersistentID:{A6F0248B-2345-4E0C-B3F0-EC90A76C3FDD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{A7080ECE-7AFB-444C-9BF3-776EE2B3076C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{A70E93DD-63D1-4875-98CD-91B33A3164D5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_A</Property>
	<Property Name="varPersistentID:{A74268A3-58FC-4E1E-8F6E-1865AA7EBB6C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-IL-Max</Property>
	<Property Name="varPersistentID:{A78D5CAB-A2E7-4D89-850F-12F26440D13C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{A7CE9D60-79F8-48FE-AC15-5F2B037593F6}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/RatTrap-IL-Enabled</Property>
	<Property Name="varPersistentID:{A81D5C1E-5D44-4303-B611-5740AD87E225}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_1</Property>
	<Property Name="varPersistentID:{A9BFDAE2-E4B9-42B5-B356-F6AE94E6716B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP2</Property>
	<Property Name="varPersistentID:{AA2DC5B8-3BDE-440B-96A9-66E12E09A675}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP5_Status</Property>
	<Property Name="varPersistentID:{AA4313EB-9F40-4ABA-A639-D80E0C8B525C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_1</Property>
	<Property Name="varPersistentID:{AA53FF3B-8429-437A-87AB-F06845B92B6A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-QMP</Property>
	<Property Name="varPersistentID:{AAE8D0E6-297C-4676-A21E-E05512A109E8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/SecValvesState</Property>
	<Property Name="varPersistentID:{AB2AFD26-8849-44FB-B8C3-960847DD8236}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{AB6C4992-8A45-4F27-883C-3E5A1570D30F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_set-position</Property>
	<Property Name="varPersistentID:{AB6CF36C-8B0D-4842-8E2E-3D30028BC1C3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS</Property>
	<Property Name="varPersistentID:{AB8C0172-C86B-4525-84A0-6FD8C07DAC80}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ResourceName</Property>
	<Property Name="varPersistentID:{AB90CB1D-F2B2-44A3-933F-E53B8A027F88}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period-Min</Property>
	<Property Name="varPersistentID:{AB9AE5C5-7914-47F6-B315-F5741E6AA4ED}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_3</Property>
	<Property Name="varPersistentID:{ABD0A7BC-D9BE-4FCF-ACCC-31A6B052E8BF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingIterations</Property>
	<Property Name="varPersistentID:{ABE14B83-5E64-4E7A-8518-2A3964E30633}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_2</Property>
	<Property Name="varPersistentID:{ABF8AD30-91B7-4878-9407-515EF54256C3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_1</Property>
	<Property Name="varPersistentID:{AC45B51E-8323-44ED-BEEF-3A4DC3C28A56}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_1</Property>
	<Property Name="varPersistentID:{AC759D58-97EA-4937-ABC8-BC17D9899C54}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{ACB7A4C7-95AF-4AD2-8261-820C271CF660}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_3</Property>
	<Property Name="varPersistentID:{ACD8AA57-F5CB-4887-BF8D-DC82536C7E29}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP2_Status</Property>
	<Property Name="varPersistentID:{AD40BC4F-5D6D-4D67-BD10-1D3A4584D4BB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_3</Property>
	<Property Name="varPersistentID:{ADF9CFE9-6E27-4637-8893-58143B2DB16E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_1</Property>
	<Property Name="varPersistentID:{AE78ED03-5ACA-4147-9976-85293775EF97}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-DMA-IQ</Property>
	<Property Name="varPersistentID:{AE970958-F116-4635-8B57-B56EEF6EADC1}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Debug</Property>
	<Property Name="varPersistentID:{AFCDEA01-8F6E-4382-8985-4E3A44DE0F2A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{B000089B-994F-4B74-A3C7-5854D8325326}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_3</Property>
	<Property Name="varPersistentID:{B019014C-9DD4-43D8-ABD7-7F625F9FB33E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD12_current</Property>
	<Property Name="varPersistentID:{B0D0EFFC-3F37-4F50-BC13-65C255241995}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC-Proxy_Activate</Property>
	<Property Name="varPersistentID:{B15AF2A5-98F5-44AE-A520-DCF47E65AEFF}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{B1C286C6-DC9A-4726-8933-4D1F46E9C88C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV1_Status</Property>
	<Property Name="varPersistentID:{B223815D-C647-4891-9670-EB8C37B5897B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{B2288EEB-ECE6-4DEC-BA0B-10AA2AD2A87F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/MP-Period</Property>
	<Property Name="varPersistentID:{B23F5C1E-B0A9-4430-92DB-BE93C059F080}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_0</Property>
	<Property Name="varPersistentID:{B275D838-B35E-4F05-BCD5-D605CADA19B9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{B2D2D93F-372F-491F-90A1-0E5E6FAEC606}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ZeroAdjustMFC</Property>
	<Property Name="varPersistentID:{B3068D46-E789-48F3-9D43-2955243D3E27}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{B309227B-FC8C-47C8-BCB1-7134648BC1AE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingInterval</Property>
	<Property Name="varPersistentID:{B365CFF8-71DF-4FFC-9993-B4924C00676C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/InterlockStatus</Property>
	<Property Name="varPersistentID:{B3C251BD-68C3-438C-9A35-74C601FFB498}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow1_TASCAGC</Property>
	<Property Name="varPersistentID:{B3C3F154-2FED-4EE5-B761-EA4444EA8287}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{B3F10189-0DF3-4AC6-A84E-4F1CBABF6396}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_1</Property>
	<Property Name="varPersistentID:{B4ABE5F8-E9BE-43F7-A46C-1B52B0B6F6C3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_3</Property>
	<Property Name="varPersistentID:{B4CDAB75-6328-4F60-B9D1-44367D46C5A7}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-Max</Property>
	<Property Name="varPersistentID:{B66117B7-8E92-4DF4-89BA-A3A605E98A0E}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD12.currents</Property>
	<Property Name="varPersistentID:{B670E074-DC70-429B-BAAB-FC122DEE1FC1}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Charge-State</Property>
	<Property Name="varPersistentID:{B699E5C5-099D-4AA6-B4B5-34CD9211A5AC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{B6BC7DDF-F128-4B4E-8E52-4475C017981C}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{B6F11236-2707-4B54-85EA-D66D92D8C662}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_0</Property>
	<Property Name="varPersistentID:{B6F846A2-B0A1-46A6-9495-3D0657A3F125}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{B77D0F01-7FAC-4ECF-9238-F24DB6865FDE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{B7C4C156-8FF9-490C-B76C-88A3A47A2208}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{B8266F62-6B09-4FB3-991A-754DA80373B1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_2</Property>
	<Property Name="varPersistentID:{B89E9E0A-C682-4288-B1AC-5F14C6577FC0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_3</Property>
	<Property Name="varPersistentID:{B8B3FF01-ADBD-4359-93B6-98AE83344C61}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_SelfTest</Property>
	<Property Name="varPersistentID:{B8C3318A-46C1-4E04-9A0F-2518D62FD22A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_1</Property>
	<Property Name="varPersistentID:{B8F7A95D-9F68-4BB2-8267-0960BA4E8846}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_0</Property>
	<Property Name="varPersistentID:{B9FBF699-9E01-41D7-8176-568F2349FA42}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Q-IL-Enabled</Property>
	<Property Name="varPersistentID:{B9FEB1CD-BE7B-49E6-872A-45D614D75C2F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ResourceName</Property>
	<Property Name="varPersistentID:{BA07C47E-D1F0-45F8-B49B-8745B8333EF3}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Status_1</Property>
	<Property Name="varPersistentID:{BA4219BD-A658-44C1-84B4-BAE6D4DA983A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingDeltaT</Property>
	<Property Name="varPersistentID:{BA925B05-BE25-4C5B-9D73-9D4275E524CB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_1</Property>
	<Property Name="varPersistentID:{BAA58FF3-61FB-4752-8E5E-975A52EA0A15}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_1</Property>
	<Property Name="varPersistentID:{BAA94989-2D4B-4E37-B27B-DD0A491A6E9E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{BAC7DDA8-F366-4057-AB4C-946DA603B726}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_FilterTime_0</Property>
	<Property Name="varPersistentID:{BB537D14-47DA-4083-9E1E-7E5021B28465}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_0</Property>
	<Property Name="varPersistentID:{BB6AB03A-3276-405C-83D2-926BD06D40EC}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXIDC6_position</Property>
	<Property Name="varPersistentID:{BB855D3D-74C4-4853-960D-82091B1A28C9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{BB89D813-2FCB-4386-87AF-74C8468FDFE6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_2</Property>
	<Property Name="varPersistentID:{BCAA330A-9ADC-4D8F-A8C8-198F41DCFA51}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-MP-Max</Property>
	<Property Name="varPersistentID:{BCB6AD67-5022-494A-8310-BE983FDA5FA5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Pressure</Property>
	<Property Name="varPersistentID:{BCC92999-9794-48A5-9D72-4A8BA0BCBBC4}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD11_current_setpoint</Property>
	<Property Name="varPersistentID:{BCCC7080-CD08-4394-8C73-E830C0651044}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP2_Status</Property>
	<Property Name="varPersistentID:{BD0CA3C2-2FE1-4297-BB99-9FC6D6BEC2E6}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SPS</Property>
	<Property Name="varPersistentID:{BD525760-FAEC-4CBB-A4B2-B6FF747CA6F4}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_PollingStartStop</Property>
	<Property Name="varPersistentID:{BD9DD6C9-017D-4DE0-A9C7-394FD9E756FE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_1</Property>
	<Property Name="varPersistentID:{BE0AEF7E-E9C5-43DD-A0C0-88D9C57052A2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_DriverRevision</Property>
	<Property Name="varPersistentID:{BE854BDE-4F2C-4AAC-A060-876CA88DA622}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{BE91203B-F99C-455C-86E2-C34D7E9749C4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_1</Property>
	<Property Name="varPersistentID:{BEA33856-0EE0-4F2F-A179-0E557CBC3285}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_CircuitMode_3</Property>
	<Property Name="varPersistentID:{BEE183A4-910D-4396-8D53-18DDBE37BB8E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-MP-Length-IL-Max</Property>
	<Property Name="varPersistentID:{BF428738-A253-4840-94C7-01AF3F5AD4E6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_SelftestResultCode</Property>
	<Property Name="varPersistentID:{BF808038-D56D-40F8-8187-427A32BAB4C4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_2</Property>
	<Property Name="varPersistentID:{C027E79D-BB18-4F87-BCD4-4B2277DBCE5F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingMode</Property>
	<Property Name="varPersistentID:{C045B139-DA7C-4A39-B6DF-F16F4D799AA9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_1</Property>
	<Property Name="varPersistentID:{C0502970-1582-4BAF-A4CE-1A519B880EF0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PIDLead</Property>
	<Property Name="varPersistentID:{C0559D7A-2294-47D9-BBAC-824467EF659B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Magnet-IL</Property>
	<Property Name="varPersistentID:{C067E761-0F94-457A-B52C-27798062CF7C}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_PollingIterations</Property>
	<Property Name="varPersistentID:{C07F443D-2F5B-4654-B524-2D97921718EF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_0</Property>
	<Property Name="varPersistentID:{C1872A9A-5542-484A-BB41-B5427BB0378D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-Rate-Limit-Enabled</Property>
	<Property Name="varPersistentID:{C1ECBE1F-9E2F-476E-927D-2CDBACF3E7B2}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_PollingTime</Property>
	<Property Name="varPersistentID:{C21D6C72-4823-460D-8163-9C86AC3EE4C2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_current</Property>
	<Property Name="varPersistentID:{C2B6DFC6-E65C-432A-BEAB-CB05ECCB2476}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_1</Property>
	<Property Name="varPersistentID:{C327D82C-6E33-47D6-AEAB-93CD8B17A0D4}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DetectorCounts</Property>
	<Property Name="varPersistentID:{C3A4B682-5E59-4D60-AADB-35598C72A040}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV6</Property>
	<Property Name="varPersistentID:{C3F2CFF5-ABDF-4473-8059-36BD4536CD7E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ResourceName</Property>
	<Property Name="varPersistentID:{C49F4C02-A9F4-42F1-9C28-63B54CB87071}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FirmwareRevision</Property>
	<Property Name="varPersistentID:{C57AA675-2F52-4017-8F58-E0EA0737667B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingInterval</Property>
	<Property Name="varPersistentID:{C61200B3-4F8B-40B2-9554-1023F936FA3D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PIDIntergral</Property>
	<Property Name="varPersistentID:{C64B6120-72D8-48A4-8375-430BC490BD2F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_-1</Property>
	<Property Name="varPersistentID:{C68889B0-972B-41E2-830F-AC4E3E33E48C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_3</Property>
	<Property Name="varPersistentID:{C707889B-CE29-4B65-89FE-CFD123FBE534}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_2</Property>
	<Property Name="varPersistentID:{C75A79B9-0B5E-447E-9E29-8B43FC2F6C1D}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2.currinfo_1</Property>
	<Property Name="varPersistentID:{C78C8765-078C-4CF1-AE8B-87D1666247CD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{C7C9AC42-810B-4ED3-BDC0-10F08D05AB9B}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DT3.currinfo</Property>
	<Property Name="varPersistentID:{C81F4EA1-68CC-4BD3-AB1B-492033CC3B40}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureOffset</Property>
	<Property Name="varPersistentID:{C83CB07F-7292-4FE0-AE65-6C89A2BE6484}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL-Main</Property>
	<Property Name="varPersistentID:{C8B95C2C-A967-4CAE-81C6-1887A1395EBF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UT2DCX_position</Property>
	<Property Name="varPersistentID:{C90CFA84-8363-4BFE-9EE7-D5B9547EE38E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_3</Property>
	<Property Name="varPersistentID:{C91BDF09-38E3-4D77-9630-EF26EFBD6574}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_0</Property>
	<Property Name="varPersistentID:{C9205DFD-34AE-4307-9791-C90FC783D43B}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Error-Log-Path</Property>
	<Property Name="varPersistentID:{C9306D9B-8868-48FF-A2D8-8F23478BF038}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_0</Property>
	<Property Name="varPersistentID:{C9537AF3-E27E-4885-AE3B-901EB3637F26}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX.currinfo_1</Property>
	<Property Name="varPersistentID:{C9C0D3E3-3935-484F-B9B8-1B841D06E492}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_2</Property>
	<Property Name="varPersistentID:{CA173208-F7A4-4908-AEF8-B16274DAE437}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_0</Property>
	<Property Name="varPersistentID:{CA1F5E83-F7ED-46EC-92AB-500D2E28B63D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{CA667A03-2C1E-4E2C-9FA6-6ABCCB6CC6C3}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Detector_Rate_Limit</Property>
	<Property Name="varPersistentID:{CA861D1F-8BC5-4003-B2C1-75B6EFE1971A}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{CAA6BD07-F161-4FA3-8766-54D23E97069D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{CAF9F4E8-5F14-41A1-B76B-9E346C37DB9B}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Array Size</Property>
	<Property Name="varPersistentID:{CB738AC9-5C73-4A38-ADFD-A5598661E3E2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_3</Property>
	<Property Name="varPersistentID:{CB9BE16E-566C-4A33-B618-66BF8E99D380}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXIDC6_current</Property>
	<Property Name="varPersistentID:{CC023DB2-97A3-4A4E-8D70-E655C791DD02}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Pyrometer-IL</Property>
	<Property Name="varPersistentID:{CC3BEBFE-DC53-4020-BABF-D71B3DA84BDB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_2</Property>
	<Property Name="varPersistentID:{CC42EA7B-6034-4C28-8FDE-C476C8E11248}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_1</Property>
	<Property Name="varPersistentID:{CC7314E0-63A5-422D-8586-11695D7F40B5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_0</Property>
	<Property Name="varPersistentID:{CCE012EB-A578-48A0-80AC-D9D5A2F0C977}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_0</Property>
	<Property Name="varPersistentID:{CD00A16C-94DC-44F7-9886-359B5C35F9C8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{CD08E48F-1D66-4000-9FAA-D5CB81B44A91}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{CD7E145C-A190-485A-9950-B693A797EA1C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-In</Property>
	<Property Name="varPersistentID:{CD865B48-C2AE-4D42-9F7C-8918841F2F99}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV1</Property>
	<Property Name="varPersistentID:{CD9D1CCA-DE57-4872-B38B-DCE33B5E4D09}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{CDA5A078-FE35-4F72-A274-2D179466982A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_2</Property>
	<Property Name="varPersistentID:{CE33EB57-6D4F-474D-8F78-D31457AFC91D}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_PollingDeltaT</Property>
	<Property Name="varPersistentID:{CE43665A-073F-4082-9D57-664D0C07A05A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_2</Property>
	<Property Name="varPersistentID:{CE9A9AA2-E3F3-4667-9BF5-434C9030EFF2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-MP-Length-IL-Min</Property>
	<Property Name="varPersistentID:{CEA908E9-1F18-442B-B712-0E32515B746A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MainValve</Property>
	<Property Name="varPersistentID:{CF8271CD-9A2E-4A28-A31F-D279F4A6B6B3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD11_current</Property>
	<Property Name="varPersistentID:{CFA6BB1B-5F6A-4F91-ADC5-DCD6ED1FDBB1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_0</Property>
	<Property Name="varPersistentID:{D055E64C-D183-4ABC-ADC9-55BEBCC18F31}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingInterval</Property>
	<Property Name="varPersistentID:{D07B1CDC-03C2-40D0-91BD-F1C125514CEF}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_SPS_A</Property>
	<Property Name="varPersistentID:{D0CBDF99-5922-4271-B0E6-DCBD85FD67D4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{D0FEE285-A795-4A77-B637-9D304E975AA9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/SecValvesBypass</Property>
	<Property Name="varPersistentID:{D10300A1-BBA6-4D7F-87DE-EDA0E5AEB6FC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{D188D775-70EE-4F6A-A984-36C1B72F69CD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ErrorCode</Property>
	<Property Name="varPersistentID:{D19238B9-7A74-4840-9BAB-D19C115F6686}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SelfTest</Property>
	<Property Name="varPersistentID:{D19A1BB2-7252-4454-9A93-E3537D724D7A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_3</Property>
	<Property Name="varPersistentID:{D19BA09E-B21E-41EC-A205-60B9650378DA}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6.currinfo_0</Property>
	<Property Name="varPersistentID:{D1C8002C-AB8A-4616-A261-1CA7B5AE899C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SelftestResultCode</Property>
	<Property Name="varPersistentID:{D1F6B878-6C87-4A1C-A596-6CB2A89EA90B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_3</Property>
	<Property Name="varPersistentID:{D2347CDD-0062-4C83-A5CB-64C2664A817A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingIterations</Property>
	<Property Name="varPersistentID:{D26F33CB-9DAC-4018-AE5B-C3D74190220C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SelftestResultCode</Property>
	<Property Name="varPersistentID:{D2EDE288-3ACA-4A82-A477-81C4F0A2886C}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Reset</Property>
	<Property Name="varPersistentID:{D37E1C4A-83E7-4389-9A83-EBCED99A7C17}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD12.currenti</Property>
	<Property Name="varPersistentID:{D3BCBF50-2236-4B54-B51B-85F55656032C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{D3CCBA7D-40E6-496B-BBC6-929A43F65443}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_3</Property>
	<Property Name="varPersistentID:{D3D2660B-3941-4586-8A88-8C13A0C8145C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IMin</Property>
	<Property Name="varPersistentID:{D3D8275E-0BC2-453B-8CF8-DBF3F396EB7B}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_UnderrangeControl</Property>
	<Property Name="varPersistentID:{D3F9FB27-9CC7-4A83-A038-9E03F965DC6B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Generate</Property>
	<Property Name="varPersistentID:{D42239B4-D8E8-4259-8AC0-1AAAFF0EFC5D}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXADT2.currinfo_1</Property>
	<Property Name="varPersistentID:{D492FEEC-5D6A-402E-AF73-1576BCF8F02F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingInterval</Property>
	<Property Name="varPersistentID:{D557D88D-55F9-469A-A6AE-EFE5AC957DDE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_0</Property>
	<Property Name="varPersistentID:{D6311024-004B-40B5-BC0B-2F65E8DF284F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripModes</Property>
	<Property Name="varPersistentID:{D633370A-7348-4BDF-9095-F72C89DBF8D2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_UnderrangeControl</Property>
	<Property Name="varPersistentID:{D6E9A46E-CA4E-4E50-ADA8-A0336234CC9C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_0</Property>
	<Property Name="varPersistentID:{D71AC4C5-AC7C-460B-A607-BCAA3DF29E00}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_2</Property>
	<Property Name="varPersistentID:{D76D32A0-D1DA-4FAD-B1BA-33C012BE2437}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_2</Property>
	<Property Name="varPersistentID:{D7C44CB1-F04F-41F7-B0A4-2B26F7A8B641}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ResourceName</Property>
	<Property Name="varPersistentID:{D7D5BE14-B48D-4481-A44E-79C4BECCD048}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{D8B8BE48-9993-45A7-AEAA-4279E0486FF7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingInterval</Property>
	<Property Name="varPersistentID:{D96AFCDD-62A7-4BCF-9D69-7FA0EFC8BD43}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SelftestResultCode</Property>
	<Property Name="varPersistentID:{D97632A2-DEDD-40E4-BE07-88F025C033A0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period-Reset</Property>
	<Property Name="varPersistentID:{D99AC762-EF06-4DB2-A237-C1CCA0AFBD60}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FirmwareRevision</Property>
	<Property Name="varPersistentID:{D9CB83D5-232B-42C1-AD54-97AC24990A38}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UT2DCX_range</Property>
	<Property Name="varPersistentID:{DB098D93-2400-4CA7-8F7F-D298A98E91F3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{DB29CF98-02BF-4E1C-86EB-71D17B30F05C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingDeltaT</Property>
	<Property Name="varPersistentID:{DB7D00A8-5A14-4F81-9CF8-2C3065C42C52}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_1</Property>
	<Property Name="varPersistentID:{DBB7AB8F-68A9-4B7A-829B-40E9A976322F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{DC5DC2B1-AED5-4E23-832D-EB4ABEB1F5B7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_2</Property>
	<Property Name="varPersistentID:{DD15DFBC-E426-44AD-BD55-178938BD1CAB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Pyrometer-IL</Property>
	<Property Name="varPersistentID:{DDC79C9B-7C90-4989-97A3-FEC329F3DC29}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{DE1AEDBF-55EB-4D56-85A6-00E237E12F3A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_0</Property>
	<Property Name="varPersistentID:{DE1C070A-5FD8-4274-8D6D-625BF7747AF5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Pressure</Property>
	<Property Name="varPersistentID:{DE2B5C28-26AC-4D99-9C44-F58C6799A26F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_2</Property>
	<Property Name="varPersistentID:{DE90CB3B-F931-48A7-96D0-9B6A962519C2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ResourceName</Property>
	<Property Name="varPersistentID:{DEE663ED-2738-4520-B39D-FD0F43C9FD15}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Chopper_status</Property>
	<Property Name="varPersistentID:{DFEB27C9-0CE3-4727-91A0-3A199BFC7044}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{E0187509-E491-44EE-9A43-8449A05173D2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_DriverRevision</Property>
	<Property Name="varPersistentID:{E0EB4EE5-9331-4477-B678-12D182574288}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_1</Property>
	<Property Name="varPersistentID:{E1666B6A-F014-41FC-84C1-1440275405E4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_2</Property>
	<Property Name="varPersistentID:{E1863D6D-F8EF-437A-BD35-40F10C52A64C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{E1D50D62-70E4-4142-BF97-83AC4449615C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-IL-Enabled</Property>
	<Property Name="varPersistentID:{E20033AD-FCED-4390-8B12-D20314DE587A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ExperimentNumber</Property>
	<Property Name="varPersistentID:{E2D96436-5363-4CA6-A8CC-B0A616C22C1A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingCounter</Property>
	<Property Name="varPersistentID:{E355F472-DE98-4681-91FF-5CA2D1A1E873}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_1</Property>
	<Property Name="varPersistentID:{E37C3EFB-D341-4505-AB99-9055C253DA9D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-QMP-Max</Property>
	<Property Name="varPersistentID:{E3A04FDC-5D05-44A1-833F-94A92B31D1B6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ZeroAdjustPressure</Property>
	<Property Name="varPersistentID:{E3D92FBB-B2B6-40C1-A51A-ED362EF0539C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-Q</Property>
	<Property Name="varPersistentID:{E49546FB-5CBD-4D48-989A-CD1DAF01B58F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_0</Property>
	<Property Name="varPersistentID:{E4E00C8C-8DBF-4975-8737-036492603262}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{E5647CBD-6C21-4C62-AD43-4AE44016CFE1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PIDLead</Property>
	<Property Name="varPersistentID:{E5A99A60-B430-466E-A296-952D6ACFE964}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ErrorMessage</Property>
	<Property Name="varPersistentID:{E5ABE123-1A32-4BC4-BAD2-046BAE964FD8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{E698106F-0A66-4EF4-A381-4610FBB81E86}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingMode</Property>
	<Property Name="varPersistentID:{E72DC9E9-03CE-42F7-856E-DBF4C21D6085}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_FirmwareRevision</Property>
	<Property Name="varPersistentID:{E74AA602-A3CA-4B64-93F8-1D229BD88C78}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_3</Property>
	<Property Name="varPersistentID:{E7C1B2F8-BC69-4766-BD67-B7264C8A5992}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingCounter</Property>
	<Property Name="varPersistentID:{E803AC97-B1FC-4AA5-8F7A-C9B9F62BE275}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{E813F30B-12D5-4142-9EEE-A569C9D8479A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_1</Property>
	<Property Name="varPersistentID:{E891BC72-439A-4808-90CB-1E781470FC1E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{E9563532-492A-4889-AF5B-0FC612FF6289}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Charge-IL</Property>
	<Property Name="varPersistentID:{E961EB2D-34B0-4A5E-8DD1-67E92559E19D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP5</Property>
	<Property Name="varPersistentID:{E982FBC0-57B0-4F04-9EAB-6C7FCBAAAB60}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_1</Property>
	<Property Name="varPersistentID:{EA2DE740-D997-425E-8E6F-60C0B8A78B20}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{EC18F1AB-3FFC-407E-B92F-E88726F7439B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-Counter</Property>
	<Property Name="varPersistentID:{EC698203-0A98-404D-92D4-DD10E8078B77}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{EC8CF35E-24F2-4D04-934D-897DBDDDCC2D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Virtual_Accelerator</Property>
	<Property Name="varPersistentID:{ECBFA71A-4EAD-48BB-A269-5D07CF8DC9D8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PressureMode</Property>
	<Property Name="varPersistentID:{ED74DDA8-E49B-4BD6-9C0D-EF6E196D6C7C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_1</Property>
	<Property Name="varPersistentID:{ED97014E-1549-4702-ADF3-107F45E26BC7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingTime</Property>
	<Property Name="varPersistentID:{ED9E563B-01B2-4330-A12C-79C2F57B726F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ErrorMessage</Property>
	<Property Name="varPersistentID:{EDB4BBE8-1F03-4B77-AC58-D3550098B0DB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Start-2</Property>
	<Property Name="varPersistentID:{EE72F3C2-18B6-4638-B924-69EB864BD513}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_0</Property>
	<Property Name="varPersistentID:{EEAD6D10-C845-4E0E-99D8-854B9F423CE0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset</Property>
	<Property Name="varPersistentID:{EEB0D783-D6CA-40D1-A94E-77CE97477BB2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3-Q</Property>
	<Property Name="varPersistentID:{EEEC6046-0648-4237-9CFB-A007DCB04352}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_1</Property>
	<Property Name="varPersistentID:{EF6141AD-B38B-494F-B7DA-AC923DB9CDCE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureOffset</Property>
	<Property Name="varPersistentID:{F0FAE5DE-F336-4D8E-8A89-67C5E9540F34}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{F122B3E8-0F9B-4696-8D50-BBB3774F7886}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ErrorMessage</Property>
	<Property Name="varPersistentID:{F1577F02-60E2-47F5-B88D-8F2DB55E3695}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ErrorCode</Property>
	<Property Name="varPersistentID:{F169A32F-7469-4BF2-9C6B-1D4B080148E8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_2</Property>
	<Property Name="varPersistentID:{F1EE5A69-E4D4-456D-B550-E333A47D832A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-DMA-IQ_11</Property>
	<Property Name="varPersistentID:{F2608F7A-5538-482C-9CA6-B6A55484D60B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_1</Property>
	<Property Name="varPersistentID:{F2625974-2F38-4F57-9A1A-4BA2670BD8CB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pyrometer-IL-Enabled</Property>
	<Property Name="varPersistentID:{F266F185-2F2A-4CDC-AF97-3F877E905969}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{F36F1542-42FB-4B66-8C4D-4B3D327ED64D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Frequency</Property>
	<Property Name="varPersistentID:{F4236AFB-52F0-4B27-A9DB-5540E1318334}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_2</Property>
	<Property Name="varPersistentID:{F460E191-5D66-4619-9A45-FF4BEFBB0956}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ResourceName</Property>
	<Property Name="varPersistentID:{F4895E45-809B-4084-9434-315D56E6164D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_2</Property>
	<Property Name="varPersistentID:{F4B2F715-1245-4B6A-96A6-C7DB78D6CA45}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingDeltaT</Property>
	<Property Name="varPersistentID:{F53C9786-30D1-4DBF-B248-FB76476D574A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP2</Property>
	<Property Name="varPersistentID:{F6DEAEAE-CAB6-4D96-A033-3528B0D33CE9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SecValvesState</Property>
	<Property Name="varPersistentID:{F74785C9-C453-4718-9FFE-A896384B0CAA}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Error Code</Property>
	<Property Name="varPersistentID:{F7899FC1-D853-4289-A5CE-C3DE3F525B4D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL-Main</Property>
	<Property Name="varPersistentID:{F7C2D917-0F88-4A17-878B-3E2EE27C24D9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_3</Property>
	<Property Name="varPersistentID:{F800E79B-46C5-4976-9265-37DF712697C1}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6_P.positi</Property>
	<Property Name="varPersistentID:{F83FEE33-BBE2-4380-8CBD-3AF8979060A6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_3</Property>
	<Property Name="varPersistentID:{F846F529-5589-4803-9BF0-EA6DD060329B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_A</Property>
	<Property Name="varPersistentID:{F8482F99-F57B-4A2F-8205-607E463FDC14}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingTime</Property>
	<Property Name="varPersistentID:{F85B40D2-FCE8-408D-8F16-8E150414371B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{F9AED3DE-16A2-4961-9DC1-9EC6388DCE1A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_UnderrangeControl</Property>
	<Property Name="varPersistentID:{FA5D223A-F364-40D8-A86F-2ACE4AC5EC62}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{FAC4DABD-F0EC-4A70-AF71-7D66524D0D46}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3-Proxy_Activate</Property>
	<Property Name="varPersistentID:{FAE0E45B-2E2E-4EFF-B2B1-F8A6B78F123A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{FAF9E791-927D-4B9F-B225-1646003010C0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{FB0B2F30-445A-49F6-B76B-498BD0A3AB25}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingTime</Property>
	<Property Name="varPersistentID:{FB5C72A3-E566-4F33-BEAB-ECAC463536F5}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX.currinfo</Property>
	<Property Name="varPersistentID:{FBF9F2CB-C2DA-4389-9BFB-C87AC50C212E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{FC2A03D2-BD9D-4ECA-8F3F-28B5FD492423}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_range</Property>
	<Property Name="varPersistentID:{FC31A4CE-67D4-402F-8D71-79487BB93A68}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_2</Property>
	<Property Name="varPersistentID:{FD0095E1-BC57-4C18-AD4F-6123986DC3C0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Interlock</Property>
	<Property Name="varPersistentID:{FD89B77C-55C2-4022-A5E8-8846A8A71E75}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_3</Property>
	<Property Name="varPersistentID:{FDC567A5-AA4B-4459-A162-8934F43A55A3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV6_Status</Property>
	<Property Name="varPersistentID:{FDF7C481-67AA-4D42-9F2D-4B46D98C2E88}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_3</Property>
	<Property Name="varPersistentID:{FE0225B6-A7A9-4D18-B1BE-872539B4ACF9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_0</Property>
	<Property Name="varPersistentID:{FE1CBCAC-CC02-4C14-9CE3-A4C2D601B9A9}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Server Type</Property>
	<Property Name="varPersistentID:{FE4157B2-7FD8-4246-A14E-254296B3CAAE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingTime</Property>
	<Property Name="varPersistentID:{FE6D2474-0EED-41D0-A17C-6EBD15A91BFC}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4_P.posits</Property>
	<Property Name="varPersistentID:{FE9BF2C2-15F4-4F91-9C5B-B9C81850891B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{FED8F9D0-21B9-4FF2-AEF3-8CE317F85778}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{FF6836C0-9D9C-486B-A05D-3D89A46784B3}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{FF9B62CF-6422-4B6E-96A3-30B59600BA9E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{FFC92A0F-D8F7-4D79-A7AE-3DBCA9093A28}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_CircuitMode_0</Property>
	<Property Name="varPersistentID:{FFED64BA-B158-4EA5-8577-4333DEF709A8}" Type="Ref">/My Computer/Test/TPG300-SV.lvlib/TPG300_Set-SwitchingLimits_1</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="ACC" Type="Folder">
			<Item Name="UTCS_AccIO.lvlib" Type="Library" URL="../SV.lib/UTCS_AccIO.lvlib"/>
			<Item Name="UTCS_AccIOS.lvlib" Type="Library" URL="../SV.lib/UTCS_AccIOS.lvlib"/>
		</Item>
		<Item Name="AF" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
		</Item>
		<Item Name="BNT" Type="Folder">
			<Item Name="BNT_DAQmx" Type="Folder">
				<Item Name="BNT_DAQmx-Content.vi" Type="VI" URL="../Packages/BNT_DAQmx/BNT_DAQmx-Content.vi"/>
				<Item Name="BNT_DAQmx.lvlib" Type="Library" URL="../Packages/BNT_DAQmx/BNT_DAQmx.lvlib"/>
			</Item>
			<Item Name="BNT_TDMS" Type="Folder"/>
		</Item>
		<Item Name="CSPP" Type="Folder">
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_PVScaler.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_PVScaler/CSPP_PVScaler.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Libs" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
			</Item>
			<Item Name="DSC" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
					<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
					<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
					<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
					<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
				</Item>
				<Item Name="Libs" Type="Folder">
					<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
				</Item>
				<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
			</Item>
			<Item Name="ObjectManager" Type="Folder">
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
				</Item>
				<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
			</Item>
		</Item>
		<Item Name="Devices" Type="Folder">
			<Item Name="MKS647C.lvlib" Type="Library" URL="../Packages/CSPP_MKS647C/MKS647C Actor/MKS647C.lvlib"/>
			<Item Name="MKS647CGUI.lvlib" Type="Library" URL="../Packages/CSPP_MKS647C/MKS647C GUI/MKS647CGUI.lvlib"/>
			<Item Name="TPG300A.lvlib" Type="Library" URL="../Packages/CSPP_TPG300/TPG300A.lvlib"/>
			<Item Name="TPG300GUI.lvlib" Type="Library" URL="../Packages/CSPP_TPG300/TPG300 GUI/TPG300GUI.lvlib"/>
			<Item Name="UTCS_BMIL.lvlib" Type="Library" URL="../Packages/UTCS/BeamControl/Host/UTCS_BMIL.lvlib"/>
		</Item>
		<Item Name="Docs &amp; EUPL" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
			<Item Name="README.md" Type="Document" URL="../README.md"/>
		</Item>
		<Item Name="instr.lib" Type="Folder">
			<Item Name="MGC647C.lvlib" Type="Library" URL="../instr.lib/MGC647C/MGC647C.lvlib"/>
			<Item Name="TPG300.lvlib" Type="Library" URL="../instr.lib/TPG300/TPG300.lvlib"/>
		</Item>
		<Item Name="libs" Type="Folder">
			<Item Name="ni_security_salapi.dll" Type="Document" URL="/&lt;vilib&gt;/Platform/security/ni_security_salapi.dll"/>
		</Item>
		<Item Name="OperatingGUIs" Type="Folder">
			<Item Name="UTCS_BeamControlGUI.lvlib" Type="Library" URL="../Packages/UTCS/BeamControl-GUI/UTCS_BeamControlGUI.lvlib"/>
			<Item Name="UTCS_GasGUI.lvlib" Type="Library" URL="../Packages/UTCS/Gas-GUI/UTCS_GasGUI.lvlib"/>
			<Item Name="UTCS_HTVGUI.lvlib" Type="Library" URL="//WinFileSvH/KC$Root/hdavid/Eigene Dateien/UTCS_HTVGUI.lvlib"/>
			<Item Name="UTCS_InterlockGUI.lvlib" Type="Library" URL="../Packages/UTCS/Interlock-GUI/UTCS_InterlockGUI.lvlib"/>
			<Item Name="UTCS_MainGUI.lvlib" Type="Library" URL="../Packages/UTCS/UTCS_MainGUI/UTCS_MainGUI.lvlib"/>
		</Item>
		<Item Name="TASCA" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="UTCS_Content.vi" Type="VI" URL="../Packages/UTCS/UTCS_Content.vi"/>
			<Item Name="UTCS_AE.lvlib" Type="Library" URL="../SV.lib/UTCS_AE.lvlib"/>
			<Item Name="UTCS_BMIL_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_BMIL_SV.lvlib"/>
			<Item Name="UTCS_MKS647_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_MKS647_SV.lvlib"/>
			<Item Name="UTCS_TPG300_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_TPG300_SV.lvlib"/>
			<Item Name="TPG300_Pressures.xml" Type="Document" URL="../SV.lib/TPG300_Pressures.xml"/>
		</Item>
		<Item Name="Test" Type="Folder">
			<Item Name="Test Sgl Precision.vi" Type="VI" URL="../Packages/UTCS/BeamControl/Test Sgl Precision.vi"/>
			<Item Name="TestHTV.vi" Type="VI" URL="../SV.lib/TestHTV.vi"/>
			<Item Name="TPG300-SV.lvlib" Type="Library" URL="../Packages/CSPP_TPG300/TPG300-SV.lvlib"/>
		</Item>
		<Item Name="COMAPCT.ico" Type="Document" URL="../Packages/COMPACT/COMAPCT.ico"/>
		<Item Name="FPGA Target 3" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{03DD19E4-2E24-4309-BC13-13EAD0FEDDEC}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT3;0;ReadMethodType=u8;WriteMethodType=u8{11483F6E-FEB0-4035-8069-F36FECE82200}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{17936D16-9B7C-4748-A17E-D7A7858077D1}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{18DBE7F3-B156-44FC-B90A-28D83938DF52}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{22E0B521-2317-41EF-9DEE-8F2069A4F92C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{25239961-E3DE-4C50-9F60-7CB2BCCEC065}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{2E1DE379-DA32-4C1F-B722-15C4692CB066}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO36;0;ReadMethodType=bool;WriteMethodType=bool{2EA1E004-B646-4CAF-9174-7994122E61BE}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{3B29909C-903A-42D2-B192-7C2C8AEF657A}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{3D2007F7-8A52-415F-90A1-745153A63B22}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{43BD90D0-F0BD-4797-9E72-54E350057904}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{43D9DE6C-F9DA-48EC-985F-0B7D436BF79B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{4F800515-F833-4737-BE44-B7BED6DD31E2}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{5469CC32-CA9F-4B6F-8D4F-6FA984BF9A2B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{5EF9F3E3-4F6B-410F-B94D-0CB361E1B858}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{659C114C-9034-45FF-A9C8-B555878807DC}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{65F5E990-981D-463E-BEE8-6D0B9CC8A0F9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{6C0EC6B0-C87D-406A-9B46-C3C4CA6E988D}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{6C397C01-A22B-48C2-9FCA-6B5A225E4462}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{6F827EB1-63C4-429A-B171-92DB3373742E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT4;0;ReadMethodType=u8;WriteMethodType=u8{7A2FF39C-4833-4A30-8D41-71BFD47C880B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{7F975E78-AB84-41E8-8820-F7D43E7921AD}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{86C92371-816D-425B-804B-07C1290E571D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{878ACE91-FCA4-4D42-B783-A67A4B11921D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{882E658F-CD99-4AF5-827D-02A4BEFE76A3}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{8BC83B97-AA60-4F54-8852-DB965859BFE0}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{8C5C4FDC-52C8-4ECC-8E85-12D84A6711CB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{9196A292-241F-4BAC-8EDA-F12428365736}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{A65946E2-B2E2-4D51-BDBA-A7A2C1AE3F71}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO32;0;ReadMethodType=bool;WriteMethodType=bool{A6ED0F74-8DB3-4E84-9280-9E821863D73D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{AA972CDD-52B2-41B3-984E-C132F660CFBA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO34;0;ReadMethodType=bool;WriteMethodType=bool{B8C1AE46-A800-4A6A-AE0B-D13B90CF130C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{BA570723-AAAC-4636-84EB-01E9D930A3D6}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{BD93E4E2-ED6B-455B-9E28-9358B9832528}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT2;0;ReadMethodType=u8;WriteMethodType=u8{BDA64F62-34F3-4E33-AAE3-8ED72ABA3018}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO35;0;ReadMethodType=bool;WriteMethodType=bool{BFFD0110-0A6F-455C-91CB-1D99300F9D06}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{C5B78BDC-6E93-4875-9D01-34272DA91028}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO38;0;ReadMethodType=bool;WriteMethodType=bool{CDD772D6-5C42-4CD1-BD3E-7EC5C398A931}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{D07A8053-0A00-4895-9AE4-319E07DCE19C}"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-SglToH;DataType=100080000000000100094009000353474C000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{D9232A5C-473F-47AD-96ED-F1CA2A15EA9D}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{D99453ED-F6EE-4E25-A55D-8783C403EC13}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{DEED89FA-9C64-4670-85AF-051A4013EB92}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{DFB05F7F-D200-4B42-BF19-28D41BB1FEB9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{E270E0AF-E6F1-4A2D-A585-49BD8E505DE4}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO33;0;ReadMethodType=bool;WriteMethodType=bool{F025C34E-5A3E-43C4-9370-18E1E9CA6C0D}"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-IQ2H;DataType=100080000000000100094008000355363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{F2580E4D-34C8-4826-851B-F42DCD0361DA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{F2675394-8BC6-4AD4-8605-23A3507D39D0}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO37;0;ReadMethodType=bool;WriteMethodType=bool{F4BAAB4E-8CEA-4B10-BECA-3779B2BA3026}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO39;0;ReadMethodType=bool;WriteMethodType=bool{F75B72CD-5211-4ADF-BBC6-6DC468B03FCE}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{F888277C-26E8-467F-80B6-C5AAC8CE5B27}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{F88FAD50-DAE7-4640-A49D-FC4ED33D3EB8}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{F8A50E3A-E909-4C2E-8739-D7E808CA1A8E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{FF3014B0-0394-48E1-B37E-FA37A3EF699D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{FFEA04CD-690F-44F1-805B-69708EE827AF}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO25;0;ReadMethodType=bool;WriteMethodType=boolPXI-7813R/DSCGetVarList,PSP;WebpubLaunchBrowser,None;DSCSecurity,False;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7813RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;5V-0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO10;0;ReadMethodType=bool;WriteMethodType=bool5V-1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO11;0;ReadMethodType=bool;WriteMethodType=boolAux In 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO12;0;ReadMethodType=bool;WriteMethodType=boolAux In 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO13;0;ReadMethodType=bool;WriteMethodType=boolAux In 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO14;0;ReadMethodType=bool;WriteMethodType=boolAux In 3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO15;0;ReadMethodType=bool;WriteMethodType=boolAux Out 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO4;0;ReadMethodType=bool;WriteMethodType=boolAux Out 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO5;0;ReadMethodType=bool;WriteMethodType=boolAux Out 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO6;0;ReadMethodType=bool;WriteMethodType=boolAux Out 3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO7;0;ReadMethodType=bool;WriteMethodType=boolChopper InArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO31;0;ReadMethodType=bool;WriteMethodType=boolChopper OutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO8;0;ReadMethodType=bool;WriteMethodType=boolDetectorArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolDetector-FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT2;0;ReadMethodType=u8;WriteMethodType=u8DMA-IQ-TOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO20;0;ReadMethodType=bool;WriteMethodType=boolF_DipoleArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolFIFO-Detector"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-MPL"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-MPP"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-SglToH"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-SglToH;DataType=100080000000000100094009000353474C000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-U64ToH"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-IQ2H;DataType=100080000000000100094008000355363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-UX8DT3"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-UXADT2"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FirstArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO36;0;ReadMethodType=bool;WriteMethodType=boolFirstOutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO18;0;ReadMethodType=bool;WriteMethodType=boolInhibitArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO17;0;ReadMethodType=bool;WriteMethodType=boolInterlock-0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO9;0;ReadMethodType=bool;WriteMethodType=boolInterlock-1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO16;0;ReadMethodType=bool;WriteMethodType=boolInterval FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO26;0;ReadMethodType=bool;WriteMethodType=boolIntervalArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMagnetArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO39;0;ReadMethodType=bool;WriteMethodType=boolMP ActiveArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMP Out 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMP Out 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMP Out 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMP Out 3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMP_GenArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMP_InArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO24;0;ReadMethodType=bool;WriteMethodType=boolPXI-7813R/DSCGetVarList,PSP;WebpubLaunchBrowser,None;DSCSecurity,False;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7813RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]PyrometerArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO38;0;ReadMethodType=bool;WriteMethodType=boolRange 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO28;0;ReadMethodType=bool;WriteMethodType=boolRange 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO29;0;ReadMethodType=bool;WriteMethodType=boolRange 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO30;0;ReadMethodType=bool;WriteMethodType=boolRatTrapArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO32;0;ReadMethodType=bool;WriteMethodType=boolSecondArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO37;0;ReadMethodType=bool;WriteMethodType=boolSecondOutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO19;0;ReadMethodType=bool;WriteMethodType=boolSecValvesByPassArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO34;0;ReadMethodType=bool;WriteMethodType=boolSecValvesStateArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO35;0;ReadMethodType=bool;WriteMethodType=boolTrafo_GenArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO22;0;ReadMethodType=bool;WriteMethodType=boolUX8DT3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolUX8DT3-FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT3;0;ReadMethodType=u8;WriteMethodType=u8UXADT2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolUXADT2-FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT4;0;ReadMethodType=u8;WriteMethodType=u8VacuumBurstArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO33;0;ReadMethodType=bool;WriteMethodType=bool</Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">PXI-7813R/DSCGetVarList,PSP;WebpubLaunchBrowser,None;DSCSecurity,False;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7813RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Property Name="Resource Name" Type="Str">RIO0</Property>
			<Property Name="Target Class" Type="Str">PXI-7813R</Property>
			<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="Connector0" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Input" Type="Folder">
					<Item Name="MP_In" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5EF9F3E3-4F6B-410F-B94D-0CB361E1B858}</Property>
					</Item>
					<Item Name="Range 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8BC83B97-AA60-4F54-8852-DB965859BFE0}</Property>
					</Item>
					<Item Name="Range 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DEED89FA-9C64-4670-85AF-051A4013EB92}</Property>
					</Item>
					<Item Name="Range 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D99453ED-F6EE-4E25-A55D-8783C403EC13}</Property>
					</Item>
					<Item Name="Chopper In" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{882E658F-CD99-4AF5-827D-02A4BEFE76A3}</Property>
					</Item>
					<Item Name="RatTrap" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO32</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A65946E2-B2E2-4D51-BDBA-A7A2C1AE3F71}</Property>
					</Item>
					<Item Name="VacuumBurst" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO33</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E270E0AF-E6F1-4A2D-A585-49BD8E505DE4}</Property>
					</Item>
					<Item Name="SecValvesByPass" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO34</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AA972CDD-52B2-41B3-984E-C132F660CFBA}</Property>
					</Item>
					<Item Name="SecValvesState" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO35</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BDA64F62-34F3-4E33-AAE3-8ED72ABA3018}</Property>
					</Item>
					<Item Name="First" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO36</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2E1DE379-DA32-4C1F-B722-15C4692CB066}</Property>
					</Item>
					<Item Name="Second" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO37</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F2675394-8BC6-4AD4-8605-23A3507D39D0}</Property>
					</Item>
					<Item Name="Pyrometer" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO38</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C5B78BDC-6E93-4875-9D01-34272DA91028}</Property>
					</Item>
					<Item Name="Magnet" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO39</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F4BAAB4E-8CEA-4B10-BECA-3779B2BA3026}</Property>
					</Item>
					<Item Name="Aux In 3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6C397C01-A22B-48C2-9FCA-6B5A225E4462}</Property>
					</Item>
					<Item Name="Aux In 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7F975E78-AB84-41E8-8820-F7D43E7921AD}</Property>
					</Item>
					<Item Name="Aux In 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2EA1E004-B646-4CAF-9174-7994122E61BE}</Property>
					</Item>
					<Item Name="Aux In 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F888277C-26E8-467F-80B6-C5AAC8CE5B27}</Property>
					</Item>
				</Item>
				<Item Name="Output" Type="Folder">
					<Item Name="Chopper Out" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{25239961-E3DE-4C50-9F60-7CB2BCCEC065}</Property>
					</Item>
					<Item Name="Interlock-0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{43D9DE6C-F9DA-48EC-985F-0B7D436BF79B}</Property>
					</Item>
					<Item Name="5V-0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{18DBE7F3-B156-44FC-B90A-28D83938DF52}</Property>
					</Item>
					<Item Name="5V-1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FF3014B0-0394-48E1-B37E-FA37A3EF699D}</Property>
					</Item>
					<Item Name="Interlock-1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A6ED0F74-8DB3-4E84-9280-9E821863D73D}</Property>
					</Item>
					<Item Name="Inhibit" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DFB05F7F-D200-4B42-BF19-28D41BB1FEB9}</Property>
					</Item>
					<Item Name="FirstOut" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{17936D16-9B7C-4748-A17E-D7A7858077D1}</Property>
					</Item>
					<Item Name="SecondOut" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B8C1AE46-A800-4A6A-AE0B-D13B90CF130C}</Property>
					</Item>
					<Item Name="DMA-IQ-TO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BA570723-AAAC-4636-84EB-01E9D930A3D6}</Property>
					</Item>
					<Item Name="MP Active" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7A2FF39C-4833-4A30-8D41-71BFD47C880B}</Property>
					</Item>
					<Item Name="Trafo_Gen" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F2580E4D-34C8-4826-851B-F42DCD0361DA}</Property>
					</Item>
					<Item Name="MP_Gen" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F75B72CD-5211-4ADF-BBC6-6DC468B03FCE}</Property>
					</Item>
					<Item Name="MP Out 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{11483F6E-FEB0-4035-8069-F36FECE82200}</Property>
					</Item>
					<Item Name="MP Out 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F88FAD50-DAE7-4640-A49D-FC4ED33D3EB8}</Property>
					</Item>
					<Item Name="MP Out 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{659C114C-9034-45FF-A9C8-B555878807DC}</Property>
					</Item>
					<Item Name="MP Out 3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8C5C4FDC-52C8-4ECC-8E85-12D84A6711CB}</Property>
					</Item>
					<Item Name="Aux Out 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5469CC32-CA9F-4B6F-8D4F-6FA984BF9A2B}</Property>
					</Item>
					<Item Name="Aux Out 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9196A292-241F-4BAC-8EDA-F12428365736}</Property>
					</Item>
					<Item Name="Aux Out 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{878ACE91-FCA4-4D42-B783-A67A4B11921D}</Property>
					</Item>
					<Item Name="Interval FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F8A50E3A-E909-4C2E-8739-D7E808CA1A8E}</Property>
					</Item>
					<Item Name="Aux Out 3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{22E0B521-2317-41EF-9DEE-8F2069A4F92C}</Property>
					</Item>
					<Item Name="Interval" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FFEA04CD-690F-44F1-805B-69708EE827AF}</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Connector 1" Type="Folder">
				<Item Name="Input" Type="Folder">
					<Item Name="Detector" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{43BD90D0-F0BD-4797-9E72-54E350057904}</Property>
					</Item>
					<Item Name="UX8DT3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CDD772D6-5C42-4CD1-BD3E-7EC5C398A931}</Property>
					</Item>
					<Item Name="UXADT2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{86C92371-816D-425B-804B-07C1290E571D}</Property>
					</Item>
					<Item Name="F_Dipole" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{65F5E990-981D-463E-BEE8-6D0B9CC8A0F9}</Property>
					</Item>
				</Item>
				<Item Name="Output" Type="Folder">
					<Item Name="Detector-FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIOPORT2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BD93E4E2-ED6B-455B-9E28-9358B9832528}</Property>
					</Item>
					<Item Name="UX8DT3-FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIOPORT3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{03DD19E4-2E24-4309-BC13-13EAD0FEDDEC}</Property>
					</Item>
					<Item Name="UXADT2-FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIOPORT4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6F827EB1-63C4-429A-B171-92DB3373742E}</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{D9232A5C-473F-47AD-96ED-F1CA2A15EA9D}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="FIFO-MPL" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">7</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{BFFD0110-0A6F-455C-91CB-1D99300F9D06}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">1000800000000001000940070003553332000100000000000000000000</Property>
			</Item>
			<Item Name="FIFO-MPP" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">7</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{3D2007F7-8A52-415F-90A1-745153A63B22}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">1000800000000001000940070003553332000100000000000000000000</Property>
			</Item>
			<Item Name="FIFO-SglToH" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">8191</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">11</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-SglToH;DataType=100080000000000100094009000353474C000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{D07A8053-0A00-4895-9AE4-319E07DCE19C}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">8191</Property>
				<Property Name="Type" Type="UInt">2</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094009000353474C000100000000000000000000</Property>
			</Item>
			<Item Name="FIFO-U64ToH" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">8191</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">8</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-IQ2H;DataType=100080000000000100094008000355363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{F025C34E-5A3E-43C4-9370-18E1E9CA6C0D}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">8191</Property>
				<Property Name="Type" Type="UInt">2</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094008000355363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="FIFO-UX8DT3" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">4</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{6C0EC6B0-C87D-406A-9B46-C3C4CA6E988D}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094004000349363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="FIFO-UXADT2" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">4</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{4F800515-F833-4737-BE44-B7BED6DD31E2}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094004000349363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="FIFO-Detector" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">4</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{3B29909C-903A-42D2-B192-7C2C8AEF657A}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094004000349363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="UTCS_BC_FPGA.lvlib" Type="Library" URL="../Packages/UTCS/BeamControl/FPGA/UTCS_BC_FPGA.lvlib"/>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="Main" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">Main</Property>
					<Property Name="Comp.BitfileName" Type="Str">UTCS_PXI-7813R_Main.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">Packages/UTCS/BeamControl/FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/User/Brand/LVP/TASCA/UTCS/Packages/UTCS/BeamControl/FPGA Bitfiles/UTCS_PXI-7813R_Main.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">Packages/UTCS/BeamControl/FPGA Bitfiles/UTCS_PXI-7813R_Main.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/User/Brand/LVP/TASCA/UTCS/UTCS.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
					<Property Name="TargetName" Type="Str">FPGA Target 3</Property>
					<Property Name="TopLevelVI" Type="Ref">/My Computer/FPGA Target 3/UTCS_BC_FPGA.lvlib/Main.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="UTCS.ico" Type="Document" URL="../UTCS.ico"/>
		<Item Name="UTCS.vi" Type="VI" URL="../Packages/UTCS/UTCS.vi"/>
		<Item Name="UTCS_HMD.ini" Type="Document" URL="../UTCS_HMD.ini"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="CIT_ReadTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/citadel/CIT_ReadTimeout.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="CTL_dbNameValid.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_dbNameValid.vi"/>
				<Item Name="CTL_dbURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_dbURLdecode.vi"/>
				<Item Name="CTL_defaultEvtDB.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultEvtDB.vi"/>
				<Item Name="CTL_defaultHistDB.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultHistDB.vi"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="CTL_extractURLMDPformat.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_extractURLMDPformat.vi"/>
				<Item Name="CTL_findDSCApp.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_findDSCApp.vi"/>
				<Item Name="CTL_getAllDBInfo.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getAllDBInfo.vi"/>
				<Item Name="CTL_getArrayPathAndTraceReentrant.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getArrayPathAndTraceReentrant.vi"/>
				<Item Name="CTL_getDBFromDir.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getDBFromDir.vi"/>
				<Item Name="CTL_getDBPathandTraceList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getDBPathandTraceList.vi"/>
				<Item Name="CTL_hdManager.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdManager.vi"/>
				<Item Name="CTL_hdManagerBuffer.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdManagerBuffer.vi"/>
				<Item Name="CTL_hdProxyManager.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdProxyManager.vi"/>
				<Item Name="CTL_lookupTagURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_lookupTagURL.vi"/>
				<Item Name="CTL_resolveSourceDBURLInput.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_resolveSourceDBURLInput.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="dsc_PrefsPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/info/dsc_PrefsPath.vi"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="dscHistD.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/historical/internal/dscHistD.dll"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="FormatTime String.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/FormatTime String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get Project Library Version.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Get Project Library Version.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="HIST_AlarmDataToControl.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_AlarmDataToControl.vi"/>
				<Item Name="HIST_BuildAlarmColumns.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_BuildAlarmColumns.vi"/>
				<Item Name="HIST_CheckAlarmCtlRef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_CheckAlarmCtlRef.vi"/>
				<Item Name="HIST_ExtractAlarmData.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_ExtractAlarmData.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="HIST_GET_FILTER_ERRORS.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_GET_FILTER_ERRORS.vi"/>
				<Item Name="HIST_GetFilterTime.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_GetFilterTime.vi"/>
				<Item Name="HIST_GetHistTagListCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_GetHistTagListCORE.vi"/>
				<Item Name="HIST_ReadBitArrayTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadBitArrayTrace.vi"/>
				<Item Name="HIST_ReadBitArrayTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadBitArrayTraceCORE.vi"/>
				<Item Name="HIST_ReadLogicalTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadLogicalTrace.vi"/>
				<Item Name="HIST_ReadLogicalTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadLogicalTraceCORE.vi"/>
				<Item Name="HIST_ReadNumericTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadNumericTrace.vi"/>
				<Item Name="HIST_ReadNumericTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadNumericTraceCORE.vi"/>
				<Item Name="HIST_ReadStringTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadStringTrace.vi"/>
				<Item Name="HIST_ReadStringTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadStringTraceCORE.vi"/>
				<Item Name="HIST_ReadVariantTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadVariantTrace.vi"/>
				<Item Name="HIST_ReadVariantTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadVariantTraceCORE.vi"/>
				<Item Name="HIST_RunAlarmQueryCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_RunAlarmQueryCORE.vi"/>
				<Item Name="HIST_VALIDATE_FILTER.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_VALIDATE_FILTER.vi"/>
				<Item Name="HIST_ValReadTrendOptions.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ValReadTrendOptions.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NET_convertLocalhostURLToMachineURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_convertLocalhostURLToMachineURL.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="NET_handleDotInTagName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_handleDotInTagName.vi"/>
				<Item Name="NET_IsComputerLocalhost.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_IsComputerLocalhost.vi"/>
				<Item Name="NET_localhostToMachineName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_localhostToMachineName.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_resolveTagURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveTagURL.vi"/>
				<Item Name="NET_SameMachine.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_SameMachine.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Security Domain.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Domain.ctl"/>
				<Item Name="NI_Security Get Domains.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Get Domains.vi"/>
				<Item Name="NI_Security Identifier.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Identifier.ctl"/>
				<Item Name="NI_Security Resolve Domain.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Resolve Domain.vi"/>
				<Item Name="NI_Security_GetTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_GetTimeout.vi"/>
				<Item Name="NI_Security_ProgrammaticLogin.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ProgrammaticLogin.vi"/>
				<Item Name="NI_Security_ResolveDomainID.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainID.vi"/>
				<Item Name="NI_Security_ResolveDomainName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainName.vi"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="PTH_EmptyOrNotAPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_EmptyOrNotAPath.vi"/>
				<Item Name="PTH_IsUNC.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_IsUNC.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="SEC Get Interactive User.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/custom/SEC Get Interactive User.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subElapsedTime.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/subElapsedTime.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="TIME_FormatTS(TS).vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/time/TIME_FormatTS(TS).vi"/>
				<Item Name="TIME_StartTsLEStopTs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/time/TIME_StartTsLEStopTs.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="UTCS_PXI-7813R_Main.lvbitx" Type="Document" URL="../Packages/UTCS/BeamControl/FPGA Bitfiles/UTCS_PXI-7813R_Main.lvbitx"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="UTCS App" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{E438EDDD-4AB6-4A1B-A2EF-F405DCA1D8EC}</Property>
				<Property Name="App_INI_GUID" Type="Str">{D5B3B86F-9F9C-441E-BBE8-1F64BE17203A}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{90E67491-152D-4A03-9F72-27DC414438AE}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">UTCS App</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/UTCS/UTCS App</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{36487EFB-0CEC-4BE6-87F2-BBA5FADEB0E7}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">/C/User/Brand/builds/TASCA/UTCS_App/UTCS_UTCS App_log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">11</Property>
				<Property Name="Bld_version.minor" Type="Int">4</Property>
				<Property Name="Destination[0].destName" Type="Str">UTCS.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/UTCS/UTCS App/UTCS.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/UTCS/UTCS App/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/UTCS.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{4171B596-AC7C-448F-AC6C-FD87D38F2A71}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/UTCS.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib</Property>
				<Property Name="Source[10].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].type" Type="Str">Library</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib</Property>
				<Property Name="Source[11].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].type" Type="Str">Library</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/libs/ni_security_salapi.dll</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.pdf</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/README.md</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib</Property>
				<Property Name="Source[5].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].type" Type="Str">Library</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib</Property>
				<Property Name="Source[7].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">Library</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/TASCA/TPG300_Pressures.xml</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">13</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">UTCS App based on NI Actorframework and CS++.</Property>
				<Property Name="TgtF_internalName" Type="Str">UTCS App</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">UTCS</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{CCC3E9BD-32A7-4FDC-8D9E-7D0DD8099685}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">UTCS.exe</Property>
			</Item>
			<Item Name="UTCS Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">UTCS</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{5BF2DFA0-38AA-48F0-A80E-4352C4506255}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI DataSocket 5.5</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{81A7E53E-9524-41CE-90D3-7DD3D90B6C58}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[1].productID" Type="Str">{9A50C1AD-D5E4-4948-85E6-F9C14AC3439C}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI Distributed System Manager 2017</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{49570182-EFA6-4B4E-9181-572ECD14B262}</Property>
				<Property Name="DistPart[10].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[10].productID" Type="Str">{D1309622-F8BA-4E8A-8420-EB0649AC3922}</Property>
				<Property Name="DistPart[10].productName" Type="Str">Microsoft Silverlight 5.1</Property>
				<Property Name="DistPart[10].upgradeCode" Type="Str">{69DA64F2-1630-4C0C-947D-6CF5590A63A4}</Property>
				<Property Name="DistPart[11].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[11].productID" Type="Str">{A1E65A0F-536D-4307-AC65-6067EB029443}</Property>
				<Property Name="DistPart[11].productName" Type="Str">NI DataFinder Desktop Edition Runtime 2017</Property>
				<Property Name="DistPart[11].upgradeCode" Type="Str">{C14CA542-0B0B-4CD1-8460-FC019E4EBB9D}</Property>
				<Property Name="DistPart[12].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[12].productID" Type="Str">{9DFD2D10-299D-4A37-9267-4F28E974F403}</Property>
				<Property Name="DistPart[12].productName" Type="Str">NI TDM Excel Add-In</Property>
				<Property Name="DistPart[12].upgradeCode" Type="Str">{6D2EBDAF-6CCD-44F3-B767-4DF9E0F2037B}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{2A02F595-ED6A-40FC-B8DC-D9612FF165B6}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI LabVIEW DSC Module Runtime 2017</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{FEC48E37-9963-4534-9A12-D9448B97266D}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[3].productID" Type="Str">{1018EFF2-28A1-4A2B-9557-0CC1A45AA6CF}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI R Series Multifunction RIO Runtime 17.0</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{BED46696-FEA8-41AC-8377-F85B6CE69BE5}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[4].productID" Type="Str">{3011FA37-DCC4-4BF8-8900-12D68EC7996F}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI Variable Engine 2017</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{EB7A3C81-1C0F-4495-8CE5-0A427E4E6285}</Property>
				<Property Name="DistPart[5].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[5].productID" Type="Str">{6B2D1DEB-CFE9-43D9-B325-AB607C62A29C}</Property>
				<Property Name="DistPart[5].productName" Type="Str">NI-488.2 Runtime 17.0</Property>
				<Property Name="DistPart[5].upgradeCode" Type="Str">{357F6618-C660-41A2-A185-5578CC876D1D}</Property>
				<Property Name="DistPart[6].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[6].productID" Type="Str">{D2A2BFA8-A630-43C4-A1E0-1E8BC47FC8A0}</Property>
				<Property Name="DistPart[6].productName" Type="Str">NI-DAQmx Runtime 17.0</Property>
				<Property Name="DistPart[6].upgradeCode" Type="Str">{923C9CD5-A0D8-4147-9A8D-998780E30763}</Property>
				<Property Name="DistPart[7].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[7].productID" Type="Str">{5E17F5AF-F47C-4C3A-A219-91F4638CE9D1}</Property>
				<Property Name="DistPart[7].productName" Type="Str">NI-Serial Runtime 17.0</Property>
				<Property Name="DistPart[7].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[8].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[8].productID" Type="Str">{F9B5B433-547E-4A74-AFE6-91C16787824E}</Property>
				<Property Name="DistPart[8].productName" Type="Str">NI-VISA Runtime 17.0</Property>
				<Property Name="DistPart[8].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[9].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[9].productID" Type="Str">{AF2FEF05-E895-4750-8F69-B5FA5388B2A3}</Property>
				<Property Name="DistPart[9].productName" Type="Str">NI LabVIEW Runtime 2017</Property>
				<Property Name="DistPart[9].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[0].productName" Type="Str">NI LabVIEW Runtime 2017 Non-English Support.</Property>
				<Property Name="DistPart[9].SoftDep[0].upgradeCode" Type="Str">{182AE811-85B6-4238-B67E-F19497CC186B}</Property>
				<Property Name="DistPart[9].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[1].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[9].SoftDep[1].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[9].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[10].productName" Type="Str">NI mDNS Responder 14.0</Property>
				<Property Name="DistPart[9].SoftDep[10].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[9].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[11].productName" Type="Str">NI Deployment Framework 2017</Property>
				<Property Name="DistPart[9].SoftDep[11].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[9].SoftDep[12].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[12].productName" Type="Str">NI Error Reporting 2017</Property>
				<Property Name="DistPart[9].SoftDep[12].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[9].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[2].productName" Type="Str">Math Kernel Libraries</Property>
				<Property Name="DistPart[9].SoftDep[2].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[9].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[3].productName" Type="Str">NI Logos 5.9</Property>
				<Property Name="DistPart[9].SoftDep[3].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[9].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[4].productName" Type="Str">NI TDM Streaming 17.0</Property>
				<Property Name="DistPart[9].SoftDep[4].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[9].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[5].productName" Type="Str">NI LabVIEW Web Server 2017</Property>
				<Property Name="DistPart[9].SoftDep[5].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[9].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[6].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2017</Property>
				<Property Name="DistPart[9].SoftDep[6].upgradeCode" Type="Str">{4F261250-2C38-488D-A9EC-9D1EFCC24D4B}</Property>
				<Property Name="DistPart[9].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[7].productName" Type="Str">NI VC2008MSMs</Property>
				<Property Name="DistPart[9].SoftDep[7].upgradeCode" Type="Str">{FDA3F8BB-BAA9-45D7-8DC7-22E1F5C76315}</Property>
				<Property Name="DistPart[9].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[8].productName" Type="Str">NI VC2010MSMs</Property>
				<Property Name="DistPart[9].SoftDep[8].upgradeCode" Type="Str">{EFBA6F9E-F934-4BD7-AC51-60CCA480489C}</Property>
				<Property Name="DistPart[9].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[9].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[9].SoftDep[9].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[9].SoftDepCount" Type="Int">13</Property>
				<Property Name="DistPart[9].upgradeCode" Type="Str">{620DBAE1-B159-4204-8186-0813C8A6434C}</Property>
				<Property Name="DistPartCount" Type="Int">13</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/F/builds/TASCA/UTCS Installer</Property>
				<Property Name="INST_buildSpecName" Type="Str">UTCS Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="INST_productName" Type="Str">UTCS</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.4</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">17008005</Property>
				<Property Name="MSI_arpCompany" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="MSI_arpContact" Type="Str">H.Brand@gsi.de</Property>
				<Property Name="MSI_arpPhone" Type="Str">2123</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.gsi.de</Property>
				<Property Name="MSI_distID" Type="Str">{245059F5-1F54-44D5-BC1A-BD10F95D58A8}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_licenseID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{E85FEE45-D449-4A6F-BACF-C2E8A16214CF}</Property>
				<Property Name="MSI_windowMessage" Type="Str">You are about to install the UTCS executable, support files and additional NI packages. Some packages are published under EUPL v1.1.

Copyright 2017 GSI  Helmholtzzentrum für Schwerionenforschung GmbH
H.Brand@gsi.de, 2123, EEL, Planckstr. 1, 64291 Darmstadt, Germany
</Property>
				<Property Name="MSI_windowTitle" Type="Str">UTCS Installer</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">UTCS.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">UTCS</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">UTCS</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{CCC3E9BD-32A7-4FDC-8D9E-7D0DD8099685}</Property>
				<Property Name="Source[0].File[1].attributes" Type="Int">1</Property>
				<Property Name="Source[0].File[1].dest" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Source[0].File[1].name" Type="Str">UTCS.ini</Property>
				<Property Name="Source[0].File[1].tag" Type="Str">{D5B3B86F-9F9C-441E-BBE8-1F64BE17203A}</Property>
				<Property Name="Source[0].FileCount" Type="Int">2</Property>
				<Property Name="Source[0].name" Type="Str">UTCS App</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/UTCS App</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
