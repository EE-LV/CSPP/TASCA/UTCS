﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="CCSymbols" Type="Str">DSCGetVarList,PSP;WebpubLaunchBrowser,None;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project "UTCS.lvproj" is used to develop the Unified TASCA Control System Project and is based on LVOOP, NI Actor Framework and CS++.

Related documents and information
=================================
- README.txt
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/TASCA/UTCS
- Documentation:
  - Refer to Documantation Folder
  - Project-Wikis: https://wiki.gsi.de/foswiki/bin/view/Tasca/UTCSP, https://github.com/HB-GSI/CSPP/wiki
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview

Author: H.Brand@gsi.de

Copyright 2017  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="utf.calculate.project.code.coverage" Type="Bool">true</Property>
	<Property Name="utf.create.arraybrackets" Type="Str">[]</Property>
	<Property Name="utf.create.arraythreshold" Type="UInt">100</Property>
	<Property Name="utf.create.captureinputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.captureoutputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.codecoverage.flag" Type="Bool">false</Property>
	<Property Name="utf.create.codecoverage.value" Type="UInt">100</Property>
	<Property Name="utf.create.editor.flag" Type="Bool">false</Property>
	<Property Name="utf.create.editor.path" Type="Path"></Property>
	<Property Name="utf.create.nameseparator" Type="Str">/</Property>
	<Property Name="utf.create.precision" Type="UInt">6</Property>
	<Property Name="utf.create.repetitions" Type="UInt">1</Property>
	<Property Name="utf.create.testpath.flag" Type="Bool">false</Property>
	<Property Name="utf.create.testpath.path" Type="Path"></Property>
	<Property Name="utf.create.timeout.flag" Type="Bool">false</Property>
	<Property Name="utf.create.timeout.value" Type="UInt">0</Property>
	<Property Name="utf.create.type" Type="UInt">0</Property>
	<Property Name="utf.enable.RT.VI.server" Type="Bool">false</Property>
	<Property Name="utf.passwords" Type="Bin">'!#!!!!!!!)!%%!Q`````Q:4&gt;(*J&lt;G=!!":!1!!"`````Q!!#6"B=X.X&lt;X*E=Q!"!!%!!!!"!!!!#F652E&amp;-4&amp;.516)!!!!!</Property>
	<Property Name="utf.report.atml.create" Type="Bool">false</Property>
	<Property Name="utf.report.atml.path" Type="Path">ATML report.xml</Property>
	<Property Name="utf.report.atml.view" Type="Bool">false</Property>
	<Property Name="utf.report.details.errors" Type="Bool">false</Property>
	<Property Name="utf.report.details.failed" Type="Bool">false</Property>
	<Property Name="utf.report.details.passed" Type="Bool">false</Property>
	<Property Name="utf.report.errors" Type="Bool">true</Property>
	<Property Name="utf.report.failed" Type="Bool">true</Property>
	<Property Name="utf.report.html.create" Type="Bool">false</Property>
	<Property Name="utf.report.html.path" Type="Path">HTML report.html</Property>
	<Property Name="utf.report.html.view" Type="Bool">false</Property>
	<Property Name="utf.report.passed" Type="Bool">true</Property>
	<Property Name="utf.report.skipped" Type="Bool">true</Property>
	<Property Name="utf.report.sortby" Type="UInt">1</Property>
	<Property Name="utf.report.stylesheet.flag" Type="Bool">false</Property>
	<Property Name="utf.report.stylesheet.path" Type="Path"></Property>
	<Property Name="utf.report.summary" Type="Bool">true</Property>
	<Property Name="utf.report.txt.create" Type="Bool">false</Property>
	<Property Name="utf.report.txt.path" Type="Path">ASCII report.txt</Property>
	<Property Name="utf.report.txt.view" Type="Bool">false</Property>
	<Property Name="utf.run.changed.days" Type="UInt">1</Property>
	<Property Name="utf.run.changed.outdated" Type="Bool">false</Property>
	<Property Name="utf.run.changed.timestamp" Type="Bin">'!#!!!!!!!%!%%"5!!9*2'&amp;U:3^U;7VF!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	<Property Name="utf.run.days.flag" Type="Bool">false</Property>
	<Property Name="utf.run.includevicallers" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.overwrite" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.path" Type="Path">test execution log.txt</Property>
	<Property Name="utf.run.modified.last.run.flag" Type="Bool">true</Property>
	<Property Name="utf.run.priority.flag" Type="Bool">false</Property>
	<Property Name="utf.run.priority.value" Type="UInt">5</Property>
	<Property Name="utf.run.statusfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.statusfile.path" Type="Path">test status log.txt</Property>
	<Property Name="utf.run.timestamp.flag" Type="Bool">false</Property>
	<Property Name="varPersistentID:{0012D517-49CA-42F3-990D-E5E856243C40}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingInterval</Property>
	<Property Name="varPersistentID:{004ACC9C-E195-449C-BD83-8A44B8572B54}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-Rate</Property>
	<Property Name="varPersistentID:{008D165A-E535-4F2B-95E1-2523271063BE}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_DMA-IQ-TO</Property>
	<Property Name="varPersistentID:{00CA9C6C-35E7-4CF0-9A78-2590A584E55B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{00D343D6-2216-48E7-A4BE-3100A80F9CE3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_1</Property>
	<Property Name="varPersistentID:{0111C5E3-0F0E-40DF-B274-7F8888CDFE27}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ChannelMode_2</Property>
	<Property Name="varPersistentID:{012BDC21-3B87-40DA-9C4E-495A865C7043}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Error</Property>
	<Property Name="varPersistentID:{013476CE-FA67-4A70-AA09-B1135272F8E7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Reset</Property>
	<Property Name="varPersistentID:{0166C6A4-868A-47C7-9A6E-3D7429C7FC65}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL-Main</Property>
	<Property Name="varPersistentID:{0180523F-2D26-411E-9B13-F27C29AF0F8F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_2</Property>
	<Property Name="varPersistentID:{0183C5DE-D83C-4477-ACEA-1326EEC6CB45}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowRange_1</Property>
	<Property Name="varPersistentID:{02311B37-F10F-4E8D-B8C3-6DEF986B3FD3}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS Error Source</Property>
	<Property Name="varPersistentID:{02437531-492E-4650-AF8B-F91CE3D11FE8}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{02B12FE8-7826-4F5F-A886-956E819E2180}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Error</Property>
	<Property Name="varPersistentID:{0306AF84-1B67-40E2-A12E-BE27D3FD2ABA}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/SV-Lib Path</Property>
	<Property Name="varPersistentID:{035529A3-86B0-49B9-819C-C0CDC61922FE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FirmwareRevision</Property>
	<Property Name="varPersistentID:{0378EC7D-0ADB-4A17-AB2A-3FCF7C142093}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{03EB1326-61A7-4C37-807D-4F5EB73DF1B8}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period</Property>
	<Property Name="varPersistentID:{04283460-2B42-4D3F-B21A-838C0B7181A9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_3</Property>
	<Property Name="varPersistentID:{04329132-2F98-4212-A9C9-6C6FDD4484CD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Pyrometer-IL-Enabled</Property>
	<Property Name="varPersistentID:{04DE0355-E754-403A-AE02-1AA49C6605F1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_3</Property>
	<Property Name="varPersistentID:{0522FDF2-205B-493D-86AA-7464F95F9EB5}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2-Q</Property>
	<Property Name="varPersistentID:{0531FA6A-D1DC-4E5F-A5CD-28D711F21534}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_3</Property>
	<Property Name="varPersistentID:{05433937-0C96-484A-AA63-936F5C0147DC}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-TSRMin</Property>
	<Property Name="varPersistentID:{059E7741-4E6E-4B04-9F0C-B7158261B2AB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_0</Property>
	<Property Name="varPersistentID:{05F65684-7910-4D0E-A684-446B7D72E707}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_DeviceTemperature</Property>
	<Property Name="varPersistentID:{05FF032A-8C05-494B-B5D2-2307443FA796}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{06A35DA8-E153-4B8C-9697-AF3F99169046}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SetID</Property>
	<Property Name="varPersistentID:{06E2514E-3131-42E6-B623-4BA0256758A2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowRange_2</Property>
	<Property Name="varPersistentID:{076578A0-13C8-4CDE-A69E-5AE06276BF6B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{076FED91-2F3B-48E6-BF90-4BE49B3F8FBF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{078CD25A-DBCB-4601-8A34-8740FE842A75}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ErrorMessage</Property>
	<Property Name="varPersistentID:{07AA317E-6167-44A8-8C73-CB0E7D440A7B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChModes</Property>
	<Property Name="varPersistentID:{084BFA67-D603-48E7-8BBE-272C2E6762FA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF</Property>
	<Property Name="varPersistentID:{08719588-C80C-4686-9A6A-A1679716FC97}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_2</Property>
	<Property Name="varPersistentID:{087F2599-3002-4A36-B6D6-0FC7F2EA2C8B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingIterations</Property>
	<Property Name="varPersistentID:{08D8ACD5-24E6-4173-9C55-BEF929AEE157}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/MP-Period</Property>
	<Property Name="varPersistentID:{096AF365-2DAD-4744-809A-E2F24974CCC2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FirmwareRevision</Property>
	<Property Name="varPersistentID:{09A00C5F-97E0-4513-B8C5-988A0073AC21}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DT3.currinfo_0</Property>
	<Property Name="varPersistentID:{09A1EE3F-FBFC-48E9-9A8C-3FAEF3FC4F7F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV3</Property>
	<Property Name="varPersistentID:{09D0C4CC-1D8B-438F-ACA8-113C78B6F4EF}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Active</Property>
	<Property Name="varPersistentID:{09F9FE67-F758-4F61-B762-59C444A567AE}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_SelftestResultCode</Property>
	<Property Name="varPersistentID:{09FFB832-B5E5-41E7-B294-83C100847D1F}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Reset</Property>
	<Property Name="varPersistentID:{0A0287E1-A308-4FA5-9575-72368EE9E8DE}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Counting</Property>
	<Property Name="varPersistentID:{0A2D930B-5068-4665-8DDF-BA7158A504AD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_1</Property>
	<Property Name="varPersistentID:{0A3837A9-0006-4A17-8F64-63B96CFFE052}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_3</Property>
	<Property Name="varPersistentID:{0A6DFE1A-848E-4679-8F8B-2745B02317B9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SelfTest</Property>
	<Property Name="varPersistentID:{0A6E473C-1EA5-4422-89D1-036E16155FE6}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIVV1S.positi</Property>
	<Property Name="varPersistentID:{0A7804A9-BD06-4802-A52B-3E5949188ED8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_0</Property>
	<Property Name="varPersistentID:{0B0A8D47-AC51-46FC-AB2E-703A16F1E2E8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Reset-Interlock</Property>
	<Property Name="varPersistentID:{0B7083BB-1A92-4BB4-902D-C3CBD3E4AA05}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SecKey</Property>
	<Property Name="varPersistentID:{0B8FE54F-348E-4173-8D6D-6A3B80764716}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_2</Property>
	<Property Name="varPersistentID:{0B979A47-2C12-4069-A7EE-AB6B851D6771}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_2</Property>
	<Property Name="varPersistentID:{0BBE5CC4-BF8A-403A-B3B7-04BBB91E534D}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Virtual Accelerator</Property>
	<Property Name="varPersistentID:{0BD5DD24-1B73-4682-9F08-4BB309DFB064}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_2</Property>
	<Property Name="varPersistentID:{0C38871D-5A8F-457B-B384-92C7F675A4D3}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingDeltaT</Property>
	<Property Name="varPersistentID:{0C6F574A-595E-4484-95D4-161124EB840F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_0</Property>
	<Property Name="varPersistentID:{0CF685D7-7D79-4F4F-9CF3-2C7323B08C5A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF</Property>
	<Property Name="varPersistentID:{0D5479A6-5A77-4487-A8BF-F7BC2A6385AB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{0DC79D0A-19FD-4F0C-BFD6-6E2D0B39CD25}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD12.currents</Property>
	<Property Name="varPersistentID:{0E6B8872-F222-49A7-AE97-2C073290723A}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD11.currents</Property>
	<Property Name="varPersistentID:{0E6ED6DF-6012-4FC3-B63A-4C9B01423B03}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_2</Property>
	<Property Name="varPersistentID:{0EAD75AC-909D-493D-A0F2-3B7226A88617}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Start-1</Property>
	<Property Name="varPersistentID:{0EC9505B-3D4D-4807-889D-DF269236F8B8}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length</Property>
	<Property Name="varPersistentID:{0F8E846A-67CC-470B-89F9-D8137CD63545}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_FirmwareRevision</Property>
	<Property Name="varPersistentID:{0F997A6B-EFA4-4C60-A422-EBC28AB99CB7}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{0FC3CA60-D9DA-4E88-A354-18F4F92628E0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_2</Property>
	<Property Name="varPersistentID:{101D01B5-CEA4-4B75-815D-C7B08036D788}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8MU1.currents</Property>
	<Property Name="varPersistentID:{10205DD1-E71B-4EC3-AC2C-C6A8EB55CFFE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingCounter</Property>
	<Property Name="varPersistentID:{104B9FDD-80F6-4FAC-A9DC-CC3CCCA7FB93}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6.currinfo</Property>
	<Property Name="varPersistentID:{11427A73-E406-4104-B697-60BB679AAAA5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_2</Property>
	<Property Name="varPersistentID:{118A95DF-8C2F-470F-A910-36F048E4CDA7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_2</Property>
	<Property Name="varPersistentID:{11E5D07D-EECC-4AFE-9DC8-801B98CAF75F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_3</Property>
	<Property Name="varPersistentID:{123C9594-2478-41A9-8307-11A1B543C688}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_3</Property>
	<Property Name="varPersistentID:{1248E7A8-747E-459C-90C4-49AC27BD9162}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5-Proxy_Activate</Property>
	<Property Name="varPersistentID:{132E3D99-4E12-423E-98AA-C3E418D7ACFB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-MP-Max-Limit</Property>
	<Property Name="varPersistentID:{136AA6B9-5018-41AB-A165-0927480F026D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_3</Property>
	<Property Name="varPersistentID:{13F06CED-5E24-4C7C-8A3F-25E8DF21A989}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{145A3DBB-F065-4EB4-A994-5E481E607FC5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_2</Property>
	<Property Name="varPersistentID:{149E53BB-251A-4766-AD21-18E009E05504}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{14FA7F1A-2BB5-4DDD-82D1-3C0898255204}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Interval-Enabled</Property>
	<Property Name="varPersistentID:{1529D110-7E04-4E77-9767-40F3D76DC1E1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{15A1A602-A057-4424-92D9-62309D5125AD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SetID</Property>
	<Property Name="varPersistentID:{15AE7191-BB71-4FF5-BCB9-72E905D5C808}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP2</Property>
	<Property Name="varPersistentID:{15CE7C51-C88F-4338-8F4C-AC9FDC865353}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP3</Property>
	<Property Name="varPersistentID:{16C63A5F-14A6-4F5F-9D5B-45E7EA6EC604}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ErrorMessage</Property>
	<Property Name="varPersistentID:{16DE7B17-F6D0-4ED1-B66F-42D47F4AC0D6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Beam</Property>
	<Property Name="varPersistentID:{16E7F6EA-747A-4C12-A2F3-4DB9552467C5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_3</Property>
	<Property Name="varPersistentID:{16F0FD54-2A44-4816-8062-F31284E8FE9A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_3</Property>
	<Property Name="varPersistentID:{172CAC76-EC58-4FD7-A558-A08F1C074D22}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2_current</Property>
	<Property Name="varPersistentID:{174E50D8-7F4F-4171-93A2-496E6797844D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_1</Property>
	<Property Name="varPersistentID:{175C2FAE-4C38-4C57-A78F-FD7F7D0E2DB3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_range</Property>
	<Property Name="varPersistentID:{177FE1B3-C274-460A-A7F2-DC68A0B1F793}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pyrometer-IL-Enabled</Property>
	<Property Name="varPersistentID:{17FEB562-23AA-4922-8942-DFB1CEDB4841}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_RatTrap-IL-Enabled</Property>
	<Property Name="varPersistentID:{187C1E03-0ADD-41AC-9A25-C767594D5382}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_3</Property>
	<Property Name="varPersistentID:{188EA689-275E-4A36-A2AB-5F143C64D216}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingIterations</Property>
	<Property Name="varPersistentID:{188F0743-347A-47CF-B10C-3DA7B521C47A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ErrorStatus</Property>
	<Property Name="varPersistentID:{18A4BCD5-7038-4013-A5DE-521EF533C899}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_DriverRevision</Property>
	<Property Name="varPersistentID:{18EF9C0A-02FF-443C-9084-B9C83709A1B6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SelfTest</Property>
	<Property Name="varPersistentID:{1902F500-99F2-4DB6-B976-70CF0D3B21BD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{195183CF-3909-4AF5-B9BF-8B9ED7B6E620}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4.currinfo</Property>
	<Property Name="varPersistentID:{1994E6C2-E32B-4AD7-A068-B702D9FF456D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_1</Property>
	<Property Name="varPersistentID:{19B60B66-FE53-47F5-919D-803A28B14449}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_0</Property>
	<Property Name="varPersistentID:{1A2D5677-1957-49FB-A155-CB56B3B0953D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_dtCounter</Property>
	<Property Name="varPersistentID:{1A5AEE5E-9D5A-4C83-8C65-A9569C63D540}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{1A5D6E27-FAF2-4F6A-89EE-70015E234AEA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ZeroAdjustPressure</Property>
	<Property Name="varPersistentID:{1A889EC0-546B-4E76-993E-0C2DF64AB255}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_2</Property>
	<Property Name="varPersistentID:{1A929E0F-5061-4D69-ACE1-7775312C0EC6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PIDIntegral</Property>
	<Property Name="varPersistentID:{1B03B074-2C59-4B1D-A302-A73C1E3FC500}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_0</Property>
	<Property Name="varPersistentID:{1B13F317-6EF8-40FA-99C8-367B2E7186C9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_2</Property>
	<Property Name="varPersistentID:{1B1BB550-9E29-4637-98B5-CD6CAE11068F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_3</Property>
	<Property Name="varPersistentID:{1B779A44-7180-4492-9776-00CF920C2C27}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD12.currenti</Property>
	<Property Name="varPersistentID:{1C2DA609-1230-4A34-9E33-4DD58A4334C0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Counting-Enabled</Property>
	<Property Name="varPersistentID:{1C2E06FE-4A43-48B6-8D4F-BE6DEF631127}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC-Proxy_Activate</Property>
	<Property Name="varPersistentID:{1C41DF31-B9DC-4E8C-9FFF-7C1FC2F4D353}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_DriverRevision</Property>
	<Property Name="varPersistentID:{1D6D24DB-F490-45F0-8CA1-8EA53B264DC2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3_range</Property>
	<Property Name="varPersistentID:{1D84F047-C9A7-454C-9BDC-E88CEEE908AF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/MP-Length-IL-Enabled</Property>
	<Property Name="varPersistentID:{1DE0660B-7E52-4E65-93A7-86BD84B35A05}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Dipole-IMax</Property>
	<Property Name="varPersistentID:{1DFD7715-BA26-4143-8BB9-221DC975C1C6}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Dipole-IMax</Property>
	<Property Name="varPersistentID:{1E6ED094-A94C-4FA5-8F4C-D54B8DDFE964}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SetID</Property>
	<Property Name="varPersistentID:{1E823E64-B064-4161-B380-E5CAD7999346}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{1E880D46-9FCB-4FE9-8F07-3C1FBCE2E3EE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{1E90A27C-4C6D-44C3-A152-2CAED3FEC64F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{1E9F8398-D83F-4742-B157-894FE20C2D0F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ControllerMode</Property>
	<Property Name="varPersistentID:{1EBA8E26-5B3E-4DCF-83B5-C01F2D77053D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{1F2042A6-0016-45A6-8874-EECE18AC2E77}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Magnet-IL</Property>
	<Property Name="varPersistentID:{206B4647-45C8-49A1-82F1-10E4B79FAD77}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period-Max</Property>
	<Property Name="varPersistentID:{2088DA6F-24D2-4514-A9B9-C0465BC0A5C7}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP3_Status</Property>
	<Property Name="varPersistentID:{20CABAE7-6B1A-4021-BA94-B9C39910693B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_1</Property>
	<Property Name="varPersistentID:{20DE2CE8-9EAD-4147-9A9E-4D333E2227B3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_2</Property>
	<Property Name="varPersistentID:{215AB5DF-6F50-483E-B0B7-0BA1C9808F15}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_3</Property>
	<Property Name="varPersistentID:{227B5A0C-C074-4086-B59B-F55CA64BEC92}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-RatTrap-IL</Property>
	<Property Name="varPersistentID:{228D083B-3582-45DB-9FB0-80287FED2392}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{22A27DBF-7EAC-4733-9EF3-1FFADFC7BB43}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureUnit</Property>
	<Property Name="varPersistentID:{22A309A2-A927-4CCD-B776-4506B37CCE16}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2-Proxy_Activate</Property>
	<Property Name="varPersistentID:{22AE3BCC-B587-4F61-B3B4-56A9942D4576}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Charge-State</Property>
	<Property Name="varPersistentID:{22D4668B-B09B-42F6-81FD-14FE71596B81}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-MP-Length-IL</Property>
	<Property Name="varPersistentID:{232E6C3D-2216-4F49-ACA6-1972367F7659}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UXADT2-I</Property>
	<Property Name="varPersistentID:{234A7027-E30D-4C1A-981C-5591B5D9D198}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{23C69D28-52DE-4632-B01C-369E7E861586}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Logging</Property>
	<Property Name="varPersistentID:{241816A0-0DAE-4876-BB86-EA2D705939DC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingMode</Property>
	<Property Name="varPersistentID:{242F93C2-A0E0-420F-AE1D-7449E0D3E5E6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingIterations</Property>
	<Property Name="varPersistentID:{24A08D6D-8ABB-49AD-A2BD-17AA2829318C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_2</Property>
	<Property Name="varPersistentID:{24A6E949-31D6-41DB-8F75-F7FBDAA61957}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{24B77937-E313-4242-8AE6-EA41470D83BE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_A</Property>
	<Property Name="varPersistentID:{2513D5F5-EF4F-442F-9CF7-9CCBB6069250}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_2</Property>
	<Property Name="varPersistentID:{25E4C37F-681F-4B68-8354-7FEE1A0B96CB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{2618E5B2-4296-4A2F-AFE5-82989FEECF45}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_position</Property>
	<Property Name="varPersistentID:{26360796-DF8B-471B-A348-9D2D22E5FBD4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingDeltaT</Property>
	<Property Name="varPersistentID:{266079D7-2D90-4A64-BA10-3FC0F55DC3E0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Dipole-IL</Property>
	<Property Name="varPersistentID:{266D5487-44E9-4BD8-A9AE-B81ECC07FE3A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{26767990-38D3-4983-AF1C-3DF8330E0D3C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Dipole-IL-Enabled</Property>
	<Property Name="varPersistentID:{26CEDBE9-5840-47E3-9E2C-55C8E2FFD650}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_VacuumBurst</Property>
	<Property Name="varPersistentID:{2751F0F9-AEF2-4F4A-B7D9-D56C1B2793A2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{275E51EB-0B32-4AB4-9933-7FAF2B7FDE21}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{277C57A3-BB44-4257-9F81-A0818E370CEE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureMode</Property>
	<Property Name="varPersistentID:{28112410-C07A-4959-8B84-BE55BEFAF3BE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_3</Property>
	<Property Name="varPersistentID:{28418009-91B4-4481-A6DB-E4DE5ED25A09}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ChannelMode_0</Property>
	<Property Name="varPersistentID:{2897909C-090D-4E85-92D8-0DFBD50BA227}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF</Property>
	<Property Name="varPersistentID:{28E1A294-4F07-41D9-BC59-25F8C239456D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{29DC9B4D-6121-49A5-98EF-7B50F90B1885}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_T-BeamOff</Property>
	<Property Name="varPersistentID:{2A3520FF-2B8D-4F76-AE05-30B5CFC13147}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/SecKey</Property>
	<Property Name="varPersistentID:{2AAFB56D-2E56-4A88-B293-4B5AA4E3B278}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_2</Property>
	<Property Name="varPersistentID:{2B157E60-B0F4-4B6C-9341-07150D3E511F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SetID</Property>
	<Property Name="varPersistentID:{2C2F2B13-76BC-42B5-9379-235BD50A5946}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Error Message</Property>
	<Property Name="varPersistentID:{2C72BD87-27DC-4AAD-A3AB-ECA6C1E4E70D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_0</Property>
	<Property Name="varPersistentID:{2CEB095D-3225-47A4-BEBD-71BB8C0C566A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_2</Property>
	<Property Name="varPersistentID:{2CEB1B1A-AC4C-41F5-A550-E49E8277C451}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXIDC6_position</Property>
	<Property Name="varPersistentID:{2D5527CC-1A93-46AD-A238-536F6EE013A6}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD12_current</Property>
	<Property Name="varPersistentID:{2D745095-07C7-4F10-861F-8BB9AC732C67}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_0</Property>
	<Property Name="varPersistentID:{2D8EA861-1DE0-4C62-A497-A2A829A52609}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{2DA47572-9BCD-4A9D-9429-F468A686B846}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{2DC723E5-3B91-4D7B-9625-CB780B6B0FA9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{2DFB75BA-1BDC-4419-A4EF-6468D7E20DBB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{2E3A9426-887E-43C3-8B78-8EA1E4DD9E8B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{2EB1FFFB-82F7-4DE3-88FC-9A0CB6E6972D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ErrorMessage</Property>
	<Property Name="varPersistentID:{2EE66EC1-FA55-452E-8903-B32E51A7B20E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_DriverRevision</Property>
	<Property Name="varPersistentID:{2F22364F-AD16-4879-BC9F-0C0AF1BAEB93}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_DMA-IQ-Enabled</Property>
	<Property Name="varPersistentID:{2F40B4D0-5C35-424B-B202-C095564555EF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_1</Property>
	<Property Name="varPersistentID:{2F757444-EA2D-4FF3-9C6A-A2801B1494B6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-RatTrap-IL</Property>
	<Property Name="varPersistentID:{2F75CBF8-26A3-45DD-9473-E15E7D0D5BD3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_UnderrangeControl</Property>
	<Property Name="varPersistentID:{2FEB9049-9DFC-498C-87F6-A1C9BB55362E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Interlock-Reason</Property>
	<Property Name="varPersistentID:{2FFC9E8E-A577-4D89-B18D-5AB086758A9F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_SelfTest</Property>
	<Property Name="varPersistentID:{30953174-BAF7-4685-9FC6-77A185736CE4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingIterations</Property>
	<Property Name="varPersistentID:{30D07277-7BB3-437C-861E-AC3DCA00EAEF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_2</Property>
	<Property Name="varPersistentID:{30F5CD71-E0E5-4F7F-9E93-4E6FA71A32CF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV5</Property>
	<Property Name="varPersistentID:{3103B388-E312-4128-B2A3-BDDA1382834B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_1</Property>
	<Property Name="varPersistentID:{311D53A1-0710-43D4-81C1-5C623AC3915B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SetID</Property>
	<Property Name="varPersistentID:{319F6B75-BB3A-44DD-A08F-E023E9234553}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Max-QMP-Limit</Property>
	<Property Name="varPersistentID:{31A4C7DC-9ADF-4EE8-BFF8-5A0A5CF0CE11}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{323231DD-89A0-41E3-84F1-CBBBA4BCD235}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Charge-IL</Property>
	<Property Name="varPersistentID:{32383D4A-3AD9-4FFC-9B46-FC776F5848BB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_1</Property>
	<Property Name="varPersistentID:{323D30CB-47EC-4771-84D9-A84A8B312DF8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_1</Property>
	<Property Name="varPersistentID:{326BE2E9-81B5-4D02-843F-6E439D86840D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Max-QMP-Limit</Property>
	<Property Name="varPersistentID:{32D81367-98C1-45EA-9B47-9F5E38A503B8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_3</Property>
	<Property Name="varPersistentID:{32F6B36F-98D1-4994-A0B6-6E09C03E3F27}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_1</Property>
	<Property Name="varPersistentID:{341A7A4D-66B9-4688-9999-C7F538CC32E9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_B</Property>
	<Property Name="varPersistentID:{344C0FB0-6AF6-443C-906C-88A8AB642D7A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ChannelMode_0</Property>
	<Property Name="varPersistentID:{3480BCF0-8D08-4411-BAB6-81ED21E2FF04}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_3</Property>
	<Property Name="varPersistentID:{34B039BE-9CA6-447C-B880-559D3DC484CF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{34BE98D2-8323-4C71-9829-9274A5D03BCE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{34C05351-E776-4288-90CC-59914AB2B626}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{3581E2BF-1564-47AC-AD96-B81F1D35412D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_3</Property>
	<Property Name="varPersistentID:{35820349-0758-482C-BE38-AEE2286045A9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingMode</Property>
	<Property Name="varPersistentID:{359D6CFA-B5DC-4E64-A1D1-BD7D7CF9E3AB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_range</Property>
	<Property Name="varPersistentID:{35AD3259-FC38-43CD-BBCF-E8D27F77E2FC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SetID</Property>
	<Property Name="varPersistentID:{361E08AA-D071-4877-9021-D226B173E4D9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{3672E402-F9D8-44FF-AB88-E4D39CE41FDC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingCounter</Property>
	<Property Name="varPersistentID:{369DC77B-FB7E-4E5E-83E2-C3BAF1034166}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Reset</Property>
	<Property Name="varPersistentID:{37048A9A-B533-4BAB-8A3D-1D4B82A731C2}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/String Size</Property>
	<Property Name="varPersistentID:{37456E81-647D-441C-B399-0FDEDD6E3218}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{3782F03B-DFCA-4FBD-94B7-F72A71EC0E87}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_1</Property>
	<Property Name="varPersistentID:{378E0B3A-B45D-49A9-8173-C4973DB25767}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_3</Property>
	<Property Name="varPersistentID:{379E0D80-FFA6-4C98-9726-C1287C456841}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_1</Property>
	<Property Name="varPersistentID:{37C242CF-967A-4C2A-951C-E322ED5E2937}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_2</Property>
	<Property Name="varPersistentID:{387628E9-F4B5-4FD8-9430-E21050574390}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingTime</Property>
	<Property Name="varPersistentID:{387F1C23-E8DB-490B-9F5B-C0A9641642E2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FirmwareRevision</Property>
	<Property Name="varPersistentID:{38FFF20F-2416-4371-8F64-FC7CCB21EBC7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_2</Property>
	<Property Name="varPersistentID:{3906D797-06F4-4D12-B268-B98B39DE2F4F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_0</Property>
	<Property Name="varPersistentID:{39C792E7-A3C8-4FE0-B9CB-52011FDDAF58}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{39E4D37B-DE32-4EFF-B279-794768EFD50D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS</Property>
	<Property Name="varPersistentID:{39F7B050-504D-493A-B373-FC645C9D3339}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ChargeState</Property>
	<Property Name="varPersistentID:{3A0DCE92-23F8-4465-A3A0-7FDA62759A0B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ErrorMessage</Property>
	<Property Name="varPersistentID:{3A226FF1-A39E-4AC3-8B17-B0399111F0D3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_0</Property>
	<Property Name="varPersistentID:{3A960953-241B-47B9-80D7-C6712F4A815C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffsets</Property>
	<Property Name="varPersistentID:{3AB8BE18-D503-44CF-8CD4-2229E9BC9F1B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{3AB9DA61-DC7F-40BC-AE92-FC459C1849C5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_3</Property>
	<Property Name="varPersistentID:{3B03C756-3070-4C1A-A233-BC48B82C337F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{3B5B31B7-B339-42E3-85C2-D1D4A92CFCAB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingInterval</Property>
	<Property Name="varPersistentID:{3B926107-3C90-4BCC-8B7F-BE38EF102A3E}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-ClearMaxTime</Property>
	<Property Name="varPersistentID:{3BD86ADC-ED65-4395-9198-8D1C2D3FA275}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_3</Property>
	<Property Name="varPersistentID:{3BDD50C3-2827-4025-8E6D-4C789E939FC5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ControllerMode</Property>
	<Property Name="varPersistentID:{3C3103CE-C568-4B57-A80F-78780B219BA4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS</Property>
	<Property Name="varPersistentID:{3C6078CA-C565-4031-A2B8-A93C3BF91B9F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{3C666255-0699-4BE3-A20D-75138577F9F3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ErrorCode</Property>
	<Property Name="varPersistentID:{3CD320A7-56A4-4CA1-8A8C-67B6449EF59D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{3CFBA496-59AD-4298-9A2C-F9DC9F1ACF54}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingIterations</Property>
	<Property Name="varPersistentID:{3D408742-12EC-489E-B0B8-F7182F122F82}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Reset</Property>
	<Property Name="varPersistentID:{3D4A7623-E6DC-4C17-A605-CD6EC8738D83}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_1</Property>
	<Property Name="varPersistentID:{3D779C96-5A9D-4C2C-8F34-9221BE4A9A2E}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{3DAC2C1A-BD4C-481B-97EA-CF4A746BAC11}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXADT2.currinfo_0</Property>
	<Property Name="varPersistentID:{3DC5D27A-4C0F-4DD0-B1D4-BB707AC7AF06}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4_P.posits</Property>
	<Property Name="varPersistentID:{3E1EC226-34D6-4A7E-9F5E-A7AB7EC08B4C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingDeltaT</Property>
	<Property Name="varPersistentID:{3E5EF56E-C0E3-4A4E-8046-D170103973F9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-MP-Length-IL-Max</Property>
	<Property Name="varPersistentID:{3E6688E6-4CD8-4AE6-A4A7-4BA791AE3AEB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD11_current_setpoint</Property>
	<Property Name="varPersistentID:{3E9BC354-FA0A-41EE-82FA-3706BC0D3D98}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_0</Property>
	<Property Name="varPersistentID:{3EF4C42B-9AB8-4871-A5F4-35420AC524D8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{3F2FEDEC-536A-4AC9-8127-A9807F894C8C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_3</Property>
	<Property Name="varPersistentID:{3F558A8F-37A1-47A1-B084-04D8081FC04D}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4.currinfo_0</Property>
	<Property Name="varPersistentID:{3F92F711-993C-4D3C-B01C-B4E5CBB5C608}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{3FC9E14C-F828-4EE4-9E0E-F3AB29181B1A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_current</Property>
	<Property Name="varPersistentID:{40065960-1EBB-45B8-AEA9-B3692E02DC1E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{402E2B99-CC81-4E2F-81AD-E77BD4D8CF73}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_3</Property>
	<Property Name="varPersistentID:{4054E165-4E85-4132-A6E3-84F8C6AC2590}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{4074DD50-4C84-4193-A41D-C5D378FE49C1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ChannelMode_3</Property>
	<Property Name="varPersistentID:{40870411-4FC1-46A8-A934-A4DB5AF978A0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4-Proxy_Activate</Property>
	<Property Name="varPersistentID:{40911ADA-4F6A-43A6-94ED-E730D4DF9C01}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_0</Property>
	<Property Name="varPersistentID:{409AF84E-DE62-4DDD-A915-E1B2042D9F15}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV2_Status</Property>
	<Property Name="varPersistentID:{40F4C1A8-7E66-4DEF-8DC0-0E98EF15CBDD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_2</Property>
	<Property Name="varPersistentID:{41283491-8DF1-40F2-85AB-60258833F3BF}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Pyrometer-IL</Property>
	<Property Name="varPersistentID:{41641542-6249-44FA-9D1F-39F407F4EA6B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ControllerMode</Property>
	<Property Name="varPersistentID:{419D6C68-04A3-4BC9-AE73-0A2C78A86A4C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DipoleCurrent</Property>
	<Property Name="varPersistentID:{41AF0030-2EE5-4296-B6E2-17B24C8288A5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SetID</Property>
	<Property Name="varPersistentID:{41BB30D2-C377-42EC-98E7-5F34118E14D3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{43776265-AF33-411F-94FC-8CA285CF4E6A}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2.currinfo_0</Property>
	<Property Name="varPersistentID:{4408AAEB-EEFB-448E-9793-9A74EDE30C0C}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140Proxy_Activate</Property>
	<Property Name="varPersistentID:{44146C74-4F4A-44E3-B192-10FAC1A6289A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingDeltaT</Property>
	<Property Name="varPersistentID:{46125F6C-052E-4FDB-8B00-5AC2596C47FE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingDeltaT</Property>
	<Property Name="varPersistentID:{46961C59-3DC5-4B20-9783-7E821161E96F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_1</Property>
	<Property Name="varPersistentID:{46EA811B-204F-4CE4-8F74-03433614FD37}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_2</Property>
	<Property Name="varPersistentID:{470B1E60-A98F-4121-9BEB-A29E1EFC5FF3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_current</Property>
	<Property Name="varPersistentID:{4710DE2E-9945-4D8B-85F4-CD86BF719A9A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_0</Property>
	<Property Name="varPersistentID:{478C76EE-786E-4305-ABA5-CCE1EAC4B9DB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{479508A5-8E10-4254-B85E-6D63198B2B38}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Dipole-IMin</Property>
	<Property Name="varPersistentID:{47E12485-28B9-4AED-B116-59E41D4BBCBF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Pyrometer-IL</Property>
	<Property Name="varPersistentID:{47E1B8FC-6873-4322-8A05-077E929B7000}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP2_Status</Property>
	<Property Name="varPersistentID:{481E28BA-4368-4256-AE8E-5F7182843F85}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{4825F563-3AE8-406D-B2CD-0A37AF2CF94E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/SecValvesBypass</Property>
	<Property Name="varPersistentID:{485894DC-370F-4F0D-AFF1-6805F593EC40}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Max-QMP-Limit</Property>
	<Property Name="varPersistentID:{491C3ED3-A7D1-46CB-AD6B-68ED6DB0D9B0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Magnet-IL</Property>
	<Property Name="varPersistentID:{492DFE24-EA67-4F0B-A811-CD41A4FA8573}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV1</Property>
	<Property Name="varPersistentID:{4931413D-86C2-4D7E-BDFE-3B28EFC2835B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{49499955-200A-42BB-9ACB-001D5DA3C3EB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_1</Property>
	<Property Name="varPersistentID:{49A0EFFF-91EE-4375-9C78-1C0CEEF6F77B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Error</Property>
	<Property Name="varPersistentID:{49A49DF3-F321-4F8D-A268-AD13EA1803E1}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6.currinfo_1</Property>
	<Property Name="varPersistentID:{49B6BD33-FCD4-402D-8889-1E4DB8809FDC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SelfTest</Property>
	<Property Name="varPersistentID:{49DAB477-D800-42FD-A487-2EAA99B10D71}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UXADT2-Q</Property>
	<Property Name="varPersistentID:{49DD5C4C-CB26-4B3D-9DB0-A4B32D755DA2}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Targetwheel-IL</Property>
	<Property Name="varPersistentID:{4A07A699-3F41-4760-9E64-7E253843754D}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingInterval</Property>
	<Property Name="varPersistentID:{4ACC23CD-C69D-431A-A78C-BD54F8BA356F}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Chopper-In</Property>
	<Property Name="varPersistentID:{4B9B3C07-766C-49BD-95CE-5F558822BEA2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SelftestResultCode</Property>
	<Property Name="varPersistentID:{4BAC0BCE-166A-457D-B371-BBD6DE845959}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Range</Property>
	<Property Name="varPersistentID:{4C096D76-2C5F-48D7-B4F2-1686089908C1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_1</Property>
	<Property Name="varPersistentID:{4C586D3E-42C1-4051-8A49-E4E0A625ED77}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Chopper-Out</Property>
	<Property Name="varPersistentID:{4C89BCA2-6031-4E7C-AFB4-15960DC3ED22}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SelftestResultCode</Property>
	<Property Name="varPersistentID:{4C8E3974-A7FF-4597-ADF6-985F5B794650}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-TO</Property>
	<Property Name="varPersistentID:{4D1CB129-B1D0-48DC-818D-E52F283941BA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureUnit</Property>
	<Property Name="varPersistentID:{4D1CEB26-B7B8-46AC-B3BA-41A188ACC526}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{4D30B408-EE97-4834-87C1-DA5B55232C73}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{4D3EA93C-F421-441D-836C-CA0296799E0D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SetID</Property>
	<Property Name="varPersistentID:{4D755B83-E71D-4973-B02E-08A0B4CC72C8}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ClearMaxTime</Property>
	<Property Name="varPersistentID:{4DC85F74-69E1-41B9-AFAA-DD7F0B57EADE}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Temperature</Property>
	<Property Name="varPersistentID:{4E33608C-7F8F-425C-9B2B-26D62F1A33AF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Targetwheel-IL</Property>
	<Property Name="varPersistentID:{4E36C434-FE5A-49B6-9E64-ADF20B581393}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-IL-Min</Property>
	<Property Name="varPersistentID:{4E38032C-C2CA-454F-B268-DE583E77E140}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_SelftestResultCode</Property>
	<Property Name="varPersistentID:{4E4E6196-3B08-45F4-892C-41199E452A3D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_DriverRevision</Property>
	<Property Name="varPersistentID:{4E5F0136-3C8C-4610-A4E1-499C1822DF71}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4_P.positi</Property>
	<Property Name="varPersistentID:{4E6FD8D9-6EC1-4587-BBF5-5913863A80DD}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP3</Property>
	<Property Name="varPersistentID:{4E907F84-6636-431F-896B-B1A817641B77}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ChannelMode_2</Property>
	<Property Name="varPersistentID:{4F120434-F83C-4E20-AEAC-8C241650AB05}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{4F4F5B86-0781-4ECA-8BC4-6A312EEA9CD0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_2</Property>
	<Property Name="varPersistentID:{4F5C196A-4864-4C4D-B0CB-423E1B53E8E6}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-MP-Length-IL-Min</Property>
	<Property Name="varPersistentID:{4F654605-F084-462D-BC41-07D9101ED891}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_2</Property>
	<Property Name="varPersistentID:{502877A5-4DD6-40B7-B148-125E4EB8FDF9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{50669BEC-36E1-4CC6-9787-FE73E4C00EC9}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS Error Message</Property>
	<Property Name="varPersistentID:{5091DC87-26F1-4C4A-8CE4-A3854684D861}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_3</Property>
	<Property Name="varPersistentID:{50DCC946-FA2A-4B41-A1AD-7CE9B4D0E124}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV4</Property>
	<Property Name="varPersistentID:{5101E6A2-1FB4-4440-ACDD-08B96FC89869}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_2</Property>
	<Property Name="varPersistentID:{517E2AF4-5FD9-49D5-85CD-AC1EACAAFE0F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_3</Property>
	<Property Name="varPersistentID:{51A0CFBC-AD65-400A-B405-CB5F7B596A76}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS</Property>
	<Property Name="varPersistentID:{51C079F9-4CFE-4807-BF1B-A0099E9C1540}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowRange_0</Property>
	<Property Name="varPersistentID:{51D74D53-C81D-4889-A42C-6FAC6179EA67}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingTime</Property>
	<Property Name="varPersistentID:{5261493F-2E03-4BB7-92CB-7DD3ED0620BD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_0</Property>
	<Property Name="varPersistentID:{52698245-C772-41E6-807B-D32F63E0B062}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VacuumBurst</Property>
	<Property Name="varPersistentID:{526B7B41-818E-4CED-9965-1E01D663FA5E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Decay-Delay-2</Property>
	<Property Name="varPersistentID:{52EDD6A6-2CD9-403B-A882-7116D3043382}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2_P.positi</Property>
	<Property Name="varPersistentID:{52EFA97F-0E40-48EB-8D78-6DD3B2BADC06}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DMA_Enabled</Property>
	<Property Name="varPersistentID:{52EFAD68-B447-46C2-AD82-84940A890150}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP2_Status</Property>
	<Property Name="varPersistentID:{53C757B3-0712-45E9-AC52-D420246D0097}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/BMIL-WDAlarm</Property>
	<Property Name="varPersistentID:{546288CB-F275-4711-A063-C7F0386759B8}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IMax</Property>
	<Property Name="varPersistentID:{546EEAFB-E74C-48E0-B20E-E9C5A557DC75}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_0</Property>
	<Property Name="varPersistentID:{549EE34D-FA28-4B55-8E25-7A61EB91DA76}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{54B20317-D236-4E9B-BB7F-C2B5F003797B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP3_Status</Property>
	<Property Name="varPersistentID:{551DDB92-D9EB-426E-9D46-B09B1C0A99DF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PIDLead</Property>
	<Property Name="varPersistentID:{559E7914-0434-4935-9793-F1AD97A0A09C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_0</Property>
	<Property Name="varPersistentID:{55BE76B8-2C0C-4280-A140-5EFBD86723CE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_1</Property>
	<Property Name="varPersistentID:{55F6A93A-E59E-4915-AC95-5A655E90DAFB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_1</Property>
	<Property Name="varPersistentID:{56BACBC2-F77D-48B9-AF7B-85CD1DD6D2BB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_1</Property>
	<Property Name="varPersistentID:{56E6E2EE-43DD-4017-A84B-A8E037B895C8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FirmwareRevision</Property>
	<Property Name="varPersistentID:{56F05A2E-5E81-4337-A3D7-EFE2E7D416CF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{57211D3D-D262-472E-BDE9-11B303D3721D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{576D2AB0-D175-414F-B297-4BC13A2500FF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_2</Property>
	<Property Name="varPersistentID:{57A5EF59-DFD7-4438-8097-E02D4F0873F2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SetID</Property>
	<Property Name="varPersistentID:{583AFC50-BF0D-44B7-97FD-B806A437EB08}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ZeroAdjustMFC</Property>
	<Property Name="varPersistentID:{58C495FF-F130-4F4A-883A-ED4366F74F74}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_1</Property>
	<Property Name="varPersistentID:{58D3EE8E-A818-4ABA-AB14-E59EE1C35AB0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Magnet-IL</Property>
	<Property Name="varPersistentID:{59332C76-F7D6-43A2-B4EF-9EEC2F628B7A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_3</Property>
	<Property Name="varPersistentID:{594EBD00-E4A1-4678-AEC6-BF1DC0D32AE8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_1</Property>
	<Property Name="varPersistentID:{59E108B7-97B7-4D5B-B106-B7EE7224F105}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{59F967B6-8681-41ED-A168-E620922E57F0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_2</Property>
	<Property Name="varPersistentID:{5A095DD2-F379-4CBC-B1C1-3EC9A5C822DE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PIDGain</Property>
	<Property Name="varPersistentID:{5A5A56A1-08E9-438C-B70C-1881530F2B1E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_RatTrap</Property>
	<Property Name="varPersistentID:{5A6E8271-B909-4843-8DA2-047CDE6B4BAF}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Period</Property>
	<Property Name="varPersistentID:{5A9081F5-E5AD-484A-95ED-A8FEFF001540}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS Error Code</Property>
	<Property Name="varPersistentID:{5A9247C8-39EC-419C-8196-FBF2773F4A17}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{5AC6142F-9366-4E71-B5B4-B3C277A5C42A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Magnet-IL-Enabled</Property>
	<Property Name="varPersistentID:{5B364891-54EB-4D1D-BF75-B7D4247222EF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Targetwheel-IL-Enabled</Property>
	<Property Name="varPersistentID:{5B64365D-E4EC-4D04-BCC3-B1E87B2681EB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_1</Property>
	<Property Name="varPersistentID:{5B7C999B-AA94-4DE7-9303-EF96AAF6F0DB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{5BE00C5D-AF19-4074-83F0-4C73D271546E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_-1</Property>
	<Property Name="varPersistentID:{5C11AB4B-B215-4C9D-8D94-78A320C7C207}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{5C42A6DC-0539-49E4-96E1-8462EB5E1A5F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXIDC6_current</Property>
	<Property Name="varPersistentID:{5CFCC4AA-3FD5-4FA7-B296-70708E5DDCA7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ErrorCode</Property>
	<Property Name="varPersistentID:{5D69E3DF-524E-40E5-8232-5B7B9EC74296}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_1</Property>
	<Property Name="varPersistentID:{5E18A15C-E8E6-4C2F-B1D5-EB8844BC3D66}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_0</Property>
	<Property Name="varPersistentID:{5E784432-60B2-4723-B232-B32033A1274C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingIterations</Property>
	<Property Name="varPersistentID:{5E81D1D7-4117-4A4D-B049-F0244D17C80D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingMode</Property>
	<Property Name="varPersistentID:{5ECFC258-6DD5-4C09-BD69-5247C83F0B41}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMILProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{5F28D5A9-58B5-49D6-92C8-50373B796206}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3-Q</Property>
	<Property Name="varPersistentID:{5F6B3350-9A73-4C8B-9A14-F4BA80D794BB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Delay-1</Property>
	<Property Name="varPersistentID:{5FA2CE5D-9C56-4126-AF88-A7DFAC8B4052}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_B</Property>
	<Property Name="varPersistentID:{5FBD4981-EBB8-432C-8515-1FD012B45043}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_0</Property>
	<Property Name="varPersistentID:{5FD2132E-C89A-48DA-9FA3-25FA31437A26}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Reset</Property>
	<Property Name="varPersistentID:{5FFEEB3E-8589-4FB7-A32F-5674CCEE56BE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-MP-Length-IL</Property>
	<Property Name="varPersistentID:{60354BBF-33EB-4D85-A851-9535F239C352}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_0</Property>
	<Property Name="varPersistentID:{60E61933-D9C7-49ED-AB65-76ABBA23C23D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-2-WDAlarm</Property>
	<Property Name="varPersistentID:{6148712E-E668-4E4F-8EB2-ECA1296F3A3B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{6159BC7B-089F-40F1-927C-50F95144F2C8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_UnderrangeControl</Property>
	<Property Name="varPersistentID:{6196F80F-BFFD-422B-9206-99E069D2F0CD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{61A307D2-D1D1-4281-8785-6BEBAB1BAE7D}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingCounter</Property>
	<Property Name="varPersistentID:{61C19091-BC7D-4CAA-84E3-FA3917B4E483}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{61F58BC5-6741-4B59-A56C-8C1AB13685D5}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ExpNumber</Property>
	<Property Name="varPersistentID:{62746352-99CB-4CA5-8324-1F1475655927}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Trafo-Frequency</Property>
	<Property Name="varPersistentID:{6289C75E-1F6F-4F07-AA9A-A5F8EBBD62DC}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-I</Property>
	<Property Name="varPersistentID:{633EB90A-473C-4627-AC4B-BEB6ABAC0A9B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{635D571C-63EB-4521-8FF6-A02DFBEBCF88}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_1</Property>
	<Property Name="varPersistentID:{638CE681-AC2B-4F73-A538-E45081EF924E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_1</Property>
	<Property Name="varPersistentID:{6398769C-0294-41C2-9FD5-0CD9F72ECE55}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV3_Status</Property>
	<Property Name="varPersistentID:{639890B3-9266-4A53-9A73-85B09063C405}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingTime</Property>
	<Property Name="varPersistentID:{64181170-8239-4DD8-9256-ED6806DFCED6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{64691758-DD14-4420-A3BD-817B6DD1D6C5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{648F505A-2823-4E10-954D-81CD0EA09CB4}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UT2DCX_current</Property>
	<Property Name="varPersistentID:{64C45646-DB63-4706-9B7A-D5328570F7E9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3_current</Property>
	<Property Name="varPersistentID:{64DEF7B5-749A-43E4-BC3A-D26DE3D93F56}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowStatus</Property>
	<Property Name="varPersistentID:{651E5599-28B0-4B90-9FD6-08D70F0A27BD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureMode</Property>
	<Property Name="varPersistentID:{66D39B39-E06E-414F-B843-CC7EFAC205A9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Decay-Delay-1</Property>
	<Property Name="varPersistentID:{67029A21-B0C4-4EF8-8781-5AF33C12E00A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingCounter</Property>
	<Property Name="varPersistentID:{673A7215-6CC1-4AA3-BD6E-C8F65C187D01}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_B</Property>
	<Property Name="varPersistentID:{67CBAF71-7018-4745-A5AC-F18802CC50D1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_2</Property>
	<Property Name="varPersistentID:{682457B6-8E9A-48D2-B003-C686EF597AD9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{68502AF9-5B47-49B6-A9A2-4B4BD8CA97DD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1-Proxy_Activate</Property>
	<Property Name="varPersistentID:{68572599-47CF-4922-8465-F935DECB56C0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{68D3EE44-539A-4572-BCC0-E5D28082DEE1}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_HW-Reset</Property>
	<Property Name="varPersistentID:{68ED3972-43A5-42EC-8CBB-F20D49D5851D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/MP-Length</Property>
	<Property Name="varPersistentID:{693BD4A8-3E8C-46AF-9CDF-043CBFE183EB}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2.currinfo</Property>
	<Property Name="varPersistentID:{69ADDA9D-EB73-4CD9-978E-0343B6F9C548}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/SecValvesState</Property>
	<Property Name="varPersistentID:{69F27054-BEDD-40B7-9224-5556EE9D3365}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ErrorStatus</Property>
	<Property Name="varPersistentID:{6A5CA634-734C-4987-AFBA-6411F16226FD}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ErrorCode</Property>
	<Property Name="varPersistentID:{6A5F1C48-9688-4398-92BD-420F1138F0D8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingCounter</Property>
	<Property Name="varPersistentID:{6A91482E-C641-4E53-B65B-00001EB0A489}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SecValvesByPass</Property>
	<Property Name="varPersistentID:{6AE0B099-8475-42F3-AB71-BE877FAA3FE5}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Trafo-Generate</Property>
	<Property Name="varPersistentID:{6B51F9C7-0596-4904-9F12-84147B12E714}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{6BA2FC94-747C-4C77-B868-C8ABC252E29F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SelfTest</Property>
	<Property Name="varPersistentID:{6BE86D80-961E-412B-B8EF-65689207D9E3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Logging_Enabled</Property>
	<Property Name="varPersistentID:{6BE878C3-515B-4E6D-9F3B-1194A264C841}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{6BE9FB4B-8330-48D3-B301-5C43A6DE1FC9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_2</Property>
	<Property Name="varPersistentID:{6BED0FD5-1A83-41CF-A719-28DFD1B8B762}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{6C49C043-5C7C-43B1-9758-27C149F10641}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/RatTrap</Property>
	<Property Name="varPersistentID:{6CEE10F6-B40F-4B10-B8D6-0410A4E9C0CE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{6CF30C4C-5F09-468A-9807-B4E3F1862AB2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_DriverRevision</Property>
	<Property Name="varPersistentID:{6CFE26CB-AEB0-4D92-8567-5A15D67073E6}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DT3.currinfo_1</Property>
	<Property Name="varPersistentID:{6D08A20A-3793-48B9-BCB2-EE29E0291DD9}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXADT2.currinfo</Property>
	<Property Name="varPersistentID:{6D21302B-DD30-4268-BF18-946CD44E9997}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-4-WDAlarm</Property>
	<Property Name="varPersistentID:{6E22D094-7E9D-426A-B0F6-093F835147DA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Reset</Property>
	<Property Name="varPersistentID:{6E284848-3FA3-4B9A-95B2-500A9398E2C9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{6E7AB485-6A23-4670-B6AA-1D00ADD7A7F5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_B</Property>
	<Property Name="varPersistentID:{6EBF613B-9A8C-4ADF-9433-D45F077CF50A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowRange_2</Property>
	<Property Name="varPersistentID:{6F047CAC-FD95-45E8-90AA-0E1DD0001047}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Interval</Property>
	<Property Name="varPersistentID:{6F52466A-236D-413D-95D2-3C271E559C03}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_0</Property>
	<Property Name="varPersistentID:{6F5E29AE-99C6-499A-B7E7-C7B7949D1507}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_AmbientTemperature</Property>
	<Property Name="varPersistentID:{6FBFC067-5428-4B14-A3B3-CC4D44CF6E05}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingMode</Property>
	<Property Name="varPersistentID:{6FC207F1-B98C-44F2-B50D-6676C73D8062}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ResourceName</Property>
	<Property Name="varPersistentID:{6FD49255-6AE1-4352-B53A-A7DA271B2CDA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{6FFEFEC2-E063-447A-AC91-98279A4F849D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_0</Property>
	<Property Name="varPersistentID:{70166AAF-B1B0-43AC-AC2A-D0FC1454E116}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{707F1BEC-E137-4822-8B15-F0E1EC12943D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Det-Rate-Limit-IL</Property>
	<Property Name="varPersistentID:{70F4BF4D-A802-470A-8CB7-4884B27E70C4}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8MU1.currenti</Property>
	<Property Name="varPersistentID:{70F84B27-3166-4DCE-9A67-448F40C90800}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_2</Property>
	<Property Name="varPersistentID:{71963AB4-1B53-4991-9E30-722F4792C60E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{71A78678-240E-40EF-BED2-FCE1F901EE76}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ErrorCode</Property>
	<Property Name="varPersistentID:{71CDC1F8-ED59-4CCE-8DB0-FF05115CC6D3}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-Interlock</Property>
	<Property Name="varPersistentID:{720BE7BB-32CE-4BCD-8FEF-9E82989DDBAF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Projectile</Property>
	<Property Name="varPersistentID:{7223FAA3-B40A-4585-92A1-AB5F8459BA87}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_1</Property>
	<Property Name="varPersistentID:{727BD3F6-0D34-4340-96AA-B06A63C9256A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{7392FF2F-699A-4B16-BC61-F2A998428DF0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_3</Property>
	<Property Name="varPersistentID:{73D24274-5452-4F4B-BE45-9ACE4897CF8D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP4</Property>
	<Property Name="varPersistentID:{745E6C30-8B28-423B-AF22-DB5EDBC90D4F}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-I</Property>
	<Property Name="varPersistentID:{748831B3-F174-4E26-BDB5-562BFC5EC0A3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{74BA9900-0EC8-4E7F-9564-BD65D471540F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_0</Property>
	<Property Name="varPersistentID:{74D877A7-57CB-46F1-B50C-421EEE7E7B44}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_0</Property>
	<Property Name="varPersistentID:{754C0185-D127-4E1A-80B6-9F1205B1BCDC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_1</Property>
	<Property Name="varPersistentID:{75960060-C716-4A6A-A479-D08834B0305B}" Type="Ref">/My Computer/instr.lib/IGA140.lvlib/App/Temperature</Property>
	<Property Name="varPersistentID:{75E30BC3-7F56-4D68-B6E0-CD3406AB2D32}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-AmbientTemperature</Property>
	<Property Name="varPersistentID:{75ED0805-2246-425B-BC1E-60B3FD37B418}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_DriverRevision</Property>
	<Property Name="varPersistentID:{76F021A1-FA2E-4D35-B0C4-FB4068ACF1DE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowStatus</Property>
	<Property Name="varPersistentID:{77EB1F09-DC46-4FAD-9D41-7CA6635A06B3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_2</Property>
	<Property Name="varPersistentID:{7855EB6D-99C4-4313-90BB-B9311A1A5FB7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{78764EC1-80F2-49AB-B1B1-73A779A60DE4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ErrorMessage</Property>
	<Property Name="varPersistentID:{78822173-7EDE-48B9-8A7B-6BA7FBE69C45}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ErrorCode</Property>
	<Property Name="varPersistentID:{78EFD606-E753-485D-B4A3-C8F1B41FC7FF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_2</Property>
	<Property Name="varPersistentID:{79423A91-B433-48BC-B22A-2488C11F3B4B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingMode</Property>
	<Property Name="varPersistentID:{79434964-C11D-4785-831D-89B2E86A5D31}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_2</Property>
	<Property Name="varPersistentID:{797BA214-6AEA-4F8F-A3D5-E32A282C1834}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_T-Integrate</Property>
	<Property Name="varPersistentID:{79B406A9-BCCB-4BFF-8C80-15BE7FDD3552}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingCounter</Property>
	<Property Name="varPersistentID:{7A3BADE4-9B52-47EE-AED2-9117281AF73F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{7A45D7BE-7F3E-416B-933F-4785A06C6A40}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Error</Property>
	<Property Name="varPersistentID:{7A5EE1C5-125E-40C1-BCAB-C0050F92B497}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_3</Property>
	<Property Name="varPersistentID:{7B37BBCB-9D0B-4706-A36F-EF0709667E36}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_3</Property>
	<Property Name="varPersistentID:{7B556986-0BF4-4E53-A6D6-419C6C14F626}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{7BD5CC24-7E7B-43DD-83B6-B5DF4894DFD0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Length</Property>
	<Property Name="varPersistentID:{7C2756C0-6A22-4DB5-9F52-F50B267ACB0E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_3</Property>
	<Property Name="varPersistentID:{7CCF3321-F64C-4E5A-8969-C81E4FF2085F}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS State</Property>
	<Property Name="varPersistentID:{7CF8B021-30F0-43C0-888F-EB60C6559E55}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{7CFFBD52-19A9-47EF-B87F-BF14AE61AC77}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripModes</Property>
	<Property Name="varPersistentID:{7D262BCC-059C-46FC-919C-C838973F05A6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Max-DetEvents-Limit</Property>
	<Property Name="varPersistentID:{7D97FDC9-86FE-40D9-B2AE-FB8D990413CD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_2</Property>
	<Property Name="varPersistentID:{7D9D0BD6-3045-484A-927E-A3E473AC504D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{7DF23428-CE28-492A-8203-A0BC4614FBC9}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4.currinfo_1</Property>
	<Property Name="varPersistentID:{7E4CA64E-2FBF-4986-A8B3-6DB7C3CC57BC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_3</Property>
	<Property Name="varPersistentID:{7E8B3786-C804-4D72-8556-B5F3AC99E043}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_0</Property>
	<Property Name="varPersistentID:{7F28ED00-BE56-41C3-A606-844F758B7394}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_A</Property>
	<Property Name="varPersistentID:{7FEB02B5-8FE8-4E7B-A60D-51B89FF75351}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_3</Property>
	<Property Name="varPersistentID:{7FF633C4-DDA6-4F16-80A0-95E55DD48235}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Dipole-IMin</Property>
	<Property Name="varPersistentID:{80B14C96-C39A-429B-9CA1-8DAAF8C298D9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{80B405E3-4210-4066-BB44-69084C943361}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{80BF1064-7E2F-48DC-81AC-1433D30947DC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{80C6DD74-2727-422D-AA58-249A215451FB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingCounter</Property>
	<Property Name="varPersistentID:{80E36194-3836-44F3-8A9B-6738E3F05FC0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_0</Property>
	<Property Name="varPersistentID:{80EBC8F0-7DD9-4872-828B-AA534811C828}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{810175F2-7753-42FD-B3C5-883762CB0E27}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripModes</Property>
	<Property Name="varPersistentID:{81069949-3850-483F-8F44-1E8B25BABFC6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ZeroAdjustPressure</Property>
	<Property Name="varPersistentID:{8114427E-20F5-403B-9787-75A5E63F5E27}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP6_Status</Property>
	<Property Name="varPersistentID:{8152A337-92D1-495D-955F-CDE699440A1B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_3</Property>
	<Property Name="varPersistentID:{815C0786-4C46-43ED-95F7-0E4173FE6895}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_3</Property>
	<Property Name="varPersistentID:{8193694A-9A41-4AA4-BB01-49F5C55A074F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Dipole-IL</Property>
	<Property Name="varPersistentID:{819B8173-41BB-4FDA-BCEF-0F1A76DB1C66}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_0</Property>
	<Property Name="varPersistentID:{81AA3D49-E81B-48F5-8E13-A076135ADCB0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_3</Property>
	<Property Name="varPersistentID:{81BB1512-FD6F-4696-8B8B-A991D9954856}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{8242CBA6-3412-4CA5-AC44-D8C7C81B3B6E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMILProxy_Activate</Property>
	<Property Name="varPersistentID:{82648CF5-809D-4246-A5C0-43112D296360}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Inhibit</Property>
	<Property Name="varPersistentID:{8278F8B1-B727-403D-814A-9C599F5D33DB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_2</Property>
	<Property Name="varPersistentID:{82F18960-C95A-4BA2-94CA-1090FB254516}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/RatTrap-IL-Enabled</Property>
	<Property Name="varPersistentID:{8318F94C-8516-4544-BB37-A329806D6E05}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_2</Property>
	<Property Name="varPersistentID:{831E8D18-D6E5-497B-B52D-0D7404A709BA}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3-I</Property>
	<Property Name="varPersistentID:{833DA055-971D-461F-B71A-124B5CBE6182}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_0</Property>
	<Property Name="varPersistentID:{834E8120-1599-4338-AD8B-8904A91693C2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Charge-IL-Enabled</Property>
	<Property Name="varPersistentID:{836ACC5F-C03D-4482-8A51-CE4B3B99415D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Length-IL-Max</Property>
	<Property Name="varPersistentID:{83DD22B4-44AC-4F92-851F-2021B4CDB5F4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_UnderrangeControl</Property>
	<Property Name="varPersistentID:{84196E15-488D-4D47-A470-BF3ACB7358B8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PressureUnit</Property>
	<Property Name="varPersistentID:{8476D1D0-330A-4B6F-A08C-856A2B31FEB0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{84B5159E-5F48-4B91-8225-69D905384004}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_B</Property>
	<Property Name="varPersistentID:{850D648E-039A-4274-98AD-626FC4D4B169}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{854B9C20-5F43-47A3-95DF-7B607363F04A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ErrorCode</Property>
	<Property Name="varPersistentID:{856586B0-796E-4BC2-90B6-7B69314E6A5B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_1</Property>
	<Property Name="varPersistentID:{862F14AD-58BD-419F-82B9-155F4484079D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_3</Property>
	<Property Name="varPersistentID:{875965A6-126A-4E13-B69C-4F5E94EE14E3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureOffset</Property>
	<Property Name="varPersistentID:{878AE749-694A-4238-8046-A579047E1073}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-DMA-IQ-Timeout</Property>
	<Property Name="varPersistentID:{87D98FF2-1C17-48D0-83A1-9EE3E59F2643}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow3_TASCAGC</Property>
	<Property Name="varPersistentID:{8811A3EC-D2F3-45FA-82CE-EFEDF4E6686C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Delay-2</Property>
	<Property Name="varPersistentID:{8814C6E2-AE2E-42FB-BFFD-84161C4C7817}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Log All Device Errors</Property>
	<Property Name="varPersistentID:{883827D5-A923-4C9F-BC6C-7EE3AA5D46AA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_3</Property>
	<Property Name="varPersistentID:{88738C1F-136D-429F-AB26-787DAF2978DE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FirmwareRevision</Property>
	<Property Name="varPersistentID:{88750632-6672-4DE1-9438-90F0ED7BA880}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX.currinfo_0</Property>
	<Property Name="varPersistentID:{88AD486A-6CF0-4333-ACA8-F95D09FC366B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Counting-Reset</Property>
	<Property Name="varPersistentID:{88F72D8E-6DAD-4BE0-9C9C-7BB43A057C31}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SetID</Property>
	<Property Name="varPersistentID:{8961E6E4-5C17-4FDF-B22C-731F5BBF56B4}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-TSRMax</Property>
	<Property Name="varPersistentID:{8978FB2A-55F0-4391-9F93-E82216D66EDA}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_TSRMax</Property>
	<Property Name="varPersistentID:{89C5293C-B3EC-4CDE-961A-0F5A3CC00FBF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PressureMode</Property>
	<Property Name="varPersistentID:{89CFD731-6BF7-43F9-987E-ECD8451746BD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-MP-Extrema</Property>
	<Property Name="varPersistentID:{8A239E13-ACA9-46D4-A818-F3D057685C05}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Error</Property>
	<Property Name="varPersistentID:{8A6E1605-946F-492F-B3A7-5F20546E5B84}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{8A7B9697-63BB-4C72-96D9-3DF6CE076A72}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Trafo-Frequency</Property>
	<Property Name="varPersistentID:{8AF0EB90-296A-4B5A-850A-41CD8365253D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_3</Property>
	<Property Name="varPersistentID:{8B072DF6-B063-4258-B733-1F1404CD2ED4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_1</Property>
	<Property Name="varPersistentID:{8B2E2BAF-6802-4D10-85C4-1D62B9051A1C}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingTime</Property>
	<Property Name="varPersistentID:{8BA50C11-D45D-4259-A9E9-FCB275B5AFB5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_0</Property>
	<Property Name="varPersistentID:{8C60725E-B524-45ED-B2C5-52BBADF178B2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_2</Property>
	<Property Name="varPersistentID:{8CB0409A-2AA6-4502-8AF4-3747A79A058A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_0</Property>
	<Property Name="varPersistentID:{8CB6A298-027D-4EC6-B801-4930C17FECEC}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-Min</Property>
	<Property Name="varPersistentID:{8CE993A9-B8E6-42AC-9293-1C6469A6D0FF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_TASCAGC</Property>
	<Property Name="varPersistentID:{8D5E926E-15B2-45DB-A8D6-B7A56BE66CB6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Reset</Property>
	<Property Name="varPersistentID:{8DA7C832-83B6-48EE-A7E6-8ED63FDDBA61}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXIDC6_range</Property>
	<Property Name="varPersistentID:{8E083EC8-DB81-4001-B258-D07B23B0F8C9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_1</Property>
	<Property Name="varPersistentID:{8E5240E6-8F6A-422B-B32E-FA69262E2D9E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PIDIntergral</Property>
	<Property Name="varPersistentID:{8EBBA5D8-D11B-4A94-B592-86042B93223E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{8EC92F26-EA1C-476A-9506-603959AF672A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_1</Property>
	<Property Name="varPersistentID:{8EE4FDD0-77A0-4751-A594-1B2ACF222B63}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL-Main</Property>
	<Property Name="varPersistentID:{8F2198A7-A0F9-48A6-AABB-1EFE1B25FEC4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_1</Property>
	<Property Name="varPersistentID:{8F974F1F-ACC1-4068-A1B1-2FEC15041D5A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_0</Property>
	<Property Name="varPersistentID:{8FF26A81-62CF-4990-BCD5-F52E13899CC5}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-Counts</Property>
	<Property Name="varPersistentID:{9002E401-619B-4CA2-8299-B0895A4FF841}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_1</Property>
	<Property Name="varPersistentID:{900B40B3-5CC5-4DB4-A6B5-C50A801CC3CF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_0</Property>
	<Property Name="varPersistentID:{90539147-1E94-4C5C-AB31-5D3ECBDD6546}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IL</Property>
	<Property Name="varPersistentID:{90AD1FE7-671D-47F5-A3A3-B4FD0974837F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Reset</Property>
	<Property Name="varPersistentID:{910C9D71-540B-45A3-B9AA-468C978BC159}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_0</Property>
	<Property Name="varPersistentID:{91494363-9D89-47D1-982A-EC1E34397C82}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_0</Property>
	<Property Name="varPersistentID:{91612ED6-9C85-4656-A8C6-4D914F7447AC}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Magnet-IL</Property>
	<Property Name="varPersistentID:{9182028F-CD4D-4765-B3F6-7DAAFD7D3B2E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{91B918A1-EC3D-480C-9752-55A9B710A72E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_2</Property>
	<Property Name="varPersistentID:{91ECC29D-0B1B-4811-88D3-CAF3136AF6D1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_A</Property>
	<Property Name="varPersistentID:{926D1E0E-45B4-461E-8DEE-B9148BCE6941}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PIDGain</Property>
	<Property Name="varPersistentID:{9302755B-9573-4D42-98A5-767671F13B0D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{93CE0386-757C-4FC8-B517-033762C5A155}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL-Main</Property>
	<Property Name="varPersistentID:{93D149E8-FF84-4408-85ED-533324861033}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ChannelMode_1</Property>
	<Property Name="varPersistentID:{93D8F130-6785-4A12-863B-4703CDBD46BE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DetectorRate</Property>
	<Property Name="varPersistentID:{93FF1BF7-854F-4512-8FEB-9F0A836E1554}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_3</Property>
	<Property Name="varPersistentID:{940A1B50-1514-4A72-A445-D747332BD273}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_2</Property>
	<Property Name="varPersistentID:{94635914-C5C5-44CA-9BDF-E117C0C2FE1A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{94692E95-3256-4B64-A105-4273C381C4C4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{95006DDC-824D-4707-B3AC-D58E3BB60B62}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ErrorCode</Property>
	<Property Name="varPersistentID:{956F72D5-177E-40BB-B4BF-AEF10A2133CE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_3</Property>
	<Property Name="varPersistentID:{95803CC7-CE5E-4D65-8AB3-3BF3FF191793}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD11_current</Property>
	<Property Name="varPersistentID:{95E79039-5217-49FF-A768-D296F92D088E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Target</Property>
	<Property Name="varPersistentID:{96379395-D462-4351-8BC8-39AE9385B22D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_3</Property>
	<Property Name="varPersistentID:{9652240D-30D3-4E39-AC58-149D15E665B4}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2_P.posits</Property>
	<Property Name="varPersistentID:{967F3386-AE3E-4382-9CCC-B91BEB3B3A03}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{970BB845-2B0F-4C6A-8F0C-DC7904492FF2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_1</Property>
	<Property Name="varPersistentID:{97E2A464-F4A6-474E-807C-AD0E006DF21A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{9822B443-D905-4F53-807E-B5571113D689}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowRange_3</Property>
	<Property Name="varPersistentID:{9907FCB9-4D86-4215-A3D1-FE1BA862FD2D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_1</Property>
	<Property Name="varPersistentID:{997A1D0D-BA74-4977-BAC7-A7F661D8143B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{9A53F076-1EB9-48EF-806B-5EF64AF01F01}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SW-Watchdog</Property>
	<Property Name="varPersistentID:{9A98E889-93A4-48D4-ABAD-05DAD3912F9D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PIDLead</Property>
	<Property Name="varPersistentID:{9AAD9ACB-6C42-4F8A-B477-52A351EFD769}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ChannelMode_3</Property>
	<Property Name="varPersistentID:{9AE716F0-9332-4B28-86C7-23A645EEADFA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_0</Property>
	<Property Name="varPersistentID:{9B2F6936-3E68-4580-864A-966B4D54B2E0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PressureUnit</Property>
	<Property Name="varPersistentID:{9B602137-B88C-4B1E-8F44-451AA8A84E1B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Max-DetEvents-Limit</Property>
	<Property Name="varPersistentID:{9B801AF8-1669-4C7D-A09E-D39A45C18DEB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-5-WDAlarm</Property>
	<Property Name="varPersistentID:{9BB721F6-AC45-4DA5-BF5C-494678339480}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SelfTest</Property>
	<Property Name="varPersistentID:{9C130C8E-9E83-4099-8332-3017A1731ACA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_0</Property>
	<Property Name="varPersistentID:{9C636238-BAA7-4AAC-83DB-198F3B6EC0E1}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pyrometer-IL</Property>
	<Property Name="varPersistentID:{9C812349-B6B0-4272-9683-45831D92A0B7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ErrorStatus</Property>
	<Property Name="varPersistentID:{9CD02E4E-9517-4F5F-BCE8-9003A2AA748A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Dipole-IL</Property>
	<Property Name="varPersistentID:{9CD9777A-7E2B-4AA1-930A-8389880D45B9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_1</Property>
	<Property Name="varPersistentID:{9D78956D-6F0B-4824-A431-804204073074}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2_range</Property>
	<Property Name="varPersistentID:{9D89C68A-9653-4A09-8B39-AAA209A6EDAD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ErrorStatus</Property>
	<Property Name="varPersistentID:{9DAEA1EB-1A22-4988-9ABB-D69FCE6164DF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_3</Property>
	<Property Name="varPersistentID:{9E18D87C-86F5-41A4-8E95-6377A407CCA6}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ResourceName</Property>
	<Property Name="varPersistentID:{9E9FA864-7DA9-429D-96A8-44686332C3DD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_2</Property>
	<Property Name="varPersistentID:{9EA7275F-645E-4C8C-826F-0E06EDAA4712}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_2</Property>
	<Property Name="varPersistentID:{9EE208E4-5861-461A-85D3-D933866F4A7D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{9F18C906-5A56-485A-9755-C68248683D21}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{9F43DD71-66AD-4F1A-84D6-DBB830084BE7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ResourceName</Property>
	<Property Name="varPersistentID:{9F8E8BFD-9D76-4B97-8C07-D9718C25F02E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV6</Property>
	<Property Name="varPersistentID:{9FA495CE-26CE-44F2-B5BA-8F1FC412F812}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_2</Property>
	<Property Name="varPersistentID:{9FBE4B14-077E-4E99-92E0-3B1C20428894}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{9FEED76A-2AC5-4606-8F63-C0155E80EFCB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_0</Property>
	<Property Name="varPersistentID:{9FF2F053-FA41-4BB8-8B37-4919BC437530}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD11.currenti</Property>
	<Property Name="varPersistentID:{A023262E-DA1B-4C28-984B-EB363823A35D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Targetwheel-IL</Property>
	<Property Name="varPersistentID:{A1130B2C-85C9-4DCD-BF22-FB1BA25B3C81}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UT2DCX_range</Property>
	<Property Name="varPersistentID:{A134DC2F-5539-4C8D-BAB6-5CA64B061AEA}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DetectorCounts</Property>
	<Property Name="varPersistentID:{A13740C9-726F-4820-83BE-5BAB6B755B64}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX_P.positi</Property>
	<Property Name="varPersistentID:{A13D5AA8-5073-4BBD-8432-DE9265A0038F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_3</Property>
	<Property Name="varPersistentID:{A14E2FC7-5475-4D5F-89D3-791146F27387}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXFVV1S.positi</Property>
	<Property Name="varPersistentID:{A1646924-56E6-4E09-BA48-0997CA278181}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingInterval</Property>
	<Property Name="varPersistentID:{A1A48ADE-587E-490F-A2FD-63DF070B19EC}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8MU1_current</Property>
	<Property Name="varPersistentID:{A2059240-C002-4FBF-A175-83D8A5C836CD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{A208812A-9BFE-40E1-AC8C-92D97C6C2423}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP5_Status</Property>
	<Property Name="varPersistentID:{A227A5C5-035F-4DE2-BDD7-943F13B0014C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MainValve</Property>
	<Property Name="varPersistentID:{A22C4C1D-65AA-40D6-A7B1-AFB30AE5367E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{A2437880-9A54-4E97-9D53-492714C153FA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingDeltaT</Property>
	<Property Name="varPersistentID:{A247A8E6-0A1E-4499-814F-548C43340547}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_1</Property>
	<Property Name="varPersistentID:{A363F9D7-37A9-49E8-9329-3EA03FACD947}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_1</Property>
	<Property Name="varPersistentID:{A38EB8CD-5C47-430B-A690-1843C63EE7C9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingTime</Property>
	<Property Name="varPersistentID:{A39B5282-E01E-4FC1-B561-092549336074}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS</Property>
	<Property Name="varPersistentID:{A40EB589-622B-47DE-8771-431B1013B646}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Length-IL-Min</Property>
	<Property Name="varPersistentID:{A53DAA25-8412-48A6-80DF-B22B82463D8A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/InterlockStatus</Property>
	<Property Name="varPersistentID:{A55AD4E6-3A84-45AF-AF81-351BBD9A437C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_0</Property>
	<Property Name="varPersistentID:{A570B54B-B520-4356-93E2-2E22B813BA91}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SelftestResultCode</Property>
	<Property Name="varPersistentID:{A5B923CA-2994-40F8-B24B-372D3FD5252B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF</Property>
	<Property Name="varPersistentID:{A6121137-4CD2-4001-A1DB-71F1A7AF966B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{A6270DD9-E7EA-4901-8431-611E7E0AEC26}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_3</Property>
	<Property Name="varPersistentID:{A674BFCC-A31A-4CCD-B5A3-7A559CD76913}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingMode</Property>
	<Property Name="varPersistentID:{A6B6892F-A37B-4032-90A2-18A4EFF01B75}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-Reset</Property>
	<Property Name="varPersistentID:{A6F0248B-2345-4E0C-B3F0-EC90A76C3FDD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{A7080ECE-7AFB-444C-9BF3-776EE2B3076C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{A70E93DD-63D1-4875-98CD-91B33A3164D5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_A</Property>
	<Property Name="varPersistentID:{A711729E-08DF-468D-A8CF-0E987ABA6EF9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_0</Property>
	<Property Name="varPersistentID:{A726947C-BDD2-4FB0-83A7-23351FA287DA}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-Emissivity</Property>
	<Property Name="varPersistentID:{A74268A3-58FC-4E1E-8F6E-1865AA7EBB6C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-IL-Max</Property>
	<Property Name="varPersistentID:{A78D5CAB-A2E7-4D89-850F-12F26440D13C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{A7C74D25-D5D6-4012-97D7-3C603295E5AB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Target-Temperature</Property>
	<Property Name="varPersistentID:{A98A138B-0AE4-4E46-A946-FFB5D747E869}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_0</Property>
	<Property Name="varPersistentID:{A99E5B10-2B36-4649-8B1D-4A1683B88ED5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowRange_3</Property>
	<Property Name="varPersistentID:{A9E5D102-B502-47A9-A66C-FAC45B01EC83}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MainValve</Property>
	<Property Name="varPersistentID:{AA206CE8-3AFA-4945-829C-3772B6DC139B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/RTCGC-WDAlarm</Property>
	<Property Name="varPersistentID:{AA4313EB-9F40-4ABA-A639-D80E0C8B525C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_1</Property>
	<Property Name="varPersistentID:{AA436457-EBE6-493B-9664-E33EFC9541B6}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP5_Status</Property>
	<Property Name="varPersistentID:{AA53FF3B-8429-437A-87AB-F06845B92B6A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-QMP</Property>
	<Property Name="varPersistentID:{AAB9BCAE-BE6D-4390-856B-66F8F5395F1E}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Reset</Property>
	<Property Name="varPersistentID:{AAC2BF2D-EE8D-4CFA-B271-05F9E801235A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_set-position</Property>
	<Property Name="varPersistentID:{AB2AFD26-8849-44FB-B8C3-960847DD8236}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{AB6CF36C-8B0D-4842-8E2E-3D30028BC1C3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS</Property>
	<Property Name="varPersistentID:{AB8C0172-C86B-4525-84A0-6FD8C07DAC80}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ResourceName</Property>
	<Property Name="varPersistentID:{AB90CB1D-F2B2-44A3-933F-E53B8A027F88}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period-Min</Property>
	<Property Name="varPersistentID:{ABD0A7BC-D9BE-4FCF-ACCC-31A6B052E8BF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingIterations</Property>
	<Property Name="varPersistentID:{ABE14B83-5E64-4E7A-8518-2A3964E30633}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_2</Property>
	<Property Name="varPersistentID:{ABE4B92E-30F8-4FD6-947C-3BDB059FB019}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-ExposureTime</Property>
	<Property Name="varPersistentID:{ABF8AD30-91B7-4878-9407-515EF54256C3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_1</Property>
	<Property Name="varPersistentID:{AC45B51E-8323-44ED-BEEF-3A4DC3C28A56}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_1</Property>
	<Property Name="varPersistentID:{AC759D58-97EA-4937-ABC8-BC17D9899C54}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{AD25D5EC-A9F7-4035-A89B-E8310392AC44}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffsets</Property>
	<Property Name="varPersistentID:{AD40BC4F-5D6D-4D67-BD10-1D3A4584D4BB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_3</Property>
	<Property Name="varPersistentID:{ADAF9027-D544-482E-9D3B-3F0B9AD476FD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureSP</Property>
	<Property Name="varPersistentID:{ADF9CFE9-6E27-4637-8893-58143B2DB16E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_1</Property>
	<Property Name="varPersistentID:{AE78ED03-5ACA-4147-9976-85293775EF97}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-DMA-IQ</Property>
	<Property Name="varPersistentID:{AE970958-F116-4635-8B57-B56EEF6EADC1}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Debug</Property>
	<Property Name="varPersistentID:{AEC16F88-1F88-45E4-AB64-A8F6DB76C0AF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ErrorStatus</Property>
	<Property Name="varPersistentID:{AFB5328E-4BBC-4960-A5DF-67466E484961}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8MU1_current_setpoint</Property>
	<Property Name="varPersistentID:{AFCDEA01-8F6E-4382-8985-4E3A44DE0F2A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{B0D0EFFC-3F37-4F50-BC13-65C255241995}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC-Proxy_Activate</Property>
	<Property Name="varPersistentID:{B1C7EB27-257A-41CD-91D4-62FAD7893DDC}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowRange_0</Property>
	<Property Name="varPersistentID:{B1F5FD13-FA4E-476C-84F8-77F75C492ECC}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_0</Property>
	<Property Name="varPersistentID:{B223815D-C647-4891-9670-EB8C37B5897B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{B23F5C1E-B0A9-4430-92DB-BE93C059F080}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_0</Property>
	<Property Name="varPersistentID:{B26933BD-194C-48DB-A3B0-939855A1AFC6}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{B275D838-B35E-4F05-BCD5-D605CADA19B9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{B2884F87-9C5C-4029-B765-2D5C8ADF4113}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PIDLead</Property>
	<Property Name="varPersistentID:{B2EE41F0-A51A-411D-B9D4-65D52539D997}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_0</Property>
	<Property Name="varPersistentID:{B309227B-FC8C-47C8-BCB1-7134648BC1AE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingInterval</Property>
	<Property Name="varPersistentID:{B31941CA-4C4B-4941-8151-2C8DA5353C67}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_3</Property>
	<Property Name="varPersistentID:{B3C32347-97AD-48FE-A58A-2055E1641CA6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_2</Property>
	<Property Name="varPersistentID:{B41ED689-6ED8-47FB-8BAF-B9FD152B0F9E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV1_Status</Property>
	<Property Name="varPersistentID:{B491A95D-12F7-47F3-8174-3AE89660CA1C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IL-Enabled</Property>
	<Property Name="varPersistentID:{B4AA9AD6-F457-4367-9C9C-AA05048FAE34}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureOffset</Property>
	<Property Name="varPersistentID:{B4B6CD4F-8E7E-42A5-84A5-84DF2E9A7132}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_0</Property>
	<Property Name="varPersistentID:{B4CDAB75-6328-4F60-B9D1-44367D46C5A7}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-Max</Property>
	<Property Name="varPersistentID:{B50A1A9C-FC07-4F93-8646-8F49BC1055B8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow1_TASCAGC</Property>
	<Property Name="varPersistentID:{B58B01FD-0246-4AA9-A4FE-903D7D9AC45A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_3</Property>
	<Property Name="varPersistentID:{B5A79013-A944-4DA4-BD6E-761091895DD8}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Error</Property>
	<Property Name="varPersistentID:{B622CD8C-0DBF-48DB-8D5C-FF27E1E2413C}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_DriverRevision</Property>
	<Property Name="varPersistentID:{B670E074-DC70-429B-BAAB-FC122DEE1FC1}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Charge-State</Property>
	<Property Name="varPersistentID:{B699E5C5-099D-4AA6-B4B5-34CD9211A5AC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{B6F11236-2707-4B54-85EA-D66D92D8C662}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_0</Property>
	<Property Name="varPersistentID:{B6F846A2-B0A1-46A6-9495-3D0657A3F125}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{B76C8802-089E-4268-97FC-2E4DB1BB55A0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Time-BeamOn</Property>
	<Property Name="varPersistentID:{B7C1E81A-A5AF-40C6-87A9-50B221236E8A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ErrorStatus</Property>
	<Property Name="varPersistentID:{B7F78ED7-E7AC-462D-9524-A37490D2E301}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Counting_Enabled</Property>
	<Property Name="varPersistentID:{B80A76B6-3821-414E-B74C-7C2F910176DE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2-I</Property>
	<Property Name="varPersistentID:{B8266F62-6B09-4FB3-991A-754DA80373B1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_2</Property>
	<Property Name="varPersistentID:{B869C73F-6EBD-460A-A396-824289BEC150}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PIDGain</Property>
	<Property Name="varPersistentID:{B89E9E0A-C682-4288-B1AC-5F14C6577FC0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_3</Property>
	<Property Name="varPersistentID:{B8B3FF01-ADBD-4359-93B6-98AE83344C61}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_SelfTest</Property>
	<Property Name="varPersistentID:{B8C5F02B-3C37-48A6-83C8-87096A54E676}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_3</Property>
	<Property Name="varPersistentID:{B8F7A95D-9F68-4BB2-8267-0960BA4E8846}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_0</Property>
	<Property Name="varPersistentID:{B9047D3B-7949-4BDD-8ACA-30F41E1A8F61}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Error</Property>
	<Property Name="varPersistentID:{B916D070-B4D5-4B69-A7CC-B21DE506E5F3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ErrorStatus</Property>
	<Property Name="varPersistentID:{B97D8F52-CB2B-450C-9FBA-DE1070FA39F2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ChannelMode_1</Property>
	<Property Name="varPersistentID:{B9FBF699-9E01-41D7-8176-568F2349FA42}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Q-IL-Enabled</Property>
	<Property Name="varPersistentID:{B9FEB1CD-BE7B-49E6-872A-45D614D75C2F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ResourceName</Property>
	<Property Name="varPersistentID:{BA4219BD-A658-44C1-84B4-BAE6D4DA983A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingDeltaT</Property>
	<Property Name="varPersistentID:{BAA58FF3-61FB-4752-8E5E-975A52EA0A15}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_1</Property>
	<Property Name="varPersistentID:{BAA94989-2D4B-4E37-B27B-DD0A491A6E9E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{BAFB4DD6-CD66-41B2-867C-02127CDFE417}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PIDLead</Property>
	<Property Name="varPersistentID:{BB2C86ED-293D-4D5A-932D-E5D456DAD2E8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_2</Property>
	<Property Name="varPersistentID:{BB537D14-47DA-4083-9E1E-7E5021B28465}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_0</Property>
	<Property Name="varPersistentID:{BB855D3D-74C4-4853-960D-82091B1A28C9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{BBEC33E3-F4D1-4A26-AE61-3994029EB1D4}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Chopper</Property>
	<Property Name="varPersistentID:{BCAA330A-9ADC-4D8F-A8C8-198F41DCFA51}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-MP-Max</Property>
	<Property Name="varPersistentID:{BCB6AD67-5022-494A-8310-BE983FDA5FA5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Pressure</Property>
	<Property Name="varPersistentID:{BD9DD6C9-017D-4DE0-A9C7-394FD9E756FE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_1</Property>
	<Property Name="varPersistentID:{BDA689A6-40BE-4103-8706-684A6BE3C2F9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Targetwheel-IL</Property>
	<Property Name="varPersistentID:{BDE017DA-68C0-4675-A7DC-6259419FE775}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL-Main</Property>
	<Property Name="varPersistentID:{BE0AEF7E-E9C5-43DD-A0C0-88D9C57052A2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_DriverRevision</Property>
	<Property Name="varPersistentID:{BE854BDE-4F2C-4AAC-A060-876CA88DA622}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{BF15CC64-B97B-4A4E-91DD-A3E49D6378F0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_2</Property>
	<Property Name="varPersistentID:{BF428738-A253-4840-94C7-01AF3F5AD4E6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_SelftestResultCode</Property>
	<Property Name="varPersistentID:{BF808038-D56D-40F8-8187-427A32BAB4C4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_2</Property>
	<Property Name="varPersistentID:{C027E79D-BB18-4F87-BCD4-4B2277DBCE5F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingMode</Property>
	<Property Name="varPersistentID:{C02CD296-723C-491C-974E-650A4D673996}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_1</Property>
	<Property Name="varPersistentID:{C07F443D-2F5B-4654-B524-2D97921718EF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_0</Property>
	<Property Name="varPersistentID:{C0B7AF95-4933-41D1-A1A9-E260A7753DE9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_0</Property>
	<Property Name="varPersistentID:{C17D3994-37DE-4591-9374-879317A093D3}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_TSRMin</Property>
	<Property Name="varPersistentID:{C1872A9A-5542-484A-BB41-B5427BB0378D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-Rate-Limit-Enabled</Property>
	<Property Name="varPersistentID:{C2496879-2B74-4924-A804-5ED26FF291B0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP5</Property>
	<Property Name="varPersistentID:{C294EB02-AB04-4375-A14D-6F7C42AE42AB}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ErrorMessage</Property>
	<Property Name="varPersistentID:{C2B6DFC6-E65C-432A-BEAB-CB05ECCB2476}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_1</Property>
	<Property Name="varPersistentID:{C3F2CFF5-ABDF-4473-8059-36BD4536CD7E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ResourceName</Property>
	<Property Name="varPersistentID:{C436E6B6-9EE3-4304-B5FF-B1D95687EA4B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TASCAGC-WDAlarm</Property>
	<Property Name="varPersistentID:{C49F4C02-A9F4-42F1-9C28-63B54CB87071}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FirmwareRevision</Property>
	<Property Name="varPersistentID:{C4C80428-D06E-4B71-81A4-B2FF1CD56A5E}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ExposureTime</Property>
	<Property Name="varPersistentID:{C57AA675-2F52-4017-8F58-E0EA0737667B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingInterval</Property>
	<Property Name="varPersistentID:{C64B6120-72D8-48A4-8375-430BC490BD2F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_-1</Property>
	<Property Name="varPersistentID:{C68889B0-972B-41E2-830F-AC4E3E33E48C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_3</Property>
	<Property Name="varPersistentID:{C7442A53-27DF-4D0C-BF75-EC65C77EAC25}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-Laser</Property>
	<Property Name="varPersistentID:{C75A79B9-0B5E-447E-9E29-8B43FC2F6C1D}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2.currinfo_1</Property>
	<Property Name="varPersistentID:{C77DF870-4709-4B04-BA94-34F6A4BE3512}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_2</Property>
	<Property Name="varPersistentID:{C78C8765-078C-4CF1-AE8B-87D1666247CD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{C7A290A2-0181-4C77-8727-41892CB52925}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureSP</Property>
	<Property Name="varPersistentID:{C7C9AC42-810B-4ED3-BDC0-10F08D05AB9B}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DT3.currinfo</Property>
	<Property Name="varPersistentID:{C88095AE-9E1B-4774-9BC2-A8C1435D239D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Chopper_output</Property>
	<Property Name="varPersistentID:{C90CFA84-8363-4BFE-9EE7-D5B9547EE38E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_3</Property>
	<Property Name="varPersistentID:{C91BDF09-38E3-4D77-9630-EF26EFBD6574}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_0</Property>
	<Property Name="varPersistentID:{C9205DFD-34AE-4307-9791-C90FC783D43B}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Error-Log-Path</Property>
	<Property Name="varPersistentID:{C92F2225-C116-4499-88A6-92A92BF946A9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowRange_0</Property>
	<Property Name="varPersistentID:{C9537AF3-E27E-4885-AE3B-901EB3637F26}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX.currinfo_1</Property>
	<Property Name="varPersistentID:{C9C0D3E3-3935-484F-B9B8-1B841D06E492}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_2</Property>
	<Property Name="varPersistentID:{CA002068-8607-45D7-AC94-827988E08A87}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD12_current_setpoint</Property>
	<Property Name="varPersistentID:{CA173208-F7A4-4908-AEF8-B16274DAE437}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_0</Property>
	<Property Name="varPersistentID:{CA1F5E83-F7ED-46EC-92AB-500D2E28B63D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{CA667A03-2C1E-4E2C-9FA6-6ABCCB6CC6C3}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Detector_Rate_Limit</Property>
	<Property Name="varPersistentID:{CAF9F4E8-5F14-41A1-B76B-9E346C37DB9B}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Array Size</Property>
	<Property Name="varPersistentID:{CB32DCA8-4025-47A2-AFF6-DD5974A6C0A1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowRange_1</Property>
	<Property Name="varPersistentID:{CB738AC9-5C73-4A38-ADFD-A5598661E3E2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_3</Property>
	<Property Name="varPersistentID:{CC023DB2-97A3-4A4E-8D70-E655C791DD02}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Pyrometer-IL</Property>
	<Property Name="varPersistentID:{CCE012EB-A578-48A0-80AC-D9D5A2F0C977}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_0</Property>
	<Property Name="varPersistentID:{CD00A16C-94DC-44F7-9886-359B5C35F9C8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{CD08E48F-1D66-4000-9FAA-D5CB81B44A91}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{CD50C5CC-9EB4-4AE1-BFF4-C9A7B8E10BD3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowRange_0</Property>
	<Property Name="varPersistentID:{CD7E145C-A190-485A-9950-B693A797EA1C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-In</Property>
	<Property Name="varPersistentID:{CD8CD479-B1CA-42EB-904B-8F590BC0F996}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_0</Property>
	<Property Name="varPersistentID:{CE2771E1-339B-45F5-BDE1-72892AADE0C6}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Det-Rate-Limit-Enabled</Property>
	<Property Name="varPersistentID:{CE289C9D-5F75-43A1-9203-A0320C6A2AD7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_1</Property>
	<Property Name="varPersistentID:{CE43665A-073F-4082-9D57-664D0C07A05A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_2</Property>
	<Property Name="varPersistentID:{D055E64C-D183-4ABC-ADC9-55BEBCC18F31}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingInterval</Property>
	<Property Name="varPersistentID:{D0A63968-A485-4E96-A246-4DBEE84B7DDA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ErrorStatus</Property>
	<Property Name="varPersistentID:{D0CBDF99-5922-4271-B0E6-DCBD85FD67D4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{D10300A1-BBA6-4D7F-87DE-EDA0E5AEB6FC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{D188D775-70EE-4F6A-A984-36C1B72F69CD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ErrorCode</Property>
	<Property Name="varPersistentID:{D19238B9-7A74-4840-9BAB-D19C115F6686}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SelfTest</Property>
	<Property Name="varPersistentID:{D19A1BB2-7252-4454-9A93-E3537D724D7A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_3</Property>
	<Property Name="varPersistentID:{D19BA09E-B21E-41EC-A205-60B9650378DA}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6.currinfo_0</Property>
	<Property Name="varPersistentID:{D1C8002C-AB8A-4616-A261-1CA7B5AE899C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SelftestResultCode</Property>
	<Property Name="varPersistentID:{D1C9018A-959A-4F66-B676-76C55F08B6DD}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP6</Property>
	<Property Name="varPersistentID:{D2347CDD-0062-4C83-A5CB-64C2664A817A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingIterations</Property>
	<Property Name="varPersistentID:{D26F33CB-9DAC-4018-AE5B-C3D74190220C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SelftestResultCode</Property>
	<Property Name="varPersistentID:{D297CD24-8EB4-45A0-9EBD-EA2D09A6FF94}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Virtual_Accelerator</Property>
	<Property Name="varPersistentID:{D3BCBF50-2236-4B54-B51B-85F55656032C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{D3C04C80-32BB-45A2-A407-2223E80A573A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pyrometer-WDAlarm</Property>
	<Property Name="varPersistentID:{D3F9FB27-9CC7-4A83-A038-9E03F965DC6B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Generate</Property>
	<Property Name="varPersistentID:{D42239B4-D8E8-4259-8AC0-1AAAFF0EFC5D}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXADT2.currinfo_1</Property>
	<Property Name="varPersistentID:{D45135C3-78F1-41C0-B22F-8EF4105BBD6A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-1-WDAlarm</Property>
	<Property Name="varPersistentID:{D4570C86-4A3F-49D5-A1BA-41148C85EF6F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{D492FEEC-5D6A-402E-AF73-1576BCF8F02F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingInterval</Property>
	<Property Name="varPersistentID:{D547A598-8F3B-4104-8832-ED8E25F50348}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV5_Status</Property>
	<Property Name="varPersistentID:{D557D88D-55F9-469A-A6AE-EFE5AC957DDE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_0</Property>
	<Property Name="varPersistentID:{D58EA930-E155-4202-975E-A70B5A9BEE01}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP5</Property>
	<Property Name="varPersistentID:{D61B038C-0F5D-4E0B-A16E-2BFA2DB2C743}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowRange_1</Property>
	<Property Name="varPersistentID:{D633370A-7348-4BDF-9095-F72C89DBF8D2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_UnderrangeControl</Property>
	<Property Name="varPersistentID:{D6E9A46E-CA4E-4E50-ADA8-A0336234CC9C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_0</Property>
	<Property Name="varPersistentID:{D71AC4C5-AC7C-460B-A607-BCAA3DF29E00}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_2</Property>
	<Property Name="varPersistentID:{D7602180-D831-4CFD-B914-96826CD8F56F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP4_Status</Property>
	<Property Name="varPersistentID:{D7C44CB1-F04F-41F7-B0A4-2B26F7A8B641}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ResourceName</Property>
	<Property Name="varPersistentID:{D7D5BE14-B48D-4481-A44E-79C4BECCD048}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{D7D97BA1-A27F-45C8-AE62-946B15175A4D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Error</Property>
	<Property Name="varPersistentID:{D7DCF3B7-3555-4C3E-898A-760C264C7BB7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_3</Property>
	<Property Name="varPersistentID:{D845A3AB-A213-4EF3-9809-7147772D34D4}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV4_Status</Property>
	<Property Name="varPersistentID:{D8B8BE48-9993-45A7-AEAA-4279E0486FF7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingInterval</Property>
	<Property Name="varPersistentID:{D8E81D0E-A182-4ADD-83F0-09A6D7D68E62}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{D96AFCDD-62A7-4BCF-9D69-7FA0EFC8BD43}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SelftestResultCode</Property>
	<Property Name="varPersistentID:{D97632A2-DEDD-40E4-BE07-88F025C033A0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period-Reset</Property>
	<Property Name="varPersistentID:{D99AC762-EF06-4DB2-A237-C1CCA0AFBD60}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FirmwareRevision</Property>
	<Property Name="varPersistentID:{DADFCA26-422C-4A71-9BD9-8BB5421E5A6E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{DB06C6B2-2302-405B-886D-DE9D1E9F703B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowRange_2</Property>
	<Property Name="varPersistentID:{DB29CF98-02BF-4E1C-86EB-71D17B30F05C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingDeltaT</Property>
	<Property Name="varPersistentID:{DBA071EF-519C-4594-82D3-6622FE138719}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Laser</Property>
	<Property Name="varPersistentID:{DBB7AB8F-68A9-4B7A-829B-40E9A976322F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{DBDA9D94-0BFE-4439-9D45-E788E9C15C84}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP4</Property>
	<Property Name="varPersistentID:{DCDE7DFA-5D2C-4F6E-81C2-530AC183F633}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{DCF7C279-3095-46CC-B3ED-C6C50E91FDEA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_0</Property>
	<Property Name="varPersistentID:{DDC79C9B-7C90-4989-97A3-FEC329F3DC29}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{DE1AEDBF-55EB-4D56-85A6-00E237E12F3A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_0</Property>
	<Property Name="varPersistentID:{DE1C070A-5FD8-4274-8D6D-625BF7747AF5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Pressure</Property>
	<Property Name="varPersistentID:{DE2B5C28-26AC-4D99-9C44-F58C6799A26F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_2</Property>
	<Property Name="varPersistentID:{DE7951E2-E36B-48E4-91CC-0FFF198E1C4A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV6_Status</Property>
	<Property Name="varPersistentID:{DE90CB3B-F931-48A7-96D0-9B6A962519C2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ResourceName</Property>
	<Property Name="varPersistentID:{DEEE4688-BFDA-4BCD-9A72-B14A976DEDA8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{DF5A33FE-B5E5-4BAE-9CCE-92B936C50E21}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingMode</Property>
	<Property Name="varPersistentID:{DFBCF276-2C31-4662-8D8B-0593D0533CD4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PressureSP</Property>
	<Property Name="varPersistentID:{DFEB27C9-0CE3-4727-91A0-3A199BFC7044}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{E0187509-E491-44EE-9A43-8449A05173D2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_DriverRevision</Property>
	<Property Name="varPersistentID:{E0ABCA2B-A252-483D-B5BA-3C920A6FB021}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PIDIntegral</Property>
	<Property Name="varPersistentID:{E0EB4EE5-9331-4477-B678-12D182574288}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_1</Property>
	<Property Name="varPersistentID:{E0ED91A7-B8D8-4EC3-88C6-C56095AC471E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Targetwheel-IL-Enabled</Property>
	<Property Name="varPersistentID:{E1666B6A-F014-41FC-84C1-1440275405E4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_2</Property>
	<Property Name="varPersistentID:{E1863D6D-F8EF-437A-BD35-40F10C52A64C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{E1D50D62-70E4-4142-BF97-83AC4449615C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-IL-Enabled</Property>
	<Property Name="varPersistentID:{E2D96436-5363-4CA6-A8CC-B0A616C22C1A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingCounter</Property>
	<Property Name="varPersistentID:{E355F472-DE98-4681-91FF-5CA2D1A1E873}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_1</Property>
	<Property Name="varPersistentID:{E37C3EFB-D341-4505-AB99-9055C253DA9D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-QMP-Max</Property>
	<Property Name="varPersistentID:{E3857994-784E-4D68-8319-B25BD62D28F2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{E3D92FBB-B2B6-40C1-A51A-ED362EF0539C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-Q</Property>
	<Property Name="varPersistentID:{E4359701-732C-497F-A29D-4D39530C9C17}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Error</Property>
	<Property Name="varPersistentID:{E4633EDB-6A73-427E-AC36-DBB3140CDD86}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Counting_Enabled_3</Property>
	<Property Name="varPersistentID:{E480A792-7DB6-4231-9367-E03D9E27D27B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Interval</Property>
	<Property Name="varPersistentID:{E49546FB-5CBD-4D48-989A-CD1DAF01B58F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_0</Property>
	<Property Name="varPersistentID:{E4CA037F-B781-446A-9E06-332D7B56AB4E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV2</Property>
	<Property Name="varPersistentID:{E4E00C8C-8DBF-4975-8737-036492603262}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{E4E03F23-EFB3-4AB4-A206-3195464342AF}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_T-Pause</Property>
	<Property Name="varPersistentID:{E4EDF415-E968-46BC-B771-6C2B1428CF68}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PIDGain</Property>
	<Property Name="varPersistentID:{E517581F-756E-4824-BB8D-CEC56FAED7D6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowRange_3</Property>
	<Property Name="varPersistentID:{E59BB411-524F-432D-9F64-F1797271E453}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow2_TASCAGC</Property>
	<Property Name="varPersistentID:{E5A99A60-B430-466E-A296-952D6ACFE964}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ErrorMessage</Property>
	<Property Name="varPersistentID:{E5ABE123-1A32-4BC4-BAD2-046BAE964FD8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{E698106F-0A66-4EF4-A381-4610FBB81E86}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingMode</Property>
	<Property Name="varPersistentID:{E699ADFC-F7EC-4CD1-A257-034A290F0CCE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_2</Property>
	<Property Name="varPersistentID:{E72DC9E9-03CE-42F7-856E-DBF4C21D6085}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_FirmwareRevision</Property>
	<Property Name="varPersistentID:{E741C210-3B45-443E-8051-81CEACF53F18}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_0</Property>
	<Property Name="varPersistentID:{E74AA602-A3CA-4B64-93F8-1D229BD88C78}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_3</Property>
	<Property Name="varPersistentID:{E7C1B2F8-BC69-4766-BD67-B7264C8A5992}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingCounter</Property>
	<Property Name="varPersistentID:{E803AC97-B1FC-4AA5-8F7A-C9B9F62BE275}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{E891BC72-439A-4808-90CB-1E781470FC1E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{E8CC7BFB-0C38-4DE9-A79E-317C17291AC9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Magnet-IL-Enabled</Property>
	<Property Name="varPersistentID:{E9563532-492A-4889-AF5B-0FC612FF6289}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Charge-IL</Property>
	<Property Name="varPersistentID:{E982FBC0-57B0-4F04-9EAB-6C7FCBAAAB60}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_1</Property>
	<Property Name="varPersistentID:{E9993F72-41BC-4ADF-A67B-AF098DDB2399}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Chopper_status</Property>
	<Property Name="varPersistentID:{EA392A93-4E1F-4AE2-909B-1A2049F97AC2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow0_TASCAGC</Property>
	<Property Name="varPersistentID:{EB74B2AC-8ADF-4DE2-8680-019405B9F6D8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ZeroAdjustMFC</Property>
	<Property Name="varPersistentID:{EC18F1AB-3FFC-407E-B92F-E88726F7439B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-Counter</Property>
	<Property Name="varPersistentID:{ECDFC043-0791-48F0-9E88-01F293718F49}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{ED74DDA8-E49B-4BD6-9C0D-EF6E196D6C7C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_1</Property>
	<Property Name="varPersistentID:{ED97014E-1549-4702-ADF3-107F45E26BC7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingTime</Property>
	<Property Name="varPersistentID:{ED9E563B-01B2-4330-A12C-79C2F57B726F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ErrorMessage</Property>
	<Property Name="varPersistentID:{EDB4BBE8-1F03-4B77-AC58-D3550098B0DB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Start-2</Property>
	<Property Name="varPersistentID:{EDFEFC1F-E090-48AB-A2A0-D8207521B972}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ErrorStatus</Property>
	<Property Name="varPersistentID:{EEAD6D10-C845-4E0E-99D8-854B9F423CE0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset</Property>
	<Property Name="varPersistentID:{EEEC6046-0648-4237-9CFB-A007DCB04352}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_1</Property>
	<Property Name="varPersistentID:{F0C555B6-44A0-41EE-A236-CF01A9E37DB2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PIDIntergral</Property>
	<Property Name="varPersistentID:{F0E49439-6D26-4B91-9A8A-FC01957BC824}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/InterlockReason</Property>
	<Property Name="varPersistentID:{F0FAE5DE-F336-4D8E-8A89-67C5E9540F34}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{F10CC7EE-D4AB-4236-A8C9-ADA8A0977479}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_1</Property>
	<Property Name="varPersistentID:{F122B3E8-0F9B-4696-8D50-BBB3774F7886}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ErrorMessage</Property>
	<Property Name="varPersistentID:{F1577F02-60E2-47F5-B88D-8F2DB55E3695}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ErrorCode</Property>
	<Property Name="varPersistentID:{F169A32F-7469-4BF2-9C6B-1D4B080148E8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_2</Property>
	<Property Name="varPersistentID:{F1CF4976-7E09-4A6B-B330-485FE0FD3727}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_RTCGC</Property>
	<Property Name="varPersistentID:{F2608F7A-5538-482C-9CA6-B6A55484D60B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_1</Property>
	<Property Name="varPersistentID:{F266F185-2F2A-4CDC-AF97-3F877E905969}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{F369DD93-CD66-456E-91F5-516779D9453F}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Selftest</Property>
	<Property Name="varPersistentID:{F36F1542-42FB-4B66-8C4D-4B3D327ED64D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Frequency</Property>
	<Property Name="varPersistentID:{F3B79E8E-EAB9-48FC-8898-A702F8F86607}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_set-position</Property>
	<Property Name="varPersistentID:{F4263EE7-D8F9-41ED-86FF-32D7F77E07BA}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP2</Property>
	<Property Name="varPersistentID:{F460E191-5D66-4619-9A45-FF4BEFBB0956}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ResourceName</Property>
	<Property Name="varPersistentID:{F4895E45-809B-4084-9434-315D56E6164D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_2</Property>
	<Property Name="varPersistentID:{F49F1C2C-5EAC-45EA-BFB7-0B7C6FD49433}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IMin</Property>
	<Property Name="varPersistentID:{F4B2F715-1245-4B6A-96A6-C7DB78D6CA45}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingDeltaT</Property>
	<Property Name="varPersistentID:{F4FB4882-A813-48CC-899F-E52A628DF622}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowRange_1</Property>
	<Property Name="varPersistentID:{F5AA4B46-2DE9-4AA9-9B8A-DF4447DFEB40}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_3</Property>
	<Property Name="varPersistentID:{F6DEAEAE-CAB6-4D96-A033-3528B0D33CE9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SecValvesState</Property>
	<Property Name="varPersistentID:{F6FC2B93-FB5E-44DC-8A60-A42AF06B34D3}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Emissivity</Property>
	<Property Name="varPersistentID:{F72FE362-BA08-490E-ADB6-A978005F8888}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-3-WDAlarm</Property>
	<Property Name="varPersistentID:{F73690C2-EDA0-418D-B053-684E709AA5CE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PressureMode</Property>
	<Property Name="varPersistentID:{F74785C9-C453-4718-9FFE-A896384B0CAA}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Error Code</Property>
	<Property Name="varPersistentID:{F7C2D917-0F88-4A17-878B-3E2EE27C24D9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_3</Property>
	<Property Name="varPersistentID:{F7DC8ECA-DD78-4D56-B2C8-019E02ABFB36}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP4_Status</Property>
	<Property Name="varPersistentID:{F7FFC7CA-E0D1-4AB9-9CDD-94D93B979A67}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowRange_3</Property>
	<Property Name="varPersistentID:{F800E79B-46C5-4976-9265-37DF712697C1}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6_P.positi</Property>
	<Property Name="varPersistentID:{F83FEE33-BBE2-4380-8CBD-3AF8979060A6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_3</Property>
	<Property Name="varPersistentID:{F846F529-5589-4803-9BF0-EA6DD060329B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_A</Property>
	<Property Name="varPersistentID:{F8482F99-F57B-4A2F-8205-607E463FDC14}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingTime</Property>
	<Property Name="varPersistentID:{F85B40D2-FCE8-408D-8F16-8E150414371B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{F86EA2C3-DB13-4392-8A66-C6A9C2BF2F83}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UT2DCX_position</Property>
	<Property Name="varPersistentID:{F90B2B6A-93E2-49A4-86E7-2A2D75169F01}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ControllerMode</Property>
	<Property Name="varPersistentID:{F9AED3DE-16A2-4961-9DC1-9EC6388DCE1A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_UnderrangeControl</Property>
	<Property Name="varPersistentID:{F9DEEBFD-E9C1-4F33-904D-AC3F305DF0A9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowRange_2</Property>
	<Property Name="varPersistentID:{FA1E5BCA-6884-4CEF-8B38-5D403EB67DD9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_position</Property>
	<Property Name="varPersistentID:{FA22A5BA-65A4-4605-BA69-9EBAE2DB3AC8}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Time-BeamOff</Property>
	<Property Name="varPersistentID:{FA2B9616-E38A-4793-85A6-C895AE6641CA}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/BMIL_SW-Watchdog</Property>
	<Property Name="varPersistentID:{FA55C812-DAAE-471B-A551-4ACB85A1EE08}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingIterations</Property>
	<Property Name="varPersistentID:{FA5D223A-F364-40D8-A86F-2ACE4AC5EC62}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{FAC4DABD-F0EC-4A70-AF71-7D66524D0D46}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3-Proxy_Activate</Property>
	<Property Name="varPersistentID:{FAE0E45B-2E2E-4EFF-B2B1-F8A6B78F123A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{FB0B2F30-445A-49F6-B76B-498BD0A3AB25}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingTime</Property>
	<Property Name="varPersistentID:{FB3FF52E-2B38-4F0F-A8D4-CD478F630622}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_1</Property>
	<Property Name="varPersistentID:{FB5C72A3-E566-4F33-BEAB-ECAC463536F5}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX.currinfo</Property>
	<Property Name="varPersistentID:{FB6EB0CF-E3D7-4981-8200-AADE0B773ECB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PressureSP</Property>
	<Property Name="varPersistentID:{FBD9D0B4-C3B1-47D5-B335-EFBC8E4B5872}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_1</Property>
	<Property Name="varPersistentID:{FBF9F2CB-C2DA-4389-9BFB-C87AC50C212E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{FC390476-FAD3-45FE-ABA6-12DDD8DA3A26}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_3</Property>
	<Property Name="varPersistentID:{FCDC2F79-6D5B-49D4-9F6B-809B49E0EA81}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChModes</Property>
	<Property Name="varPersistentID:{FD0095E1-BC57-4C18-AD4F-6123986DC3C0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Interlock</Property>
	<Property Name="varPersistentID:{FD4CD38F-1CC0-4A52-88E3-877CFB465D0B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_T-BeamOn</Property>
	<Property Name="varPersistentID:{FE1CBCAC-CC02-4C14-9CE3-A4C2D601B9A9}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Server Type</Property>
	<Property Name="varPersistentID:{FE4157B2-7FD8-4246-A14E-254296B3CAAE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingTime</Property>
	<Property Name="varPersistentID:{FE9BF2C2-15F4-4F91-9C5B-B9C81850891B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{FED8F9D0-21B9-4FF2-AEF3-8CE317F85778}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{FF3F6DFF-331A-4905-B6F7-62047C3E3330}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_1</Property>
	<Property Name="varPersistentID:{FF9B62CF-6422-4B6E-96A3-30B59600BA9E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SelftestResultMessage</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="ACC" Type="Folder">
			<Item Name="UTCS_AccIO.lvlib" Type="Library" URL="../SV.lib/UTCS_AccIO.lvlib"/>
			<Item Name="UTCS_AccIOS.lvlib" Type="Library" URL="../SV.lib/UTCS_AccIOS.lvlib"/>
		</Item>
		<Item Name="AF" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
		</Item>
		<Item Name="BNT" Type="Folder">
			<Item Name="BNT_DAQmx" Type="Folder">
				<Item Name="BNT_DAQmx-Content.vi" Type="VI" URL="../Packages/BNT_DAQmx/BNT_DAQmx-Content.vi"/>
				<Item Name="BNT_DAQmx.lvlib" Type="Library" URL="../Packages/BNT_DAQmx/BNT_DAQmx.lvlib"/>
			</Item>
		</Item>
		<Item Name="CSPP" Type="Folder">
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Libs" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
			</Item>
			<Item Name="DSC" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
					<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
					<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
					<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
					<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
				</Item>
				<Item Name="Libs" Type="Folder">
					<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
				</Item>
				<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
			</Item>
			<Item Name="ObjectManager" Type="Folder">
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
					<Item Name="CSPP_PVScaler.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_PVScaler/CSPP_PVScaler.lvlib"/>
				</Item>
				<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
			</Item>
		</Item>
		<Item Name="Devices" Type="Folder">
			<Item Name="CSPP_IGA140.lvlib" Type="Library" URL="../Packages/UTCS/IGA140/CSPP_IGA140.lvlib"/>
			<Item Name="CSPP_IGA140GUI.lvlib" Type="Library" URL="../Packages/UTCS/IGA140/CSPP_IGA140GUI.lvlib"/>
			<Item Name="MKS647C.lvlib" Type="Library" URL="../Packages/CSPP_MKS647C/MKS647C Actor/MKS647C.lvlib"/>
			<Item Name="MKS647CGUI.lvlib" Type="Library" URL="../Packages/CSPP_MKS647C/MKS647C GUI/MKS647CGUI.lvlib"/>
			<Item Name="TPG300A.lvlib" Type="Library" URL="../Packages/CSPP_TPG300/TPG300A.lvlib"/>
			<Item Name="TPG300GUI.lvlib" Type="Library" URL="../Packages/CSPP_TPG300/TPG300 GUI/TPG300GUI.lvlib"/>
			<Item Name="UTCS_BMIL.lvlib" Type="Library" URL="../Packages/UTCS/BeamControl/Host/UTCS_BMIL.lvlib"/>
		</Item>
		<Item Name="Docs &amp; EUPL" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
			<Item Name="README.md" Type="Document" URL="../README.md"/>
		</Item>
		<Item Name="instr.lib" Type="Folder">
			<Item Name="IGA140.lvlib" Type="Library" URL="../instr.lib/IGA140/IGA140.lvlib"/>
			<Item Name="MGC647C.lvlib" Type="Library" URL="../instr.lib/MGC647C/MGC647C.lvlib"/>
			<Item Name="TPG300.lvlib" Type="Library" URL="../instr.lib/TPG300/TPG300.lvlib"/>
		</Item>
		<Item Name="libs" Type="Folder">
			<Item Name="ni_security_salapi.dll" Type="Document" URL="/&lt;vilib&gt;/Platform/security/ni_security_salapi.dll"/>
		</Item>
		<Item Name="OperatingGUIs" Type="Folder">
			<Item Name="UTCS_BeamControlGUI.lvlib" Type="Library" URL="../Packages/UTCS/BeamControl-GUI/UTCS_BeamControlGUI.lvlib"/>
			<Item Name="UTCS_GasGUI.lvlib" Type="Library" URL="../Packages/UTCS/Gas-GUI/UTCS_GasGUI.lvlib"/>
			<Item Name="UTCS_InterlockGUI.lvlib" Type="Library" URL="../Packages/UTCS/Interlock-GUI/UTCS_InterlockGUI.lvlib"/>
			<Item Name="UTCS_MainGUI.lvlib" Type="Library" URL="../Packages/UTCS/UTCS_MainGUI/UTCS_MainGUI.lvlib"/>
		</Item>
		<Item Name="TASCA" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="UTCS_Content.vi" Type="VI" URL="../Packages/UTCS/UTCS_Content.vi"/>
			<Item Name="UTCS_AE.lvlib" Type="Library" URL="../SV.lib/UTCS_AE.lvlib"/>
			<Item Name="UTCS_BMIL_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_BMIL_SV.lvlib"/>
			<Item Name="UTCS_MKS647_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_MKS647_SV.lvlib"/>
			<Item Name="UTCS_TPG300_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_TPG300_SV.lvlib"/>
			<Item Name="TPG300_Pressures.xml" Type="Document" URL="../SV.lib/TPG300_Pressures.xml"/>
			<Item Name="UTCS_IGA140_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_IGA140_SV.lvlib"/>
		</Item>
		<Item Name="Test" Type="Folder">
			<Item Name="Test Sgl Precision.vi" Type="VI" URL="../Packages/UTCS/BeamControl/Test Sgl Precision.vi"/>
			<Item Name="TestHTV.vi" Type="VI" URL="../SV.lib/TestHTV.vi"/>
		</Item>
		<Item Name="COMAPCT.ico" Type="Document" URL="../Packages/COMPACT/COMAPCT.ico"/>
		<Item Name="FPGA Target 3" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{06B84960-5922-4E36-AD74-AA109A2BD1AD}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO12;1{2402528C-ACDA-4DE1-8F00-4B9711AF3735}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO0;1{24E748BD-21E9-4C84-8D16-C84F6071EF4E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT2;1{25C1E0DE-1CA6-426B-B014-829CD0B66416}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO9;1{2955ED38-2AE5-48F0-A779-C3F88B62B7DE}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;1{301CDE06-3881-448A-83B1-5186AFA90DA2}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO3;1{330DAA54-713C-4035-B6E8-2A8146E52B83}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO33;1{37E28051-6BBA-4C48-8200-5E47711CBDC3}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO13;1{3B29909C-903A-42D2-B192-7C2C8AEF657A}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{3C337124-6D31-4DFA-99A6-445866E89EE1}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO2;1{3D2007F7-8A52-415F-90A1-745153A63B22}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{421356DA-FB65-4D88-9EF1-919ADDB2B40F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO25;1{4470CD1E-D4C8-4F3A-8D53-C6D554A515DC}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO11;1{46E07D9F-0433-4495-96BA-E49F00B151A3}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO36;1{48BF0CA8-5533-48F0-9D4B-76CE3B5F52A5}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO15;1{4A0716D0-6CE1-47E8-9E12-7F4222B2D7E5}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO5;1{4AD5A6DF-FDD3-46FC-8E53-68D20774AA98}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO17;1{4F800515-F833-4737-BE44-B7BED6DD31E2}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{53B0E858-A40B-461D-85E7-9F904AA93999}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO35;1{5D47A317-DA72-46D1-AA03-4E15557AD4D9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO19;1{64288DD8-463D-4017-BFF6-6E2680F99709}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;1{673471BA-28DB-43AD-9AC5-5EDEF4D83AC9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO10;1{6C0EC6B0-C87D-406A-9B46-C3C4CA6E988D}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{73134D31-9FB1-471F-BBE2-C2C680E1CDA9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO34;1{8517D071-8745-48BF-A626-61213764A00C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO14;1{860CA467-7CBA-41E5-B2DD-EA7085C97740}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO18;1{87B91C9E-B91B-4101-B51C-A0890F8F20F9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO22;1{951BBDA4-A8A6-4FD5-A69A-3B4B7373AAB8}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO23;1{9DE61D97-E276-408D-BC9C-0B98E4E5DA18}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO16;1{9E0B667B-7B64-4487-B584-A3D603972A07}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO1;1{A1C32FB3-FCD1-4D9E-A1F2-954F5FA65876}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO32;1{A6AEF759-8649-4AD6-818D-A3BC5EF8E50C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO39;1{AD4749F2-1340-4ABE-9F31-C2BC4D7E254D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT4;1{ADB3F92E-3158-4CE6-ACEC-54CB6CE5A080}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;1{AE036352-9861-48F8-9A63-61FC0DFB73C6}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO21;1{B0A02A48-B63A-472B-AB5F-03ACA4ABB5CC}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO24;1{BFFD0110-0A6F-455C-91CB-1D99300F9D06}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{C5D2AB48-CE26-4E4A-93DD-58C07B598629}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;1{C73143FB-8DC6-40C2-A10B-A9B4602E9180}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO37;1{C8763847-0964-4F65-BF91-B1578FE5B632}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO4;1{C8B17119-77E3-4D24-9EDF-7FA0D5DA6C0D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO7;1{D03C9D5A-9E53-431D-95A8-693E88926718}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO29;1{D07A8053-0A00-4895-9AE4-319E07DCE19C}"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-SglToH;DataType=100080000000000100094009000353474C000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{D33507BA-3065-4104-8CB9-6E006E07B97A}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO26;1{D7D5CD54-51C2-4000-93EA-D06208DC0A84}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO38;1{D917232F-F725-4E88-AF96-40F2C5C2BCC8}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO30;1{D9232A5C-473F-47AD-96ED-F1CA2A15EA9D}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{E599C669-E9FC-42C9-8B42-9FBE0D757E7F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO28;1{EF1B801C-000D-4CF1-8B46-A24DC8DFE715}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO31;1{F025C34E-5A3E-43C4-9370-18E1E9CA6C0D}"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-IQ2H;DataType=100080000000000100094008000355363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{F23C399B-C505-4A76-AAF1-D2135284DD3E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO6;1{F6F7DAE8-7DFD-4ED5-BBAB-D00C60830315}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT3;1{FA704F37-96B5-46F8-B16B-588BC6C4AFFA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO8;1{FFFE07D4-4233-4E7D-9A3B-4C4CD50C68BA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO20;1PXI-7813R/DSCGetVarList,PSP;WebpubLaunchBrowser,None;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7813RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;5V-0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO10;15V-1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO11;1Aux In 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO12;1Aux In 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO13;1Aux In 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO14;1Aux In 3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO15;1Aux Out 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO4;1Aux Out 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO5;1Aux Out 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO6;1Aux Out 3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO7;1Chopper InArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO31;1Chopper OutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO8;1DetectorArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;1Detector-FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT2;1DMA-IQ-TOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO20;1F_DipoleArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;1FIFO-Detector"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-MPL"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-MPP"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-SglToH"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-SglToH;DataType=100080000000000100094009000353474C000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-U64ToH"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-IQ2H;DataType=100080000000000100094008000355363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-UX8DT3"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-UXADT2"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FirstArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO36;1FirstOutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO18;1InhibitArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO17;1Interlock-0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO9;1Interlock-1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO16;1Interval FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO26;1IntervalArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO25;1MagnetArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO39;1MP ActiveArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO21;1MP Out 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO0;1MP Out 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO1;1MP Out 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO2;1MP Out 3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO3;1MP_GenArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO23;1MP_InArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO24;1PXI-7813R/DSCGetVarList,PSP;WebpubLaunchBrowser,None;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7813RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]PyrometerArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO38;1Range 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO28;1Range 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO29;1Range 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO30;1RatTrapArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO32;1SecondArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO37;1SecondOutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO19;1SecValvesByPassArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO34;1SecValvesStateArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO35;1Trafo_GenArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO22;1UX8DT3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;1UX8DT3-FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT3;1UXADT2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;1UXADT2-FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIOPORT4;1VacuumBurstArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector0/DIO33;1</Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">PXI-7813R/DSCGetVarList,PSP;WebpubLaunchBrowser,None;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7813RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Property Name="Resource Name" Type="Str">RIO0</Property>
			<Property Name="Target Class" Type="Str">PXI-7813R</Property>
			<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="Connector0" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Input" Type="Folder">
					<Item Name="MP_In" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B0A02A48-B63A-472B-AB5F-03ACA4ABB5CC}</Property>
					</Item>
					<Item Name="Range 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E599C669-E9FC-42C9-8B42-9FBE0D757E7F}</Property>
					</Item>
					<Item Name="Range 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D03C9D5A-9E53-431D-95A8-693E88926718}</Property>
					</Item>
					<Item Name="Range 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D917232F-F725-4E88-AF96-40F2C5C2BCC8}</Property>
					</Item>
					<Item Name="Chopper In" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{EF1B801C-000D-4CF1-8B46-A24DC8DFE715}</Property>
					</Item>
					<Item Name="RatTrap" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO32</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A1C32FB3-FCD1-4D9E-A1F2-954F5FA65876}</Property>
					</Item>
					<Item Name="VacuumBurst" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO33</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{330DAA54-713C-4035-B6E8-2A8146E52B83}</Property>
					</Item>
					<Item Name="SecValvesByPass" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO34</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{73134D31-9FB1-471F-BBE2-C2C680E1CDA9}</Property>
					</Item>
					<Item Name="SecValvesState" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO35</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{53B0E858-A40B-461D-85E7-9F904AA93999}</Property>
					</Item>
					<Item Name="First" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO36</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{46E07D9F-0433-4495-96BA-E49F00B151A3}</Property>
					</Item>
					<Item Name="Second" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO37</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C73143FB-8DC6-40C2-A10B-A9B4602E9180}</Property>
					</Item>
					<Item Name="Pyrometer" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO38</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D7D5CD54-51C2-4000-93EA-D06208DC0A84}</Property>
					</Item>
					<Item Name="Magnet" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO39</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A6AEF759-8649-4AD6-818D-A3BC5EF8E50C}</Property>
					</Item>
					<Item Name="Aux In 3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{48BF0CA8-5533-48F0-9D4B-76CE3B5F52A5}</Property>
					</Item>
					<Item Name="Aux In 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8517D071-8745-48BF-A626-61213764A00C}</Property>
					</Item>
					<Item Name="Aux In 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{37E28051-6BBA-4C48-8200-5E47711CBDC3}</Property>
					</Item>
					<Item Name="Aux In 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{06B84960-5922-4E36-AD74-AA109A2BD1AD}</Property>
					</Item>
				</Item>
				<Item Name="Output" Type="Folder">
					<Item Name="Chopper Out" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FA704F37-96B5-46F8-B16B-588BC6C4AFFA}</Property>
					</Item>
					<Item Name="Interlock-0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{25C1E0DE-1CA6-426B-B014-829CD0B66416}</Property>
					</Item>
					<Item Name="5V-0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{673471BA-28DB-43AD-9AC5-5EDEF4D83AC9}</Property>
					</Item>
					<Item Name="5V-1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4470CD1E-D4C8-4F3A-8D53-C6D554A515DC}</Property>
					</Item>
					<Item Name="Interlock-1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9DE61D97-E276-408D-BC9C-0B98E4E5DA18}</Property>
					</Item>
					<Item Name="Inhibit" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4AD5A6DF-FDD3-46FC-8E53-68D20774AA98}</Property>
					</Item>
					<Item Name="FirstOut" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{860CA467-7CBA-41E5-B2DD-EA7085C97740}</Property>
					</Item>
					<Item Name="SecondOut" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5D47A317-DA72-46D1-AA03-4E15557AD4D9}</Property>
					</Item>
					<Item Name="DMA-IQ-TO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FFFE07D4-4233-4E7D-9A3B-4C4CD50C68BA}</Property>
					</Item>
					<Item Name="MP Active" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AE036352-9861-48F8-9A63-61FC0DFB73C6}</Property>
					</Item>
					<Item Name="Trafo_Gen" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{87B91C9E-B91B-4101-B51C-A0890F8F20F9}</Property>
					</Item>
					<Item Name="MP_Gen" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{951BBDA4-A8A6-4FD5-A69A-3B4B7373AAB8}</Property>
					</Item>
					<Item Name="MP Out 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2402528C-ACDA-4DE1-8F00-4B9711AF3735}</Property>
					</Item>
					<Item Name="MP Out 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9E0B667B-7B64-4487-B584-A3D603972A07}</Property>
					</Item>
					<Item Name="MP Out 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3C337124-6D31-4DFA-99A6-445866E89EE1}</Property>
					</Item>
					<Item Name="MP Out 3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{301CDE06-3881-448A-83B1-5186AFA90DA2}</Property>
					</Item>
					<Item Name="Aux Out 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C8763847-0964-4F65-BF91-B1578FE5B632}</Property>
					</Item>
					<Item Name="Aux Out 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4A0716D0-6CE1-47E8-9E12-7F4222B2D7E5}</Property>
					</Item>
					<Item Name="Aux Out 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F23C399B-C505-4A76-AAF1-D2135284DD3E}</Property>
					</Item>
					<Item Name="Aux Out 3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C8B17119-77E3-4D24-9EDF-7FA0D5DA6C0D}</Property>
					</Item>
					<Item Name="Interval FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D33507BA-3065-4104-8CB9-6E006E07B97A}</Property>
					</Item>
					<Item Name="Interval" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{421356DA-FB65-4D88-9EF1-919ADDB2B40F}</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Connector 1" Type="Folder">
				<Item Name="Input" Type="Folder">
					<Item Name="F_Dipole" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{ADB3F92E-3158-4CE6-ACEC-54CB6CE5A080}</Property>
					</Item>
					<Item Name="Detector" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{64288DD8-463D-4017-BFF6-6E2680F99709}</Property>
					</Item>
					<Item Name="UX8DT3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C5D2AB48-CE26-4E4A-93DD-58C07B598629}</Property>
					</Item>
					<Item Name="UXADT2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2955ED38-2AE5-48F0-A779-C3F88B62B7DE}</Property>
					</Item>
				</Item>
				<Item Name="Output" Type="Folder">
					<Item Name="Detector-FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIOPORT2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{24E748BD-21E9-4C84-8D16-C84F6071EF4E}</Property>
					</Item>
					<Item Name="UX8DT3-FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIOPORT3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F6F7DAE8-7DFD-4ED5-BBAB-D00C60830315}</Property>
					</Item>
					<Item Name="UXADT2-FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIOPORT4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AD4749F2-1340-4ABE-9F31-C2BC4D7E254D}</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{D9232A5C-473F-47AD-96ED-F1CA2A15EA9D}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="FIFO-MPL" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">7</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{BFFD0110-0A6F-455C-91CB-1D99300F9D06}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">1000800000000001000940070003553332000100000000000000000000</Property>
			</Item>
			<Item Name="FIFO-MPP" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">7</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{3D2007F7-8A52-415F-90A1-745153A63B22}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">1000800000000001000940070003553332000100000000000000000000</Property>
			</Item>
			<Item Name="FIFO-SglToH" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">8191</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">11</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-SglToH;DataType=100080000000000100094009000353474C000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{D07A8053-0A00-4895-9AE4-319E07DCE19C}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">8191</Property>
				<Property Name="Type" Type="UInt">2</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094009000353474C000100000000000000000000</Property>
			</Item>
			<Item Name="FIFO-U64ToH" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">8191</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">8</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-IQ2H;DataType=100080000000000100094008000355363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{F025C34E-5A3E-43C4-9370-18E1E9CA6C0D}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">8191</Property>
				<Property Name="Type" Type="UInt">2</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094008000355363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="FIFO-UX8DT3" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">4</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{6C0EC6B0-C87D-406A-9B46-C3C4CA6E988D}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094004000349363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="FIFO-UXADT2" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">4</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{4F800515-F833-4737-BE44-B7BED6DD31E2}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094004000349363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="FIFO-Detector" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">4</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{3B29909C-903A-42D2-B192-7C2C8AEF657A}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094004000349363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="UTCS_BC_FPGA.lvlib" Type="Library" URL="../Packages/UTCS/BeamControl/FPGA/UTCS_BC_FPGA.lvlib"/>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="Main" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">Main</Property>
					<Property Name="Comp.BitfileName" Type="Str">UTCS_PXI-7813R_Main.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">2</Property>
					<Property Name="Comp.Version.Fix" Type="Int">4</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">4</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">Packages/UTCS/BeamControl/FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/User/Helena/LVP/TASCA/UTCS/Packages/UTCS/BeamControl/FPGA Bitfiles/UTCS_PXI-7813R_Main.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">Packages/UTCS/BeamControl/FPGA Bitfiles/UTCS_PXI-7813R_Main.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/User/Helena/LVP/TASCA/UTCS/UTCS.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
					<Property Name="TargetName" Type="Str">FPGA Target 3</Property>
					<Property Name="TopLevelVI" Type="Ref">/My Computer/FPGA Target 3/UTCS_BC_FPGA.lvlib/Main.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="UTCS.ico" Type="Document" URL="../UTCS.ico"/>
		<Item Name="UTCS.ini" Type="Document" URL="../UTCS.ini"/>
		<Item Name="UTCS.vi" Type="VI" URL="../Packages/UTCS/UTCS.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="Check for multiple of dt.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for multiple of dt.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="CIT_ReadTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/citadel/CIT_ReadTimeout.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="CTL_dbNameValid.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_dbNameValid.vi"/>
				<Item Name="CTL_dbURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_dbURLdecode.vi"/>
				<Item Name="CTL_defaultEvtDB.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultEvtDB.vi"/>
				<Item Name="CTL_defaultHistDB.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultHistDB.vi"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="CTL_extractURLMDPformat.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_extractURLMDPformat.vi"/>
				<Item Name="CTL_findDSCApp.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_findDSCApp.vi"/>
				<Item Name="CTL_getAllDBInfo.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getAllDBInfo.vi"/>
				<Item Name="CTL_getArrayPathAndTraceReentrant.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getArrayPathAndTraceReentrant.vi"/>
				<Item Name="CTL_getDBFromDir.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getDBFromDir.vi"/>
				<Item Name="CTL_getDBPathandTraceList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getDBPathandTraceList.vi"/>
				<Item Name="CTL_hdManager.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdManager.vi"/>
				<Item Name="CTL_hdManagerBuffer.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdManagerBuffer.vi"/>
				<Item Name="CTL_hdProxyManager.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdProxyManager.vi"/>
				<Item Name="CTL_lookupTagURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_lookupTagURL.vi"/>
				<Item Name="CTL_resolveSourceDBURLInput.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_resolveSourceDBURLInput.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Control Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Control Task.vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Timing (Burst Export Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Export Clock).vi"/>
				<Item Name="DAQmx Timing (Burst Import Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Import Clock).vi"/>
				<Item Name="DAQmx Timing (Change Detection).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Change Detection).vi"/>
				<Item Name="DAQmx Timing (Handshaking).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Handshaking).vi"/>
				<Item Name="DAQmx Timing (Implicit).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Implicit).vi"/>
				<Item Name="DAQmx Timing (Pipelined Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Pipelined Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Use Waveform).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Use Waveform).vi"/>
				<Item Name="DAQmx Timing.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="dsc_PrefsPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/info/dsc_PrefsPath.vi"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="dscHistD.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/historical/internal/dscHistD.dll"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Digital Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Subset.vi"/>
				<Item Name="DTbl Get Digital Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Get Digital Value.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital Size.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="DWDT Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get Waveform Subset.vi"/>
				<Item Name="DWDT Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get XY Value.vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="FormatTime String.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/FormatTime String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get Project Library Version.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Get Project Library Version.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get Waveform Subset.vi"/>
				<Item Name="Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get XY Value.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="HIST_AlarmDataToControl.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_AlarmDataToControl.vi"/>
				<Item Name="HIST_BuildAlarmColumns.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_BuildAlarmColumns.vi"/>
				<Item Name="HIST_CheckAlarmCtlRef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_CheckAlarmCtlRef.vi"/>
				<Item Name="HIST_ExtractAlarmData.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_ExtractAlarmData.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="HIST_GET_FILTER_ERRORS.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_GET_FILTER_ERRORS.vi"/>
				<Item Name="HIST_GetFilterTime.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_GetFilterTime.vi"/>
				<Item Name="HIST_GetHistTagListCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_GetHistTagListCORE.vi"/>
				<Item Name="HIST_ReadBitArrayTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadBitArrayTrace.vi"/>
				<Item Name="HIST_ReadBitArrayTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadBitArrayTraceCORE.vi"/>
				<Item Name="HIST_ReadLogicalTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadLogicalTrace.vi"/>
				<Item Name="HIST_ReadLogicalTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadLogicalTraceCORE.vi"/>
				<Item Name="HIST_ReadNumericTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadNumericTrace.vi"/>
				<Item Name="HIST_ReadNumericTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadNumericTraceCORE.vi"/>
				<Item Name="HIST_ReadStringTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadStringTrace.vi"/>
				<Item Name="HIST_ReadStringTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadStringTraceCORE.vi"/>
				<Item Name="HIST_ReadVariantTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadVariantTrace.vi"/>
				<Item Name="HIST_ReadVariantTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadVariantTraceCORE.vi"/>
				<Item Name="HIST_RunAlarmQueryCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_RunAlarmQueryCORE.vi"/>
				<Item Name="HIST_VALIDATE_FILTER.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_VALIDATE_FILTER.vi"/>
				<Item Name="HIST_ValReadTrendOptions.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ValReadTrendOptions.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NET_convertLocalhostURLToMachineURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_convertLocalhostURLToMachineURL.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="NET_handleDotInTagName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_handleDotInTagName.vi"/>
				<Item Name="NET_IsComputerLocalhost.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_IsComputerLocalhost.vi"/>
				<Item Name="NET_localhostToMachineName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_localhostToMachineName.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_resolveTagURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveTagURL.vi"/>
				<Item Name="NET_SameMachine.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_SameMachine.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Security Domain.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Domain.ctl"/>
				<Item Name="NI_Security Get Domains.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Get Domains.vi"/>
				<Item Name="NI_Security Identifier.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Identifier.ctl"/>
				<Item Name="NI_Security Resolve Domain.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Resolve Domain.vi"/>
				<Item Name="NI_Security_GetTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_GetTimeout.vi"/>
				<Item Name="NI_Security_ProgrammaticLogin.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ProgrammaticLogin.vi"/>
				<Item Name="NI_Security_ResolveDomainID.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainID.vi"/>
				<Item Name="NI_Security_ResolveDomainName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainName.vi"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Number of Waveform Samples.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Number of Waveform Samples.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="PTH_EmptyOrNotAPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_EmptyOrNotAPath.vi"/>
				<Item Name="PTH_IsUNC.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_IsUNC.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="SEC Get Interactive User.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/custom/SEC Get Interactive User.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subElapsedTime.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/subElapsedTime.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="TIME_FormatTS(TS).vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/time/TIME_FormatTS(TS).vi"/>
				<Item Name="TIME_StartTsLEStopTs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/time/TIME_StartTsLEStopTs.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="VISA Lock Async.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Lock Async.vi"/>
				<Item Name="WDT Get Waveform Subset CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset CDB.vi"/>
				<Item Name="WDT Get Waveform Subset DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset DBL.vi"/>
				<Item Name="WDT Get Waveform Subset EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset EXT.vi"/>
				<Item Name="WDT Get Waveform Subset I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I8.vi"/>
				<Item Name="WDT Get Waveform Subset I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I16.vi"/>
				<Item Name="WDT Get Waveform Subset I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I32.vi"/>
				<Item Name="WDT Get Waveform Subset SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset SGL.vi"/>
				<Item Name="WDT Get XY Value CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CDB.vi"/>
				<Item Name="WDT Get XY Value CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CXT.vi"/>
				<Item Name="WDT Get XY Value DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value DBL.vi"/>
				<Item Name="WDT Get XY Value EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value EXT.vi"/>
				<Item Name="WDT Get XY Value I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I16.vi"/>
				<Item Name="WDT Get XY Value I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I32.vi"/>
				<Item Name="WDT Get XY Value I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I64.vi"/>
				<Item Name="WDT Number of Waveform Samples CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples CDB.vi"/>
				<Item Name="WDT Number of Waveform Samples DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples DBL.vi"/>
				<Item Name="WDT Number of Waveform Samples EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples EXT.vi"/>
				<Item Name="WDT Number of Waveform Samples I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I8.vi"/>
				<Item Name="WDT Number of Waveform Samples I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I16.vi"/>
				<Item Name="WDT Number of Waveform Samples I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I32.vi"/>
				<Item Name="WDT Number of Waveform Samples SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples SGL.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="CSPP_DSCUtilities.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Libs/CSPP_DSCUtilities/CSPP_DSCUtilities.lvlib"/>
			<Item Name="CSPP_LMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LMMonitor/CSPP_LMMonitor.lvlib"/>
			<Item Name="CSPP_LNMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LNMonitor/CSPP_LNMonitor.lvlib"/>
			<Item Name="CSPP_PV2ArrayConverter.lvlib" Type="Library" URL="../Packages/CSPP_PVConverter/CSPP_PV2ArrayConverter.lvlib"/>
			<Item Name="CSPP_PVConverter-Content.vi" Type="VI" URL="../Packages/CSPP_PVConverter/CSPP_PVConverter-Content.vi"/>
			<Item Name="CSPP_PVScaler.lvlib" Type="Library" URL="../Packages/CSPP_PVConverter/CSPP_PVScaler.lvlib"/>
			<Item Name="CSPP_PVSubscriber.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVSubscriber/CSPP_PVSubscriber.lvlib"/>
			<Item Name="CSPP_SystemMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_SystemMonitor/CSPP_SystemMonitor.lvlib"/>
			<Item Name="CSPP_Watchdog Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_Watchdog Msg/CSPP_Watchdog Msg.lvlib"/>
			<Item Name="CSPP_Watchdog.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_Watchdog/CSPP_Watchdog.lvlib"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="TPG300A.lvlib" Type="Library" URL="../Packages/CSPP_Vacuum/CSPP_TPG300/TPG300A.lvlib"/>
			<Item Name="TPG300GUI.lvlib" Type="Library" URL="../Packages/CSPP_Vacuum/CSPP_TPG300/TPG300 GUI/TPG300GUI.lvlib"/>
			<Item Name="UTCS_PXI-7842R_Main.lvbitx" Type="Document" URL="../Packages/UTCS/BeamControl/FPGA Bitfiles/UTCS_PXI-7842R_Main.lvbitx"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="UTCS App" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{E438EDDD-4AB6-4A1B-A2EF-F405DCA1D8EC}</Property>
				<Property Name="App_INI_GUID" Type="Str">{D5B3B86F-9F9C-441E-BBE8-1F64BE17203A}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/UTCS.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{90E67491-152D-4A03-9F72-27DC414438AE}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">UTCS App</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/D/Builds/NI_AB_PROJECTNAME/UTCS App</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{36487EFB-0CEC-4BE6-87F2-BBA5FADEB0E7}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">/D/builds/UTCS/UTCS App/UTCS_UTCS App_log.txt</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">UTCS.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/D/Builds/NI_AB_PROJECTNAME/UTCS App/UTCS.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/D/Builds/NI_AB_PROJECTNAME/UTCS App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/UTCS.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{8111D2A6-B497-4292-9AA7-C1C91A6B76CD}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/UTCS.ini</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/TASCA/TPG300_Pressures.xml</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib</Property>
				<Property Name="Source[11].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].type" Type="Str">Library</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib</Property>
				<Property Name="Source[12].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[12].type" Type="Str">Library</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/libs/ni_security_salapi.dll</Property>
				<Property Name="Source[13].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib</Property>
				<Property Name="Source[14].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[14].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[14].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/UTCS.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.pdf</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/README.md</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib</Property>
				<Property Name="Source[7].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">Library</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib</Property>
				<Property Name="Source[9].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">15</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">UTCS App based on NI Actorframework and CS++.</Property>
				<Property Name="TgtF_internalName" Type="Str">UTCS App</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">UTCS</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{CCC3E9BD-32A7-4FDC-8D9E-7D0DD8099685}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">UTCS.exe</Property>
			</Item>
			<Item Name="UTCS Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">UTCS</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{ABC8933C-1B0D-4935-AF72-F23EB3D5DC0C}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI DataSocket 18.0</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{81A7E53E-9524-41CE-90D3-7DD3D90B6C58}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[1].productID" Type="Str">{D1ED3157-57F9-4363-BFE1-C0FF62071201}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI Distributed System Manager 2018</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{56CE3148-AF48-49F0-8CEC-85F5AFC7F843}</Property>
				<Property Name="DistPart[10].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[10].productID" Type="Str">{C5BDA5B8-6D6E-44BD-AB4F-C14C09331604}</Property>
				<Property Name="DistPart[10].productName" Type="Str">Microsoft Silverlight 5.1</Property>
				<Property Name="DistPart[10].upgradeCode" Type="Str">{69DA64F2-1630-4C0C-947D-6CF5590A63A4}</Property>
				<Property Name="DistPart[11].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[11].productID" Type="Str">{2E4379D6-AF08-4ADB-B15B-EAEEE71AC697}</Property>
				<Property Name="DistPart[11].productName" Type="Str">NI DataFinder Desktop Edition Runtime 2017</Property>
				<Property Name="DistPart[11].upgradeCode" Type="Str">{C14CA542-0B0B-4CD1-8460-FC019E4EBB9D}</Property>
				<Property Name="DistPart[12].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[12].productID" Type="Str">{2C2037BA-3D68-4C24-9E30-EF630EAFA7DD}</Property>
				<Property Name="DistPart[12].productName" Type="Str">NI DataFinder Toolkit Runtime 2018</Property>
				<Property Name="DistPart[12].upgradeCode" Type="Str">{E3313A7A-E43C-486C-A2AE-1EAE5F3F9E6F}</Property>
				<Property Name="DistPart[13].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[13].productID" Type="Str">{1F68B324-59C7-4A7C-8921-ECF6F3233A42}</Property>
				<Property Name="DistPart[13].productName" Type="Str">NI TDM Excel Add-In</Property>
				<Property Name="DistPart[13].upgradeCode" Type="Str">{6D2EBDAF-6CCD-44F3-B767-4DF9E0F2037B}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{C02E174E-A57A-4B5F-A762-8FCA4854370A}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI LabVIEW Remote Execution Support 2019</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{1DB06C72-60F2-48DC-BAEA-935A3CFFFA3C}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[3].productID" Type="Str">{FFD5D52C-2DD5-44D0-A8A4-9709987F255B}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI R Series Multifunction RIO Runtime 18.5</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{BED46696-FEA8-41AC-8377-F85B6CE69BE5}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[4].productID" Type="Str">{33C1B63D-5B8F-4932-8441-B87E8C72021F}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI Variable Engine 2018</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{EB7A3C81-1C0F-4495-8CE5-0A427E4E6285}</Property>
				<Property Name="DistPart[5].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[5].productID" Type="Str">{FF2FC67E-7962-419C-AD6F-F0158D364A3F}</Property>
				<Property Name="DistPart[5].productName" Type="Str">NI-488.2 Runtime 18.5</Property>
				<Property Name="DistPart[5].upgradeCode" Type="Str">{357F6618-C660-41A2-A185-5578CC876D1D}</Property>
				<Property Name="DistPart[6].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[6].productID" Type="Str">{B0527EB7-53B2-4E95-9B0E-23B9B0237D21}</Property>
				<Property Name="DistPart[6].productName" Type="Str">NI-DAQmx Runtime 18.5</Property>
				<Property Name="DistPart[6].upgradeCode" Type="Str">{923C9CD5-A0D8-4147-9A8D-998780E30763}</Property>
				<Property Name="DistPart[7].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[7].productID" Type="Str">{AD0D6322-6E99-4244-BF1E-F2FBB67C70B2}</Property>
				<Property Name="DistPart[7].productName" Type="Str">NI-Serial Runtime 18.5</Property>
				<Property Name="DistPart[7].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[8].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[8].productID" Type="Str">{EDF95F83-017A-4425-8F94-63FF8533A5EA}</Property>
				<Property Name="DistPart[8].productName" Type="Str">NI-VISA Runtime 18.5</Property>
				<Property Name="DistPart[8].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[9].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[9].productID" Type="Str">{EE27B7AE-EC56-49EC-9153-7D4CE64EDCA2}</Property>
				<Property Name="DistPart[9].productName" Type="Str">NI LabVIEW Runtime 2019 f2</Property>
				<Property Name="DistPart[9].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[0].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[9].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[9].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[1].productName" Type="Str">NI Deployment Framework 2019</Property>
				<Property Name="DistPart[9].SoftDep[1].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[9].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[2].productName" Type="Str">NI Error Reporting 2019</Property>
				<Property Name="DistPart[9].SoftDep[2].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[9].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[3].productName" Type="Str">NI Logos 19.0</Property>
				<Property Name="DistPart[9].SoftDep[3].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[9].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[4].productName" Type="Str">NI LabVIEW Web Server 2019</Property>
				<Property Name="DistPart[9].SoftDep[4].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[9].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[5].productName" Type="Str">NI mDNS Responder 19.0</Property>
				<Property Name="DistPart[9].SoftDep[5].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[9].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[6].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[9].SoftDep[6].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[9].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[7].productName" Type="Str">Math Kernel Libraries 2018</Property>
				<Property Name="DistPart[9].SoftDep[7].upgradeCode" Type="Str">{33A780B9-8BDE-4A3A-9672-24778EFBEFC4}</Property>
				<Property Name="DistPart[9].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[8].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[9].SoftDep[8].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[9].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[9].productName" Type="Str">NI TDM Streaming 19.0</Property>
				<Property Name="DistPart[9].SoftDep[9].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[9].SoftDepCount" Type="Int">10</Property>
				<Property Name="DistPart[9].upgradeCode" Type="Str">{7D6295E5-8FB8-4BCE-B1CD-B5B396FA1D3F}</Property>
				<Property Name="DistPartCount" Type="Int">14</Property>
				<Property Name="INST_buildLocation" Type="Path">/D/builds/UTCS/UTCS Installer</Property>
				<Property Name="INST_buildSpecName" Type="Str">UTCS Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="INST_productName" Type="Str">UTCS</Property>
				<Property Name="INST_productVersion" Type="Str">1.18.0</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">19008009</Property>
				<Property Name="MSI_arpCompany" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="MSI_arpContact" Type="Str">H.Brand@gsi.de</Property>
				<Property Name="MSI_arpPhone" Type="Str">2123</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.gsi.de</Property>
				<Property Name="MSI_distID" Type="Str">{245059F5-1F54-44D5-BC1A-BD10F95D58A8}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_licenseID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{E85FEE45-D449-4A6F-BACF-C2E8A16214CF}</Property>
				<Property Name="MSI_windowMessage" Type="Str">You are about to install the UTCS executable, support files and additional NI packages. Some packages are published under EUPL v1.1.

Copyright 2019 GSI  Helmholtzzentrum für Schwerionenforschung GmbH
H.Brand@gsi.de, 2123, EEL, Planckstr. 1, 64291 Darmstadt, Germany
</Property>
				<Property Name="MSI_windowTitle" Type="Str">UTCS Installer</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">UTCS.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">UTCS</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">UTCS</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{CCC3E9BD-32A7-4FDC-8D9E-7D0DD8099685}</Property>
				<Property Name="Source[0].File[1].attributes" Type="Int">1</Property>
				<Property Name="Source[0].File[1].dest" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Source[0].File[1].name" Type="Str">UTCS.ini</Property>
				<Property Name="Source[0].File[1].tag" Type="Str">{D5B3B86F-9F9C-441E-BBE8-1F64BE17203A}</Property>
				<Property Name="Source[0].FileCount" Type="Int">2</Property>
				<Property Name="Source[0].name" Type="Str">UTCS App</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/UTCS App</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
