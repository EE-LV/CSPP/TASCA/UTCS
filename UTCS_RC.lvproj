﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="CCSymbols" Type="Str">DSCGetVarList,PSP;WebpubLaunchBrowser,None;DSCSecurity,False;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project "UTCS.lvproj" is used to develop the Unified TASCA Control System Project and is based on LVOOP, NI Actor Framework and CS++.

Related documents and information
=================================
- README.txt
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/TASCA/UTCS
- Documentation:
  - Refer to Documantation Folder
  - Project-Wikis: https://wiki.gsi.de/foswiki/bin/view/Tasca/UTCSP, https://github.com/HB-GSI/CSPP/wiki
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview

Author: r.a.cantemir@gsi.de

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{007DF67C-B64F-471E-824E-AC45FE54753A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{009BC4AE-51E3-475B-A00B-9252815F5B9C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Chopper_output</Property>
	<Property Name="varPersistentID:{00F6378F-F977-4718-8E4E-45076F9D6FBB}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{010C41E0-2D6D-4E8D-B152-B9B0DD42517A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_3</Property>
	<Property Name="varPersistentID:{0133DB1D-073C-4A49-B9EC-31E72825A6A4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureSP</Property>
	<Property Name="varPersistentID:{01A28FC7-EC77-42F1-8023-30C21D3D17B7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_3</Property>
	<Property Name="varPersistentID:{01B837C9-64C0-4683-80D7-5CB7674B806B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Decay-Delay-1</Property>
	<Property Name="varPersistentID:{01C92BAA-E324-432D-B695-DADA041683E5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{01CF005A-AAFC-460C-9118-C28434F822DD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{022B7631-2796-4A66-8846-916D83E0864F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Magnet-IL-Enabled</Property>
	<Property Name="varPersistentID:{02311B37-F10F-4E8D-B8C3-6DEF986B3FD3}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS Error Source</Property>
	<Property Name="varPersistentID:{0293EA90-466E-46C8-B152-DE94D67532E3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-2-WDAlarm</Property>
	<Property Name="varPersistentID:{02B23EB6-A066-44D9-98EF-92E20AD3E078}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PIDGain</Property>
	<Property Name="varPersistentID:{02E3BB14-4FDA-4FD5-8D2B-1AFAB1B83E2B}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_Error</Property>
	<Property Name="varPersistentID:{02F3C2D7-1185-450D-B60A-C6569B6261D0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{0306AF84-1B67-40E2-A12E-BE27D3FD2ABA}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/SV-Lib Path</Property>
	<Property Name="varPersistentID:{032843F9-76D2-4FB0-B2DA-7A7AC2A915ED}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ErrorStatus</Property>
	<Property Name="varPersistentID:{0335CBB6-A608-40F9-A9BE-2F4C060E66B6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ErrorMessage</Property>
	<Property Name="varPersistentID:{039819DA-17D6-4D4C-A35A-8B6BD5A43B9D}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TASCAGC_Flow_2</Property>
	<Property Name="varPersistentID:{03E960D6-D828-4BF6-9CE0-9DA8ADD495DC}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowRange_1</Property>
	<Property Name="varPersistentID:{0429EDA8-2E9B-4112-A674-1B33319589D9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP6</Property>
	<Property Name="varPersistentID:{042E6963-DE35-4121-AACB-004EC5025CF3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_B</Property>
	<Property Name="varPersistentID:{0431E482-7414-4F85-B39E-69DEBECFF5CC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_3</Property>
	<Property Name="varPersistentID:{043CF9BC-6019-4A7E-AC2E-151C966D6FDD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_2</Property>
	<Property Name="varPersistentID:{049C10CE-F5CA-4CF2-B40C-1EB66AC4A250}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{04DC930D-5410-47F9-8E39-C635B26B94C2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ErrorCode</Property>
	<Property Name="varPersistentID:{051E3D78-0985-4997-BB1E-3E19ECCEE81C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_2</Property>
	<Property Name="varPersistentID:{05374178-EB99-4687-A6D8-8ED1E7AD5198}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IL</Property>
	<Property Name="varPersistentID:{05EA7FCD-D590-4184-9E9C-7777E15E61B3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_0</Property>
	<Property Name="varPersistentID:{06517BFB-D902-42DA-B132-FE2A4CE9E87C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PressureSP</Property>
	<Property Name="varPersistentID:{067BEA30-3E34-4F85-95EF-2F03A02D4914}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{06DA108D-F4E5-4855-9820-514BE332266C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{074FBB55-BF1D-4011-A444-9193C3442B0E}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TASCAGC_Flow_1</Property>
	<Property Name="varPersistentID:{0751550E-15D0-4A20-B91F-39A1E16B0908}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ResourceName</Property>
	<Property Name="varPersistentID:{07E85B04-CBF3-4A6C-8BD5-3FFA9F2B581D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/SecValvesBypass</Property>
	<Property Name="varPersistentID:{0840C83D-9E99-4FAE-AA08-C36300F06D37}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{088BC67A-5F7E-46AA-86E0-190E9ABF08C3}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/GUXIDC6.currinfo_0</Property>
	<Property Name="varPersistentID:{08BA8AAF-94B6-4B1C-AACE-F402E0F93A60}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{08C6FF8E-3BF5-4640-9078-45679FA03C6F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_3</Property>
	<Property Name="varPersistentID:{08F9A15B-8DD2-4E4F-88A9-D08BF8A804F6}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Dipole-V-IL-Enabled</Property>
	<Property Name="varPersistentID:{090A2677-B592-46A5-97EF-BF6B3DE162FC}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Length-IL-Max</Property>
	<Property Name="varPersistentID:{0919C8BF-484C-4EC8-9101-FA50EAF91420}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SelfTest</Property>
	<Property Name="varPersistentID:{093DAA0A-2872-41B6-BB23-20AEFBDBA550}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/InterlockStatus</Property>
	<Property Name="varPersistentID:{099064E5-88D0-45B4-9FAC-541FF3867C81}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Selftest</Property>
	<Property Name="varPersistentID:{099C1123-CAA6-431D-94FF-EC3F47AC01B4}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-2_Pressure_3</Property>
	<Property Name="varPersistentID:{09A00C5F-97E0-4513-B8C5-988A0073AC21}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DT3.currinfo_0</Property>
	<Property Name="varPersistentID:{09B5504B-2E46-4DAE-8722-6332099A464A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Decay-Delay-2</Property>
	<Property Name="varPersistentID:{09CD114C-9509-4731-8DE2-B7365DF1316C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_0</Property>
	<Property Name="varPersistentID:{09D0C4CC-1D8B-438F-ACA8-113C78B6F4EF}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Active</Property>
	<Property Name="varPersistentID:{09FECF2B-3FBC-4EB7-A456-80DD306BB519}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_0</Property>
	<Property Name="varPersistentID:{09FFB832-B5E5-41E7-B294-83C100847D1F}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Reset</Property>
	<Property Name="varPersistentID:{0A077153-657F-41E5-AF9E-40B1FBC6D6D3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP2_Status</Property>
	<Property Name="varPersistentID:{0A1CDDF8-064E-4724-8AD7-E27AE201C4DC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_1</Property>
	<Property Name="varPersistentID:{0A2013F3-6CF4-4A73-A44D-94538E5A9041}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1-Proxy_Activate</Property>
	<Property Name="varPersistentID:{0A4B79FE-10CB-424B-AA9B-0FBD81E783B5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ChannelMode_0</Property>
	<Property Name="varPersistentID:{0A6458A3-03E1-445C-A8D4-A20A601E7070}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_0</Property>
	<Property Name="varPersistentID:{0A6E473C-1EA5-4422-89D1-036E16155FE6}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIVV1S.positi</Property>
	<Property Name="varPersistentID:{0AB53160-67E4-4796-BADC-467664F903C0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD12_current_setpoint</Property>
	<Property Name="varPersistentID:{0ADA41F4-2CE2-47E4-B17A-34C8D0B67600}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-V-IL-Enabled</Property>
	<Property Name="varPersistentID:{0B06C0F9-5335-4BA9-8BC7-C8B4EFC59668}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV1_Status</Property>
	<Property Name="varPersistentID:{0B0F0C3B-0C75-4CE5-BD3D-0887F8882EE6}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/PScalerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{0B0F9BBC-40EB-4B94-8216-80D37A03FDED}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{0B288EF9-4102-4FCE-9C40-CD62588496EE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP2_Status</Property>
	<Property Name="varPersistentID:{0B6C2DEC-6BFC-4F21-A725-3896D9C3B32E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_0</Property>
	<Property Name="varPersistentID:{0B6DDCA2-486C-43BC-BE45-A518AFED5A72}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{0B9C342F-DB13-4075-90B1-A83001CB5873}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_0</Property>
	<Property Name="varPersistentID:{0BBE5CC4-BF8A-403A-B3B7-04BBB91E534D}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Virtual Accelerator</Property>
	<Property Name="varPersistentID:{0BC5EEC2-AD8D-42FF-9F66-50FFD10A4B45}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SecValvesByPass</Property>
	<Property Name="varPersistentID:{0BD335EF-ABDB-419B-B533-C6951286111C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{0CF2A045-5D58-4540-B76D-3977A9CE0723}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Det-Rate-Limit-Enabled</Property>
	<Property Name="varPersistentID:{0D065711-FDAC-4592-AB32-312F8F56E229}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{0D11E2FB-7D62-4BBD-A85E-E9E6A4133CB2}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-In</Property>
	<Property Name="varPersistentID:{0D173105-CE5C-4398-BD57-57BC246A0FE1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_1</Property>
	<Property Name="varPersistentID:{0D1E5786-AB6B-4EFD-97D7-72E61A72CF99}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3-Q</Property>
	<Property Name="varPersistentID:{0D373EBC-1040-46D4-BA6B-519D03FA81A6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_SelfTest</Property>
	<Property Name="varPersistentID:{0D39739B-55FB-4E26-890F-6E6466DB110A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4-Proxy_Activate</Property>
	<Property Name="varPersistentID:{0D75AD03-72B2-4020-B78C-424D91D4B52D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_DriverRevision</Property>
	<Property Name="varPersistentID:{0D75D0EC-1443-4A16-B79A-19330C393A30}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SecValvesState</Property>
	<Property Name="varPersistentID:{0D91C2CC-A4E4-4864-89BD-2E57A7F1ABDF}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_DriverRevision</Property>
	<Property Name="varPersistentID:{0E8F465A-28F3-4880-BC8D-A96A4EB29910}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{0EC787CC-C6EA-4A86-A09B-3094FC559FB7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_3</Property>
	<Property Name="varPersistentID:{0ECEE979-C95F-4BC3-A617-04CD4CEECFC9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_3</Property>
	<Property Name="varPersistentID:{0EFEF1F4-C84B-4D17-89C2-27065EA9B9E5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_3</Property>
	<Property Name="varPersistentID:{0F04934E-D924-4E56-9ED1-155BF4B771E6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_1</Property>
	<Property Name="varPersistentID:{0F3776BD-ED80-4086-A192-FAEF1FB1644E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_0</Property>
	<Property Name="varPersistentID:{0F6880D6-2790-4569-AA34-2F79EA19506D}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ExposureTime</Property>
	<Property Name="varPersistentID:{0F69025E-4DC1-489E-8512-9EA3A346470B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_1</Property>
	<Property Name="varPersistentID:{0F787444-420A-4DFA-B5DB-2D8F1A7B1F91}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_0</Property>
	<Property Name="varPersistentID:{0F9621E1-7AF7-414F-B109-95D502C1A024}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_2</Property>
	<Property Name="varPersistentID:{0FB3990E-45CE-4ACA-9413-C2AE0493A91B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Magnet-IL</Property>
	<Property Name="varPersistentID:{0FBCADF2-FA0F-4469-95E6-8679C81492EB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingInterval</Property>
	<Property Name="varPersistentID:{104B9FDD-80F6-4FAC-A9DC-CC3CCCA7FB93}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6.currinfo</Property>
	<Property Name="varPersistentID:{107D7C84-7DE3-4D81-A4DE-005955B79210}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_0</Property>
	<Property Name="varPersistentID:{1089D19C-2FD3-4746-AE48-E97183F4D863}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_0</Property>
	<Property Name="varPersistentID:{10B0CE96-E2CB-44CB-9757-4784A10BDE54}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{10BE3683-2AAC-437B-ABC3-8DDE1F868FF8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pyrometer-IL-Enabled</Property>
	<Property Name="varPersistentID:{115B95ED-8B4E-4492-99C2-3A2F9418E220}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-5_Pressure_0</Property>
	<Property Name="varPersistentID:{11A1C07B-C2B0-41FD-AB3E-16D0208EB68E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_0</Property>
	<Property Name="varPersistentID:{11C08FAB-D967-4035-9F03-396DA12DF895}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{120A4E88-C6A8-49D2-8C8C-BA2CF16E13B3}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-QMP-Max</Property>
	<Property Name="varPersistentID:{1269596B-853D-4047-83F3-07B0028CD81B}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{1289C959-CABC-4BF0-91D7-DB703D996D82}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{128E4C16-7822-4EFD-AB57-5BDB5768179F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_0</Property>
	<Property Name="varPersistentID:{1297F0B0-E9AD-4E5B-836C-E29601593339}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingDeltaT</Property>
	<Property Name="varPersistentID:{12A5CB46-E46C-41B8-9147-434DF273E19C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_3</Property>
	<Property Name="varPersistentID:{1312DEB1-CD5A-4705-9FAF-9EA376A1B03F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{13304E7F-07DD-4D13-98B2-D7DF937932DB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{1397644E-BFDC-4F34-9B54-5D471D209E3E}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_Error</Property>
	<Property Name="varPersistentID:{13C2CF37-E0A0-49B9-9B82-F13B2DE6D8AB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_A</Property>
	<Property Name="varPersistentID:{13F75767-0273-4793-BC13-3CCF2FB06854}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD11.currenti</Property>
	<Property Name="varPersistentID:{141CE144-FA05-4E34-887F-654CF86D80BF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC-Proxy_Activate</Property>
	<Property Name="varPersistentID:{1425773B-FBEC-4625-AEAB-27D20A12D9DD}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Virtual_Accelerator</Property>
	<Property Name="varPersistentID:{14582C87-55E1-412E-8777-65F5B7DC0147}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Error</Property>
	<Property Name="varPersistentID:{14762B06-AAFC-4B9A-B87F-E328AD6F6C2F}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Inhibit</Property>
	<Property Name="varPersistentID:{14C4E229-1D81-4DC6-ADBD-A292EED28FE7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{156006BE-CC69-430E-AD94-DCEE76978C35}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowStatus</Property>
	<Property Name="varPersistentID:{158DF1D6-F497-43FA-98B7-884FB97E80CD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Frequency</Property>
	<Property Name="varPersistentID:{15B14423-8FE6-4334-9093-C7273B938A3F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{15B76679-0662-4309-BF7E-1DEBF73AE568}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP3</Property>
	<Property Name="varPersistentID:{163C5E3F-2B8F-49FA-A81C-07BE2AB5CF55}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PIDLead</Property>
	<Property Name="varPersistentID:{1686608B-4846-4022-84AF-BD40619E0061}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_1</Property>
	<Property Name="varPersistentID:{169EDB05-78CD-46F5-BDF7-6BB51ED0ACA2}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_T-Integrate</Property>
	<Property Name="varPersistentID:{1718F2B2-18F0-4641-AA01-5CD62DC19851}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DipoleCurrent-V</Property>
	<Property Name="varPersistentID:{174580FD-CCFE-44BB-900F-CFC039CD9437}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingTime</Property>
	<Property Name="varPersistentID:{174FAA51-A477-4BAE-8EB3-1099B73DD883}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-MP-Extrema</Property>
	<Property Name="varPersistentID:{1809D9D1-AB76-491F-9CF4-B848D0E89FAE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureUnit</Property>
	<Property Name="varPersistentID:{18B99DF5-F730-46B8-B4C8-99F9DC8F1BEB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-3-WDAlarm</Property>
	<Property Name="varPersistentID:{195183CF-3909-4AF5-B9BF-8B9ED7B6E620}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4.currinfo</Property>
	<Property Name="varPersistentID:{19A05405-105D-4809-9735-8008D3E7612D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-RatTrap-IL</Property>
	<Property Name="varPersistentID:{1A2BF959-64C4-483F-B661-36E9B7193BFC}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-Reset</Property>
	<Property Name="varPersistentID:{1A2C518C-EF3D-4E78-A8A5-0DEB6380CFD5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_3</Property>
	<Property Name="varPersistentID:{1AB0BB13-EF07-4E40-8663-E6868EC62E0A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_UnderrangeControl</Property>
	<Property Name="varPersistentID:{1AB71466-F0B8-4E70-81D6-1D7A0A14D9BB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{1B2546D8-002D-44A7-B6B1-E7D269910263}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ErrorStatus</Property>
	<Property Name="varPersistentID:{1B4B290B-5FC8-4958-A749-A150F00FAEC7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ChannelMode_1</Property>
	<Property Name="varPersistentID:{1B695C8A-48F9-40D7-A80B-6E8D0BD140F0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_1</Property>
	<Property Name="varPersistentID:{1C46555B-4E3C-45E9-AA87-C3DB81886BF0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{1C5847B7-5ECE-4B1C-B3DF-1E0F6519CE13}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FilterTime_3</Property>
	<Property Name="varPersistentID:{1C585BA8-742A-4F8A-AE2D-5642F53550EF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{1CA1F2EA-0FC6-4B4B-9351-5106E4526DBA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingDeltaT</Property>
	<Property Name="varPersistentID:{1D11E061-8068-46FC-BDD7-403635E5BEBB}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{1D656C0D-5A79-4CBE-9EA9-33D176002347}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-Max</Property>
	<Property Name="varPersistentID:{1DD88C86-B5C5-479D-AE1F-E02830C50435}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Error</Property>
	<Property Name="varPersistentID:{1E4B090C-97DA-42AB-8696-3914B271ECCA}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SecKey</Property>
	<Property Name="varPersistentID:{1E75F94F-14BB-4E70-A5E1-254BA1CCAA4B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingIterations</Property>
	<Property Name="varPersistentID:{1EB27F90-E873-41CF-80E5-CA37C182C172}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{1ED8CACF-B081-4986-A924-C2BB9C6D6865}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{1EE1D8C6-D410-4883-9ACB-6701C651671D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_-1</Property>
	<Property Name="varPersistentID:{2060A0C3-63B0-47EF-B631-0AD3FFF45731}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Charge-IL</Property>
	<Property Name="varPersistentID:{20BF370C-0B0D-45F9-8FDC-214E06DF3CEB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{21050F52-CE29-403C-9BAD-05A18C33ABF7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_2</Property>
	<Property Name="varPersistentID:{215DFDCE-68FB-42F1-88F9-9A82B92EB0DF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingCounter</Property>
	<Property Name="varPersistentID:{21B4ADE1-7A65-45E2-8BF5-41D9E607B83A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_-1</Property>
	<Property Name="varPersistentID:{21B6A572-3B68-4E7A-A3E5-D7284E794F8A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{21E57ADF-2226-42B8-9B30-A5AC107A3BDA}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMILProxy_Activate</Property>
	<Property Name="varPersistentID:{220FA8D9-4BE6-49D4-835F-DB5EA0B7643B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_3</Property>
	<Property Name="varPersistentID:{22DE7C18-70E2-4A5D-86F8-32215473D407}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/GUX8DC2.currinfo_0</Property>
	<Property Name="varPersistentID:{234FFEA6-135D-414F-8215-C09E9D68ABAE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_1</Property>
	<Property Name="varPersistentID:{235054CF-8BE9-42C5-832D-759EB2C108A1}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Laser</Property>
	<Property Name="varPersistentID:{23EC430D-1BBB-4AEF-907B-B878A397E5D4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_SelftestResultCode</Property>
	<Property Name="varPersistentID:{24034B26-6CF5-44D0-B3C3-A44E88394EC8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{24AD96F6-03BB-4948-B265-F153D1B1FA9E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Reset</Property>
	<Property Name="varPersistentID:{24D991DD-7957-4ECA-86CD-50BA902A958D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureSP</Property>
	<Property Name="varPersistentID:{24E4261D-0D85-44D9-A054-A17AE4EFA312}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{2516E7A4-BBB8-4249-830E-25AC17DCEE98}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MainValve</Property>
	<Property Name="varPersistentID:{2562B12E-DEB4-4D4E-81E7-95AF798C6C6B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{257DF761-E481-4BDD-8CD6-9FEDC5CFD4F7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_3</Property>
	<Property Name="varPersistentID:{26865A83-8C21-4AC6-8BC2-D80C0FA49D89}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffsets</Property>
	<Property Name="varPersistentID:{268B4FC5-987F-4C09-94D3-B8D0CBF1270C}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_ErrorCode</Property>
	<Property Name="varPersistentID:{26DAA29D-982A-44F7-9BFA-938EE349630E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Time-BeamOff</Property>
	<Property Name="varPersistentID:{26F29E89-C39D-459F-A8B6-E4BD8136F32A}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActorProxy_Activate</Property>
	<Property Name="varPersistentID:{26FD8166-3C14-4119-BF27-1714DA4A5069}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingInterval</Property>
	<Property Name="varPersistentID:{275128BD-1C47-46ED-AE71-D159F6E8B11D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ZeroAdjustMFC</Property>
	<Property Name="varPersistentID:{275F7089-58DD-4462-A190-207F50720A3C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_2</Property>
	<Property Name="varPersistentID:{2769AF02-6B32-4E5F-A428-548C93326D5A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Logging_Enabled</Property>
	<Property Name="varPersistentID:{27951049-C168-4FE7-8EAD-F04F274B2094}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{27D6AB0A-C919-4AB9-93EB-851303C91120}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_PollingInterval</Property>
	<Property Name="varPersistentID:{28215AA5-9850-45F6-9567-376DD2B59293}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Reset</Property>
	<Property Name="varPersistentID:{283D7505-76F2-4D8D-B5EF-68FB1FEACD5F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{2845593A-4D66-4244-8F24-89777D641EA1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_1</Property>
	<Property Name="varPersistentID:{2896BD6E-CC14-4CBF-9FFE-E991A07BB835}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_0</Property>
	<Property Name="varPersistentID:{28CCCF7E-2D8B-4F58-B9D9-EEE8C8689642}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Max-QMP-Limit</Property>
	<Property Name="varPersistentID:{28DE14D4-DE2C-4028-9CEB-12A4C67F89D8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_A</Property>
	<Property Name="varPersistentID:{2936FA07-E8DB-4B48-A6D9-7C83742AD67D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_DriverRevision</Property>
	<Property Name="varPersistentID:{294232EF-C70A-4E97-85AE-9E3470A3A6E7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_2</Property>
	<Property Name="varPersistentID:{2963F823-690B-4550-9D00-CA519F390336}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{29A4D12A-F0E0-478C-ACC9-5759E8B1AA4A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_0</Property>
	<Property Name="varPersistentID:{29D13576-D21F-48A4-A346-ACF416B8DDA9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Pyrometer-IL</Property>
	<Property Name="varPersistentID:{2A2418E2-29FA-40EE-8616-554DBE692A23}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_2</Property>
	<Property Name="varPersistentID:{2A295144-064A-4EF9-8727-CD171F40EF0E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_1</Property>
	<Property Name="varPersistentID:{2A37D77D-ACA6-47FC-B06A-7C405D4424BF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_3</Property>
	<Property Name="varPersistentID:{2A41FD62-9C1E-4D29-B122-AB9EE3380060}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_2</Property>
	<Property Name="varPersistentID:{2A5C0FB0-9F38-4510-85CD-716F2A4DEBFB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_1</Property>
	<Property Name="varPersistentID:{2A6F6133-FDCC-4893-B4F5-08631B964D9C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF</Property>
	<Property Name="varPersistentID:{2AB05B6B-D266-4247-919A-3791F4275028}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_T-Pause</Property>
	<Property Name="varPersistentID:{2AB4C7BA-5C3B-417E-B35F-D2B0CD660B0D}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-5_Pressure_3</Property>
	<Property Name="varPersistentID:{2AC9B13E-F3A6-43AB-B5A8-35AA73786107}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Interlock</Property>
	<Property Name="varPersistentID:{2ADAE2B4-E367-454E-90EF-EA969C1A939E}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/GUXADT2.currinfo_0</Property>
	<Property Name="varPersistentID:{2BC389FC-C104-46BF-B018-BC160372BC75}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-ExposureTime</Property>
	<Property Name="varPersistentID:{2BEC4B90-0746-4F1A-805D-E0A2852CCB87}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FirmwareRevision</Property>
	<Property Name="varPersistentID:{2C022994-7603-4559-83BB-33474F0CF12D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-Min</Property>
	<Property Name="varPersistentID:{2C2F2B13-76BC-42B5-9379-235BD50A5946}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Error Message</Property>
	<Property Name="varPersistentID:{2C4AC0F2-B37F-4810-B668-6D187F642D13}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FirmwareRevision</Property>
	<Property Name="varPersistentID:{2C5BC1E7-E9DA-4251-A9C6-1EA86F10D213}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ErrorMessage</Property>
	<Property Name="varPersistentID:{2C93408E-6B28-46CA-AC3C-50060BC0AB4E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Reset</Property>
	<Property Name="varPersistentID:{2CD99D5E-EFE8-4980-AA2B-56426C1C0DB4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{2D1500F6-45EC-40DB-95A2-70532F2340B3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{2D366ED6-BE4D-4054-87F2-E15EEAB322B3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_3</Property>
	<Property Name="varPersistentID:{2D7718CE-E037-468F-9DF1-3451CCB55146}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FirmwareRevision</Property>
	<Property Name="varPersistentID:{2EF766BC-0913-4F24-9A1D-CE1F37D6B651}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{2F024D23-BDB4-4926-91DB-54BB9CE68699}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SelftestResultCode</Property>
	<Property Name="varPersistentID:{2F6334E1-23F6-4BF7-91EC-E4BCAE12A048}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Charge-IL</Property>
	<Property Name="varPersistentID:{2FAE4580-DC10-435B-8085-0551BAE4D011}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingMode</Property>
	<Property Name="varPersistentID:{30423815-CBAA-4F8F-B9E0-120CE32B9447}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_1</Property>
	<Property Name="varPersistentID:{30A6FD25-4230-4D4F-8CF9-4F0EB07D0AE7}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/GUT2DCX.currinfo_0</Property>
	<Property Name="varPersistentID:{30C4150E-6637-4753-A40C-ED93C997DEB1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Error</Property>
	<Property Name="varPersistentID:{30E2871A-FDEB-4323-B471-C373D1248DC1}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Counting-Reset</Property>
	<Property Name="varPersistentID:{31809EA7-42A7-426A-8591-D68BD6418D15}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{31A77F8B-5E7A-4C01-A59A-C2AB6EB8F748}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VacuumBurst</Property>
	<Property Name="varPersistentID:{3241A831-CC88-4711-9832-092E532A708E}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD12.currents</Property>
	<Property Name="varPersistentID:{3270AB1A-E395-48CF-ACA8-645B3A26EAD9}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{3296B444-4D48-4962-B38B-36254C3300EA}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingInterval</Property>
	<Property Name="varPersistentID:{329C7615-D61A-4A42-A49D-5E83A4E8B51A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_set-position</Property>
	<Property Name="varPersistentID:{32B14FA9-6104-45DD-902E-D19D0383960A}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{3332D223-9BEB-4DF0-807D-3300460C9BC0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_0</Property>
	<Property Name="varPersistentID:{333F7B01-48BF-404F-A73F-20333B2F78BD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{336FAF73-3CA3-42A2-B37A-F3228EFF2DB4}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{337D70CE-4F53-4E8D-9237-A03E2F3B22B5}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/InterlockReason</Property>
	<Property Name="varPersistentID:{3387D102-300A-4C00-914A-FB368B277E5B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_3</Property>
	<Property Name="varPersistentID:{338B4E06-343B-45EE-BBDB-B62474BD3BDD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingIterations</Property>
	<Property Name="varPersistentID:{33C9529E-7CC5-44A0-9629-DD86ADB27F38}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_0</Property>
	<Property Name="varPersistentID:{33ECA93A-C86E-4FCD-88FB-2EEE68FFF2FE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UT2DCX_position</Property>
	<Property Name="varPersistentID:{3402F70D-DB51-4B7B-9547-61DCB2524249}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_SelftestResultCode</Property>
	<Property Name="varPersistentID:{343881E0-4690-4CC0-B74F-9274F7F8B6FC}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_0</Property>
	<Property Name="varPersistentID:{3461D587-C0E7-4ABE-B9B4-59080889F5D5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_0</Property>
	<Property Name="varPersistentID:{34A2BDBB-D07A-42E7-B482-C5AE16C99DE9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC-Proxy_Activate</Property>
	<Property Name="varPersistentID:{34A7AD72-B79B-48DC-B069-B9EDD9FDB605}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_VacuumBurst</Property>
	<Property Name="varPersistentID:{35395DC7-FF70-458C-AB71-8FD0D211F176}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_1</Property>
	<Property Name="varPersistentID:{35736D88-3B9B-430B-862C-E7F7A69EEB51}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{3590DA2D-ACDB-45FC-A44D-AE43C7A4DABF}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_PollingTime</Property>
	<Property Name="varPersistentID:{35CF978A-347C-4826-93CA-39F756EB487A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-Counter</Property>
	<Property Name="varPersistentID:{36173E6D-DFCA-4DAB-AFA6-7B06A9243C40}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandlerProxy_Activate</Property>
	<Property Name="varPersistentID:{361AF782-4DBC-4DBF-AE8A-DAAA6F2DE8CD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SetID</Property>
	<Property Name="varPersistentID:{36CC3130-70D4-44E4-940F-A259695CF4D3}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-AmbientTemperature</Property>
	<Property Name="varPersistentID:{36CCA309-E99C-4329-B738-F8650177B234}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_PollingIterations</Property>
	<Property Name="varPersistentID:{37048A9A-B533-4BAB-8A3D-1D4B82A731C2}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/String Size</Property>
	<Property Name="varPersistentID:{375127C1-7D5D-479A-B1A1-6196CB3825F6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ResourceName</Property>
	<Property Name="varPersistentID:{37B4B60F-419D-4970-9CFB-CAC25F523890}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{382C49B9-7CB0-46C8-BBF5-0A765D54038C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_2</Property>
	<Property Name="varPersistentID:{383D9827-6AE6-4823-BD76-9307AEAA4A8E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_2</Property>
	<Property Name="varPersistentID:{38791812-2804-4F48-8B3F-66FDFE6704DB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_2</Property>
	<Property Name="varPersistentID:{38D08336-521A-4322-9B1B-46459CD540E0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP4_Status</Property>
	<Property Name="varPersistentID:{38D7C20E-88A2-4823-A24D-4F2E6A008731}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_ErrorStatus</Property>
	<Property Name="varPersistentID:{396731F7-2C1C-474E-B1C8-C61BDABD647E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingCounter</Property>
	<Property Name="varPersistentID:{3989A7D8-E435-4816-9428-38EBAEFF264B}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-2_Pressure_0</Property>
	<Property Name="varPersistentID:{3A3E438D-7399-409E-8304-9F76BE9F9E37}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{3AA54458-539E-4024-9156-93F940ED8348}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD11_current</Property>
	<Property Name="varPersistentID:{3AAD2DFD-98C4-493D-804F-905001FF9CA2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP5</Property>
	<Property Name="varPersistentID:{3AD23582-521E-4F19-B081-1267452891A9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Max-DetEvents-Limit</Property>
	<Property Name="varPersistentID:{3B5D110C-2D5A-42A8-B677-329421D500E6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{3B98C89F-1589-4E52-8D97-2BD46741AA6D}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{3BC09368-99C0-4ACC-B8C6-076254D70C32}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_2</Property>
	<Property Name="varPersistentID:{3BC55516-B562-49D4-B8E5-1B71462C5C83}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_current</Property>
	<Property Name="varPersistentID:{3C0DA121-6096-4299-A69A-F68936D471F3}" Type="Ref">/My Computer/instr.lib/IGA140.lvlib/App/Temperature</Property>
	<Property Name="varPersistentID:{3CB9CEBC-FAF6-4782-9CAD-4F0707290465}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_2</Property>
	<Property Name="varPersistentID:{3CC75EB3-D47A-4B61-B9C8-EFE0FE9E8879}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{3D365B20-8D7D-44E9-9BC6-5BCBFF1EC81B}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_ErrorMessage</Property>
	<Property Name="varPersistentID:{3D62354E-F4E2-4CDE-95A0-750B9FAC1415}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_3</Property>
	<Property Name="varPersistentID:{3D86B80E-0C29-4103-A270-A32874CB08C8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{3DAC2C1A-BD4C-481B-97EA-CF4A746BAC11}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXADT2.currinfo_0</Property>
	<Property Name="varPersistentID:{3DE2976E-5551-4468-A5A6-A00E2F295DDC}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_ErrorCode</Property>
	<Property Name="varPersistentID:{3DF2674F-77EB-45FD-ABF4-7764BD613EFA}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Reset</Property>
	<Property Name="varPersistentID:{3DFCB94D-218F-4C0F-A39A-8EA777F156DF}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_3</Property>
	<Property Name="varPersistentID:{3E059C13-DB52-4791-9E4E-E7694A6A7450}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_PollingInterval</Property>
	<Property Name="varPersistentID:{3E4B7B82-4D05-4375-961E-0DA5922E560E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_0</Property>
	<Property Name="varPersistentID:{3E83DFC0-09F3-49EC-ABB1-E0E4F988418D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{3EB7C99A-1AEA-40A6-8A91-3996AD846FE7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ZeroAdjustMFC</Property>
	<Property Name="varPersistentID:{3F1747F4-32EC-42DC-932F-16AAFF42C804}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF_2</Property>
	<Property Name="varPersistentID:{3F558A8F-37A1-47A1-B084-04D8081FC04D}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4.currinfo_0</Property>
	<Property Name="varPersistentID:{3F57023C-8E35-4DB5-A819-4C4E0156C64C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{3FF17D83-B468-4BF1-A848-B24C4A89BDEC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{403CF8FF-A587-4162-B694-32FDE1EEFDAF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_1</Property>
	<Property Name="varPersistentID:{40D0141F-83B3-4E18-ADCB-0C43F53C1A8F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP3</Property>
	<Property Name="varPersistentID:{4109EFC9-2BDB-477F-9F75-79AA64CCDC7F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_3</Property>
	<Property Name="varPersistentID:{415F45EB-7143-4198-97C3-C52E3A9428C0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Targetwheel-IL</Property>
	<Property Name="varPersistentID:{419FD404-DB7E-41A5-B342-7FF7DC3DE180}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_1</Property>
	<Property Name="varPersistentID:{41BD1FC2-D261-4CF4-9A3E-A205796B52A8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{421C3296-A2BF-43C4-B0E6-D9976D60E847}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{422C49B2-8C49-4D27-88F8-0901747EE2C6}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Dipole-IMax</Property>
	<Property Name="varPersistentID:{42383A59-7234-4863-95FB-32CA0B526F77}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ResourceName</Property>
	<Property Name="varPersistentID:{42683878-31BC-4519-A773-411C7C53AC32}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_0</Property>
	<Property Name="varPersistentID:{430F46E2-7CB0-4AE6-AA7D-88B1D99ABC1F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{43776265-AF33-411F-94FC-8CA285CF4E6A}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2.currinfo_0</Property>
	<Property Name="varPersistentID:{440DD52F-0B1D-406B-BE39-8BDDE982EDCA}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_TSRMin</Property>
	<Property Name="varPersistentID:{44412ED3-C795-443C-BD4D-78410C901794}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{445F714B-0E6D-4414-A164-A17A3A642BB8}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_ResourceName</Property>
	<Property Name="varPersistentID:{4489B737-9124-4650-B342-36FB88ADCA60}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{45458B9E-D09E-4D0A-8867-A62F9C30F800}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_2</Property>
	<Property Name="varPersistentID:{45517BCA-A95E-4F5C-A3D5-7BD60CE6FA37}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_3</Property>
	<Property Name="varPersistentID:{456C655F-7305-4F3A-AFFB-BE8C9F84CEC3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV1</Property>
	<Property Name="varPersistentID:{45AD4EF3-E04C-49D5-869B-98D9C22A68A7}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DipoleCurrent</Property>
	<Property Name="varPersistentID:{468DF90B-62AD-4A3C-917A-C7A9F1FC9440}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DetectorRate</Property>
	<Property Name="varPersistentID:{46B1F1F9-FDFB-4D80-9679-AFFCA94C8FC8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_2</Property>
	<Property Name="varPersistentID:{47113C2B-F8B5-46BA-A8FC-90FB1AA82C7E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ChannelMode_0</Property>
	<Property Name="varPersistentID:{471CE587-6295-4913-B634-CBCBC1703A12}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{476019D9-D947-4D04-BE10-6E7BBC8E3C1B}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_Error</Property>
	<Property Name="varPersistentID:{47F788DE-870F-4A90-847F-BA62A2B7C86B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_FirmwareRevision</Property>
	<Property Name="varPersistentID:{4812EC1F-16DC-4D19-90CA-12549AE3B6A5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{488F4F40-917D-486A-91E3-3B891858599A}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/PScalerProxy_Activate</Property>
	<Property Name="varPersistentID:{49134E6B-70C6-4DF3-A9C0-D78720C65228}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV3_Status</Property>
	<Property Name="varPersistentID:{494C5CA8-CA14-48A9-A453-FDDB745E4F82}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_2</Property>
	<Property Name="varPersistentID:{496996F8-15E7-4178-8F62-A1166A110092}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{4974D095-285D-4C4B-B38D-27C805A921D9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ResourceName</Property>
	<Property Name="varPersistentID:{49893F5B-48EA-4FB3-B1A8-C8B3A026D7BD}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{49A49DF3-F321-4F8D-A268-AD13EA1803E1}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6.currinfo_1</Property>
	<Property Name="varPersistentID:{4A20A03C-B3EE-425C-88E2-C4A18D643ABD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Length-IL-Min</Property>
	<Property Name="varPersistentID:{4A539420-E902-464C-8FA0-F01F7C6ED835}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Counting</Property>
	<Property Name="varPersistentID:{4A8CA78E-A08E-44E1-922E-D7240D72D603}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IMin</Property>
	<Property Name="varPersistentID:{4AC1F080-406A-4908-82D2-8A16A74FE10C}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{4AEAE6B3-F87D-44AE-A71A-034434759D3B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_0</Property>
	<Property Name="varPersistentID:{4B47536F-672C-4D93-9BAD-74886CC07B2E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_1</Property>
	<Property Name="varPersistentID:{4B494A4C-8B1C-4111-BD7B-3C47533F37D0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pyrometer-WDAlarm</Property>
	<Property Name="varPersistentID:{4B4D1C39-E762-45B7-BBA5-630AE3DBB20F}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/Watchdog_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{4B785B95-109A-4F3B-BE8F-9F37EABE0AA7}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Dipole-IMin</Property>
	<Property Name="varPersistentID:{4BA11262-E676-434E-969B-06497B303026}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Pressure_0</Property>
	<Property Name="varPersistentID:{4BC8A957-76F7-4622-9F93-623464B82612}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_2</Property>
	<Property Name="varPersistentID:{4C2C69A2-84F9-4F23-808A-CF4C2F1C021E}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-3_Pressure_2</Property>
	<Property Name="varPersistentID:{4C4E2433-18AD-4741-86D9-258802AAC3F0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3_range</Property>
	<Property Name="varPersistentID:{4CCA4DDD-6262-46B7-8269-9471B2261790}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{4D153858-5FAB-4C36-BF77-B3A18D53ACC5}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Counting_Enabled</Property>
	<Property Name="varPersistentID:{4D4F2984-DAC2-4D0D-84AE-8EB9F35127B8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_0</Property>
	<Property Name="varPersistentID:{4D6E4C16-4FA1-47E0-A651-82A700811D4B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow2_TASCAGC</Property>
	<Property Name="varPersistentID:{4DA8A600-4DE0-4FDA-8677-16ACF26A8AFE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{4DBEEA47-B80C-45CC-8C16-F2C0C21B7A19}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingIterations</Property>
	<Property Name="varPersistentID:{4DECBD30-B6E0-47FC-B15B-B886FFC244D5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureMode</Property>
	<Property Name="varPersistentID:{4E5CAA9B-BC88-4E42-B07A-183207FCF2C8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP2</Property>
	<Property Name="varPersistentID:{4E5F0136-3C8C-4610-A4E1-499C1822DF71}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4_P.positi</Property>
	<Property Name="varPersistentID:{4E76D16A-FBE2-4EFB-B2D2-29DC562353D8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{4F30EE4D-66DE-4ADA-AE5F-FA096F7EC3D3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Error</Property>
	<Property Name="varPersistentID:{4F69C75F-46B1-43AB-A298-8CB582572C89}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{4FC095FB-ACA0-4345-8302-28DD47A6775F}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-TO</Property>
	<Property Name="varPersistentID:{500F6CDA-E271-4F85-974A-0D7EB4B88D04}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowRange_3</Property>
	<Property Name="varPersistentID:{502730E1-CCE7-41D3-9FBC-7893AE34591F}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-3_Pressure_0</Property>
	<Property Name="varPersistentID:{50669BEC-36E1-4CC6-9787-FE73E4C00EC9}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS Error Message</Property>
	<Property Name="varPersistentID:{508D2213-2FE7-41CE-8DA5-2D7E1300051F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ErrorStatus</Property>
	<Property Name="varPersistentID:{50933438-C602-4882-B307-607786A342B4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingDeltaT</Property>
	<Property Name="varPersistentID:{50EC3FA4-3602-41B1-B5B2-9871A4FD79EE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL-Main</Property>
	<Property Name="varPersistentID:{522DE7DB-6695-47B6-9108-B414897E9874}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ChannelMode_2</Property>
	<Property Name="varPersistentID:{52A6DDAC-B21F-4DE7-AA07-7CE5FBDFF158}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{52B4B734-F126-494D-874F-212C1A73BA62}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL-Main</Property>
	<Property Name="varPersistentID:{52EDD6A6-2CD9-403B-A882-7116D3043382}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2_P.positi</Property>
	<Property Name="varPersistentID:{530486AB-3A02-4F78-9BC7-0B3C7BF1D54F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_1</Property>
	<Property Name="varPersistentID:{530B6BAE-DE30-42D2-9592-B5DE67AF8F7C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_2</Property>
	<Property Name="varPersistentID:{534FBBAE-1E10-4FE7-BD16-80EB4D9FD606}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ErrorStatus</Property>
	<Property Name="varPersistentID:{5391C4D0-8B3F-47D4-8195-7DB16B433265}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Chopper-Out</Property>
	<Property Name="varPersistentID:{53A2D805-F14F-4691-9553-D79B078EAD33}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingDeltaT</Property>
	<Property Name="varPersistentID:{53A628EA-3BFA-4874-9ED5-B6FD281E2D69}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{53D1BBC8-66CF-4EA6-8688-F6DE03A1F74E}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/GUX8DC4.currinfo_0</Property>
	<Property Name="varPersistentID:{5437DDFC-00CC-41CD-9347-491B51CE8EB6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Interlock-Reason</Property>
	<Property Name="varPersistentID:{544BF60A-C272-4B06-921E-5944788DDE1A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_0</Property>
	<Property Name="varPersistentID:{54981DBF-1E93-45BF-B026-9D4A97732C20}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Time-BeamOn</Property>
	<Property Name="varPersistentID:{54CF0984-1B91-430C-8EE5-4D291B6B3314}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_SelfTest</Property>
	<Property Name="varPersistentID:{54D513C2-2EB3-4148-BFA5-B5834F10D760}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2_range</Property>
	<Property Name="varPersistentID:{54FF58E7-20AD-4049-B7D5-C0357ADCFEDF}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SelftestResultCode</Property>
	<Property Name="varPersistentID:{5503681B-6D09-4BC1-AB75-DCE4E430BF21}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_2</Property>
	<Property Name="varPersistentID:{550E060C-AD37-464C-ACDD-91D138CAE0F3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SetID</Property>
	<Property Name="varPersistentID:{566CECD3-4150-4757-A35C-16E5C4E68651}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_3</Property>
	<Property Name="varPersistentID:{567C7D0A-E5A9-4D4A-9BA4-21F966420615}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TASCAGC-WDAlarm</Property>
	<Property Name="varPersistentID:{56D64AB9-F423-4507-872C-711A854E2397}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{56EEB21B-642D-48CD-8660-3D4E1E6BAB18}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_1</Property>
	<Property Name="varPersistentID:{56F86615-2856-41DA-BEBD-29F61FE17F03}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_PollingTime</Property>
	<Property Name="varPersistentID:{56FD722B-220B-4268-A410-1631FADA0063}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_T-BeamOff</Property>
	<Property Name="varPersistentID:{57037E1E-755B-4374-85CD-FD88A0608C1A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_DriverRevision</Property>
	<Property Name="varPersistentID:{57439F8B-1F58-41DC-BC0A-23691720DB93}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period</Property>
	<Property Name="varPersistentID:{5781E6E6-1412-4AAC-99B8-0C6878E24587}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS</Property>
	<Property Name="varPersistentID:{5816119C-E70F-49E8-9CC3-4FED3229F02F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{5831D6F5-05DF-4028-AAEC-5EE90491CBE2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_UnderrangeControl</Property>
	<Property Name="varPersistentID:{5843AD46-A916-43AE-8E37-3CCCBD236E14}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Reset</Property>
	<Property Name="varPersistentID:{584810D5-21B9-4F40-B350-0757D203AB97}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXIDC6_position</Property>
	<Property Name="varPersistentID:{58A090F3-3C09-4718-B9E1-4F26BCDEF878}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ErrorCode</Property>
	<Property Name="varPersistentID:{58FFCB7B-2199-4EA3-8DB6-603B04CD5230}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_2</Property>
	<Property Name="varPersistentID:{59AF0566-8BA5-490F-ACED-35479F9F8BB1}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8MU1.currents</Property>
	<Property Name="varPersistentID:{59BA75D5-5D24-4AF4-A77B-7DBB377FC680}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{59C95D81-9AF0-470E-88F9-1120FDF4B071}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_3</Property>
	<Property Name="varPersistentID:{5A2AF746-3B7B-4152-B0EC-C3944ACFB9E4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_0</Property>
	<Property Name="varPersistentID:{5A5C5F54-49B7-4052-A30D-55E41736ADD7}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Trafo-Frequency</Property>
	<Property Name="varPersistentID:{5A9081F5-E5AD-484A-95ED-A8FEFF001540}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS Error Code</Property>
	<Property Name="varPersistentID:{5B53BF4B-A517-4ED1-BA39-4DABC36EC45A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{5BB9D871-1AAB-49EB-B7C3-EA94BA3F61E8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow0_TASCAGC</Property>
	<Property Name="varPersistentID:{5BBB2A6C-B645-4623-A30B-536B209DFCE5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SetID</Property>
	<Property Name="varPersistentID:{5C305A38-2FBD-4D1B-8CFF-A9B54F264F07}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_0</Property>
	<Property Name="varPersistentID:{5CE01724-DDBF-4A9F-9E61-54E9DA270B54}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Error</Property>
	<Property Name="varPersistentID:{5D067F99-FD04-40E7-8F07-3CF3FE6D0F7A}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingCounter</Property>
	<Property Name="varPersistentID:{5D137D23-31E1-4593-B41A-4832110F92F0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{5D2A032F-5FA6-4529-AC2E-A6BDF06F554C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{5D5DB3C8-EDAB-4F52-B479-B829889F82D8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2-I</Property>
	<Property Name="varPersistentID:{5DAE450B-C653-4DB0-BD9B-26BC68A42D44}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_TASCAGC</Property>
	<Property Name="varPersistentID:{5E0554B7-1233-4C70-8912-8568B2493FF8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ErrorMessage</Property>
	<Property Name="varPersistentID:{5E5BE896-D80E-405B-8CD0-772E0A5A45D8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowRange_1</Property>
	<Property Name="varPersistentID:{5E7C8DD9-646D-43F3-BC75-8511F4C3DBCA}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV5_Status</Property>
	<Property Name="varPersistentID:{5E9CE6BC-4696-4ED0-AB93-171009C16CF6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{5EF59605-B04C-41F3-865A-3B1918A37160}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-MP-Length-IL-Max</Property>
	<Property Name="varPersistentID:{5EFAF0F3-60F2-4675-A7BF-EF8539E92997}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_3</Property>
	<Property Name="varPersistentID:{5F018252-002C-4BDA-AC51-3EF788655177}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8MU1.currenti</Property>
	<Property Name="varPersistentID:{5F1C2E4A-557C-4ED7-9C5D-9D9AF7F6922E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{5FA1258D-E5C2-48DD-893A-15BDB2927837}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/RTCGC-WDAlarm</Property>
	<Property Name="varPersistentID:{60182CBE-4FAF-4A6D-A449-BAB3D232124E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_current</Property>
	<Property Name="varPersistentID:{60408B6D-79B9-46FB-8283-257E8C03C03E}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ErrorCode</Property>
	<Property Name="varPersistentID:{605CF7C4-EB40-40FC-978F-C1611D7D02E2}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-V-I-Mean</Property>
	<Property Name="varPersistentID:{60815898-FD43-4FFA-A9D5-C8A78AA5C6CB}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_PollingMode</Property>
	<Property Name="varPersistentID:{60C15894-A04F-4648-AA07-79895ACFE199}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ResourceName</Property>
	<Property Name="varPersistentID:{61576981-E36F-4E3E-92B5-15EED99753E0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowRange_0</Property>
	<Property Name="varPersistentID:{6170FAFE-4AA2-4B84-B14E-487503B0D200}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_2</Property>
	<Property Name="varPersistentID:{6174E195-FCEA-43CB-B4A1-E32D1DA0CE50}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingIterations</Property>
	<Property Name="varPersistentID:{618F2696-A84F-4F7A-BE8B-787AA9E63F19}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{619A497F-C617-4F63-AEB6-C8B2229F97DA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{61BCD75D-8E2B-49A3-A348-7B1E8BE3B4B2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP4_Status</Property>
	<Property Name="varPersistentID:{61FE32AD-F2CA-41FB-A389-524D7DA35A5A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureOffset</Property>
	<Property Name="varPersistentID:{6204AC7A-9518-49E0-BF3A-5D2851161BD0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP2</Property>
	<Property Name="varPersistentID:{6206B496-80E4-4468-BE7F-A78C4D6F4C40}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_2</Property>
	<Property Name="varPersistentID:{627FF069-71FB-4BF8-8D2F-91D10CB5008A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Error</Property>
	<Property Name="varPersistentID:{62ABB978-B42A-4B24-852D-BB3C1BF1A45D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{63499973-C15C-42BA-9982-83DC923658C4}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ErrorMessage</Property>
	<Property Name="varPersistentID:{63594AF2-EA43-484B-AC61-0EAACA46F812}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-1_Pressure_2</Property>
	<Property Name="varPersistentID:{635A589F-F8AB-47DC-844C-69629F372746}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_2</Property>
	<Property Name="varPersistentID:{63AB6159-2193-47C9-836D-83CC7C7C1881}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Delay-1</Property>
	<Property Name="varPersistentID:{63DD12A9-28E4-41A9-BC28-5B6150F92BE7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_0</Property>
	<Property Name="varPersistentID:{64057820-93A8-43CD-92D0-F69F01661C55}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingCounter</Property>
	<Property Name="varPersistentID:{64377139-9DF0-43D3-AAB7-F1FC78FF763D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_1</Property>
	<Property Name="varPersistentID:{64410B63-A667-4AA3-A66A-4D32FA14FFC2}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period-Reset</Property>
	<Property Name="varPersistentID:{648DC492-9CEF-4EE5-A49C-B0ECD2BA48B8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{649D5300-B0EF-46BA-A8F8-7F7093CEBFBE}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8MU1_current</Property>
	<Property Name="varPersistentID:{6501B713-20A2-4F88-B28C-6996B21C5A44}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Reset</Property>
	<Property Name="varPersistentID:{650E7C1F-438E-46E4-BA91-DE75993B0E3F}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActorProxy_Activate</Property>
	<Property Name="varPersistentID:{657F679E-9358-48AF-89E5-E5B4032B1B7D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FirmwareRevision</Property>
	<Property Name="varPersistentID:{6594B9A4-00B2-41F6-94CD-E1AC78587394}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_FirmwareRevision</Property>
	<Property Name="varPersistentID:{65959725-8C5C-428C-86BF-07D48A91278A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_0</Property>
	<Property Name="varPersistentID:{659FB644-0CD2-4D8B-80CE-DB866427E817}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/Watchdog_PollingIterations</Property>
	<Property Name="varPersistentID:{65C2132D-8A28-4B86-8729-C5059EF5376D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{65F6204D-CCDD-4551-8620-24596BF52292}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PressureUnit</Property>
	<Property Name="varPersistentID:{6650C7CD-B975-4F9B-A7CD-AD46E91BB281}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingTime</Property>
	<Property Name="varPersistentID:{666FBA33-3BBA-4C3A-9604-E3EAC8CFD127}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Det-Rate-Limit-IL</Property>
	<Property Name="varPersistentID:{6718043C-5DCC-40B9-BAD9-3E71C652D8D6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_3</Property>
	<Property Name="varPersistentID:{676DD409-B0AF-4918-87F4-FBD407A8AA3C}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{67863258-709A-402A-BB08-2CC0D3C9D89C}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_ErrorCode</Property>
	<Property Name="varPersistentID:{67CF5334-CBEE-4F16-8602-8422603C0E2E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Counting-Enabled</Property>
	<Property Name="varPersistentID:{67E61605-1262-41BB-BFCC-A735C75269BA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{68102663-5212-4664-8DFF-D619D1CD860A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DMA_Enabled</Property>
	<Property Name="varPersistentID:{682DBE6F-77FA-41D4-94CE-0904725708F5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{687F218D-6EF9-4EE3-A3E7-86AF7FF79445}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{68A0A571-C061-4662-BE65-2DCC94CFD85F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/RatTrap</Property>
	<Property Name="varPersistentID:{68D86022-2571-40DB-B6E6-0454479F5A26}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Chopper</Property>
	<Property Name="varPersistentID:{693BD4A8-3E8C-46AF-9CDF-043CBFE183EB}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2.currinfo</Property>
	<Property Name="varPersistentID:{6943142D-FE71-4543-ACDD-1B3256A0182B}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2_P.posits</Property>
	<Property Name="varPersistentID:{6948AF3B-3EB1-4607-895D-11BA8ACAFF0F}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{699897FE-BA82-4D4E-9860-5944803E450F}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV2_Status</Property>
	<Property Name="varPersistentID:{69C65DD2-D449-4B5B-A454-049271BE5D09}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_2</Property>
	<Property Name="varPersistentID:{6A7F0A57-CBFB-4A75-9841-0EC596188467}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{6A8EE2AB-FC6F-4D9A-B373-E7230BF8CA95}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/Watchdog_PollingDeltaT</Property>
	<Property Name="varPersistentID:{6A9828B5-3925-4BB7-906E-39D79A3D075A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Dipole-IMin</Property>
	<Property Name="varPersistentID:{6A9BFD67-4A41-4993-9093-6B2790B4BA2F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_2</Property>
	<Property Name="varPersistentID:{6AD3E52B-BD2F-48B4-BE2A-3EF4DD1A2E0D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-RatTrap-IL</Property>
	<Property Name="varPersistentID:{6B64FFC1-8B47-4236-A1A2-7EF9A067E517}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_1</Property>
	<Property Name="varPersistentID:{6BBE18CE-37D8-46A5-A5CD-BE5925CC560E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_2</Property>
	<Property Name="varPersistentID:{6C1FBD8E-4A78-4168-9541-951811FACB71}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/BMIL_UXADT2-I</Property>
	<Property Name="varPersistentID:{6C202FC9-F54C-4B07-8991-08EFCC8231AD}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{6CBA57AA-B4DC-4B8C-BCB8-D57EEEC054CD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IMax</Property>
	<Property Name="varPersistentID:{6CFE26CB-AEB0-4D92-8567-5A15D67073E6}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DT3.currinfo_1</Property>
	<Property Name="varPersistentID:{6D08A20A-3793-48B9-BCB2-EE29E0291DD9}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXADT2.currinfo</Property>
	<Property Name="varPersistentID:{6D09753C-1913-411C-9188-2F422A4F9A43}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-MP-Max</Property>
	<Property Name="varPersistentID:{6D64C08F-D5EA-4817-9F7F-74628D3683E7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_0</Property>
	<Property Name="varPersistentID:{6D75A9BE-EE19-409D-8D60-1A1608837092}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_1</Property>
	<Property Name="varPersistentID:{6DA54960-9BCD-47C7-AA93-0D0CD0177A43}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_Size_D</Property>
	<Property Name="varPersistentID:{6DB02020-2CA6-4214-8D55-0BAB0C12E798}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingIterations</Property>
	<Property Name="varPersistentID:{6DB61A11-910E-4967-BACB-5C9EE443617B}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/WatchdogProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{6EE1017E-8DE9-47CA-86EE-A14489318EB4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingIterations</Property>
	<Property Name="varPersistentID:{6EE615FF-9BE0-44CA-8D0A-0112F0DC4E2F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_0</Property>
	<Property Name="varPersistentID:{6EF1FE33-2D71-440F-B102-2A6DB026EA88}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{6F047CAC-FD95-45E8-90AA-0E1DD0001047}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Interval</Property>
	<Property Name="varPersistentID:{6F1904C5-4FF5-4019-B566-CC7BB9C7513B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Targetwheel-IL</Property>
	<Property Name="varPersistentID:{6F4F3B9C-EC95-4BB7-8A97-DF07F52750F3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ChannelMode_3</Property>
	<Property Name="varPersistentID:{6F7F11AB-639B-4354-9249-827793D2C471}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_3</Property>
	<Property Name="varPersistentID:{6F8E5BC1-34A8-48CE-9E1E-A942C4A2FBD1}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingIterations</Property>
	<Property Name="varPersistentID:{6F9743DE-8B31-4696-8105-823FC0E00A80}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PIDLead</Property>
	<Property Name="varPersistentID:{6FB53C5A-050F-4F64-BBFF-50A2E9ADAE15}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ErrorStatus</Property>
	<Property Name="varPersistentID:{6FC4873F-35E1-4490-B664-270D4ADA99EC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ErrorMessage</Property>
	<Property Name="varPersistentID:{6FDA8C34-4442-4D6E-8461-6EBD5252AB21}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_1</Property>
	<Property Name="varPersistentID:{7049E1A5-3335-4396-A8FA-94F6FBFB79C8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/SecValvesState</Property>
	<Property Name="varPersistentID:{7086A0D7-1D24-42E2-B976-5DBF2258B79C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Magnet-IL</Property>
	<Property Name="varPersistentID:{71464209-087C-421E-B6CC-82D73970634E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP5</Property>
	<Property Name="varPersistentID:{71518556-9876-4FDF-908C-EB59F1EC84F8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{7186ACB5-6FE5-4619-BDD3-FA8CAD15B30A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2-Q</Property>
	<Property Name="varPersistentID:{71B5E740-FD8D-42FA-864F-33CF56579671}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultCode</Property>
	<Property Name="varPersistentID:{71C0A701-7A94-4271-B1D2-22E913058B3B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_0</Property>
	<Property Name="varPersistentID:{71FF2403-26C6-4DC4-906B-E595C5A68A6F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowRange_0</Property>
	<Property Name="varPersistentID:{72002C14-FB7C-4FDF-A41C-08C126EBF270}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD11_current_setpoint</Property>
	<Property Name="varPersistentID:{72233AAC-DCB1-4B70-A067-FF838230E5BA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_0</Property>
	<Property Name="varPersistentID:{739E43F7-B383-425D-96E7-F40F085FBA5B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_1</Property>
	<Property Name="varPersistentID:{73A4ABEC-AABC-4C19-8CBD-4B321EA77836}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS_2</Property>
	<Property Name="varPersistentID:{73E18E7C-3CDE-4CB1-815E-5624DF13ED7F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SelftestResultCode</Property>
	<Property Name="varPersistentID:{74193068-F60B-4A10-AFE0-DAF3303B0AE0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{741BD37E-FA14-466E-B1DA-7ABCBA568881}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{7469223E-D06A-40BC-AC06-0BF03BDB5576}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Dipole-IMax</Property>
	<Property Name="varPersistentID:{75BA7FD9-B5F9-46E2-82A7-4669CD4A900E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ZeroAdjustPressure</Property>
	<Property Name="varPersistentID:{75DEE9D3-D32E-4FCC-BFD5-5CB9F2DA9AA4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{7606F232-00E3-4CC1-9939-53C24BBE326C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_2</Property>
	<Property Name="varPersistentID:{760B7105-460D-4AFD-9C63-A234F451D7F6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-V-IMin</Property>
	<Property Name="varPersistentID:{76BED25B-0354-4959-B988-A2AAED2F343D}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-5_Pressure_1</Property>
	<Property Name="varPersistentID:{77812585-F5F4-406E-8094-22519D933517}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-Rate-Limit-Enabled</Property>
	<Property Name="varPersistentID:{78AF7B46-E179-4369-ACBB-A499EF52CDFD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-DMA-IQ-Timeout</Property>
	<Property Name="varPersistentID:{78EE54D2-3DC1-44D4-9BC5-DA0BFDB4FC15}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXIDC6_range</Property>
	<Property Name="varPersistentID:{79086B64-15B4-4444-BF6B-C216EFFFD288}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ControllerMode</Property>
	<Property Name="varPersistentID:{796FB9FA-8294-416E-B269-6B75E27BCAF2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{7973D3E0-66DD-4128-9934-10102802E24B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingDeltaT</Property>
	<Property Name="varPersistentID:{799555E5-23A1-4BA4-8E10-7FE2C42EE6E1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{7A0B374B-C61A-45D3-B7F5-38B86DC5F7E8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_2</Property>
	<Property Name="varPersistentID:{7A35CC0A-27BB-46B9-B38E-1ACDCDFF3010}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_A</Property>
	<Property Name="varPersistentID:{7AAE11C5-CFD9-482A-B334-41F1F8480867}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{7AF41F05-013C-48F6-8662-4E17FC084A96}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ErrorStatus</Property>
	<Property Name="varPersistentID:{7B003ECA-A1CA-463B-B2C0-CF619F63FAEC}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/MP-Period</Property>
	<Property Name="varPersistentID:{7B96826C-BC46-402D-92C8-73BDD332C7D5}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_FirmwareRevision</Property>
	<Property Name="varPersistentID:{7C75EE93-A72B-4854-8387-49BE1776A016}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ErrorCode</Property>
	<Property Name="varPersistentID:{7C78DF4F-ED74-49F0-891A-5450BAD776D3}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Temperature</Property>
	<Property Name="varPersistentID:{7CCF3321-F64C-4E5A-8969-C81E4FF2085F}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/IOS State</Property>
	<Property Name="varPersistentID:{7D1F2ECA-DC14-432E-A29E-7F2460F8B163}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-2_Pressure_1</Property>
	<Property Name="varPersistentID:{7D39AF8C-F43C-46C9-9320-B5361ABE41C5}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/BMIL_UX8DT3-Q</Property>
	<Property Name="varPersistentID:{7D690671-4233-4EB3-902F-9FCBFFE63289}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ControllerMode</Property>
	<Property Name="varPersistentID:{7DE18433-C9C3-4AAE-9FC1-DEC9BA13AD05}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Beam</Property>
	<Property Name="varPersistentID:{7DF23428-CE28-492A-8203-A0BC4614FBC9}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4.currinfo_1</Property>
	<Property Name="varPersistentID:{7E1102F3-DE03-4F98-8783-99D17543CF42}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{7F3583B1-82D5-4BF7-8726-BB0AB6C74139}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowRange_3</Property>
	<Property Name="varPersistentID:{7F38FECE-36F3-46A1-B4C4-EEAF457982E9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Interval-Enabled</Property>
	<Property Name="varPersistentID:{7FD2B143-0243-4B00-A2AA-DE27BB2AC4B4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingCounter</Property>
	<Property Name="varPersistentID:{7FDA8CF3-EFE8-4A6B-9B38-C171DFCBDA35}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{806E23FA-ABE0-49EA-A60C-7724F1E6ED5A}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{80DE61AD-305B-4D5C-82C9-4577EEC3E73A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_0</Property>
	<Property Name="varPersistentID:{80F0EB3D-F29B-4A7C-B934-16F4E500E436}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_PollingDeltaT</Property>
	<Property Name="varPersistentID:{81016F8F-ACA8-473D-B547-EE39AE6D18D4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{8133166F-CA09-41F8-A63F-7CB4679A91D0}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{81D50541-3306-45F2-B496-9ABAF25DCEBE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{81E2D377-A599-4563-8D06-96A06AF40F02}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_1</Property>
	<Property Name="varPersistentID:{82170104-9095-4EC1-84F2-51E1EF513199}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingCounter</Property>
	<Property Name="varPersistentID:{8235A556-C9B4-4998-9B08-4D472E169658}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{8252B083-51DD-46DD-93EC-13E90E5866CB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_dtCounter</Property>
	<Property Name="varPersistentID:{82C1B09E-8018-4556-A36F-216983447E7B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{831364CB-AD77-4A5D-8C83-22B94AE26E58}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SetID</Property>
	<Property Name="varPersistentID:{8347FFFC-A9F6-43EB-AA84-794EA04FE203}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Pyrometer-IL-Enabled</Property>
	<Property Name="varPersistentID:{83755CAC-8D52-480E-AB93-664F1F5C9E07}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_0</Property>
	<Property Name="varPersistentID:{83FFA41D-7A40-4F2E-A9BD-EC3D819705D2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3-I</Property>
	<Property Name="varPersistentID:{843EBDF1-8B3E-4ACC-931E-92EC295C8A97}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-MP-Max-Limit</Property>
	<Property Name="varPersistentID:{845393C3-8A45-48C7-82AD-98DE8200C813}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_2</Property>
	<Property Name="varPersistentID:{84F45E89-6130-4563-B1E8-F2E61AEA5518}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{8537D8C7-9300-4B04-92CC-3CBA692D36F3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP6_Status</Property>
	<Property Name="varPersistentID:{856743CD-E0DA-4DFC-BF26-FBDC059CFC0F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_3</Property>
	<Property Name="varPersistentID:{85B1F50D-5960-4615-9179-BD4BDB1F7229}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/MP-Length-IL-Enabled</Property>
	<Property Name="varPersistentID:{85B4E41C-DD9A-47BE-87C5-F4E037A9767B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV5</Property>
	<Property Name="varPersistentID:{865616E6-9D7D-4C0E-B301-69A4055A1CC8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PIDIntegral</Property>
	<Property Name="varPersistentID:{86570E61-F697-4226-9678-3771B3143FC0}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Period</Property>
	<Property Name="varPersistentID:{871094BC-FBE2-4BA3-B406-D0338A66691D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_A</Property>
	<Property Name="varPersistentID:{8743B2CE-1DCB-4A10-8893-BF569C83DAA0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{875B8315-42FE-4BE5-81FC-480782F368B9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-DMA-IQ</Property>
	<Property Name="varPersistentID:{8762C3CE-C498-41F3-8C1C-6643A2BB7C2E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_GCF</Property>
	<Property Name="varPersistentID:{87A32959-E9DC-4C96-B4E1-8579A59F369D}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_0</Property>
	<Property Name="varPersistentID:{87A83ADA-EB2D-4009-9F68-03A796642F0E}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_ErrorStatus</Property>
	<Property Name="varPersistentID:{87B09D37-D42E-4496-B92A-CF3E402D5C31}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_B</Property>
	<Property Name="varPersistentID:{87C8997C-9A4F-4275-80FD-5F67082AF04E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-IL-Max</Property>
	<Property Name="varPersistentID:{87F56DF4-960A-40DF-8BB5-C7C976B14325}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Error</Property>
	<Property Name="varPersistentID:{88115ECF-3E5C-49F6-B467-1D0B209928FE}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ErrorCode</Property>
	<Property Name="varPersistentID:{8814C6E2-AE2E-42FB-BFFD-84161C4C7817}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Log All Device Errors</Property>
	<Property Name="varPersistentID:{882B3F3F-EF15-4CEE-9272-6846BF912075}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Projectile</Property>
	<Property Name="varPersistentID:{88750632-6672-4DE1-9438-90F0ED7BA880}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX.currinfo_0</Property>
	<Property Name="varPersistentID:{887E46A1-655E-4B08-939A-DDD61E7F448C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ResourceName</Property>
	<Property Name="varPersistentID:{88AB0827-C85B-4E65-96B3-48B191CA35D3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_2</Property>
	<Property Name="varPersistentID:{88F561FB-A26A-4AEC-A07A-86A6EC91EA9F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_B</Property>
	<Property Name="varPersistentID:{88FFCC8C-282F-4FE7-A313-CF909F972E68}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_2</Property>
	<Property Name="varPersistentID:{891E4196-CED1-4A13-B1B2-E295BF52BC72}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ChannelMode_1</Property>
	<Property Name="varPersistentID:{892742FA-8D54-466B-B07A-8DFACD9FDCD8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-1-WDAlarm</Property>
	<Property Name="varPersistentID:{89665A38-07D1-48B6-BBF9-89544448109D}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TASCAGC_Flow_3</Property>
	<Property Name="varPersistentID:{89B037F8-8102-4F4C-BF51-A74A78ABF879}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{89BBCC3B-B8D9-4033-85FB-7954FA289941}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{89D717CC-96A9-47DF-ABF3-438F1DA7AD1B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ExpNumber</Property>
	<Property Name="varPersistentID:{8A00616A-688F-40EA-BA3E-BE8BD4F17686}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_ErrorMessage</Property>
	<Property Name="varPersistentID:{8A1730E2-6982-4E66-9308-85400B42D7FB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowRange_3</Property>
	<Property Name="varPersistentID:{8A24D7B4-BFCE-4611-885F-EE4AA05D260E}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period-Min</Property>
	<Property Name="varPersistentID:{8A6D3646-C956-4127-B75B-F1DE17F1DCD3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SetID</Property>
	<Property Name="varPersistentID:{8A790CC1-28B9-4FB0-AA76-96255D3C0F53}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingMode</Property>
	<Property Name="varPersistentID:{8B3BD173-196C-4231-A0C8-4B9BBE36EBAC}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_3</Property>
	<Property Name="varPersistentID:{8B57462D-1963-46C5-ACAA-E90CF5BB5703}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_2</Property>
	<Property Name="varPersistentID:{8B86C086-B7B9-4BB6-9CCD-FE5E728EE72C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_DMA-IQ-Enabled</Property>
	<Property Name="varPersistentID:{8B95852D-E2EB-466F-BD88-0887D0B3C298}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/BMIL-WDAlarm</Property>
	<Property Name="varPersistentID:{8BC2D6CA-28AC-492C-99D1-BFD8E6456B53}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_DriverRevision</Property>
	<Property Name="varPersistentID:{8BF62ABD-ED89-4B68-AD3F-6EAE16E3D9F1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_3</Property>
	<Property Name="varPersistentID:{8C82C0B7-7EB1-4E05-AF4F-8CA5B7DDECFB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Start-1</Property>
	<Property Name="varPersistentID:{8C968B5E-7D67-4A7C-A55C-F4613F9F6156}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC4_P.posits</Property>
	<Property Name="varPersistentID:{8D8505FF-7949-418E-B308-DA9961BC3EC8}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{8DD08AF9-224D-4970-89D8-4D838A95E154}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{8E5C38D0-6060-4248-8D84-1C1AD69B6BB7}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{8E79AB32-1E3A-47CA-8764-623C16FC17CA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{8FA0486E-E787-48E3-9EBF-D129D17E28E8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DT3_current</Property>
	<Property Name="varPersistentID:{903F6F6A-866D-48A1-B974-8478E6550C6E}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_PollingCounter</Property>
	<Property Name="varPersistentID:{905C3402-D1FA-4DF0-BF08-9582F0D9DDEA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowRange_2</Property>
	<Property Name="varPersistentID:{906D114E-AE69-43BD-9B83-226D5E5798DB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Pressure</Property>
	<Property Name="varPersistentID:{90A55897-D8E5-49C9-9EA1-0CE5BF039765}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_DriverRevision</Property>
	<Property Name="varPersistentID:{90E138C5-9685-4B7A-BAF3-A679A9336D94}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{90F4A93A-57DE-4E74-B2CE-779F46C48D1E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingInterval</Property>
	<Property Name="varPersistentID:{911E44AA-3C1B-4E27-9C51-B866081F394B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Generate</Property>
	<Property Name="varPersistentID:{912F1616-FD92-440D-8702-1437B4B8BC56}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingInterval</Property>
	<Property Name="varPersistentID:{91897FEB-9427-48FF-B766-960C28016014}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3-Proxy_Activate</Property>
	<Property Name="varPersistentID:{9251A9E9-A5C1-47F5-A2AC-C4B8D6305569}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{9274A8E8-1331-4965-89E0-004E66D9FBFA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{92FE0C08-D427-4B09-A953-39031C4114BD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_1</Property>
	<Property Name="varPersistentID:{933DF0DD-D00C-4F6E-8283-6027A8F4847F}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD11.currents</Property>
	<Property Name="varPersistentID:{93458B0C-9BB5-47EA-86F4-B9F8CA527ABC}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXIDC6_current</Property>
	<Property Name="varPersistentID:{93468623-73AD-4F0C-A927-E9BCCA6FB702}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_SelfTest</Property>
	<Property Name="varPersistentID:{93AE07B6-E7DD-423C-8B42-19FC690A8B1B}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{93B76C98-066E-4F5D-BA54-DE10799C32A6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-QMP</Property>
	<Property Name="varPersistentID:{93E59C66-2E83-4CD8-9EB1-60EF4B1FB602}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ZeroAdjustPressure</Property>
	<Property Name="varPersistentID:{940E15AC-3AEB-4898-8849-2550DDCFB4E7}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_2</Property>
	<Property Name="varPersistentID:{949931F5-1D6E-4004-8B31-73866FD1C3F1}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-4_Pressure_0</Property>
	<Property Name="varPersistentID:{94DBB177-2356-4A40-92CA-40061D145CC1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_3</Property>
	<Property Name="varPersistentID:{94DDDC6D-1CE4-4A2E-9EEC-53DD4D9D078A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingInterval</Property>
	<Property Name="varPersistentID:{94E842DC-45D1-4305-9EBF-EC3639104791}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_3</Property>
	<Property Name="varPersistentID:{9511384F-6365-42BE-9495-A978FDC72F64}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingMode</Property>
	<Property Name="varPersistentID:{951F4FB2-448A-48D6-A06F-D3D27BFBA882}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Error</Property>
	<Property Name="varPersistentID:{95247C1D-952D-4C45-9726-1F6556E72526}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_4</Property>
	<Property Name="varPersistentID:{96634D44-F423-4C66-A676-4E8878CA0560}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{96A5071C-51AC-4E1B-A220-FA2EE8BEAB82}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_3</Property>
	<Property Name="varPersistentID:{96A97825-423D-41F2-9275-9D9DE924AF6C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowRange_3</Property>
	<Property Name="varPersistentID:{96ADD0A0-D4F9-43C8-8ADA-33B2D60F0424}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{96CA3361-D912-4B9D-911B-C92B6F3207E0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Reset</Property>
	<Property Name="varPersistentID:{96CD9107-6C9E-4D65-9B9B-A23D0C235953}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChModes</Property>
	<Property Name="varPersistentID:{96E94C1C-0C35-4F7B-8DCD-E7BB98BC30D0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_CircuitMode_3</Property>
	<Property Name="varPersistentID:{970B5401-42B4-440A-96E4-A456B4C09CB7}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{97356127-1C61-46FC-B998-FA872E7AED6C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_UnderrangeControl</Property>
	<Property Name="varPersistentID:{976669A5-D3A0-4035-940B-65D16C32A8FB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_1</Property>
	<Property Name="varPersistentID:{97AFD55D-AD6F-48DD-A6FB-0C7D348C6D5E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PIDLead</Property>
	<Property Name="varPersistentID:{981734E0-A894-4CB7-AB99-13AC2FE1609F}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-V-IMax</Property>
	<Property Name="varPersistentID:{983D0921-6E52-4F11-8BFB-6942C73F853A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingIterations</Property>
	<Property Name="varPersistentID:{9867786B-0792-4630-8859-7DA83C805C20}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_0</Property>
	<Property Name="varPersistentID:{98BFB6A0-25FB-484D-8225-93398029C040}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Magnet-IL-Enabled</Property>
	<Property Name="varPersistentID:{98FE36ED-CE83-403D-93F9-155CBD987ED0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL-Main</Property>
	<Property Name="varPersistentID:{98FEBED5-A99A-4917-9C2E-8CEE7F4A44F2}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/Watchdog_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{993AA01C-9DDC-426D-A15E-AB6442706E04}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_3</Property>
	<Property Name="varPersistentID:{995B1172-5499-4ECC-9A42-CC1FCC116AD3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_3</Property>
	<Property Name="varPersistentID:{99B8B7B1-7A21-4290-BB48-4C8919FBABB7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ErrorMessage</Property>
	<Property Name="varPersistentID:{99EF51A4-1D16-4CB3-B3B5-D2B743DF8723}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SPS</Property>
	<Property Name="varPersistentID:{9A0247B2-7D58-4D68-BB96-6162293D1D4F}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_DeviceTemperature</Property>
	<Property Name="varPersistentID:{9AA53B36-9791-4A25-A32F-48768BA5FFA2}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-2_Pressure_2</Property>
	<Property Name="varPersistentID:{9ADD70F3-AE5F-4A74-A2D5-98500780ABDE}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_Free_D</Property>
	<Property Name="varPersistentID:{9B30FE25-715C-4AF9-BAAB-56E3669E7381}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{9B767A84-4E19-48CB-8A07-606A697A1406}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{9B768339-8CB3-42A6-9AC0-B47649FCA0FA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{9BEB20BB-7C95-42CE-9B15-C7B545D277C4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_0</Property>
	<Property Name="varPersistentID:{9C151626-3B0B-4309-AA5E-A2AC71C84586}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_2</Property>
	<Property Name="varPersistentID:{9D0DB873-7C4B-4029-8572-8186EFBEDB74}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Chopper_status</Property>
	<Property Name="varPersistentID:{9D3875ED-ED85-4ACE-B245-E0FC8D95B278}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ErrorStatus</Property>
	<Property Name="varPersistentID:{9D58ABED-FAEE-4E1A-A6A0-60A63CBCFA89}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_0</Property>
	<Property Name="varPersistentID:{9DA3FD54-2422-4E71-BE9A-BA1BA0092BE3}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChModes</Property>
	<Property Name="varPersistentID:{9DF2CEF4-EBA4-42A4-8D62-59762507CBC5}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/BMIL_WDAlarm</Property>
	<Property Name="varPersistentID:{9E7CAFB7-B940-4636-A13F-80526310CDBF}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Trafo-Generate</Property>
	<Property Name="varPersistentID:{9F35F51D-BC76-42D7-8AAC-2162E839F397}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{A048A0DF-0E42-44B4-842B-DF97E490BE05}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_1</Property>
	<Property Name="varPersistentID:{A0D41FC0-6FE9-4C54-BEFB-34F1CE86198C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Status_0</Property>
	<Property Name="varPersistentID:{A0E6FCD9-98AA-4563-999A-FF8CB8061F4F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingMode</Property>
	<Property Name="varPersistentID:{A1250F18-35EB-430C-A985-71F296502B12}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ChannelMode_3</Property>
	<Property Name="varPersistentID:{A13740C9-726F-4820-83BE-5BAB6B755B64}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX_P.positi</Property>
	<Property Name="varPersistentID:{A14E2FC7-5475-4D5F-89D3-791146F27387}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXFVV1S.positi</Property>
	<Property Name="varPersistentID:{A159F633-E4BA-4840-9757-568E527644F2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_3</Property>
	<Property Name="varPersistentID:{A1716611-89F7-4160-ACD1-E3A8D1580BD9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowRange_2</Property>
	<Property Name="varPersistentID:{A1BD008C-5437-4A5C-9C12-6264A604F662}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingCounter</Property>
	<Property Name="varPersistentID:{A1E49455-96E3-4075-84A8-EE49443A4A7A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_0</Property>
	<Property Name="varPersistentID:{A2274039-4E52-4BE0-99E0-FE144A61465C}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-3_Pressure_3</Property>
	<Property Name="varPersistentID:{A239B67B-5204-4B7B-90B5-7B0A386474D4}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_ErrorCode</Property>
	<Property Name="varPersistentID:{A27E2FE2-ED9A-4AB7-9A2F-70C2E93F90E3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VVP4</Property>
	<Property Name="varPersistentID:{A28C0092-CFC4-4A75-89B1-895D27366DD5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripModes</Property>
	<Property Name="varPersistentID:{A334A322-21BF-4735-9FC9-DD5F396866AF}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_T-BeamOn</Property>
	<Property Name="varPersistentID:{A38880C3-6A24-40A1-A779-6DBE44B8642A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ErrorStatus</Property>
	<Property Name="varPersistentID:{A487F001-375A-4A11-8477-92A81B42CD17}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_DriverRevision</Property>
	<Property Name="varPersistentID:{A4CF89E9-AFDE-4F9E-B32D-016CD5C3D2C9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ChannelMode_2</Property>
	<Property Name="varPersistentID:{A5962AFA-69ED-48EF-84D9-BF4C22359405}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{A60AD7CA-4830-4071-B70F-819BF4D03041}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Targetwheel-IL</Property>
	<Property Name="varPersistentID:{A60D02E5-1AFE-443C-A7BF-DA39B0E24BDD}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingDeltaT</Property>
	<Property Name="varPersistentID:{A6A4B841-FD2C-45B7-A0A1-A69D186D282A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingTime</Property>
	<Property Name="varPersistentID:{A7078177-6AA4-4BE8-9EFD-9FA1473E600C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{A78F4B67-9C50-4B77-B728-53639FF37668}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GON_3</Property>
	<Property Name="varPersistentID:{A82D4B70-F7E6-433C-B790-CD9016EE0985}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS</Property>
	<Property Name="varPersistentID:{A844AF63-C2EC-4E82-8F00-3D39EFA7C4F3}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingMode</Property>
	<Property Name="varPersistentID:{A881E981-0F31-4E3C-980C-859D0216C249}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{A8BCB6ED-D810-433A-A555-8FDC1C18E7FC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{A8CB4D7F-9C29-4CE3-823D-443E970E07E0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_DriverRevision</Property>
	<Property Name="varPersistentID:{A96B2BCA-23A7-4E34-90A6-336F05CA4B58}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Dipole-V-IL</Property>
	<Property Name="varPersistentID:{A97A42AC-4B90-4CD1-AB0B-79F295A21FEC}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_1</Property>
	<Property Name="varPersistentID:{A9FF13D9-EB2C-47E1-A871-84F75B1DB5FC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SelfTest</Property>
	<Property Name="varPersistentID:{A9FF79CA-97AF-4110-8C3A-405479FB82FA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_2</Property>
	<Property Name="varPersistentID:{AA5AA3FE-F9CB-4D5C-94E2-E2020049014C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV3</Property>
	<Property Name="varPersistentID:{AA73A0D8-5AD5-48CB-AF75-0BCDDB6A7358}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ChMode_3</Property>
	<Property Name="varPersistentID:{AA905BDB-D9CB-4436-9A0F-5A759E267AD3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV4_Status</Property>
	<Property Name="varPersistentID:{AAA47C8A-3411-49F5-813D-9FD86F9E61FA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_2</Property>
	<Property Name="varPersistentID:{AB58F373-63D0-4FD4-9BAF-4630383908C1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_3</Property>
	<Property Name="varPersistentID:{AB73BBEA-7F7B-4CEF-B4D3-430B5435FE56}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Charge-State</Property>
	<Property Name="varPersistentID:{AB7BF391-EDBB-4CF3-B770-648CBC8B40F2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{AB9CF2F0-9396-43F2-98DA-633226E0A805}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_1</Property>
	<Property Name="varPersistentID:{ABA0986B-735A-4775-AD0E-DBF6AE086618}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-I</Property>
	<Property Name="varPersistentID:{AD066676-C7B0-413D-AE78-C216F94B2DA5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowHL_3</Property>
	<Property Name="varPersistentID:{AD4980F8-950C-47C5-ACF1-1AAA25152539}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureMode</Property>
	<Property Name="varPersistentID:{AD7A2A74-D826-4D75-B75E-559E77E2E493}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ErrorMessage</Property>
	<Property Name="varPersistentID:{AD910A42-F55E-4270-894F-1B202C39ABEF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8MU1_current_setpoint</Property>
	<Property Name="varPersistentID:{AD95B9D4-5089-44D8-AE3A-578CE9AD8FDF}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMILProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{AE10E9AF-00A5-428B-A7D3-BCBA064958B3}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-MP-Length</Property>
	<Property Name="varPersistentID:{AE2310B6-5585-48E0-B429-286D3917DF47}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TASCAGC_Flow_0</Property>
	<Property Name="varPersistentID:{AE446D5B-F152-44C6-B4DD-A5567A4DFD21}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/BMIL_Det-Rate</Property>
	<Property Name="varPersistentID:{AE527D19-823F-4AAE-AC9D-9A0BD9CCAE5E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_FilterTime_3</Property>
	<Property Name="varPersistentID:{AE970958-F116-4635-8B57-B56EEF6EADC1}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Debug</Property>
	<Property Name="varPersistentID:{AE97F8D5-DBF7-413E-A6EC-5E84533510F0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{AEA3C38B-FE0A-431E-8C16-C3A73C21D2DB}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{AEBB2D9D-F7F3-47C1-B062-7F1D597786E8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PressureMode</Property>
	<Property Name="varPersistentID:{AEC4883A-6BAD-4EA4-806D-37D95A74AD62}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_PollingCounter</Property>
	<Property Name="varPersistentID:{AEC5E8B5-BE1E-4CB7-9768-94E8D0CE23D0}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{AEEF3B23-AECC-4E2D-87D1-71CFA0248472}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingInterval</Property>
	<Property Name="varPersistentID:{AEF9CF08-BCE7-4598-A34E-42132FC62F38}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowStatus</Property>
	<Property Name="varPersistentID:{AF0DEE92-E203-4AF8-AB1D-95A9DB72E612}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{AF599EDB-097A-48A5-9AAC-5192EC328983}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{AF5F953C-7274-46AD-BBA8-A8DBCC9F22AF}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_0</Property>
	<Property Name="varPersistentID:{AF6D3A7D-66FF-4BFA-AE5C-CE4767E52891}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_1</Property>
	<Property Name="varPersistentID:{B01F054A-FB53-47A4-9761-F09A73C32EF3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{B0E13173-FF64-41DE-B0E3-EA0C6902FFF5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Flow_2</Property>
	<Property Name="varPersistentID:{B0F41FEC-D41D-473B-8778-01C1913CF2CE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_3</Property>
	<Property Name="varPersistentID:{B1381DEE-1857-4B0E-8EB3-46D03750ADF0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{B1D328E2-954C-49DF-9537-D2BF106C0DA6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-TripMode_1</Property>
	<Property Name="varPersistentID:{B1FF6AC5-F8E3-4F00-AE5A-8F33668B5310}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_CircuitMode_1</Property>
	<Property Name="varPersistentID:{B23C4D33-D25C-454E-8D69-B7A3FBAAA31E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DetectorCounts</Property>
	<Property Name="varPersistentID:{B27DE211-8ACA-47AD-A69C-77FEA6420FB1}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingMode</Property>
	<Property Name="varPersistentID:{B2D24A80-D82B-4758-98EA-25D00DEAD1C9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FirmwareRevision</Property>
	<Property Name="varPersistentID:{B2DC9A12-2704-4CF1-8BB2-C7529230E10B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_2</Property>
	<Property Name="varPersistentID:{B2F7EF05-F6CE-402F-8CE7-83E36ECD2752}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_3</Property>
	<Property Name="varPersistentID:{B332F8F7-D5C2-4431-BA18-09FFA4ED6B25}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingTime</Property>
	<Property Name="varPersistentID:{B3577FA2-A35A-4896-8A24-588205273BAD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-Counts</Property>
	<Property Name="varPersistentID:{B3612791-C094-475D-BC8E-1320CCDDAE0E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{B364C10C-FF2A-4E14-944D-C847941A0271}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{B37C7405-A6BA-4252-8FA1-2BD122F0B1A3}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-Emissivity</Property>
	<Property Name="varPersistentID:{B3ACDA97-02D1-420E-80C7-1A616C714F3F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_2</Property>
	<Property Name="varPersistentID:{B3B7D2A3-72C0-419F-B704-6CA7F7BE5AF5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SelfTest</Property>
	<Property Name="varPersistentID:{B3D76FAC-FDC1-4CD0-80EB-D0BE4B532C16}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FirmwareRevision</Property>
	<Property Name="varPersistentID:{B433C021-7612-4B68-83A2-A82BADC8CF5B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_3</Property>
	<Property Name="varPersistentID:{B446AC89-55A3-47E7-B783-9D65F958E482}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pyrometer-IL</Property>
	<Property Name="varPersistentID:{B47AF271-022E-488E-8796-82ECB0E58994}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SelftestResultCode</Property>
	<Property Name="varPersistentID:{B4B7C0D4-CFF7-4CAC-ABDF-8FEEC2B499D9}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/Watchdog_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{B4BFE183-1283-47CE-BC0C-184BE23AB1D6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{B50A43F1-CBCB-4148-A871-9E272EA9B467}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Target-Temperature</Property>
	<Property Name="varPersistentID:{B59335D9-F933-4826-B673-64F6CA53B1F2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{B5B301C1-643C-4A05-9CAB-3481648EA6BB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow1_TASCAGC</Property>
	<Property Name="varPersistentID:{B5B52B31-AB01-4294-8879-95598B935B2E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ResourceName</Property>
	<Property Name="varPersistentID:{B5E4D7FE-F189-48B8-9D09-BE8E4DC7A178}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PIDIntergral</Property>
	<Property Name="varPersistentID:{B5EB095F-3392-40C7-9904-89C2BB8E3719}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/Watchdog_PollingMode</Property>
	<Property Name="varPersistentID:{B60F0FCE-8EC5-48E0-B3D3-4D3F645BF7B8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_2</Property>
	<Property Name="varPersistentID:{B6108E74-807C-4480-BD2E-2D63E6AF218F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_1</Property>
	<Property Name="varPersistentID:{B612FDDB-32B8-41A3-B63B-9647B8B02862}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingInterval</Property>
	<Property Name="varPersistentID:{B644435D-47E8-48B4-8ACA-73B71C8C0C0A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_0</Property>
	<Property Name="varPersistentID:{B6483301-8E32-4656-B6B8-19AA91727D11}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-MP-Length-IL</Property>
	<Property Name="varPersistentID:{B64E4FF8-E69B-4951-A32C-8F42C369EC2E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingTime</Property>
	<Property Name="varPersistentID:{B666637F-9247-4E27-9913-FAB33628B6ED}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_3</Property>
	<Property Name="varPersistentID:{B77A1D31-13E3-4349-A7B9-8CEEB2ECC86A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowRange_1</Property>
	<Property Name="varPersistentID:{B785A65F-124C-4779-9D2A-0DD0AE0A4504}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_1</Property>
	<Property Name="varPersistentID:{B7DB1497-B6FF-4E6D-96F9-FB3F030E099B}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_ClearMaxTime</Property>
	<Property Name="varPersistentID:{B7DC2243-0475-480E-BE0D-FAC5DCBDDFCB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_3</Property>
	<Property Name="varPersistentID:{B7FB89F3-E4FC-4840-92B7-0A5D55848325}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{B964AAB1-F195-4D1F-827A-EE6C78A0F885}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_2</Property>
	<Property Name="varPersistentID:{B9ABF4A0-FD57-47BA-AE35-D65C2DEF9EE2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingCounter</Property>
	<Property Name="varPersistentID:{B9F75939-38E9-42C6-9135-5DCA0777CEAA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SelfTest</Property>
	<Property Name="varPersistentID:{BA3B79CC-44E5-426D-98B1-1C4BD9ABB8DE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SelftestResultCode</Property>
	<Property Name="varPersistentID:{BA8CA289-5242-48FC-80C1-6A9C0B7D955C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/SecKey</Property>
	<Property Name="varPersistentID:{BAD6A6BB-46E5-445D-A9DD-34679C7AD8FC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingIterations</Property>
	<Property Name="varPersistentID:{BAF918EC-E1B9-481B-B4CE-ABE496C16A47}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-IL-Enabled</Property>
	<Property Name="varPersistentID:{BB0C3083-8298-44C7-A3D6-5009F533AE88}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/Watchdog_PollingInterval</Property>
	<Property Name="varPersistentID:{BB817FB4-1535-442D-AFA3-7E499DFEF725}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PIDIntergral</Property>
	<Property Name="varPersistentID:{BC04784F-48AE-46A6-A2FF-860F1C2CCF3F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowLL_1</Property>
	<Property Name="varPersistentID:{BC0CB0B9-BA24-4456-BD7D-EEF26E2CB35A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_3</Property>
	<Property Name="varPersistentID:{BC2C407F-9AAF-4AF5-96A9-11A0909BBC22}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/WatchdogProxy_Activate</Property>
	<Property Name="varPersistentID:{BCDBA0B8-F31C-4C8C-996B-094B686D08CD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_1</Property>
	<Property Name="varPersistentID:{BD0463C1-60D1-4D0D-81A4-8A0158BEE349}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowLL_0</Property>
	<Property Name="varPersistentID:{BD0E3632-5E33-4867-9F91-9B0915D2F7E9}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandlerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{BD6A8F8A-F9D0-4134-931C-6FECFC05378C}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Max-DetEvents-Limit</Property>
	<Property Name="varPersistentID:{BD867632-FA6D-449F-9104-87D9B0AEFFA8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/BMIL_SW-Watchdog</Property>
	<Property Name="varPersistentID:{BDCA8EAA-B6DD-4538-9253-45D53ED37DFA}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{BDDD092F-D95E-4EEF-8315-487C996C188C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_0</Property>
	<Property Name="varPersistentID:{BE060BD3-D35D-4448-A18D-F1F3A71F6D8C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_VGTP4</Property>
	<Property Name="varPersistentID:{BE65C630-C007-421E-8FF4-745B0FC6DC56}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_PollingMode</Property>
	<Property Name="varPersistentID:{BE686DFC-5DDC-4C27-9B4B-AB4C93B27BF4}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_0</Property>
	<Property Name="varPersistentID:{BEDB1FDB-3565-4176-A68A-D9995EC27DE8}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_ErrorCode</Property>
	<Property Name="varPersistentID:{BF2B17EF-BC43-4D5C-B50B-5D04F0D5F9EB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV2</Property>
	<Property Name="varPersistentID:{BF8017B4-6B04-49BB-B529-D0B778C2F708}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_2</Property>
	<Property Name="varPersistentID:{BF9CDBCA-AA48-4979-9411-A2AA1489F50E}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_Usage_D</Property>
	<Property Name="varPersistentID:{BFCBADDB-A30F-4FBF-B457-51A9BD93FDB5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Pressure</Property>
	<Property Name="varPersistentID:{C00C9006-78A4-4397-86E8-353EE9EFC0B7}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_ErrorMessage</Property>
	<Property Name="varPersistentID:{C0638F24-76E9-45E5-846A-F13C082DB01B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PollingTime</Property>
	<Property Name="varPersistentID:{C096B827-166A-4B55-A788-6C095FF4AE5D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_1</Property>
	<Property Name="varPersistentID:{C0EC84D1-9B85-4D84-91A3-00B4D21CDCA5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_1</Property>
	<Property Name="varPersistentID:{C10AAD2E-6C54-41E9-AF46-6306182CF1B9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5-Proxy_Activate</Property>
	<Property Name="varPersistentID:{C1A26F6F-5A68-451B-9409-2E90D97122C9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PIDIntegral</Property>
	<Property Name="varPersistentID:{C1A630FD-E76A-437A-B751-2D7D459E6F54}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_range</Property>
	<Property Name="varPersistentID:{C1E038B6-D392-4388-BCD4-072B17534572}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PIDGain</Property>
	<Property Name="varPersistentID:{C1F7A212-C691-47DB-A55F-5493407109B4}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_Reset</Property>
	<Property Name="varPersistentID:{C209490F-0986-4C0A-8C4E-92A627F1412F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-CircuitMode_0</Property>
	<Property Name="varPersistentID:{C2EAECBB-7C9B-4EB7-ADAD-848497A0ADCC}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/Watchdog_PollingTime</Property>
	<Property Name="varPersistentID:{C372978F-44FE-4E9A-881F-287EC7113B48}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/DipoleCurrent-AlmAction-Enabled</Property>
	<Property Name="varPersistentID:{C38DEE55-4BB7-4732-BA81-7B8B0920427F}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-5_Pressure_2</Property>
	<Property Name="varPersistentID:{C3AF274A-E78B-49E0-831F-ACF4B13B8434}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_2</Property>
	<Property Name="varPersistentID:{C3B0D6B0-AB41-4759-832D-E2C975C26C03}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingDeltaT</Property>
	<Property Name="varPersistentID:{C3BCD909-A9D2-4C5F-986C-24A03BA86390}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_FilterTime_1</Property>
	<Property Name="varPersistentID:{C3CBCF34-97C3-40DC-9C34-E6B7F80BC174}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-TSRMax</Property>
	<Property Name="varPersistentID:{C3F04713-6CDC-4C30-A5ED-F9126754C16B}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_Enable-DipoleCurrent</Property>
	<Property Name="varPersistentID:{C4653F0C-1750-4FC8-ADBF-CF829D5B6AA9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Pyrometer-IL</Property>
	<Property Name="varPersistentID:{C47E7380-F077-45DE-8538-7E331844CB85}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV6</Property>
	<Property Name="varPersistentID:{C49024E2-F120-4166-8915-B495C7F806FD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripModes</Property>
	<Property Name="varPersistentID:{C50958B1-A1E4-4414-8288-1984D9926DE8}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Error</Property>
	<Property Name="varPersistentID:{C52C2626-890B-45DD-B75E-E6132BA86F04}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UT2DCX_current</Property>
	<Property Name="varPersistentID:{C56DC1FC-51EC-4A12-822D-B33921A8657B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Magnet-IL</Property>
	<Property Name="varPersistentID:{C5843156-48E4-42BD-A771-14C44E8285F0}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/BMIL_UX8DT3-I</Property>
	<Property Name="varPersistentID:{C5C806C7-4A57-44C4-B92F-03A3E2BB65CB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_0</Property>
	<Property Name="varPersistentID:{C5C856A8-91F5-4BC4-AA42-CE0B4333B73C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_3</Property>
	<Property Name="varPersistentID:{C61FFE66-4D54-4828-A1D3-A00F40EEEF2C}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{C65307A0-3E2D-4EC4-BF94-244B2015C0E2}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Dipole-IL</Property>
	<Property Name="varPersistentID:{C6575AED-9674-4EF1-8B5F-6A82A834DCC9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS</Property>
	<Property Name="varPersistentID:{C65F0FD7-EB3C-4B73-8DDB-D930712A0B36}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SwitchingLimits_3</Property>
	<Property Name="varPersistentID:{C6E19F4B-CECF-4426-BCDD-300F81763A98}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-MP-Length-IL</Property>
	<Property Name="varPersistentID:{C6E2F121-E0DD-4423-B7B0-1F56BE343C25}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_position</Property>
	<Property Name="varPersistentID:{C70B9A27-6618-4086-AE54-519CCCD335AD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PressureUnit</Property>
	<Property Name="varPersistentID:{C71EDF82-E545-4DD1-B05F-DAC0CF83C037}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_3</Property>
	<Property Name="varPersistentID:{C72ACB7E-4353-45F3-A52F-CF4251803A76}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Dipole-V-IL</Property>
	<Property Name="varPersistentID:{C75A79B9-0B5E-447E-9E29-8B43FC2F6C1D}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DC2.currinfo_1</Property>
	<Property Name="varPersistentID:{C78B4BA2-2A62-45EE-BB84-223E8C7469B8}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{C7944190-F93B-4B6E-9F18-3DB3C14CF0FC}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingIterations</Property>
	<Property Name="varPersistentID:{C7945914-8A60-458B-9CFB-8A277EE441E8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowRange_2</Property>
	<Property Name="varPersistentID:{C79BAD5C-09BC-4F33-A946-9B2C1A7CB2E8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/FilterTime5_0</Property>
	<Property Name="varPersistentID:{C7C9AC42-810B-4ED3-BDC0-10F08D05AB9B}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8DT3.currinfo</Property>
	<Property Name="varPersistentID:{C8230DE2-AB34-4B88-AD0A-8665ABED8C56}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_CircuitMode_3</Property>
	<Property Name="varPersistentID:{C8ACA8CE-6721-43C4-8438-8194A6D55628}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8QD12_current</Property>
	<Property Name="varPersistentID:{C8FBB0D3-64A8-427C-B5BC-5BA1F601EA20}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_position</Property>
	<Property Name="varPersistentID:{C9205DFD-34AE-4307-9791-C90FC783D43B}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Error-Log-Path</Property>
	<Property Name="varPersistentID:{C9210617-99BB-4F32-BC40-13425A079BA2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PressureMode</Property>
	<Property Name="varPersistentID:{C9537AF3-E27E-4885-AE3B-901EB3637F26}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX.currinfo_1</Property>
	<Property Name="varPersistentID:{C97431D8-CD75-44EA-9640-1757D459F9BC}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowSP_1</Property>
	<Property Name="varPersistentID:{C9B0F930-BEF1-4BB6-B01E-6BA5B73CEFC4}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length</Property>
	<Property Name="varPersistentID:{C9BC05DC-4217-4D09-A010-CC4496ACC96D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/MP-Length</Property>
	<Property Name="varPersistentID:{C9CF8EEC-F2EA-47F3-838B-D91E80E3E806}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SetID</Property>
	<Property Name="varPersistentID:{C9D199C0-5344-43D8-86C9-AEC5DCF02060}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureOffset</Property>
	<Property Name="varPersistentID:{C9DCF41F-A470-4DE0-9DFE-D9639C0F5C06}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Pressure_3</Property>
	<Property Name="varPersistentID:{CA24F0AB-D596-4191-8228-FB4B1E8C9798}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Start-2</Property>
	<Property Name="varPersistentID:{CA6118DE-2A66-429C-9348-097EF09841F0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_1</Property>
	<Property Name="varPersistentID:{CA958D8B-428E-4779-9304-98664AAC113F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowRange_1</Property>
	<Property Name="varPersistentID:{CAAA2226-47BF-4E4F-8032-CDF343488AD5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_ErrorCode</Property>
	<Property Name="varPersistentID:{CAE88E71-865D-4B0F-92CC-17B2286DCB7D}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-MP-Length-IL-Min</Property>
	<Property Name="varPersistentID:{CAF9F4E8-5F14-41A1-B76B-9E346C37DB9B}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Array Size</Property>
	<Property Name="varPersistentID:{CB28304C-5640-4C4F-B36D-B43EF05CCE8C}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PressureUnit</Property>
	<Property Name="varPersistentID:{CB9EC547-6D9E-494F-960C-A9861CAFB1E6}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ChMode_2</Property>
	<Property Name="varPersistentID:{CBA14BD8-4447-4683-96DF-89106A5C2040}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Status_2</Property>
	<Property Name="varPersistentID:{CBC9FE7F-8E2C-484A-8A27-B387582CAA1E}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Targetwheel-IL-Enabled</Property>
	<Property Name="varPersistentID:{CBCD306F-969B-4D84-B790-28CE16EAF9A2}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/BMIL_UXADT2-Q</Property>
	<Property Name="varPersistentID:{CBEF20B6-CCF4-4BFD-9219-04B3D0BB863A}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_CircuitMode_2</Property>
	<Property Name="varPersistentID:{CBF2045F-6DEB-4DA4-8903-1923B9E7A4BD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SW-Watchdog</Property>
	<Property Name="varPersistentID:{CBF8E62B-9634-4EE5-82D5-520DB7E561F6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SelftestResultCode</Property>
	<Property Name="varPersistentID:{CC39408B-E0DF-4C59-90EA-95B48A439B16}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-CircuitMode_3</Property>
	<Property Name="varPersistentID:{CC80C34A-7777-4BC5-8452-F34F2C56EB00}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingMode</Property>
	<Property Name="varPersistentID:{CCDA0EE3-0789-4061-93A7-1723D1A2E6CC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{CD169514-6588-4809-9A4A-1281EB6F5A32}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{CD6148CA-ACB5-496C-9447-6D1169A36AD7}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Magnet-IL</Property>
	<Property Name="varPersistentID:{CE563050-0A57-4254-B336-52094161A9C6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{CE601A28-A8ED-4994-B9C3-BA228B6CC52B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_SelfTest</Property>
	<Property Name="varPersistentID:{CE62544A-8BA6-4901-AFB0-22E498E3521A}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-V-I</Property>
	<Property Name="varPersistentID:{CE99BE73-A55F-4B1D-BE7B-821A00528812}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{CF075640-0966-439C-84C9-7EC553DE3911}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_0</Property>
	<Property Name="varPersistentID:{CFB01F10-4585-4B70-8F91-D8925A139822}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset-Interlock</Property>
	<Property Name="varPersistentID:{CFF6BC54-07A3-4BCA-9969-6033E412C97B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Counting_Enabled_3</Property>
	<Property Name="varPersistentID:{D0424E7B-AD17-4488-B28C-3C1C08ADB3F6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Charge-State</Property>
	<Property Name="varPersistentID:{D0EC4450-12A6-40BB-90ED-B73855016DF4}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TASCAGC_Pressure</Property>
	<Property Name="varPersistentID:{D11CE130-AE2E-4CA8-81C5-E8238BFB64CE}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{D16E0B6F-CE94-4934-AA6A-7038E21B798A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MainValve</Property>
	<Property Name="varPersistentID:{D19BA09E-B21E-41EC-A205-60B9650378DA}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6.currinfo_0</Property>
	<Property Name="varPersistentID:{D1BA774E-539C-46EA-A783-568AB8F24DAD}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC2_set-position</Property>
	<Property Name="varPersistentID:{D2104599-7005-4CE7-88DE-464747F41EA8}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_PollingTime</Property>
	<Property Name="varPersistentID:{D2105633-B759-4C46-BD16-7602468F0A25}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_AmbientTemperature</Property>
	<Property Name="varPersistentID:{D23DF20C-05A3-4670-A659-DA6CC5050E47}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_PollingIterations</Property>
	<Property Name="varPersistentID:{D25AF0C4-EB6D-4474-B1D6-4CFD60051FF1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_PollingTime</Property>
	<Property Name="varPersistentID:{D26F994E-6441-4C26-8002-79CE0DB17F99}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Energy</Property>
	<Property Name="varPersistentID:{D2CE2176-F4B8-4389-8BB4-FFF41543FC7F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffset_0</Property>
	<Property Name="varPersistentID:{D2FD2006-5D7B-496C-AB4E-DC3D0BDCCC44}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP5_Status</Property>
	<Property Name="varPersistentID:{D300D8F3-532A-472E-9534-323698CB8C6B}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowLL_3</Property>
	<Property Name="varPersistentID:{D356F02B-CABC-4D69-A8D5-D5D2D999F8FC}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_2</Property>
	<Property Name="varPersistentID:{D35A08DB-0758-406E-88F7-7BF40F7B1694}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_FilterTime_0</Property>
	<Property Name="varPersistentID:{D3DFF0C8-77A2-4D8C-88C6-8E61D6087920}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_1</Property>
	<Property Name="varPersistentID:{D42239B4-D8E8-4259-8AC0-1AAAFF0EFC5D}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXADT2.currinfo_1</Property>
	<Property Name="varPersistentID:{D43F1DFF-D973-428D-8C00-C15F9D73884B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Dipole-IL-Enabled</Property>
	<Property Name="varPersistentID:{D4789831-CF41-48DC-8589-49C3E6459612}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Det-Rate</Property>
	<Property Name="varPersistentID:{D4BEE74C-2EB7-4001-A19A-2F2E4B32B3F6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{D4E69781-4510-45CD-9300-F669BF6ABCAC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingDeltaT</Property>
	<Property Name="varPersistentID:{D4E80FE7-FB84-4943-8BEA-CA05A0F5EE66}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowSP_0</Property>
	<Property Name="varPersistentID:{D4FAAB5E-CBDD-45D7-8AC1-82E847D37239}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SetID</Property>
	<Property Name="varPersistentID:{D502DB6E-4C7C-45EA-B917-0E43DD40A626}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PressureSP</Property>
	<Property Name="varPersistentID:{D53744F9-3B02-438C-ACF7-89F4F0BABD04}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_RatTrap-IL-Enabled</Property>
	<Property Name="varPersistentID:{D5686955-D67B-4213-A77C-962DB7ECFD96}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_0</Property>
	<Property Name="varPersistentID:{D5C2B3B4-027C-432F-9A43-40B2953CC509}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-4-WDAlarm</Property>
	<Property Name="varPersistentID:{D6D0256B-CDDA-4149-A2F8-14C53CBA8E32}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Status_0</Property>
	<Property Name="varPersistentID:{D6E7E99A-52C0-4C81-966F-3240E28DF75B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_DriverRevision</Property>
	<Property Name="varPersistentID:{D79FD4B5-3990-4274-8427-55F3C1F634E0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2-Proxy_Activate</Property>
	<Property Name="varPersistentID:{D7C1B46E-F25F-4F1B-B678-E545C00BDA4B}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140Proxy_Activate</Property>
	<Property Name="varPersistentID:{D7D6C1C3-F366-4EDE-A316-204EF10BBD0E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_2</Property>
	<Property Name="varPersistentID:{D7FDC903-6170-453B-BFB9-F7D6F8E2B0F0}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_TSRMax</Property>
	<Property Name="varPersistentID:{D81076E3-E4F9-4ADE-9A24-AC4B1E0BD408}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL_2</Property>
	<Property Name="varPersistentID:{D838F954-FF01-494A-B473-93B9603745A1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_1</Property>
	<Property Name="varPersistentID:{D85FBA93-86A7-41D5-B46B-1A144ED7850E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_ErrorCode</Property>
	<Property Name="varPersistentID:{D875693D-B559-40E6-A427-E6BA4CDC752B}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingMode</Property>
	<Property Name="varPersistentID:{D8DC863F-0181-4E55-A03A-DCC21B61A1BB}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{D923F410-D92C-4221-B266-7484610833A0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_0</Property>
	<Property Name="varPersistentID:{D98E9B25-EF8D-4518-9E4D-1966A6C5E115}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP5_Status</Property>
	<Property Name="varPersistentID:{D9EB216C-66EE-41E0-AA3F-5C3BEB7DB6BB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_SPS_B</Property>
	<Property Name="varPersistentID:{DA30A650-09D3-4B20-A428-5490B2B86915}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Period-Max</Property>
	<Property Name="varPersistentID:{DA99351F-9A9A-44D1-93B7-9E2BA306053F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF_1</Property>
	<Property Name="varPersistentID:{DAC572B9-8BFD-4406-9909-B0FC17949F9D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowRange_0</Property>
	<Property Name="varPersistentID:{DAFA98D3-ACB6-4103-8301-571BF421C431}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/TPG300-5-WDAlarm</Property>
	<Property Name="varPersistentID:{DB02C13F-F7B1-4378-A859-7627EA180A33}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Status_1</Property>
	<Property Name="varPersistentID:{DB097E36-13C7-4236-B135-81EE3BA8FCC1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_PollingInterval</Property>
	<Property Name="varPersistentID:{DB18F309-D239-4950-8AFF-AACDC3F4533F}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_MP-Length-IL-Min</Property>
	<Property Name="varPersistentID:{DB46500B-3D41-4AB6-96AB-C300DE729E29}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/TPG300-3_Pressure_1</Property>
	<Property Name="varPersistentID:{DBD22025-2492-4686-9435-28C83F8ACFB5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingCounter</Property>
	<Property Name="varPersistentID:{DC1F1244-656B-45C8-B6A7-19FEC3753561}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_ErrorCode</Property>
	<Property Name="varPersistentID:{DC25DF2C-ECAA-440B-AA48-4C08BFD2921C}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-FilterTime_1</Property>
	<Property Name="varPersistentID:{DC4129A9-1828-490B-9337-91E8F0E4FDE1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_PollingMode</Property>
	<Property Name="varPersistentID:{DC6C5C4C-8324-4602-B7A8-CC525DC6090E}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Pressure_2</Property>
	<Property Name="varPersistentID:{DC9AC66B-76AC-48EF-867D-0148655E1762}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Decay-Delay-2</Property>
	<Property Name="varPersistentID:{DCAA5670-416C-4F77-B290-A5C245B101DE}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{DCCD11BD-4E76-4E12-A366-BA6E0A6B3DA0}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Dipole-V-IL</Property>
	<Property Name="varPersistentID:{DCE5BC88-60E3-46AA-A203-91451395F3D1}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_3</Property>
	<Property Name="varPersistentID:{DCFAB599-28A2-47CC-AFE3-1126FEC28B5B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-Max-QMP-Limit</Property>
	<Property Name="varPersistentID:{DD3188DC-3368-4CE8-B1CD-22741A301334}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{DD9A8C63-A16B-4742-9F39-5B85534BB14A}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VVP3_Status</Property>
	<Property Name="varPersistentID:{DDABB48B-69F2-4111-847A-8F2F149C1B01}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UXADT2_current</Property>
	<Property Name="varPersistentID:{DDF2C966-296E-4C9E-82A9-B7C09EED5A96}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-Q</Property>
	<Property Name="varPersistentID:{DE86AC73-CD32-4C1C-A23C-EE1C925D75BB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Logging</Property>
	<Property Name="varPersistentID:{DEDEA2EA-1941-4887-9A8A-8782C1E88135}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowSP_2</Property>
	<Property Name="varPersistentID:{DEE7AD15-D51D-46D1-B72C-AC10F389B1E3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{DF057392-6525-4FF6-BD89-A90F760D22AC}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SwitchingLimits_1</Property>
	<Property Name="varPersistentID:{DF27DBD6-D1E6-469B-A968-B72DC13D17D2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{DF504D96-0988-4AB5-BE7F-463F9C1518F1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SwitchingLimits_5</Property>
	<Property Name="varPersistentID:{DF632603-04B3-42A8-A345-00C17FFBF846}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_A</Property>
	<Property Name="varPersistentID:{DF6F8687-BD63-4953-9179-41825138F651}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PressureSetpoint</Property>
	<Property Name="varPersistentID:{DFAB4656-4A31-47A3-9DFD-507E50C34354}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_1</Property>
	<Property Name="varPersistentID:{E0543B46-AD09-4797-8FA9-A0B311AB0BA9}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_MFCOffset_0</Property>
	<Property Name="varPersistentID:{E08E12EC-5070-477C-842C-CC763EF9DF1D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{E119E96E-A5C9-4E40-9559-9875BA8753CB}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_DriverRevision</Property>
	<Property Name="varPersistentID:{E12BF8D2-94F9-448C-956C-656F062F14DA}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUX8QD12.currenti</Property>
	<Property Name="varPersistentID:{E144FAF0-BA85-43FC-AD3C-25C3E17A3280}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-V-IL</Property>
	<Property Name="varPersistentID:{E20E872D-B97F-4B96-B9F6-3E447E685B54}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowSP_1</Property>
	<Property Name="varPersistentID:{E29C55BB-86C0-4F0F-AB7F-31E712A935D5}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Trafo-Frequency</Property>
	<Property Name="varPersistentID:{E2B1BA47-1006-4827-B6EC-0D8863FC731E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GCF_1</Property>
	<Property Name="varPersistentID:{E2B8929E-B0BA-4892-B741-20CF312F8532}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Dipole-IL</Property>
	<Property Name="varPersistentID:{E2B93B44-2DCB-40E5-8C2B-716AF59B97A0}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_UnderrangeControl</Property>
	<Property Name="varPersistentID:{E2C26CA6-3763-420E-81EF-AC975AC740E0}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_Error</Property>
	<Property Name="varPersistentID:{E32565CF-338B-4030-8C96-822C27BCE9AA}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-TripMode_2</Property>
	<Property Name="varPersistentID:{E33E6C93-62DB-411D-906F-30A0DF5F2D80}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Status_2</Property>
	<Property Name="varPersistentID:{E3CBB756-69A4-457A-A13E-8CCF039247E5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-ControllerMode</Property>
	<Property Name="varPersistentID:{E3DE9647-D319-455B-9428-AC443A7CBF15}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Q-IL-Enabled</Property>
	<Property Name="varPersistentID:{E3E0A498-C971-46D8-8FAC-EC5F13871280}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_DipoleCurrent-Enabled</Property>
	<Property Name="varPersistentID:{E3F5FC0E-FC24-40EF-8160-5E08923872E0}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{E48A3238-8433-4DAC-9B2C-52B27543E5C8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF</Property>
	<Property Name="varPersistentID:{E4E7781F-0611-4961-A299-9412B314D855}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{E5082316-0736-471B-ACF1-BBFC8179BB2D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-GCF_2</Property>
	<Property Name="varPersistentID:{E54EB81B-069F-45CD-B523-6A0E73AF2D16}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Set-Max-QMP-Limit</Property>
	<Property Name="varPersistentID:{E5C851E3-2482-4B2B-A8E8-11671BDDC74A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PIDGain</Property>
	<Property Name="varPersistentID:{E5DAE156-E53F-4E36-90EB-3174C13814F4}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Targetwheel-IL</Property>
	<Property Name="varPersistentID:{E61BB95D-0345-4F95-8021-5B4A2CE05E36}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-Dipole-IL</Property>
	<Property Name="varPersistentID:{E692D2A2-C1D7-4B39-A3F0-68DE36723AC4}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Set-CircuitMode_2</Property>
	<Property Name="varPersistentID:{E6C07298-4D7A-44CE-8E72-155F5DD755FF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ChargeState</Property>
	<Property Name="varPersistentID:{E6C994BF-58D7-45F4-A188-67C1938AEC68}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_DMA-IQ-TO</Property>
	<Property Name="varPersistentID:{E7820D53-D2D1-4D03-A5F1-75CD2A6B5DB8}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Reset-Interlock</Property>
	<Property Name="varPersistentID:{E7D9B5D9-828B-46B9-96A1-3AB2FA32D6A5}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-Laser</Property>
	<Property Name="varPersistentID:{E863F3B5-9477-42F3-9AA5-45EF9D930B2E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_TripMode_3</Property>
	<Property Name="varPersistentID:{E91180EA-34DE-45D2-AA09-73F37A3A90F9}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{E914FA4F-A6AE-4B76-ADDE-D48292FE9E28}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Pyrometer-IL</Property>
	<Property Name="varPersistentID:{E9198D77-D3CA-40CE-81DA-17F6BFBAF748}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_SelftestResultCode</Property>
	<Property Name="varPersistentID:{E93D2610-F94C-48F3-9C49-9C62E1203F1D}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/BaseActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{E9CF39E5-EBDE-4DED-85F0-5ADBF3638EE8}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_SelfTest</Property>
	<Property Name="varPersistentID:{EA1A39AF-2379-4CD3-BEB8-85B424507320}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Flow3_TASCAGC</Property>
	<Property Name="varPersistentID:{EA302A2B-1B82-4702-B4D4-B54C6EA57BF6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_2</Property>
	<Property Name="varPersistentID:{EA360C3F-6992-4F86-8B6B-2931E65CD861}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Pressure_0</Property>
	<Property Name="varPersistentID:{EA7F497D-4877-46F6-9CAC-D34DFC8F12E4}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-TSRMin</Property>
	<Property Name="varPersistentID:{EAF8E96E-3D8F-48B0-9EEB-7A4B90DF42B7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS</Property>
	<Property Name="varPersistentID:{EB6E9D66-A387-4F24-9379-9ED8FF4C1EAD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_PIDGain</Property>
	<Property Name="varPersistentID:{EB7447ED-DDC5-4C4B-AF85-3DD59A39C1D1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SPS_1</Property>
	<Property Name="varPersistentID:{EBAC33ED-A87F-47D9-AB77-C2182B06EDF8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-Flow_1</Property>
	<Property Name="varPersistentID:{EC2AD6F0-1E6F-4640-984C-8A1D27034864}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_Pressure_3</Property>
	<Property Name="varPersistentID:{EC33D810-F07E-4134-A20D-B90F41C80ACB}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Interval</Property>
	<Property Name="varPersistentID:{ECB8EA5C-0EE6-4532-816A-9F0038F6F690}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UXADT2-Q</Property>
	<Property Name="varPersistentID:{ED394D2B-7F36-44AA-9C6C-FDF96612E139}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SetID</Property>
	<Property Name="varPersistentID:{ED4CE2DD-D539-440F-B396-5A554335D013}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_2</Property>
	<Property Name="varPersistentID:{ED7BBC66-670C-4A9B-9A39-A91153791394}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SwitchingLimits_0</Property>
	<Property Name="varPersistentID:{EDD1FCF8-35E9-482D-9AF2-75A5D87B8D78}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_FirmwareRevision</Property>
	<Property Name="varPersistentID:{EDE72EF2-F165-4BCF-A48D-06D08E9BECCD}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_GCF</Property>
	<Property Name="varPersistentID:{EE22620D-DF49-4101-94BD-94C95D054E73}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Set-UnderrangeControl</Property>
	<Property Name="varPersistentID:{EE573713-8ACE-4272-9AB3-188C48C94DAE}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Flow_1</Property>
	<Property Name="varPersistentID:{EE6B6CFC-529E-420C-B6C3-33F25151834B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UX8DT3-I</Property>
	<Property Name="varPersistentID:{EE9E1933-511B-4BDF-9172-4111D9A1875B}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_3</Property>
	<Property Name="varPersistentID:{EEBAC4D7-1F23-4B37-B8C3-0D273FFA8B88}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_CircuitMode_0</Property>
	<Property Name="varPersistentID:{EF061B5B-BE95-40A8-A6CC-EDE11B6FD494}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{F0026ED5-68BD-4BCA-A8C6-32568B2DA294}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_ErrorMessage</Property>
	<Property Name="varPersistentID:{F04995B1-9B89-4710-A32A-8172C5C137D1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Status_1</Property>
	<Property Name="varPersistentID:{F0D49C6B-02DC-4C5E-8B2F-CCCD698CB1A5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_PollingInterval</Property>
	<Property Name="varPersistentID:{F0F59619-9A09-4D46-AA0D-F451A255B588}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Chopper-In</Property>
	<Property Name="varPersistentID:{F0FD0169-6E81-452C-AC3F-10C99B538A53}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_B</Property>
	<Property Name="varPersistentID:{F16EB9F3-BD00-4268-9AFB-854B56ECCA75}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Dipole-IL-Enabled</Property>
	<Property Name="varPersistentID:{F195F511-FC4B-4D47-9F79-A475FE1DFACB}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_3</Property>
	<Property Name="varPersistentID:{F1B58577-72DE-4117-BCD8-A930948096F9}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Targetwheel-IL-Enabled</Property>
	<Property Name="varPersistentID:{F24EF9DD-220C-4B5A-9BAB-0533951BDAB8}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-FlowRange_2</Property>
	<Property Name="varPersistentID:{F334AA58-26AB-4EFE-896A-0277BBC6F51A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-GON_1</Property>
	<Property Name="varPersistentID:{F34DA994-3F1B-42B7-86CE-EAD2DFFADDB6}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingTime</Property>
	<Property Name="varPersistentID:{F38320F9-FBCD-44C0-9D17-0A1C3EC0AD77}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_UnderrangeControl</Property>
	<Property Name="varPersistentID:{F3CD39EC-32DF-42FA-A752-BFA6203258C3}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UX8DC4_range</Property>
	<Property Name="varPersistentID:{F4002F9D-188D-42FF-89A3-E34C9F6DFCCC}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-ControllerMode</Property>
	<Property Name="varPersistentID:{F40290B4-190C-41B9-86CC-3CCB89E5684A}" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib/GUX8DT3.currinfo_0</Property>
	<Property Name="varPersistentID:{F405210A-8DE4-43FB-9382-EF0A614A2CBD}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Range</Property>
	<Property Name="varPersistentID:{F45603B8-0781-4FCF-97C3-DAC9708E54E7}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Enable-DipoleCurrent-AlarmAction</Property>
	<Property Name="varPersistentID:{F4CCE1D0-AAEA-4626-9E05-731BB4CA668C}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Charge-IL-Enabled</Property>
	<Property Name="varPersistentID:{F4DDA5B5-2A20-495E-B5B5-7F96E2A972A0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_ResourceName</Property>
	<Property Name="varPersistentID:{F4F5704C-565D-486A-AD3C-64D913C264AF}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Target</Property>
	<Property Name="varPersistentID:{F5C485FC-A163-423D-9FF3-F765866B47D6}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{F5C7E44F-DA50-44CF-B61A-10216E5137A5}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_Set-SetID</Property>
	<Property Name="varPersistentID:{F5E7D363-5F69-4979-8997-15F34B013C9F}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{F60A488F-D148-4325-807C-EF85002999C7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_FilterTime_1</Property>
	<Property Name="varPersistentID:{F648314E-00E1-4013-8590-487F8E09B933}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{F64CC437-B982-41ED-9DE2-5EC482BAC2E0}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowHL_3</Property>
	<Property Name="varPersistentID:{F68B6D41-56D5-43D6-96D7-A9A07374E4F4}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Set-ClearMaxTime</Property>
	<Property Name="varPersistentID:{F6CBE41E-280F-45E6-B074-CD2F31919EA1}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_2</Property>
	<Property Name="varPersistentID:{F7052E16-8865-4E02-9787-FAC43B25D8CB}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_HV4</Property>
	<Property Name="varPersistentID:{F74785C9-C453-4718-9FFE-A896384B0CAA}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Error Code</Property>
	<Property Name="varPersistentID:{F75365C1-15E3-4A85-A6A1-1B330121ADC2}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingDeltaT</Property>
	<Property Name="varPersistentID:{F7BF3259-B2B7-44AF-8A34-3378A6AE65C7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ResourceName</Property>
	<Property Name="varPersistentID:{F800E79B-46C5-4976-9265-37DF712697C1}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUXIDC6_P.positi</Property>
	<Property Name="varPersistentID:{F80BA1CA-830D-4BE7-94E2-A36811A69D03}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_PollingCounter</Property>
	<Property Name="varPersistentID:{F82EBA93-78AA-4CC3-AB96-85C9B1ACAC28}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Enable-Detector_Rate_Limit</Property>
	<Property Name="varPersistentID:{F8383D5E-5474-4898-A912-23CBD84A9185}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-3_SetID</Property>
	<Property Name="varPersistentID:{F8428611-4558-4F6E-9350-0364C01627C5}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowHL_3</Property>
	<Property Name="varPersistentID:{F84FBCDF-C9B1-4777-8C11-0848747E5BE7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1-Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{F87DD463-5F2D-4360-91DD-11E60BFA192D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-PIDLead</Property>
	<Property Name="varPersistentID:{F88313D3-5D47-4883-8B6B-B1A48BF867C9}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/Pressure_RTCGC</Property>
	<Property Name="varPersistentID:{F8CCB2B8-A74C-4597-B911-B57DA9BC035D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_Set-FlowRange_0</Property>
	<Property Name="varPersistentID:{F963BA19-93E9-4787-BDD4-51FE9B014DEF}" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib/UTCSAlarmHandler_PollingMode</Property>
	<Property Name="varPersistentID:{F9A825B4-5208-4564-B980-89166CEF91F2}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_FlowLL_0</Property>
	<Property Name="varPersistentID:{F9B51BEC-20B4-41F3-B435-9CE4D564C72D}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_HW-Reset</Property>
	<Property Name="varPersistentID:{F9BF5C20-A306-4201-BAB5-68FB62B2485F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_ErrorStatus</Property>
	<Property Name="varPersistentID:{FA968601-1BD4-4F82-B722-51BC6FFA6C0F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-FilterTime_3</Property>
	<Property Name="varPersistentID:{FAB4456D-1D82-41D0-8EC4-1736FD6F449D}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_TripMode_1</Property>
	<Property Name="varPersistentID:{FACBB0C5-E83F-4DC2-A4D5-BB0E3A530736}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/HV6_Status</Property>
	<Property Name="varPersistentID:{FB3643DA-03DA-4569-ADE2-F09180EA6F62}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/RatTrap-IL-Enabled</Property>
	<Property Name="varPersistentID:{FB5C72A3-E566-4F33-BEAB-ECAC463536F5}" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib/GUT2DCX.currinfo</Property>
	<Property Name="varPersistentID:{FB6C9DBC-9F22-46C0-A822-98A601D52D36}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_SPS_1</Property>
	<Property Name="varPersistentID:{FB803290-28C0-47BE-A231-EA51FC907D2B}" Type="Ref">/My Computer/CSPP/Core/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{FB8E8538-0F98-4795-9A5D-F3AE61F82B85}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_Reset</Property>
	<Property Name="varPersistentID:{FD16DF3C-79A3-4518-B931-12C40FCEFA33}" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib/Watchdog_PollingCounter</Property>
	<Property Name="varPersistentID:{FD6F43E0-CE77-4DF5-9166-C7EE61B07148}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/VGTP3_Status</Property>
	<Property Name="varPersistentID:{FD79C244-A754-44C5-BC8C-ADD168640139}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_UXADT2-I</Property>
	<Property Name="varPersistentID:{FD902F4C-3B01-4CD7-A230-A1C899CC0B74}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_MFCOffsets</Property>
	<Property Name="varPersistentID:{FE1CBCAC-CC02-4C14-9CE3-A4C2D601B9A9}" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib/Server Type</Property>
	<Property Name="varPersistentID:{FE407E89-669D-4B4E-B62A-E9ACD8DCFA97}" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{FE55DE71-F422-446E-B82C-F02A0265B3D3}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-5_PollingTime</Property>
	<Property Name="varPersistentID:{FE5BCE45-3DA3-41BB-83B8-958F0FA4E01B}" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib/UT2DCX_range</Property>
	<Property Name="varPersistentID:{FE8F3179-AC4F-46B6-B3F5-28F481404D8B}" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib/BMIL_RatTrap</Property>
	<Property Name="varPersistentID:{FE989D66-FC7B-42E3-BB5C-9712270CDE43}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-1_Status_1</Property>
	<Property Name="varPersistentID:{FEEEC2D8-7177-4C8A-9FDF-7706DB576D3F}" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib/IGA140_Emissivity</Property>
	<Property Name="varPersistentID:{FF35716A-21CC-4315-BCC8-D617A2173FB7}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Reset</Property>
	<Property Name="varPersistentID:{FF5D532C-956E-4FE4-97E6-9016604A8E2F}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-4_Set-CircuitMode_1</Property>
	<Property Name="varPersistentID:{FFA6A020-186F-4C99-B934-14968F874E8E}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/TASCAGC_FlowHL-Main</Property>
	<Property Name="varPersistentID:{FFCBA391-5347-4CD9-93A2-334A8AA6FF1A}" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib/RTCGC_Set-Flow_1</Property>
	<Property Name="varPersistentID:{FFE5BD75-2613-46E3-83ED-75A512F77F20}" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib/TPG300-2_SPS_0</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="ACC" Type="Folder">
			<Item Name="UTCS_AccIO.lvlib" Type="Library" URL="../SV.lib/UTCS_AccIO.lvlib"/>
			<Item Name="UTCS_AccIOS.lvlib" Type="Library" URL="../SV.lib/UTCS_AccIOS.lvlib"/>
		</Item>
		<Item Name="AF" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
		</Item>
		<Item Name="BNT" Type="Folder">
			<Item Name="BNT_DAQmx" Type="Folder">
				<Item Name="BNT_DAQmx-Content.vi" Type="VI" URL="../Packages/BNT_DAQmx/BNT_DAQmx-Content.vi"/>
				<Item Name="BNT_DAQmx.lvlib" Type="Library" URL="../Packages/BNT_DAQmx/BNT_DAQmx.lvlib"/>
			</Item>
		</Item>
		<Item Name="COMPACT" Type="Folder">
			<Item Name="COMPACT_PressureAlarm.lvlib" Type="Library" URL="../Packages/COMPACT/COMPACT_PressureAlarm/COMPACT_PressureAlarm.lvlib"/>
		</Item>
		<Item Name="CSPP" Type="Folder">
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_LMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LMMonitor/CSPP_LMMonitor.lvlib"/>
					<Item Name="CSPP_LNMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LNMonitor/CSPP_LNMonitor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_PVSubscriber.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVSubscriber/CSPP_PVSubscriber.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
					<Item Name="CSPP_Watchdog.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_Watchdog/CSPP_Watchdog.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Libs" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_NAInitialized Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_NAInitialized Msg/CSPP_NAInitialized Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
					<Item Name="CSPP_Watchdog Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_Watchdog Msg/CSPP_Watchdog Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_Core_SV.lvlib" Type="Library" URL="../Packages/CSPP_Core/CSPP_Core_SV.lvlib"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
				<Item Name="CSPP_Post-Build Action.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_Post-Build Action.vi"/>
			</Item>
			<Item Name="DSC" Type="Folder">
				<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
				<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
				<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
				<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
				<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
				<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
				<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				<Item Name="CSPP_DSCUtilities.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Libs/CSPP_DSCUtilities/CSPP_DSCUtilities.lvlib"/>
				<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
			</Item>
			<Item Name="ObjectManager" Type="Folder">
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
			</Item>
			<Item Name="PVConverter" Type="Folder">
				<Item Name="CSPP_PV2ArrayConverter.lvlib" Type="Library" URL="../Packages/CSPP_PVConverter/CSPP_PV2ArrayConverter.lvlib"/>
				<Item Name="CSPP_PVConverter-Content.vi" Type="VI" URL="../Packages/CSPP_PVConverter/CSPP_PVConverter-Content.vi"/>
				<Item Name="CSPP_PVConverter.ini" Type="Document" URL="../Packages/CSPP_PVConverter/CSPP_PVConverter.ini"/>
				<Item Name="CSPP_PVScaler.lvlib" Type="Library" URL="../Packages/CSPP_PVConverter/CSPP_PVScaler.lvlib"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
				<Item Name="CSPP_SystemMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_SystemMonitor/CSPP_SystemMonitor.lvlib"/>
				<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
			</Item>
			<Item Name="Vacuum" Type="Folder">
				<Item Name="TPG300A.lvlib" Type="Library" URL="../Packages/CSPP_Vacuum/CSPP_TPG300/TPG300A.lvlib"/>
				<Item Name="TPG300GUI.lvlib" Type="Library" URL="../Packages/CSPP_Vacuum/CSPP_TPG300/TPG300 GUI/TPG300GUI.lvlib"/>
			</Item>
		</Item>
		<Item Name="Devices" Type="Folder">
			<Item Name="CSPP_IGA140.lvlib" Type="Library" URL="../Packages/UTCS/IGA140/CSPP_IGA140.lvlib"/>
			<Item Name="CSPP_IGA140GUI.lvlib" Type="Library" URL="../Packages/UTCS/IGA140/CSPP_IGA140GUI.lvlib"/>
			<Item Name="MKS647C.lvlib" Type="Library" URL="../Packages/CSPP_MKS647C/MKS647C Actor/MKS647C.lvlib"/>
			<Item Name="MKS647CGUI.lvlib" Type="Library" URL="../Packages/CSPP_MKS647C/MKS647C GUI/MKS647CGUI.lvlib"/>
			<Item Name="UTCS_BMIL.lvlib" Type="Library" URL="../Packages/UTCS/BeamControl/Host/UTCS_BMIL.lvlib"/>
		</Item>
		<Item Name="Docs &amp; EUPL" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
			<Item Name="README.md" Type="Document" URL="../README.md"/>
		</Item>
		<Item Name="instr.lib" Type="Folder">
			<Item Name="IGA140.lvlib" Type="Library" URL="../instr.lib/IGA140/IGA140.lvlib"/>
			<Item Name="MGC647C.lvlib" Type="Library" URL="../instr.lib/MGC647C/MGC647C.lvlib"/>
			<Item Name="TPG300.lvlib" Type="Library" URL="../instr.lib/TPG300/TPG300.lvlib"/>
		</Item>
		<Item Name="libs" Type="Folder">
			<Item Name="ni_security_salapi.dll" Type="Document" URL="/&lt;vilib&gt;/Platform/security/ni_security_salapi.dll"/>
		</Item>
		<Item Name="Operating GUIs" Type="Folder">
			<Item Name="UTCS_BeamControlGUI.lvlib" Type="Library" URL="../Packages/UTCS/BeamControl-GUI/UTCS_BeamControlGUI.lvlib"/>
			<Item Name="UTCS_GasGUI.lvlib" Type="Library" URL="../Packages/UTCS/Gas-GUI/UTCS_GasGUI.lvlib"/>
			<Item Name="UTCS_InterlockGUI.lvlib" Type="Library" URL="../Packages/UTCS/Interlock-GUI/UTCS_InterlockGUI.lvlib"/>
			<Item Name="UTCS_MainGUI.lvlib" Type="Library" URL="../Packages/UTCS/UTCS_MainGUI/UTCS_MainGUI.lvlib"/>
		</Item>
		<Item Name="TASCA" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="UTCS_Content.vi" Type="VI" URL="../Packages/UTCS/UTCS_Content.vi"/>
			<Item Name="UTCS_AE.lvlib" Type="Library" URL="../SV.lib/UTCS_AE.lvlib"/>
			<Item Name="UTCS_BMIL_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_BMIL_SV.lvlib"/>
			<Item Name="UTCS_MKS647_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_MKS647_SV.lvlib"/>
			<Item Name="UTCS_TPG300_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_TPG300_SV.lvlib"/>
			<Item Name="TPG300_Pressures.xml" Type="Document" URL="../SV.lib/TPG300_Pressures.xml"/>
			<Item Name="UTCS_Scaled.lvlib" Type="Library" URL="../SV.lib/UTCS_Scaled.lvlib"/>
			<Item Name="UTCS_IGA140_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_IGA140_SV.lvlib"/>
			<Item Name="UTCS_Watchdog_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_Watchdog_SV.lvlib"/>
			<Item Name="UTCS_SystemMonitor_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_SystemMonitor_SV.lvlib"/>
			<Item Name="UTCS_AlarmHandler.lvlib" Type="Library" URL="../Packages/UTCS/UTCS_AlarmHandler/UTCS_AlarmHandler.lvlib"/>
			<Item Name="UTCS_AlarmHandler_SV.lvlib" Type="Library" URL="../SV.lib/UTCS_AlarmHandler_SV.lvlib"/>
		</Item>
		<Item Name="Test" Type="Folder">
			<Item Name="Test Sgl Precision.vi" Type="VI" URL="../Packages/UTCS/BeamControl/Test Sgl Precision.vi"/>
			<Item Name="TestHTV.vi" Type="VI" URL="../SV.lib/TestHTV.vi"/>
		</Item>
		<Item Name="COMAPCT.ico" Type="Document" URL="../Packages/COMPACT/COMAPCT.ico"/>
		<Item Name="FPGA Target" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{00066C3D-82D3-433F-8F36-A3BAD945E3A4}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=bool{01F907F0-836D-4C96-B720-54813DEBC90B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{0293F661-3699-4CAD-B061-66A871F91C46}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIOPORT3;0;ReadMethodType=u8;WriteMethodType=u8{03BFC9DF-98F1-4CE0-82E0-8D3293D899D1}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{061F5441-6011-4843-B9D9-B5A6FC89D685}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{0E2BC540-879A-4221-A151-6F663E8A8C14}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{0ED9774E-FE0E-4F41-9CF4-108A99628B68}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{11AF4D10-A39C-4F8A-A085-F1A5AF09398F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{14C58F7D-E8FB-4C78-A6A9-1F9A804FA277}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{1A4084AD-8B1D-4D25-A481-B35907B208D2}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=bool{1BC7C33C-3A64-4F26-903C-4A91935BE252}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{1E6BC860-409A-435C-BCEC-81FEB659B516}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{266C2459-A855-4A91-BCD1-115165AB9767}"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-IQ2H;DataType=100080000000000100094008000355363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{299D0051-F350-4A31-BB36-D72FCE3DA06D}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{37C23112-6317-42D9-AF48-CDFD55E634D4}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{44919FD4-3D71-483F-8C4E-2845A9624742}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{48829377-E5BC-4B77-AFE9-128876A49AD7}"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-SglToH;DataType=100080000000000100094009000353474C000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{57B6D4CD-5D67-4A9C-8663-2292847FEE4C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=bool{58A07884-4C14-4D6F-A97F-0BBDFCB5F093}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{5CD1830C-DAD7-4E47-A38D-6A1F63880D02}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=bool{62B50772-CACB-4AE8-AB55-F554C51BEEE6}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{63808D5B-E07A-46B5-8D49-179AD5AE0D8A}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{643EEED8-9D17-4A9A-855C-DADAC52DA4AC}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{6A2B33CF-F17B-429E-8C51-B2B2AB359A35}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{6AABD936-71C1-44FE-B37D-55BE40171BF8}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{71EFCEF8-43EF-40A2-B612-167E1EBA4242}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{79012C1B-89C2-4E21-A4D5-141E0A3FA4D4}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{791B95AA-A2B7-4030-B2CF-A2BC00359520}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=bool{887E094A-10A2-425A-B758-E8FBB50DD85F}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{8A129C21-862E-4892-963B-DBDD5D131773}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=bool{8DCBB1F0-DEC7-4730-A3F9-F756E68BEAFE}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{9462100C-A467-4498-9D56-014D26321CFB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIOPORT2;0;ReadMethodType=u8;WriteMethodType=u8{9A7B3AC1-B914-42AE-8CED-54B00E74B914}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{A2A1863B-3FA1-4B0A-AB04-D87F5A55935E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{A303A87F-91FC-4F85-AE76-87E15FDB8D5D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{A938D12B-B5FE-4FA6-893E-2F5330FD3D08}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{B1C9E840-BE04-4824-B85A-F76BA1C41744}"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{B378D278-6A90-41ED-B0B7-4AC50BF667BC}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{BA05C583-54F9-4D3B-B71A-51F1BE0B00D9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIOPORT4;0;ReadMethodType=u8;WriteMethodType=u8{BD1719CC-9D39-4EFC-850C-D438C018C65E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{BFD850DC-9303-4E42-8CC8-7D9B3C684777}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{C2899B23-FE20-4397-ACFD-E04CD4E7D6CA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{C3D5E706-22E3-49E4-933F-14620AB2D742}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{CFF436AB-7C8D-4963-AC09-C3169A5EC90B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{D25B11EF-6508-4E1C-A7AA-FCDADDE86B6D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=bool{D3FB915C-4AE2-482D-91EE-4374F6200D6B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{D7C80D55-8199-4F11-86C9-54718D8FFF6C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{DC1EDA73-A601-42A6-A92A-43947DF473A4}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{DF25A013-DD62-4286-8058-368404E419BE}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{E07ACC35-0858-4790-ACF6-E4DE709E5FDF}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{E4EFF697-0451-40F1-814D-86BD275BCF29}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{E8BC867A-43D0-468D-B927-E169436626FB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{EA6305EB-B729-4EF7-ACA8-900E5C381BE8}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=bool{EADF8592-96A9-4B0F-8FBC-A93DA0BA8656}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{F3D12624-0E8B-4E26-B978-7B2AF101C28F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{F937254A-9780-4A1F-81CA-4633F7679326}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{FE85A530-012A-4190-AC85-44CABE29CF77}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{FEF4853E-9A73-40AE-9652-4FE0593CD49C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolPXI-7842R/DSCGetVarList,PSP;WebpubLaunchBrowser,None;DSCSecurity,False;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7842RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;5V-0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool5V-1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=boolAux In 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=boolAux In 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=boolAux In 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=boolAux Out 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=boolAux Out 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolAux Out 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=boolChopper InArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=boolChopper OutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolDetectorArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO1;0;ReadMethodType=bool;WriteMethodType=boolDetector-FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIOPORT2;0;ReadMethodType=u8;WriteMethodType=u8DMA-IQ-TOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=boolF_DipoleArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO8;0;ReadMethodType=bool;WriteMethodType=boolFIFO-Detector"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-MPL"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-MPP"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-SglToH"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-SglToH;DataType=100080000000000100094009000353474C000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-U64ToH"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-IQ2H;DataType=100080000000000100094008000355363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-UX8DT3"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FIFO-UXADT2"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"FirstArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=boolFirstOutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=boolInhibitArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=boolInterlock-0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolInterlock-1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=boolInterval FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=boolIntervalArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=boolKeyArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=boolMagnetArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=boolMP ActiveArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMP Out 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMP Out 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMP Out 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMP Out 3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMP_GenArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMP_InArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=boolPXI-7842R/DSCGetVarList,PSP;WebpubLaunchBrowser,None;DSCSecurity,False;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7842RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]PyrometerArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=boolRange 0ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=boolRange 1ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=boolRange 2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=boolRatTrapArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=boolSecondArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=boolSecondOutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=boolSecValvesByPassArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=boolSecValvesStateArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=boolSW-WatchdogArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO2;0;ReadMethodType=bool;WriteMethodType=boolTargetwheel OutArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolTargetwheelArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=boolTrafo_GenArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=boolUX8DT3ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO3;0;ReadMethodType=bool;WriteMethodType=boolUX8DT3-FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIOPORT3;0;ReadMethodType=u8;WriteMethodType=u8UXADT2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIO6;0;ReadMethodType=bool;WriteMethodType=boolUXADT2-FOArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector2/DIOPORT4;0;ReadMethodType=u8;WriteMethodType=u8V_Dipole_InArbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16V_Dipole_OutArbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16VacuumBurstArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=bool</Property>
			<Property Name="Mode" Type="Int">0</Property>
			<Property Name="NI.LV.FPGA.CLIPDeclarationsArraySize" Type="Int">0</Property>
			<Property Name="NI.LV.FPGA.CLIPDeclarationSet" Type="Xml">
<CLIPDeclarationSet>
</CLIPDeclarationSet></Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">PXI-7842R/DSCGetVarList,PSP;WebpubLaunchBrowser,None;DSCSecurity,False;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7842RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Property Name="Resource Name" Type="Str">PXI1Slot5</Property>
			<Property Name="SWEmulationSubMode" Type="UInt">0</Property>
			<Property Name="SWEmulationVIPath" Type="Path"></Property>
			<Property Name="Target Class" Type="Str">PXI-7842R</Property>
			<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="Connector0" Type="Folder">
				<Item Name="Input" Type="Folder">
					<Item Name="V_Dipole_In" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8DCBB1F0-DEC7-4730-A3F9-F756E68BEAFE}</Property>
					</Item>
				</Item>
				<Item Name="Output" Type="Folder">
					<Item Name="V_Dipole_Out" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{887E094A-10A2-425A-B758-E8FBB50DD85F}</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Connector 1" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Input" Type="Folder">
					<Item Name="MP_In" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D7C80D55-8199-4F11-86C9-54718D8FFF6C}</Property>
					</Item>
					<Item Name="Range 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DF25A013-DD62-4286-8058-368404E419BE}</Property>
					</Item>
					<Item Name="Range 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F3D12624-0E8B-4E26-B978-7B2AF101C28F}</Property>
					</Item>
					<Item Name="Range 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C3D5E706-22E3-49E4-933F-14620AB2D742}</Property>
					</Item>
					<Item Name="Chopper In" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F937254A-9780-4A1F-81CA-4633F7679326}</Property>
					</Item>
					<Item Name="RatTrap" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO32</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8A129C21-862E-4892-963B-DBDD5D131773}</Property>
					</Item>
					<Item Name="VacuumBurst" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO33</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5CD1830C-DAD7-4E47-A38D-6A1F63880D02}</Property>
					</Item>
					<Item Name="SecValvesByPass" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO34</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{791B95AA-A2B7-4030-B2CF-A2BC00359520}</Property>
					</Item>
					<Item Name="SecValvesState" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO35</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{57B6D4CD-5D67-4A9C-8663-2292847FEE4C}</Property>
					</Item>
					<Item Name="First" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO36</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D25B11EF-6508-4E1C-A7AA-FCDADDE86B6D}</Property>
					</Item>
					<Item Name="Second" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO37</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{EA6305EB-B729-4EF7-ACA8-900E5C381BE8}</Property>
					</Item>
					<Item Name="Pyrometer" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO38</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{00066C3D-82D3-433F-8F36-A3BAD945E3A4}</Property>
					</Item>
					<Item Name="Magnet" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO39</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1A4084AD-8B1D-4D25-A481-B35907B208D2}</Property>
					</Item>
					<Item Name="Aux In 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{37C23112-6317-42D9-AF48-CDFD55E634D4}</Property>
					</Item>
					<Item Name="Aux In 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E8BC867A-43D0-468D-B927-E169436626FB}</Property>
					</Item>
					<Item Name="Aux In 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{71EFCEF8-43EF-40A2-B612-167E1EBA4242}</Property>
					</Item>
					<Item Name="Targetwheel" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6AABD936-71C1-44FE-B37D-55BE40171BF8}</Property>
					</Item>
					<Item Name="Key" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{01F907F0-836D-4C96-B720-54813DEBC90B}</Property>
					</Item>
				</Item>
				<Item Name="Output" Type="Folder">
					<Item Name="Chopper Out" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E07ACC35-0858-4790-ACF6-E4DE709E5FDF}</Property>
					</Item>
					<Item Name="Interlock-0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FEF4853E-9A73-40AE-9652-4FE0593CD49C}</Property>
					</Item>
					<Item Name="5V-0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1BC7C33C-3A64-4F26-903C-4A91935BE252}</Property>
					</Item>
					<Item Name="5V-1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CFF436AB-7C8D-4963-AC09-C3169A5EC90B}</Property>
					</Item>
					<Item Name="Interlock-1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9A7B3AC1-B914-42AE-8CED-54B00E74B914}</Property>
					</Item>
					<Item Name="Inhibit" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{EADF8592-96A9-4B0F-8FBC-A93DA0BA8656}</Property>
					</Item>
					<Item Name="FirstOut" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{63808D5B-E07A-46B5-8D49-179AD5AE0D8A}</Property>
					</Item>
					<Item Name="SecondOut" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{62B50772-CACB-4AE8-AB55-F554C51BEEE6}</Property>
					</Item>
					<Item Name="DMA-IQ-TO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DC1EDA73-A601-42A6-A92A-43947DF473A4}</Property>
					</Item>
					<Item Name="MP Active" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6A2B33CF-F17B-429E-8C51-B2B2AB359A35}</Property>
					</Item>
					<Item Name="Trafo_Gen" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{58A07884-4C14-4D6F-A97F-0BBDFCB5F093}</Property>
					</Item>
					<Item Name="MP_Gen" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{14C58F7D-E8FB-4C78-A6A9-1F9A804FA277}</Property>
					</Item>
					<Item Name="MP Out 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C2899B23-FE20-4397-ACFD-E04CD4E7D6CA}</Property>
					</Item>
					<Item Name="MP Out 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D3FB915C-4AE2-482D-91EE-4374F6200D6B}</Property>
					</Item>
					<Item Name="MP Out 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0ED9774E-FE0E-4F41-9CF4-108A99628B68}</Property>
					</Item>
					<Item Name="MP Out 3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B378D278-6A90-41ED-B0B7-4AC50BF667BC}</Property>
					</Item>
					<Item Name="Targetwheel Out" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A303A87F-91FC-4F85-AE76-87E15FDB8D5D}</Property>
					</Item>
					<Item Name="Aux Out 0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{11AF4D10-A39C-4F8A-A085-F1A5AF09398F}</Property>
					</Item>
					<Item Name="Aux Out 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BFD850DC-9303-4E42-8CC8-7D9B3C684777}</Property>
					</Item>
					<Item Name="Aux Out 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0E2BC540-879A-4221-A151-6F663E8A8C14}</Property>
					</Item>
					<Item Name="Interval FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BD1719CC-9D39-4EFC-850C-D438C018C65E}</Property>
					</Item>
					<Item Name="Interval" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E4EFF697-0451-40F1-814D-86BD275BCF29}</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Connector 2" Type="Folder">
				<Item Name="Input" Type="Folder">
					<Item Name="Detector" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector2/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{79012C1B-89C2-4E21-A4D5-141E0A3FA4D4}</Property>
					</Item>
					<Item Name="F_Dipole" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector2/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{643EEED8-9D17-4A9A-855C-DADAC52DA4AC}</Property>
					</Item>
					<Item Name="UX8DT3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector2/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FE85A530-012A-4190-AC85-44CABE29CF77}</Property>
					</Item>
					<Item Name="UXADT2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector2/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A2A1863B-3FA1-4B0A-AB04-D87F5A55935E}</Property>
					</Item>
				</Item>
				<Item Name="Output" Type="Folder">
					<Item Name="Detector-FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector2/DIOPORT2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9462100C-A467-4498-9D56-014D26321CFB}</Property>
					</Item>
					<Item Name="SW-Watchdog" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector2/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1E6BC860-409A-435C-BCEC-81FEB659B516}</Property>
					</Item>
					<Item Name="UX8DT3-FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector2/DIOPORT3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0293F661-3699-4CAD-B061-66A871F91C46}</Property>
					</Item>
					<Item Name="UXADT2-FO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector2/DIOPORT4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BA05C583-54F9-4D3B-B71A-51F1BE0B00D9}</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{061F5441-6011-4843-B9D9-B5A6FC89D685}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="FIFO-Detector" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">4</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{A938D12B-B5FE-4FA6-893E-2F5330FD3D08}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094004000349363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="FIFO-MPL" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">7</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{299D0051-F350-4A31-BB36-D72FCE3DA06D}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">1000800000000001000940070003553332000100000000000000000000</Property>
			</Item>
			<Item Name="FIFO-MPP" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">7</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{03BFC9DF-98F1-4CE0-82E0-8D3293D899D1}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">1000800000000001000940070003553332000100000000000000000000</Property>
			</Item>
			<Item Name="FIFO-SglToH" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">8191</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">11</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-SglToH;DataType=100080000000000100094009000353474C000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{48829377-E5BC-4B77-AFE9-128876A49AD7}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">8191</Property>
				<Property Name="Type" Type="UInt">2</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094009000353474C000100000000000000000000</Property>
			</Item>
			<Item Name="FIFO-U64ToH" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">8191</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">8</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=8191;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO-IQ2H;DataType=100080000000000100094008000355363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{266C2459-A855-4A91-BCD1-115165AB9767}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">8191</Property>
				<Property Name="Type" Type="UInt">2</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094008000355363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="FIFO-UX8DT3" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">4</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{44919FD4-3D71-483F-8C4E-2845A9624742}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094004000349363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="FIFO-UXADT2" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">133</Property>
				<Property Name="Arbitration for Read" Type="UInt">2</Property>
				<Property Name="Arbitration for Write" Type="UInt">2</Property>
				<Property Name="Control Logic" Type="UInt">1</Property>
				<Property Name="Data Type" Type="UInt">4</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=1;NumberOfElements=133;Type=0;ReadArbs=Never Arbitrate;ElementsPerRead=1;WriteArbs=Never Arbitrate;ElementsPerWrite=1;Implementation=2;;DataType=100080000000000100094004000349363400010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{B1C9E840-BE04-4824-B85A-F76BA1C41744}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">2</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">100080000000000100094004000349363400010000000000000000000000000000</Property>
			</Item>
			<Item Name="IP Builder" Type="IP Builder Target">
				<Item Name="Dependencies" Type="Dependencies"/>
				<Item Name="Build Specifications" Type="Build"/>
			</Item>
			<Item Name="UTCS_BC_FPGA.lvlib" Type="Library" URL="../Packages/UTCS/BeamControl/FPGA/UTCS_BC_FPGA.lvlib"/>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="FxpSim.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/FXPMathLib/sim/FxpSim.dll"/>
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="UTCS_PXI-7842R_Main" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">UTCS_PXI-7842R_Main</Property>
					<Property Name="Comp.BitfileName" Type="Str">UTCS_PXI-7842R_Main.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">11</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">1</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">Packages/UTCS/BeamControl/FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/User/Brand/LVP/TASCA/UTCS/Packages/UTCS/BeamControl/FPGA Bitfiles/UTCS_PXI-7842R_Main.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">Packages/UTCS/BeamControl/FPGA Bitfiles/UTCS_PXI-7842R_Main.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/User/Brand/LVP/UTCS/UTCS_RC.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
					<Property Name="TargetName" Type="Str">FPGA Target</Property>
					<Property Name="TopLevelVI" Type="Ref">/My Computer/FPGA Target/UTCS_BC_FPGA.lvlib/Main.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="niwebserver.conf" Type="Document" URL="../niwebserver.conf"/>
		<Item Name="UTCS.ico" Type="Document" URL="../UTCS.ico"/>
		<Item Name="UTCS.ini" Type="Document" URL="../UTCS.ini"/>
		<Item Name="UTCS.vi" Type="VI" URL="../Packages/UTCS/UTCS.vi"/>
		<Item Name="UTCS_RC.ini" Type="Document" URL="../UTCS_RC.ini"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Batch Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Batch Msg/Batch Msg.lvclass"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="Check for multiple of dt.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for multiple of dt.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="CIT_ReadTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/citadel/CIT_ReadTimeout.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="CTL_dbNameValid.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_dbNameValid.vi"/>
				<Item Name="CTL_dbURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_dbURLdecode.vi"/>
				<Item Name="CTL_defaultEvtDB.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultEvtDB.vi"/>
				<Item Name="CTL_defaultHistDB.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultHistDB.vi"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="CTL_extractURLMDPformat.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_extractURLMDPformat.vi"/>
				<Item Name="CTL_findDSCApp.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_findDSCApp.vi"/>
				<Item Name="CTL_getAllDBInfo.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getAllDBInfo.vi"/>
				<Item Name="CTL_getArrayPathAndTraceReentrant.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getArrayPathAndTraceReentrant.vi"/>
				<Item Name="CTL_getDBFromDir.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getDBFromDir.vi"/>
				<Item Name="CTL_getDBPathandTraceList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_getDBPathandTraceList.vi"/>
				<Item Name="CTL_hdManager.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdManager.vi"/>
				<Item Name="CTL_hdManagerBuffer.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdManagerBuffer.vi"/>
				<Item Name="CTL_hdProxyManager.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_hdProxyManager.vi"/>
				<Item Name="CTL_lookupTagURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_lookupTagURL.vi"/>
				<Item Name="CTL_resolveSourceDBURLInput.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_resolveSourceDBURLInput.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Control Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Control Task.vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Timing (Burst Export Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Export Clock).vi"/>
				<Item Name="DAQmx Timing (Burst Import Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Import Clock).vi"/>
				<Item Name="DAQmx Timing (Change Detection).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Change Detection).vi"/>
				<Item Name="DAQmx Timing (Handshaking).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Handshaking).vi"/>
				<Item Name="DAQmx Timing (Implicit).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Implicit).vi"/>
				<Item Name="DAQmx Timing (Pipelined Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Pipelined Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Use Waveform).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Use Waveform).vi"/>
				<Item Name="DAQmx Timing.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="dsc_PrefsPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/info/dsc_PrefsPath.vi"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="dscHistD.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/historical/internal/dscHistD.dll"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Digital Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Subset.vi"/>
				<Item Name="DTbl Get Digital Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Get Digital Value.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital Size.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="DWDT Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get Waveform Subset.vi"/>
				<Item Name="DWDT Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get XY Value.vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="FormatTime String.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/FormatTime String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get Project Library Version.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Get Project Library Version.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get Waveform Subset.vi"/>
				<Item Name="Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get XY Value.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="HIST_AlarmDataToControl.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_AlarmDataToControl.vi"/>
				<Item Name="HIST_BuildAlarmColumns.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_BuildAlarmColumns.vi"/>
				<Item Name="HIST_CheckAlarmCtlRef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_CheckAlarmCtlRef.vi"/>
				<Item Name="HIST_ExtractAlarmData.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_ExtractAlarmData.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="HIST_GET_FILTER_ERRORS.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_GET_FILTER_ERRORS.vi"/>
				<Item Name="HIST_GetFilterTime.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_GetFilterTime.vi"/>
				<Item Name="HIST_GetHistTagListCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_GetHistTagListCORE.vi"/>
				<Item Name="HIST_ReadBitArrayTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadBitArrayTrace.vi"/>
				<Item Name="HIST_ReadBitArrayTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadBitArrayTraceCORE.vi"/>
				<Item Name="HIST_ReadLogicalTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadLogicalTrace.vi"/>
				<Item Name="HIST_ReadLogicalTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadLogicalTraceCORE.vi"/>
				<Item Name="HIST_ReadNumericTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadNumericTrace.vi"/>
				<Item Name="HIST_ReadNumericTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadNumericTraceCORE.vi"/>
				<Item Name="HIST_ReadStringTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadStringTrace.vi"/>
				<Item Name="HIST_ReadStringTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadStringTraceCORE.vi"/>
				<Item Name="HIST_ReadVariantTrace.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadVariantTrace.vi"/>
				<Item Name="HIST_ReadVariantTraceCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ReadVariantTraceCORE.vi"/>
				<Item Name="HIST_RunAlarmQueryCORE.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_RunAlarmQueryCORE.vi"/>
				<Item Name="HIST_VALIDATE_FILTER.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_VALIDATE_FILTER.vi"/>
				<Item Name="HIST_ValReadTrendOptions.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/HIST_ValReadTrendOptions.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NET_convertLocalhostURLToMachineURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_convertLocalhostURLToMachineURL.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="NET_handleDotInTagName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_handleDotInTagName.vi"/>
				<Item Name="NET_IsComputerLocalhost.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_IsComputerLocalhost.vi"/>
				<Item Name="NET_localhostToMachineName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_localhostToMachineName.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_resolveTagURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveTagURL.vi"/>
				<Item Name="NET_SameMachine.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_SameMachine.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Security Domain.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Domain.ctl"/>
				<Item Name="NI_Security Get Domains.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Get Domains.vi"/>
				<Item Name="NI_Security Identifier.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Identifier.ctl"/>
				<Item Name="NI_Security Resolve Domain.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Resolve Domain.vi"/>
				<Item Name="NI_Security_GetTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_GetTimeout.vi"/>
				<Item Name="NI_Security_ProgrammaticLogin.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ProgrammaticLogin.vi"/>
				<Item Name="NI_Security_ResolveDomainID.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainID.vi"/>
				<Item Name="NI_Security_ResolveDomainName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainName.vi"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Number of Waveform Samples.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Number of Waveform Samples.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="PTH_EmptyOrNotAPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_EmptyOrNotAPath.vi"/>
				<Item Name="PTH_IsUNC.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_IsUNC.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="SEC Get Interactive User.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/custom/SEC Get Interactive User.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subElapsedTime.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/subElapsedTime.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="TIME_FormatTS(TS).vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/time/TIME_FormatTS(TS).vi"/>
				<Item Name="TIME_StartTsLEStopTs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/time/TIME_StartTsLEStopTs.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="VISA Lock Async.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Lock Async.vi"/>
				<Item Name="WDT Get Waveform Subset CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset CDB.vi"/>
				<Item Name="WDT Get Waveform Subset DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset DBL.vi"/>
				<Item Name="WDT Get Waveform Subset EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset EXT.vi"/>
				<Item Name="WDT Get Waveform Subset I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I8.vi"/>
				<Item Name="WDT Get Waveform Subset I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I16.vi"/>
				<Item Name="WDT Get Waveform Subset I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I32.vi"/>
				<Item Name="WDT Get Waveform Subset SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset SGL.vi"/>
				<Item Name="WDT Get XY Value CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CDB.vi"/>
				<Item Name="WDT Get XY Value CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CXT.vi"/>
				<Item Name="WDT Get XY Value DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value DBL.vi"/>
				<Item Name="WDT Get XY Value EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value EXT.vi"/>
				<Item Name="WDT Get XY Value I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I16.vi"/>
				<Item Name="WDT Get XY Value I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I32.vi"/>
				<Item Name="WDT Get XY Value I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I64.vi"/>
				<Item Name="WDT Number of Waveform Samples CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples CDB.vi"/>
				<Item Name="WDT Number of Waveform Samples DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples DBL.vi"/>
				<Item Name="WDT Number of Waveform Samples EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples EXT.vi"/>
				<Item Name="WDT Number of Waveform Samples I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I8.vi"/>
				<Item Name="WDT Number of Waveform Samples I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I16.vi"/>
				<Item Name="WDT Number of Waveform Samples I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I32.vi"/>
				<Item Name="WDT Number of Waveform Samples SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples SGL.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="UTCS_PXI-7842R_Main.lvbitx" Type="Document" URL="../Packages/UTCS/BeamControl/FPGA Bitfiles/UTCS_PXI-7842R_Main.lvbitx"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="UTCS App" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{E438EDDD-4AB6-4A1B-A2EF-F405DCA1D8EC}</Property>
				<Property Name="App_INI_GUID" Type="Str">{D5B3B86F-9F9C-441E-BBE8-1F64BE17203A}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/UTCS.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="App_winsec.certificate" Type="Str">Cantemir Raul (Cantemir)</Property>
				<Property Name="App_winsec.description" Type="Str">http://timestamp.digicert.com</Property>
				<Property Name="App_winsec.timestamp" Type="Str">http://timestamp.digicert.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{90E67491-152D-4A03-9F72-27DC414438AE}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">UTCS App</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/D/builds/NI_AB_PROJECTNAME/UTCS App</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/CSPP/Core/CSPP_Post-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{36487EFB-0CEC-4BE6-87F2-BBA5FADEB0E7}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">/D/builds/UTCS_RC/UTCS App/UTCS_UTCS App_log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">5</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.minor" Type="Int">2</Property>
				<Property Name="Bld_version.patch" Type="Int">2</Property>
				<Property Name="Destination[0].destName" Type="Str">UTCS.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/D/builds/NI_AB_PROJECTNAME/UTCS App/UTCS.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/D/builds/NI_AB_PROJECTNAME/UTCS App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">Exe Directory</Property>
				<Property Name="Destination[2].path" Type="Path">/D/builds/NI_AB_PROJECTNAME/UTCS App</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/UTCS.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{E370AE73-4BEF-48C2-A170-91609AF92F55}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/UTCS.ini</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/TASCA/TPG300_Pressures.xml</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/ACC/UTCS_AccIO.lvlib</Property>
				<Property Name="Source[11].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].type" Type="Str">Library</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/ACC/UTCS_AccIOS.lvlib</Property>
				<Property Name="Source[12].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[12].type" Type="Str">Library</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/UTCS_RC.ini</Property>
				<Property Name="Source[13].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/libs/ni_security_salapi.dll</Property>
				<Property Name="Source[14].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/TASCA/UTCS_Scaled.lvlib</Property>
				<Property Name="Source[15].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[15].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[15].type" Type="Str">Library</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref">/My Computer/TASCA/UTCS_IGA140_SV.lvlib</Property>
				<Property Name="Source[16].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[16].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[16].type" Type="Str">Library</Property>
				<Property Name="Source[17].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[17].itemID" Type="Ref">/My Computer/TASCA/UTCS_Watchdog_SV.lvlib</Property>
				<Property Name="Source[17].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[17].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[17].type" Type="Str">Library</Property>
				<Property Name="Source[18].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[18].itemID" Type="Ref">/My Computer/TASCA/UTCS_SystemMonitor_SV.lvlib</Property>
				<Property Name="Source[18].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[18].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[18].type" Type="Str">Library</Property>
				<Property Name="Source[19].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[19].itemID" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler.lvlib/UTCSAlarmHandler-Settings.xml</Property>
				<Property Name="Source[19].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/UTCS.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[20].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[20].itemID" Type="Ref">/My Computer/TASCA/UTCS_AlarmHandler_SV.lvlib</Property>
				<Property Name="Source[20].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[20].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[20].type" Type="Str">Library</Property>
				<Property Name="Source[21].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[21].itemID" Type="Ref">/My Computer/niwebserver.conf</Property>
				<Property Name="Source[21].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.pdf</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/README.md</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/TASCA/UTCS_AE.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/TASCA/UTCS_BMIL_SV.lvlib</Property>
				<Property Name="Source[7].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">Library</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/TASCA/UTCS_MKS647_SV.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/TASCA/UTCS_TPG300_SV.lvlib</Property>
				<Property Name="Source[9].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">22</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">UTCS App based on NI Actor Framework and CS++.</Property>
				<Property Name="TgtF_internalName" Type="Str">UTCS App</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">UTCS</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{CCC3E9BD-32A7-4FDC-8D9E-7D0DD8099685}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">UTCS.exe</Property>
			</Item>
			<Item Name="UTCS Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">UTCS</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{6C2EA93A-BE4B-4929-BC67-ECE3DC942BFB}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI DataSocket 19.0</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{81A7E53E-9524-41CE-90D3-7DD3D90B6C58}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[1].productID" Type="Str">{D1ED3157-57F9-4363-BFE1-C0FF62071201}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI Distributed System Manager 2018</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{56CE3148-AF48-49F0-8CEC-85F5AFC7F843}</Property>
				<Property Name="DistPart[10].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[10].productID" Type="Str">{66383CB2-06C9-4CFE-AD8E-658B3218EBF4}</Property>
				<Property Name="DistPart[10].productName" Type="Str">Microsoft Silverlight 5.1</Property>
				<Property Name="DistPart[10].upgradeCode" Type="Str">{69DA64F2-1630-4C0C-947D-6CF5590A63A4}</Property>
				<Property Name="DistPart[11].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[11].productID" Type="Str">{2E4379D6-AF08-4ADB-B15B-EAEEE71AC697}</Property>
				<Property Name="DistPart[11].productName" Type="Str">NI DataFinder Desktop Edition Runtime 2017</Property>
				<Property Name="DistPart[11].upgradeCode" Type="Str">{C14CA542-0B0B-4CD1-8460-FC019E4EBB9D}</Property>
				<Property Name="DistPart[12].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[12].productID" Type="Str">{2C2037BA-3D68-4C24-9E30-EF630EAFA7DD}</Property>
				<Property Name="DistPart[12].productName" Type="Str">NI DataFinder Toolkit Runtime 2018</Property>
				<Property Name="DistPart[12].upgradeCode" Type="Str">{E3313A7A-E43C-486C-A2AE-1EAE5F3F9E6F}</Property>
				<Property Name="DistPart[13].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[13].productID" Type="Str">{EBCDF6CD-76D1-48AF-A6D5-6721CDDA3C17}</Property>
				<Property Name="DistPart[13].productName" Type="Str">NI TDM Excel Add-In 2019</Property>
				<Property Name="DistPart[13].upgradeCode" Type="Str">{6D2EBDAF-6CCD-44F3-B767-4DF9E0F2037B}</Property>
				<Property Name="DistPart[14].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[14].productID" Type="Str">{BDA132C2-3098-4B63-8194-1D7798583145}</Property>
				<Property Name="DistPart[14].productName" Type="Str">NI-VISA Remote Server 19.5</Property>
				<Property Name="DistPart[14].upgradeCode" Type="Str">{951E7F56-F1CD-403C-B138-9BEFC6CEB343}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{C02E174E-A57A-4B5F-A762-8FCA4854370A}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI LabVIEW Remote Execution Support 2019</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{1DB06C72-60F2-48DC-BAEA-935A3CFFFA3C}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[3].productID" Type="Str">{2E638747-D4E9-4B64-818B-1F65392C79F6}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI R Series Multifunction RIO Runtime 19.0</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{BED46696-FEA8-41AC-8377-F85B6CE69BE5}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[4].productID" Type="Str">{FA0DB08E-BC18-4194-9ADC-026B7C8D5CEA}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI Variable Engine 2019</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{EB7A3C81-1C0F-4495-8CE5-0A427E4E6285}</Property>
				<Property Name="DistPart[5].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[5].productID" Type="Str">{727FC9D0-D89C-4872-9428-F98F0862F735}</Property>
				<Property Name="DistPart[5].productName" Type="Str">NI-488.2 Runtime 19.5</Property>
				<Property Name="DistPart[5].upgradeCode" Type="Str">{357F6618-C660-41A2-A185-5578CC876D1D}</Property>
				<Property Name="DistPart[6].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[6].productID" Type="Str">{175DA080-55C0-420A-B44D-780B8D4E6AA8}</Property>
				<Property Name="DistPart[6].productName" Type="Str">NI-DAQmx Runtime 19.5</Property>
				<Property Name="DistPart[6].upgradeCode" Type="Str">{923C9CD5-A0D8-4147-9A8D-998780E30763}</Property>
				<Property Name="DistPart[7].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[7].productID" Type="Str">{C1F48628-DF94-40A8-BA2B-77BEE154C572}</Property>
				<Property Name="DistPart[7].productName" Type="Str">NI-Serial Runtime 19.5</Property>
				<Property Name="DistPart[7].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[8].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[8].productID" Type="Str">{D9084972-85DA-4F19-B6B3-527E3948D5A1}</Property>
				<Property Name="DistPart[8].productName" Type="Str">NI-VISA Runtime 19.5</Property>
				<Property Name="DistPart[8].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[9].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[9].productID" Type="Str">{EE27B7AE-EC56-49EC-9153-7D4CE64EDCA2}</Property>
				<Property Name="DistPart[9].productName" Type="Str">NI LabVIEW Runtime 2019 f2</Property>
				<Property Name="DistPart[9].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[0].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[9].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[9].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[1].productName" Type="Str">NI Deployment Framework 2019</Property>
				<Property Name="DistPart[9].SoftDep[1].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[9].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[10].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2019</Property>
				<Property Name="DistPart[9].SoftDep[10].upgradeCode" Type="Str">{8386B074-C90C-43A8-99F2-203BAAB4111C}</Property>
				<Property Name="DistPart[9].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[11].productName" Type="Str">NI LabVIEW Runtime 2019 Non-English Support.</Property>
				<Property Name="DistPart[9].SoftDep[11].upgradeCode" Type="Str">{446D49A5-F830-4ADF-8C78-F03284D6882D}</Property>
				<Property Name="DistPart[9].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[2].productName" Type="Str">NI Error Reporting 2019</Property>
				<Property Name="DistPart[9].SoftDep[2].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[9].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[3].productName" Type="Str">NI Logos 19.0</Property>
				<Property Name="DistPart[9].SoftDep[3].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[9].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[4].productName" Type="Str">NI LabVIEW Web Server 2019</Property>
				<Property Name="DistPart[9].SoftDep[4].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[9].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[5].productName" Type="Str">NI mDNS Responder 19.0</Property>
				<Property Name="DistPart[9].SoftDep[5].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[9].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[6].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[9].SoftDep[6].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[9].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[7].productName" Type="Str">Math Kernel Libraries 2018</Property>
				<Property Name="DistPart[9].SoftDep[7].upgradeCode" Type="Str">{33A780B9-8BDE-4A3A-9672-24778EFBEFC4}</Property>
				<Property Name="DistPart[9].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[8].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[9].SoftDep[8].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[9].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[9].SoftDep[9].productName" Type="Str">NI TDM Streaming 19.0</Property>
				<Property Name="DistPart[9].SoftDep[9].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[9].SoftDepCount" Type="Int">12</Property>
				<Property Name="DistPart[9].upgradeCode" Type="Str">{7D6295E5-8FB8-4BCE-B1CD-B5B396FA1D3F}</Property>
				<Property Name="DistPartCount" Type="Int">15</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/D/builds/UTCS_RC/UTCS Installer</Property>
				<Property Name="INST_buildSpecName" Type="Str">UTCS Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="INST_installerName" Type="Str">setup.exe</Property>
				<Property Name="INST_productName" Type="Str">UTCS</Property>
				<Property Name="INST_productVersion" Type="Str">1.2.20</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">19008009</Property>
				<Property Name="MSI_arpCompany" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="MSI_arpContact" Type="Str">r.a.cantemir@gsi.de</Property>
				<Property Name="MSI_arpPhone" Type="Str">2123</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.gsi.de</Property>
				<Property Name="MSI_distID" Type="Str">{245059F5-1F54-44D5-BC1A-BD10F95D58A8}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_licenseID" Type="Ref">/My Computer/Docs &amp; EUPL/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{40FA0554-2ADB-497C-8356-7E10A81CC7CD}</Property>
				<Property Name="MSI_windowMessage" Type="Str">You are about to install the UTCS executable, support files and additional NI packages. Some packages are published under EUPL v1.1.

Copyright 2019 GSI  Helmholtzzentrum für Schwerionenforschung GmbH
R.A.Cantemir@gsi.de, 3477, SHE CHEMIE, Planckstr. 1, 64291 Darmstadt, Germany
</Property>
				<Property Name="MSI_windowTitle" Type="Str">UTCS Installer</Property>
				<Property Name="MSI_winsec.certificate" Type="Str">Cantemir Raul (Cantemir)</Property>
				<Property Name="MSI_winsec.description" Type="Str">http://timestamp.digicert.com</Property>
				<Property Name="MSI_winsec.timestamp" Type="Str">http://timestamp.digicert.com</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">UTCS.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">UTCS</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">UTCS</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{CCC3E9BD-32A7-4FDC-8D9E-7D0DD8099685}</Property>
				<Property Name="Source[0].File[1].attributes" Type="Int">1</Property>
				<Property Name="Source[0].File[1].dest" Type="Str">{6731952F-B602-455B-8E56-32A207402888}</Property>
				<Property Name="Source[0].File[1].name" Type="Str">UTCS.ini</Property>
				<Property Name="Source[0].File[1].tag" Type="Str">{D5B3B86F-9F9C-441E-BBE8-1F64BE17203A}</Property>
				<Property Name="Source[0].FileCount" Type="Int">2</Property>
				<Property Name="Source[0].name" Type="Str">UTCS App</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/UTCS App</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
