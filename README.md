
Readme
======
This LabVIEW project "UTCS.lvproj" is used to develop the Unified TASCA Control System Project and is based on LVOOP, NI Actor Framework and CS++.

LabVIEW 2017 is currently used development.

Related documents and information
=================================
- README.txt
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/TASCA/UTCS
- Documentation:
  - Refer to Documantation Folder
  - Project-Wikis: https://wiki.gsi.de/foswiki/bin/view/Tasca/TascaUTCS
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview

GIT Submodules
=================================
Following git submodules are defined in this project.

- Packages/CSPP_Core: containing the CS++ core libraries; [https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git](https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git)
- Packages/CSPP_ObjectManager; [https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager.git](https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager.git)
- Packages/CSPP_DSC: containing Alarm- & Trend-Viewer; [https://git.gsi.de/EE-LV/CSPP/CSPP_DSC.git](https://git.gsi.de/EE-LV/CSPP/CSPP_DSC.git)
- Packages/CSPP_Utilities; [https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities.git](https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities.git)
- Packages/CSPP_MKS647C: Gas Mixer
- MKS6476C LabVIEW Instrument driver; [https://git.gsi.de/EE-LV/Drivers/MGC647C.git](https://git.gsi.de/EE-LV/Drivers/MGC647C.git)
- Packages/CSPP_MKS647C; [https://git.gsi.de/EE-LV/CSPP/TASCA/CSPP_MKS647C.git](https://git.gsi.de/EE-LV/CSPP/TASCA/CSPP_MKS647C.git)
- TPG300 LabVIEW Instrument driver; [https://git.gsi.de/EE-LV/Drivers/TPG300.git](https://git.gsi.de/EE-LV/Drivers/TPG300.git)
- Packages/CSPP_TPG300; [https://git.gsi.de/EE-LV/CSPP/TASCA/CSPP_TPG300.git](https://git.gsi.de/EE-LV/CSPP/TASCA/CSPP_TPG300.git)

External Dependencies
=================================


Optional:

- Syslog; Refer to http://sine.ni.com/nips/cds/view/p/lang/de/nid/209116

Getting started:
=================================
- Create a project specific copy of "UTCS.lvproj"
- You need to create your project specific ini-file, like "UTCS.ini"
  - Sample ini-file should be available for all classes, either in the LV-Project or on disk in the corresponding class or package folder.
- You need to create and deploy your project specific shared Variable libraries.
  - Sample shared Variable libraries should be available for all concerned classes on disk in the corresponding class or package folder.
- Run your project specific _My Computer/UTCS.vi_.
- Build application
  - The corresponding build specification _UTCS App_ uses
    - _UTCS.vi_ as startup-VI. It calls
    - _TASCA\UTCS___Content.vi_; Including your project specific Content-VIs 
  - Duplicate the build specification and adapt it to your needs.


Author: H.Brand@gsi.de

Copyright 2017  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.