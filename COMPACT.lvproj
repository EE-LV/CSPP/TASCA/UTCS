﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">DSCGetVarList,PSP;WebpubLaunchBrowser,None;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project "UTCS.lvproj" is used to develop the Unified TASCA Control System Project and is based on LVOOP, NI Actor Framework and CS++.

Related documents and information
=================================
- README.txt
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/TASCA/UTCS
- Documentation:
  - Refer to Documantation Folder
  - Project-Wikis: https://wiki.gsi.de/foswiki/bin/view/Tasca/UTCSP, https://github.com/HB-GSI/CSPP/wiki
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview

Author: H.Brand@gsi.de

Copyright 2017  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{02311B37-F10F-4E8D-B8C3-6DEF986B3FD3}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/IOS Error Source</Property>
	<Property Name="varPersistentID:{0306AF84-1B67-40E2-A12E-BE27D3FD2ABA}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/SV-Lib Path</Property>
	<Property Name="varPersistentID:{036B9234-CC84-4B69-87DA-7DCB350301E5}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_PollingStartStop</Property>
	<Property Name="varPersistentID:{0599BE46-B7CD-40DE-B1A1-7B960535150B}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_AI3</Property>
	<Property Name="varPersistentID:{065954AB-BB45-431E-B0E9-EF7EA0B97B72}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_FirmwareRevision</Property>
	<Property Name="varPersistentID:{06973BD1-8F68-4EC4-BC4B-8DC1D8F373B1}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC5_P.positi</Property>
	<Property Name="varPersistentID:{06DD2512-ACA1-42D8-A079-0C9DCA394FF2}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_SelftestResultCode</Property>
	<Property Name="varPersistentID:{079BBA7B-453E-4AFC-A628-EF24B524DD8F}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_SelfTest</Property>
	<Property Name="varPersistentID:{09A00C5F-97E0-4513-B8C5-988A0073AC21}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DT3.currinfo_0</Property>
	<Property Name="varPersistentID:{09D0C4CC-1D8B-438F-ACA8-113C78B6F4EF}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Active</Property>
	<Property Name="varPersistentID:{09FFB832-B5E5-41E7-B294-83C100847D1F}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Reset</Property>
	<Property Name="varPersistentID:{0A6E473C-1EA5-4422-89D1-036E16155FE6}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUXIVV1S.positi</Property>
	<Property Name="varPersistentID:{0A85CA64-093C-4BF1-9BE5-3DF4583E81C6}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_ErrorCode</Property>
	<Property Name="varPersistentID:{0B0A742B-8B30-4FC4-9DF4-E5EFDE2F73ED}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/MKS647C_ChannelStatus_3</Property>
	<Property Name="varPersistentID:{0BBE5CC4-BF8A-403A-B3B7-04BBB91E534D}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Virtual Accelerator</Property>
	<Property Name="varPersistentID:{0BDEE8CE-5E47-4362-8E5A-2B33589E52FC}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Temperature_3</Property>
	<Property Name="varPersistentID:{0CD6F8C9-924D-4307-A80A-E1D46D954D96}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/COMPACT_PollingInterval</Property>
	<Property Name="varPersistentID:{0DA94D82-978D-47DA-8417-0F739306A725}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_Activate</Property>
	<Property Name="varPersistentID:{0DCEF83A-3C54-43CC-82B3-E3E3AE1145EE}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_PollingInterval</Property>
	<Property Name="varPersistentID:{1012D7A9-E715-4877-85E9-9580A4882508}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_AI2</Property>
	<Property Name="varPersistentID:{104B9FDD-80F6-4FAC-A9DC-CC3CCCA7FB93}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUXIDC6.currinfo</Property>
	<Property Name="varPersistentID:{10CDE49E-06E4-40B0-B40F-A86146AF8F6C}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{13081E61-46D6-45D4-B4A8-BCCC380F8F03}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_DI2</Property>
	<Property Name="varPersistentID:{195183CF-3909-4AF5-B9BF-8B9ED7B6E620}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC4.currinfo</Property>
	<Property Name="varPersistentID:{1A73D592-653C-434E-877A-BA2D26A70FB6}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{1AF179C1-419F-4B79-A5FE-8E1FFDC6F1EB}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_DO</Property>
	<Property Name="varPersistentID:{1DC25248-79E6-4866-A755-D8EAE68A73B1}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_AI</Property>
	<Property Name="varPersistentID:{1FD77BC9-B7D1-4EFF-A40E-B5306093C74D}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Dewpoint</Property>
	<Property Name="varPersistentID:{22937A07-D516-484A-9FA9-2B04717565C0}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_12</Property>
	<Property Name="varPersistentID:{23DFBDAE-5E41-4ED1-8137-2CDFE1F1C35F}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_SelfTest</Property>
	<Property Name="varPersistentID:{242CE1E7-8E7C-49F6-A077-8C12B58910B5}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_PollingStartStop</Property>
	<Property Name="varPersistentID:{2432F23D-F439-4637-BE5E-51706037DC18}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_SelfTest</Property>
	<Property Name="varPersistentID:{25802F3F-440A-43DC-A0DD-7F77B9FBE726}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_3</Property>
	<Property Name="varPersistentID:{2615FA76-8B61-4379-8097-E6EEF413C1E4}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_SelftestResultCode</Property>
	<Property Name="varPersistentID:{2617CF23-DAFE-49A1-A189-1190B9578BCD}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_DriverRevision</Property>
	<Property Name="varPersistentID:{2819B159-C298-4AA4-9F24-7C60F92D884E}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_Activate</Property>
	<Property Name="varPersistentID:{2957593D-CC2A-47D7-B0AD-06FC1B026E40}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_0</Property>
	<Property Name="varPersistentID:{2C2F2B13-76BC-42B5-9379-235BD50A5946}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Error Message</Property>
	<Property Name="varPersistentID:{2D537E05-E4A1-4EC0-A897-4F9EDEB0003B}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_1</Property>
	<Property Name="varPersistentID:{2D714A93-9E80-4F99-BA4D-6B2851423272}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_8</Property>
	<Property Name="varPersistentID:{2E570CF7-4D82-4309-9095-EBF9EBFB6C78}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_PollingStartStop</Property>
	<Property Name="varPersistentID:{2F962AA8-7FC9-4CB7-BA8E-5CADA3840659}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_DO0</Property>
	<Property Name="varPersistentID:{31C15CC1-C826-418D-8245-F486BEE6428B}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_Reset</Property>
	<Property Name="varPersistentID:{334EE59D-DB1A-45DF-B880-2578C5197EBF}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_9</Property>
	<Property Name="varPersistentID:{3553769F-CF84-4426-8E37-23B4F87C909E}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_ErrorMessage</Property>
	<Property Name="varPersistentID:{37048A9A-B533-4BAB-8A3D-1D4B82A731C2}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/String Size</Property>
	<Property Name="varPersistentID:{37A4FC62-7BD8-4A89-AAD5-21331D3A8C89}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DCX.currinfo_1</Property>
	<Property Name="varPersistentID:{3AC55FEA-0D43-4C66-92F5-5CCF8C8CC620}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Valve_State_1_closed</Property>
	<Property Name="varPersistentID:{3AFBEA5E-8C8A-4204-9D91-03B530C3C0E1}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_Activate</Property>
	<Property Name="varPersistentID:{3DAC2C1A-BD4C-481B-97EA-CF4A746BAC11}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUXADT2.currinfo_0</Property>
	<Property Name="varPersistentID:{3DDF4C87-BB06-410A-857B-5EBBD4CCDC25}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/MKS647C_ChannelStatus_1</Property>
	<Property Name="varPersistentID:{3F558A8F-37A1-47A1-B084-04D8081FC04D}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC4.currinfo_0</Property>
	<Property Name="varPersistentID:{43776265-AF33-411F-94FC-8CA285CF4E6A}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC2.currinfo_0</Property>
	<Property Name="varPersistentID:{4496445E-D3F6-45C0-8B26-E9CD82354012}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_11</Property>
	<Property Name="varPersistentID:{49A49DF3-F321-4F8D-A268-AD13EA1803E1}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUXIDC6.currinfo_1</Property>
	<Property Name="varPersistentID:{4AEE264F-22CE-487C-BD38-E0F0EC204B51}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_PollingTime</Property>
	<Property Name="varPersistentID:{4E5F0136-3C8C-4610-A4E1-499C1822DF71}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC4_P.positi</Property>
	<Property Name="varPersistentID:{4E687EBE-17B6-4F30-9601-9689DB3C4435}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_ErrorCode</Property>
	<Property Name="varPersistentID:{501C6F64-B6B0-4A6F-9B05-5A7FEE4D8BAC}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC5.currinfo_0</Property>
	<Property Name="varPersistentID:{50669BEC-36E1-4CC6-9787-FE73E4C00EC9}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/IOS Error Message</Property>
	<Property Name="varPersistentID:{51234EC0-F92A-40C7-8199-A23D285C0540}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_AI1</Property>
	<Property Name="varPersistentID:{52EDD6A6-2CD9-403B-A882-7116D3043382}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC2_P.positi</Property>
	<Property Name="varPersistentID:{56703E6B-ECB9-4E43-9D36-81EF249B5183}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.currinfo</Property>
	<Property Name="varPersistentID:{5A9081F5-E5AD-484A-95ED-A8FEFF001540}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/IOS Error Code</Property>
	<Property Name="varPersistentID:{5AA3E305-D826-4869-929A-50969721F6DF}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_Set-DO</Property>
	<Property Name="varPersistentID:{5ACD6BDD-0CA8-4FF0-AAF6-DB01D095C4DF}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_PollingMode</Property>
	<Property Name="varPersistentID:{5C3F576A-FD15-48CA-86DD-E0241F72E28B}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_Set-DO_1</Property>
	<Property Name="varPersistentID:{5DDA7555-9382-4B86-9C69-4F02FCCF0CD3}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Valve_State_0_open</Property>
	<Property Name="varPersistentID:{5E71FFD6-6DC3-48E2-9D6E-4A46C27A20F6}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_ResourceName</Property>
	<Property Name="varPersistentID:{5EF7E0C2-9C9D-48CE-B813-9FA181CA911C}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_DI1</Property>
	<Property Name="varPersistentID:{63482247-E133-4BAF-A278-F2B59ADA9C5F}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DCX.currinfo_0</Property>
	<Property Name="varPersistentID:{636EFDD9-26EE-4CC1-9CD2-DBA4D534FBA3}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_PollingInterval</Property>
	<Property Name="varPersistentID:{64092DC0-4939-49A3-9750-E498F57C8D4D}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_WorkerActor</Property>
	<Property Name="varPersistentID:{655A3FB4-1372-4C1C-BB38-16E399A74BA8}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/MKS647C_ChannelStatus_0</Property>
	<Property Name="varPersistentID:{668DE8B8-CBAD-4C65-82AC-5422543D6DDB}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_PollingTime</Property>
	<Property Name="varPersistentID:{690110B6-C835-44CD-8D35-A79BDC747A3F}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.currinfo</Property>
	<Property Name="varPersistentID:{693BD4A8-3E8C-46AF-9CDF-043CBFE183EB}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC2.currinfo</Property>
	<Property Name="varPersistentID:{69D1C4F7-A709-4BD4-8745-166EBE2D3806}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/MKS647C_ChannelStatus_2</Property>
	<Property Name="varPersistentID:{6B060773-44AE-4FC5-A9B3-F6AA65799B14}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_10</Property>
	<Property Name="varPersistentID:{6C4EB369-4FE1-40EB-BDCB-71929BE98669}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_PollingTime</Property>
	<Property Name="varPersistentID:{6CFE26CB-AEB0-4D92-8567-5A15D67073E6}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DT3.currinfo_1</Property>
	<Property Name="varPersistentID:{6D08A20A-3793-48B9-BCB2-EE29E0291DD9}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUXADT2.currinfo</Property>
	<Property Name="varPersistentID:{6F047CAC-FD95-45E8-90AA-0E1DD0001047}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Interval</Property>
	<Property Name="varPersistentID:{7077FC96-25AD-4E68-BC17-22932524E1AD}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_AI1</Property>
	<Property Name="varPersistentID:{71510349-D9E1-4F76-BBD2-D2055EB794E6}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_4</Property>
	<Property Name="varPersistentID:{72CA8762-9FF0-4A07-88AA-8A379B1E37EB}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_DO1</Property>
	<Property Name="varPersistentID:{75670051-A13C-4E8B-B7F0-81A87941601C}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_9</Property>
	<Property Name="varPersistentID:{7729EEC1-CE82-4450-8D90-809A767DECAE}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{788D52A2-1106-4FA9-9822-8FFF26CF97F3}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_6</Property>
	<Property Name="varPersistentID:{7CCF3321-F64C-4E5A-8969-C81E4FF2085F}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/IOS State</Property>
	<Property Name="varPersistentID:{7D542BCC-F24A-49E3-B2FC-48B1E7C3977C}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_12</Property>
	<Property Name="varPersistentID:{7D87203C-7DB5-4165-B430-A4102CB898F2}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_AI0</Property>
	<Property Name="varPersistentID:{7DA2F663-CA7C-4B94-BED3-163C8E2679FC}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Valve_Set_0</Property>
	<Property Name="varPersistentID:{7DF23428-CE28-492A-8203-A0BC4614FBC9}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC4.currinfo_1</Property>
	<Property Name="varPersistentID:{7E23C96D-B90A-4D69-9BBB-3005410CAE55}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_ResourceName</Property>
	<Property Name="varPersistentID:{7F08A55A-8AC7-4671-B97B-6143005CC450}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_3</Property>
	<Property Name="varPersistentID:{7F134D45-C229-4AC4-A4AC-CD0443ED1412}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_DI3</Property>
	<Property Name="varPersistentID:{7F2FC371-036B-4B5A-958F-6DAE8B77F9A2}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/COMPACT_PollingCounter</Property>
	<Property Name="varPersistentID:{7FF53835-7290-40F2-A0A7-FF1D6B51E1B0}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Flow_1</Property>
	<Property Name="varPersistentID:{810F7F7E-B2DC-4FA5-92AC-A8F9450E1B66}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_Activate</Property>
	<Property Name="varPersistentID:{81321E50-0EEB-4A91-9454-39F9FBD47C16}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/COMPACT_PollingTime</Property>
	<Property Name="varPersistentID:{813903E9-17F8-43F6-98AF-8F17DD5EFE46}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_DriverRevision</Property>
	<Property Name="varPersistentID:{86EA468F-7BB9-4D7C-B3DB-040C97AEB935}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_PollingInterval</Property>
	<Property Name="varPersistentID:{8814C6E2-AE2E-42FB-BFFD-84161C4C7817}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Log All Device Errors</Property>
	<Property Name="varPersistentID:{88750632-6672-4DE1-9438-90F0ED7BA880}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUT2DCX.currinfo_0</Property>
	<Property Name="varPersistentID:{89A355D9-618C-4F14-AFCE-85C63D49E800}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_DI0</Property>
	<Property Name="varPersistentID:{8C0D781E-7296-4662-AB08-12356191001E}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DCX.currinfo</Property>
	<Property Name="varPersistentID:{9018D1F8-715C-49F5-A72A-24A167E70374}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_1</Property>
	<Property Name="varPersistentID:{906E3DE9-20A6-4475-A4FC-3027EC5E22A0}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_7</Property>
	<Property Name="varPersistentID:{957F9AE0-C734-4BFE-A46D-B24DF6BDC571}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_0</Property>
	<Property Name="varPersistentID:{96FD1AEB-EB02-42CD-A539-E604BD15B0E0}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_ErrorMessage</Property>
	<Property Name="varPersistentID:{9A3010CA-BB26-45AA-8B29-E073E3F9D33B}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_WorkerActor</Property>
	<Property Name="varPersistentID:{9A91F0FC-2DA8-4D50-BEC7-4878501AEEDF}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Flow_0</Property>
	<Property Name="varPersistentID:{9BF551C8-47AA-44E8-958A-78CEE52E644B}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_PollingMode</Property>
	<Property Name="varPersistentID:{9E9B7BBD-76A1-4BA5-B8D1-595D8B6E37D1}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Pressure</Property>
	<Property Name="varPersistentID:{A0548605-CE7C-49E8-B64C-3C76100C4ECD}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_DI</Property>
	<Property Name="varPersistentID:{A11423B3-8942-486C-BBE5-3500606D7B36}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Temperature_0</Property>
	<Property Name="varPersistentID:{A13740C9-726F-4820-83BE-5BAB6B755B64}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUT2DCX_P.positi</Property>
	<Property Name="varPersistentID:{A14E2FC7-5475-4D5F-89D3-791146F27387}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUXFVV1S.positi</Property>
	<Property Name="varPersistentID:{A4631F52-D635-46E7-94A9-CCAC3175460C}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_4</Property>
	<Property Name="varPersistentID:{A5573E5A-A700-40D7-BB50-D4152BA1C7F6}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_7</Property>
	<Property Name="varPersistentID:{A64EFA49-A148-4841-98F5-6D9392576279}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_11</Property>
	<Property Name="varPersistentID:{ACAF9E2B-AB19-4917-9EDD-BD6B71E89092}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_PollingMode</Property>
	<Property Name="varPersistentID:{AE688110-E623-4C21-A2E8-5A14CB51CE35}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_2</Property>
	<Property Name="varPersistentID:{AE970958-F116-4635-8B57-B56EEF6EADC1}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Debug</Property>
	<Property Name="varPersistentID:{AFBFC2DA-4B85-4072-AAED-D737F197D182}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_DriverRevision</Property>
	<Property Name="varPersistentID:{B01FBAC2-4357-4B14-83D2-FEC95FBBC149}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_6</Property>
	<Property Name="varPersistentID:{B04E1E84-A82F-4DFE-8194-FA32A367E1E5}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/COMPACT_PollingMode</Property>
	<Property Name="varPersistentID:{B09D3F35-F6BB-4B0B-887F-B8B8045CAB72}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_8</Property>
	<Property Name="varPersistentID:{B175FC64-7096-4E98-BB08-512A1E87BA23}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_Reset</Property>
	<Property Name="varPersistentID:{B1B9A7B8-A7DF-4084-9FE9-BE01611862D3}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_Reset</Property>
	<Property Name="varPersistentID:{B533ACFB-2FFF-4246-9B2F-778E002671E4}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC5.currinfo_1</Property>
	<Property Name="varPersistentID:{B7EE3661-E1C1-40B9-8100-C0B8471FB381}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_ResourceName</Property>
	<Property Name="varPersistentID:{B82FDE81-725C-4D81-82D6-C2926C4AABD2}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Temperature_2</Property>
	<Property Name="varPersistentID:{B89D85F3-55DB-43B1-8803-B9A8125B863D}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_ErrorCode</Property>
	<Property Name="varPersistentID:{B8A8A3A1-06EB-4C93-BCD6-654B2EDF8DDC}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_WorkerActor</Property>
	<Property Name="varPersistentID:{B8A90942-4243-42D4-AF74-E51F86797EFA}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_SelftestResultCode</Property>
	<Property Name="varPersistentID:{BB32FA8C-6205-4FF7-9F84-86CEFEBBB18A}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Valve_State_0_closed</Property>
	<Property Name="varPersistentID:{BD658A66-184F-4C75-B116-4798CCC2C1A1}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_2</Property>
	<Property Name="varPersistentID:{BD787ED5-B5B9-40A8-A188-10FD0E607972}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Flow_2</Property>
	<Property Name="varPersistentID:{BE7B87D8-D820-4925-A21F-29E1E95F85BD}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_FirmwareRevision</Property>
	<Property Name="varPersistentID:{BEC88918-2D1B-48A9-B3DD-21FA86ECA768}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC5.currinfo</Property>
	<Property Name="varPersistentID:{BF0555A7-E8C4-41B9-A69C-6C1BDFD37E06}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_Set-DO_0</Property>
	<Property Name="varPersistentID:{C68721B2-3CB7-45A2-B049-F495CC1CFD1A}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_PollingStartStop</Property>
	<Property Name="varPersistentID:{C698D731-B1D9-4F41-8CBB-70B96E17C346}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_PollingTime</Property>
	<Property Name="varPersistentID:{C6F0789F-7965-4D2F-9095-202A35558DEB}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{C75A79B9-0B5E-447E-9E29-8B43FC2F6C1D}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DC2.currinfo_1</Property>
	<Property Name="varPersistentID:{C7C9AC42-810B-4ED3-BDC0-10F08D05AB9B}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUX8DT3.currinfo</Property>
	<Property Name="varPersistentID:{C9063A10-4F4D-442D-9FF7-A61900A9832F}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_Reset</Property>
	<Property Name="varPersistentID:{C9205DFD-34AE-4307-9791-C90FC783D43B}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Error-Log-Path</Property>
	<Property Name="varPersistentID:{C9537AF3-E27E-4885-AE3B-901EB3637F26}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUT2DCX.currinfo_1</Property>
	<Property Name="varPersistentID:{CAF9F4E8-5F14-41A1-B76B-9E346C37DB9B}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Array Size</Property>
	<Property Name="varPersistentID:{CCF9EA51-09D5-4A8E-993E-57BDC6FC2633}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_FirmwareRevision</Property>
	<Property Name="varPersistentID:{CD5005EB-0D6F-4459-A995-9B227F044FE3}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Flow_3</Property>
	<Property Name="varPersistentID:{D19BA09E-B21E-41EC-A205-60B9650378DA}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUXIDC6.currinfo_0</Property>
	<Property Name="varPersistentID:{D42239B4-D8E8-4259-8AC0-1AAAFF0EFC5D}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUXADT2.currinfo_1</Property>
	<Property Name="varPersistentID:{D5B645C0-861B-4D8F-AA1D-FB6344488637}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Valve_State_1_open</Property>
	<Property Name="varPersistentID:{D5D02CDE-DD77-48F5-AEAB-DC1735D07928}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_WorkerActor</Property>
	<Property Name="varPersistentID:{DEC1E10C-C0C9-45A5-A4E9-59388E813445}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Valve_Set_1</Property>
	<Property Name="varPersistentID:{DFB2682C-06A0-43B1-9DB5-B6C0C8AC2CD6}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_ErrorMessage</Property>
	<Property Name="varPersistentID:{E0692C60-D6AE-441D-BC0F-316842795C7F}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDO_ErrorCode</Property>
	<Property Name="varPersistentID:{E1EA2DC7-97FA-499C-93E3-9C8E695489FD}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_PollingInterval</Property>
	<Property Name="varPersistentID:{E1F00A6F-FE02-454D-9F07-3825A9C5A2D5}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_FirmwareRevision</Property>
	<Property Name="varPersistentID:{E46355D6-24BF-4BE8-B999-49CC881337C5}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/COMPACT_PollingIterations</Property>
	<Property Name="varPersistentID:{E61365E5-B52D-4041-A401-C5F9DB000F44}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_5</Property>
	<Property Name="varPersistentID:{E94E9F51-BC7C-435C-8F06-C0650D25B474}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_DriverRevision</Property>
	<Property Name="varPersistentID:{EBE41AF5-1B7B-4B0D-8525-D9CDC3B4276A}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxRTD_SelfTest</Property>
	<Property Name="varPersistentID:{EC96177C-9B4D-44EA-9868-7B88FEFEE4CB}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_AI</Property>
	<Property Name="varPersistentID:{EF26379B-0234-43B3-B8BC-7B54B5489E23}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_ResourceName</Property>
	<Property Name="varPersistentID:{F5BF7DBD-8147-43BA-8DB0-6BEE233E480F}" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib/Temperature_1</Property>
	<Property Name="varPersistentID:{F61248EB-FA82-43C2-9F83-15CF6252BB59}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN5DT1.CURRINFO_10</Property>
	<Property Name="varPersistentID:{F6ADF5A4-D381-4F1B-BA82-40FA789D55BF}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_PollingMode</Property>
	<Property Name="varPersistentID:{F6F67EE0-2916-43EC-8D76-38ED9FDA4F1B}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_AI0</Property>
	<Property Name="varPersistentID:{F74785C9-C453-4718-9FFE-A896384B0CAA}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Error Code</Property>
	<Property Name="varPersistentID:{F800E79B-46C5-4976-9265-37DF712697C1}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUXIDC6_P.positi</Property>
	<Property Name="varPersistentID:{F80EECF2-3CA7-4FD7-9987-75A476560B70}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUN7DT1.CURRINFO_5</Property>
	<Property Name="varPersistentID:{FAD5411B-53FE-46A9-83D1-67075CEF06F8}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxDI_SelftestResultCode</Property>
	<Property Name="varPersistentID:{FB5C72A3-E566-4F33-BEAB-ECAC463536F5}" Type="Ref">/My Computer/ACC/TASCA_AccIO.lvlib/GUT2DCX.currinfo</Property>
	<Property Name="varPersistentID:{FB76E0D6-BE1C-4CE2-AB77-75567F1DFA78}" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib/DAQmxAI_ErrorMessage</Property>
	<Property Name="varPersistentID:{FE1CBCAC-CC02-4C14-9CE3-A4C2D601B9A9}" Type="Ref">/My Computer/ACC/TASCA_AccIOS.lvlib/Server Type</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="ACC" Type="Folder">
			<Item Name="TASCA_AccIO.lvlib" Type="Library" URL="../SV.lib/TASCA_AccIO.lvlib"/>
			<Item Name="TASCA_AccIOS.lvlib" Type="Library" URL="../SV.lib/TASCA_AccIOS.lvlib"/>
			<Item Name="Test AccSV.vi" Type="VI" URL="../SV.lib/Test AccSV.vi"/>
		</Item>
		<Item Name="BNT" Type="Folder">
			<Item Name="BNT_DAQmx" Type="Folder">
				<Item Name="BNT_DAQmx-Content.vi" Type="VI" URL="../Packages/BNT_DAQmx/BNT_DAQmx-Content.vi"/>
				<Item Name="BNT_DAQmx.lvlib" Type="Library" URL="../Packages/BNT_DAQmx/BNT_DAQmx.lvlib"/>
				<Item Name="README.md" Type="Document" URL="../Packages/BNT_DAQmx/README.md"/>
			</Item>
			<Item Name="BNT_TDMS" Type="Folder">
				<Item Name="BNT_TDMS.lvlib" Type="Library" URL="../Packages/BNT_TDMS/BNT_TDMS.lvlib"/>
			</Item>
		</Item>
		<Item Name="COMPACT" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="COMPACT_Content.vi" Type="VI" URL="../Packages/COMPACT/COMPACT_Content.vi"/>
			<Item Name="COMPACT-DAQ.lvlib" Type="Library" URL="../Packages/COMPACT/COMPACT-DAQ.lvlib"/>
			<Item Name="COMPACT.lvlib" Type="Library" URL="../Packages/COMPACT/COMPACT.lvlib"/>
			<Item Name="COMPACT_Control.lvlib" Type="Library" URL="../Packages/COMPACT/COMPACT_Control/COMPACT_Control.lvlib"/>
			<Item Name="COMPACT_GUI.lvlib" Type="Library" URL="../Packages/COMPACT/COMPACT_GUI/COMPACT_GUI.lvlib"/>
			<Item Name="COMPACT_PressureAlarm.lvlib" Type="Library" URL="../Packages/COMPACT/COMPACT_PressureAlarm/COMPACT_PressureAlarm.lvlib"/>
		</Item>
		<Item Name="CSPP" Type="Folder">
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Libs" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
				<Item Name="README.md" Type="Document" URL="../Packages/CSPP_Core/README.md"/>
			</Item>
			<Item Name="DSC" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
					<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
					<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
					<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
					<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CS++DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
				</Item>
				<Item Name="Libs" Type="Folder">
					<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
				</Item>
				<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
				<Item Name="README.md" Type="Document" URL="../Packages/CSPP_DSC/README.md"/>
			</Item>
			<Item Name="ObjectManager" Type="Folder">
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
				<Item Name="README.txt" Type="Document" URL="../Packages/CSPP_ObjectManager/README.txt"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
				</Item>
				<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
			</Item>
		</Item>
		<Item Name="Devices" Type="Folder">
			<Item Name="MKS647C.lvlib" Type="Library" URL="../Packages/CSPP_MKS647C/MKS647C Actor/MKS647C.lvlib"/>
			<Item Name="MKS647CGUI.lvlib" Type="Library" URL="../Packages/CSPP_MKS647C/MKS647C GUI/MKS647CGUI.lvlib"/>
		</Item>
		<Item Name="Docs &amp; EUPL" Type="Folder">
			<Item Name="Change_Log.md" Type="Document" URL="../Change_Log.md"/>
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
			<Item Name="README.md" Type="Document" URL="../README.md"/>
			<Item Name="Release_Notes.md" Type="Document" URL="../Release_Notes.md"/>
		</Item>
		<Item Name="instr.lib" Type="Folder">
			<Item Name="MGC647C.lvlib" Type="Library" URL="../instr.lib/MGC647C/MGC647C.lvlib"/>
		</Item>
		<Item Name="Libs" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
		</Item>
		<Item Name="Test" Type="Folder"/>
		<Item Name="COMAPCT.ico" Type="Document" URL="../Packages/COMPACT/COMAPCT.ico"/>
		<Item Name="COMPACT.ini" Type="Document" URL="../Packages/COMPACT/COMPACT.ini"/>
		<Item Name="COMPACT.vi" Type="VI" URL="../Packages/COMPACT/COMPACT.vi"/>
		<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../Packages/COMPACT/EUPL v.1.1 - Lizenz.pdf"/>
		<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../Packages/COMPACT/EUPL v.1.1 - Lizenz.rtf"/>
		<Item Name="README.md" Type="Document" URL="../Packages/COMPACT/README.md"/>
		<Item Name="Release_Notes.md" Type="Document" URL="../Packages/COMPACT/Release_Notes.md"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Batch Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Batch Msg/Batch Msg.lvclass"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get Project Library Version.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Get Project Library Version.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="COMPACT App" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{E43F2275-A8DD-4EF9-8DC0-EE1C9EBCCCE1}</Property>
				<Property Name="App_INI_GUID" Type="Str">{86BBFDB8-F921-410B-9947-962D7CB66431}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/COMPACT.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{B6F06776-E6ED-4376-AC73-0B027DA1A9E6}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">COMPACT App</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/F/builds/COMAPCT/COMPACT_App</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{FEB25662-D7B6-483F-B8F1-3FADD8543EC9}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_version.build" Type="Int">3</Property>
				<Property Name="Bld_version.minor" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">COMPACT.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/F/builds/COMAPCT/COMPACT_App/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/F/builds/COMAPCT/COMPACT_App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/COMAPCT.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{68EAC6C0-A910-4AB2-8CE8-5C60EA33AF37}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/COMPACT.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/COMPACT.ini</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/COMPACT/COMPACT-DAQ.lvlib</Property>
				<Property Name="Source[3].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/COMPACT/COMPACT.lvlib</Property>
				<Property Name="Source[4].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">Library</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/EUPL v.1.1 - Lizenz.pdf</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Release_Notes.md</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">9</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">COMPACT App based on NI Actorframework and CS++.</Property>
				<Property Name="TgtF_internalName" Type="Str">COMPACT App</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">UTCS-COMPACT</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{AB0DF4C7-5C8C-4948-A629-D827AFA96444}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">COMPACT.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
