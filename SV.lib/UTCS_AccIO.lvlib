﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2016_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2016\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2016_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2016\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This library contains the Shared Variable connected to device properties belonging to the GSI accelerator control system using a Acc-IO-Server library.

Copyright 2017 H.Brand@gsi.de

GSI Helmholtzzentrum für Schwerionenforschung GmbH
EE, Planckstr. 1, 64291 Darmstadt, Germany</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)7!!!*Q(C=\&gt;4"&lt;=*!%)8BFSC(8/EAII5ZZU9,N$!N=-W2&amp;FY,N%!,UQ)NU),T?T6#E%AB5B+&amp;!X;7Y/@&gt;W9`VSN)YHK5H4&gt;?/VS_0&lt;^U@63Z[HK\\]W/F;@1YPT_&gt;"IXW_@Y]S&gt;3VPD(_9PY@`LYP\V]Z&gt;$`OR`]?&lt;V=X[:`PW1@.,S*;U5R,7GC?;CLS)C`S)C`S)D?ZS5VO=J/&lt;0-G40-G40-G40-C$0-C$0-C$@*TE)B?ZS#%6ER=4&amp;57,!E6H+#I_#E`B+4S&amp;BUM6HM*4?!J0Y;',#E`B+4S&amp;J`!Q4)7H]"3?QF.Y+$5E.5ZS0)7(]D)?YT%?YT%?JJ4R')#:T"1W27$)X$2@D-&gt;YD)?P-B\D-2\D-2ZOSXC-RXC-RXA9-F&lt;&amp;1T/@Z(AII]34?"*0YEE]F&amp;&lt;C34S**`%E(K:4YEE]#3+:-#E/1=GAJ%.SE8A3$`_5?"*0YEE]C9&gt;&lt;YQHF7*F:-Z`E?!*0Y!E]A3@Q5%+"*`!%HM!4?#CLQ".Y!E`A#4R-J=!4?!*0!!EG:8I&amp;R9+"1;=A#$T]D&gt;U3YSFZ3'+=5G^?^;:5&lt;T&lt;V*F*P$P6$6T^-^5.3,\Z[5&gt;7,J6Y%^9^4I^59^34KQ8.((@E]U0;U(7V,W^$7N"6N36P-1X_ZY`&amp;YV/&amp;QU([`VW[XUX;\V7;TU8K^VGKVUH+ZV'+R/,U'8DB0,Y2&lt;?#`&gt;B/&amp;2ZX6:IX?8XL'[!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="GUT2DCX.currinfo" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"E!A!!!!!!#!!V!#A!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"%&lt;X6C&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUT2DCX.currinfo_0" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUT2DCX.currinfo_1" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUT2DCX_P.positi" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Position of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC2.currinfo" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"E!A!!!!!!#!!V!#A!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"%&lt;X6C&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC2.currinfo_0" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC2.currinfo_1" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC2_P.positi" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Position of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC2_P.posits" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Position of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC4.currinfo" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"E!A!!!!!!#!!V!#A!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"%&lt;X6C&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC4.currinfo_0" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC4.currinfo_1" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC4_P.positi" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Position of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DC4_P.posits" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Position of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DT3.currinfo" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"E!A!!!!!!#!!V!#A!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"%&lt;X6C&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DT3.currinfo_0" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8DT3.currinfo_1" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8MU1.currenti" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Actual current of dc magnet.</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">False</Property>
		<Property Name="Logging:LogEvents" Type="Str">False</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!F)1!!!"E!A!!!!!!"!!V!#1!'5WFO:WRF!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8MU1.currents" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Actual current of dc magnet.</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="Security:__hidden__ACL" Type="Str">nQAAAAMAAAACAAAAOAAAAAcAAAC7UvLtM7EJTbVoaRAFPsrHEAAAAAMAAAABAAAAAgAAAN746Ka0vmNCm83eayGrFeQ4AAAABwAAACQvgzGjBDFBoAACnPC/3L4QAAAAAwAAAAMAAAAAAAAA3vjoprS+Y0Kbzd5rIasV5AEAAAAdAAAAAwAAAAEAAAAqEAAAAAMAAAADAAAAAAAAAA==</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!F)1!!!"E!A!!!!!!"!!V!#1!'5WFO:WRF!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8QD11.currenti" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Actual current of dc magnet.</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">False</Property>
		<Property Name="Logging:LogEvents" Type="Str">False</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:EngMax" Type="Str">100</Property>
		<Property Name="Scaling:EngMin" Type="Str">0</Property>
		<Property Name="Scaling:EngUnit" Type="Str"></Property>
		<Property Name="Scaling:RawMax" Type="Str">100</Property>
		<Property Name="Scaling:RawMin" Type="Str">0</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!F)1!!!"E!A!!!!!!"!!V!#1!'5WFO:WRF!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8QD11.currents" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Actual current of dc magnet.</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="Security:__hidden__ACL" Type="Str">nQAAAAMAAAACAAAAOAAAAAcAAAC7UvLtM7EJTbVoaRAFPsrHEAAAAAMAAAABAAAAAgAAAN746Ka0vmNCm83eayGrFeQ4AAAABwAAACQvgzGjBDFBoAACnPC/3L4QAAAAAwAAAAMAAAAAAAAA3vjoprS+Y0Kbzd5rIasV5AEAAAAdAAAAAwAAAAEAAAAqEAAAAAMAAAADAAAAAAAAAA==</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!F)1!!!"E!A!!!!!!"!!V!#1!'5WFO:WRF!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8QD12.currenti" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Actual current of dc magnet.</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">False</Property>
		<Property Name="Logging:LogEvents" Type="Str">False</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!F)1!!!"E!A!!!!!!"!!V!#1!'5WFO:WRF!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUX8QD12.currents" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Actual current of dc magnet.</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="Security:__hidden__ACL" Type="Str">nQAAAAMAAAACAAAAOAAAAAcAAAC7UvLtM7EJTbVoaRAFPsrHEAAAAAMAAAABAAAAAgAAAN746Ka0vmNCm83eayGrFeQ4AAAABwAAACQvgzGjBDFBoAACnPC/3L4QAAAAAwAAAAMAAAAAAAAA3vjoprS+Y0Kbzd5rIasV5AEAAAAdAAAAAwAAAAEAAAAqEAAAAAMAAAADAAAAAAAAAA==</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!F)1!!!"E!A!!!!!!"!!V!#1!'5WFO:WRF!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUXADT2.currinfo" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"E!A!!!!!!#!!V!#A!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"%&lt;X6C&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUXADT2.currinfo_0" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUXADT2.currinfo_1" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUXFVV1S.positi" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Position of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUXIDC6.currinfo" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"E!A!!!!!!#!!V!#A!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"%&lt;X6C&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUXIDC6.currinfo_0" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUXIDC6.currinfo_1" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Status of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUXIDC6_P.positi" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Position of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GUXIVV1S.positi" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Device inactive</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Inactive</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">0.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">False</Property>
		<Property Name="Alarming:Status:Area" Type="Str">UNILAC</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Position of device</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIO.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
