﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2016_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2016\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2016_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2016\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This Shared Variable library contains the Acc-IO-Server. It connects SV of a separate SV.lvlib to device properties belonging to the GSI accelerator control system.

Copyright 2017 H.Brand@gsi.de

GSI Helmholtzzentrum für Schwerionenforschung GmbH
EE, Planckstr. 1, 64291 Darmstadt, Germany</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*"!!!*Q(C=\&gt;5^4ON!&amp;)&lt;BDSO1;,U$F#W=,&lt;CECJ1&amp;X/&lt;U6'GB1;'C0FP)&amp;L+&amp;MY55&lt;#"&lt;-/]-"SM3C#!%OL@!:IT^T&gt;`$?'3E@FR+ZZJ/(2=@(J_L0WYVV?.5V\@V5VXGGGHO.5WP^==^/\-;TP8V^%\`OHMT]^@`PI`K4RT[08[0@XP=HNSE0\ZHT^1_2*3E"-5JJD&lt;6F/2*HO2*HO2*(O2"(O2"(O2"\O2/\O2/\O2/&lt;O2'&lt;O2'&lt;O2'XE^SE9N=Z*#3S:/*EE'4!:,'5*2=%E`C34S*BU=FHM34?"*0YK'*%E`C34S**`(149EH]33?R*.Y'+J,MJ`E?")0QSPQ"*\!%XA#$V-K]!3!9,*AY'!1'!IKAZP!%XA#$\=+0)%H]!3?Q%/V!E`A#4S"*`$1J;^+&gt;%U\S@%QD"S0YX%]DM@R-,1=D_.R0)\(]4#&gt;()`D=2$/B-\A%/2U=BIY$Y\(]@",DM@R/"\(YXCI[G`)_]IU44P*]2A?QW.Y$)`B91A:(M.D?!S0Y7&amp;9'2\$9XA-D_&amp;B+BE?QW.Y$)AR+&gt;0,'-TI;$1S!M0$4^]NVN^3&gt;)HV5[L.K^K5KMWGWE3KT;&amp;[[;K8K8J*KM68,;JKM63,I0LD6'A62D7*KH.LK!08076(W6)WF$6F22EJ#]L1OHZTQ]0BI0V_L^VOJ_VWK]VGI`6[L&gt;6KJ8%=N6AM.!T$`"GYYJQ`##`@J6PO7\H8^=W^FH=0NPT\/#[("\N_?KVLZ9@_F`Y(XU&lt;^U@'YL.%T&amp;@F?@Q!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Active" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Server Status.Active</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Server Status.Active</Property>
	</Item>
	<Item Name="Array Size" Type="Variable">
		<Property Name="featurePacks" Type="Str">Initial Value,Network</Property>
		<Property Name="Initial Value:Value" Type="Str">13.000000</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Array Size</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/TASCA_AccIOS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Array Size</Property>
	</Item>
	<Item Name="Debug" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Debug</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Debug</Property>
	</Item>
	<Item Name="Error Code" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">ACC-IO-Server Error. Refer to Error message for details.</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">1.000000</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">HI</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Lo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Lo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:Lo:Description" Type="Str">ACC-IO-Server Error. Refer to Error message for details.</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Limit" Type="Str">-1.000000</Property>
		<Property Name="Alarming:Lo:Name" Type="Str">LO</Property>
		<Property Name="Alarming:Lo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Status:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">15</Property>
		<Property Name="featurePacks" Type="Str">Alarming,Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Server Status\Error Code</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Server Status\Error Code</Property>
	</Item>
	<Item Name="Error Message" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Server Status\Error Message</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"E!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Server Status\Error Message</Property>
	</Item>
	<Item Name="Error-Log-Path" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Error-Log-Path</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"E!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Error-Log-Path</Property>
	</Item>
	<Item Name="Interval" Type="Variable">
		<Property Name="Description:Description" Type="Str">Unit: Seconds</Property>
		<Property Name="featurePacks" Type="Str">Description,Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Interval</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Interval</Property>
	</Item>
	<Item Name="IOS Error Code" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Acc-Error. Refer to Error source and message for details or call experts from BELAB.</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">2.000000</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">HI</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Alarming,Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\IOS Error Code</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">IOS Error Code</Property>
	</Item>
	<Item Name="IOS Error Message" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\IOS Error Message</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"E!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">IOS Error Message</Property>
	</Item>
	<Item Name="IOS Error Source" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\IOS Error Source</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"E!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">IOS Error Source</Property>
	</Item>
	<Item Name="IOS State" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">ACC-IO-Server not in active state</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">3.000000</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">HI</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Lo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Lo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:Lo:Description" Type="Str">ACC-IO-Server not in active state</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Limit" Type="Str">1.000000</Property>
		<Property Name="Alarming:Lo:Name" Type="Str">LO</Property>
		<Property Name="Alarming:Lo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Status:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">15</Property>
		<Property Name="featurePacks" Type="Str">Alarming,Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\IOS State</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">IOS State</Property>
	</Item>
	<Item Name="Log All Device Errors" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Log All Device Errors</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Log All Device Errors</Property>
	</Item>
	<Item Name="Reset" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Reset</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Reset</Property>
	</Item>
	<Item Name="Server Type" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Server Type</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"E!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Server Type</Property>
	</Item>
	<Item Name="String Size" Type="Variable">
		<Property Name="featurePacks" Type="Str">Initial Value,Network</Property>
		<Property Name="Initial Value:Value" Type="Str">1000</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\String Size</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/TASCA_AccIOS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">String Size</Property>
	</Item>
	<Item Name="SV-Lib Path" Type="Variable">
		<Property Name="Description:Description" Type="Str"></Property>
		<Property Name="featurePacks" Type="Str">Initial Value,Network</Property>
		<Property Name="Initial Value:Value" Type="Str">C:\MyProjects\UTCS\SV.lib\UTCS_AccIO.lvlib</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\SV-Lib Path</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/ACC/UTCS_AccIOS.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"E!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">SV-Lib Path</Property>
	</Item>
	<Item Name="UTCS-AccIOS" Type="IO Server">
		<Property Name="atrb" Type="Str">X!)!!!Q1!!!,!!!!"B!!!!I!!!""!()!=A"B!(E!)!"4!'E!?A"F!!-!!Q!!!!!!!!!K1!91!!!2!!!!1Q"P!'Y!:A"J!'=!&gt;1"S!'%!&gt;!"J!']!&lt;A"/!'%!&lt;1"F!!91!!!.!!!!11"D!'-!,1"*!%]!,1"4!'5!=A"W!'5!=A!'%!!!"1!!!%1!:1"C!(5!:Q!#!!!!!!91!!!)!!!!31"O!(1!:1"S!(9!91"M!!=Q!!!/!!!!!Q!%!!!!!!!!!#R!!Q!%!!!!!!!!!!!!!Q!%!!!!!!!!!!!!!Q!%!!!!!!!!!!!!!Q!%!!!!!!!!!!"!!Q!%!!!!!!!!!""!!Q!%!!!!!!!!!!!!!Q!%!!!!!!!!!!!!!Q!%!!!!!!!!!!!!!Q!%!!!!!!!!!!!!!Q!%!!!!!!!!!!!!!Q!%!!!!!!!!!!!!!Q!%!!!!!!!!!'Z!!Q!%!!!!!!!!A%^!"B!!!!]!!!"-!'%!9A"7!%E!21"8!#!!6A"F!()!=Q"J!']!&lt;A!$!!!!!!!!!!!!-U!'%!!!&amp;1!!!%Q!&lt;Q"H!#!!11"M!'Q!)!"%!'5!&gt;A"J!'-!:1!A!%5!=A"S!']!=A"T!!)!!!!!"B!!!!5!!!"3!'5!=Q"F!(1!!A!!!!!'%!!!"!!!!&amp;-!&gt;!"P!(!!!A!!!!!'%!!!#Q!!!&amp;-!&gt;!"S!'E!&lt;A"H!#!!5Q"J!(I!:1!$!!-!!!!!!!"!DU!'%!!!#Q!!!&amp;-!6A!N!%Q!;1"C!#!!5!"B!(1!;!!'%!!!-1!!!%-!/A"=!&amp;!!=A"P!'=!=A"B!'U!)!"'!'E!&lt;!"F!(-!)!!I!(A!/!!W!#E!8!"6!&amp;1!1Q"4!&amp;Q!:!"B!(1!91"=!&amp;5!6!"$!&amp;-!8Q""!'-!9Q"*!%]!,A"M!(9!&lt;!"J!')!"B!!!"-!!!"7!'E!=A"U!(5!91"M!#!!11"D!'-!:1"M!'5!=A"B!(1!&lt;Q"S!!-!!Q!!!!!!!!$QPQ</Property>
		<Property Name="className" Type="Str">Custom VI - Periodic</Property>
	</Item>
	<Item Name="Virtual Accelerator" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Invalid virtual accelerator ID. Valid range: 0-15.</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">16.000000</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">HI</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Lo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Lo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:Lo:Description" Type="Str">Invalid virtual accelerator ID. Valid range: 0-15.</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Limit" Type="Str">-1.000000</Property>
		<Property Name="Alarming:Lo:Name" Type="Str">LO</Property>
		<Property Name="Alarming:Lo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Status:Area" Type="Str">Accelerator</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad Status</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">15</Property>
		<Property Name="Description:Description" Type="Str"></Property>
		<Property Name="featurePacks" Type="Str">Alarming,Initial Value,Network</Property>
		<Property Name="Initial Value:Value" Type="Str">1.000000</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS\Virtual Accelerator</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AccIOServer.lvproj/My Computer/myAccIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"E!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\UTCS_AccIOS.lvlib\UTCS-AccIOS</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Virtual Accelerator</Property>
	</Item>
</Library>
